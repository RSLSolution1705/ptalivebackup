-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 19, 2020 at 02:30 PM
-- Server version: 5.7.31
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ptacadem_ptatesting`
--

-- --------------------------------------------------------

--
-- Table structure for table `accessor`
--

CREATE TABLE `accessor` (
  `id` int(30) NOT NULL,
  `username` varchar(50) NOT NULL,
  `middle_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `dob` varchar(30) NOT NULL DEFAULT '00-00-0000',
  `gender` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  `post_code` varchar(50) NOT NULL,
  `number` bigint(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accessor`
--

INSERT INTO `accessor` (`id`, `username`, `middle_name`, `last_name`, `dob`, `gender`, `email`, `address`, `post_code`, `number`) VALUES
(2, 'sonali', 'ajit', 'gunjal', '1/1/2020', 'female', 'aart@gmail.com', 'pune', '100', 5656),
(4, 'agfdgdfg', 'nandaram', 'kale', '25/06/1995', 'female', 'aarti25695@gmail.com', 'pukjkjhkne', '412408', 7219251512),
(5, 'agfdgdfg', 'nandaram', 'kale', '25/06/1995', 'female', 'aarti25695@gmail.com', 'pukjkjhkne', '412408', 7219251512),
(6, 'shubham', 'vilas', 'sawant', '7/9/2019', 'male', 'shubham@gmail.com', 'wahlekarwadi', '411033', 1234567890),
(7, 'agfdgdfg', 'nandaram', 'kale', '25/06/1995', 'female', 'aarti25695@gmail.com', 'pukjkjhkne', '412408', 7219251512),
(8, 'agfdgdfg', 'nandaram', 'kale', '25/06/1995', 'female', 'aarti25695@gmail.com', 'pukjkjhkne', '412408', 7219251512),
(9, 'agfdgdfg', 'nandaram', 'kale', '25/06/1995', 'female', 'aarti25695@gmail.com', 'pukjkjhkne', '412408', 7219251512),
(11, 'pranya', 'nandaram', 'kale', '25/06/1995', 'female', 'aarti25695@gmail.com', 'pukjkjhkne', '412408', 7219251512),
(12, 'pranya', 'nandaram', 'kale', '25/06/1995', 'female', 'aarti25695@gmail.com', 'pukjkjhkne', '412408', 7219251512),
(13, 'pranya', 'nandaram', 'ghadge', '25/06/1995', 'female', 'aarti25695@gmail.com', 'pukjkjhkne', '412408', 7219251512),
(14, 'pranya', 'nandaram', 'ghadge', '25/06/1995', 'female', 'aarti25695@gmail.com', 'pukjkjhkne', '412408', 7219251512),
(15, 'pranya', 'nandaram', 'ghadge', '25/06/1995', 'female', 'aarti25695@gmail.com', 'pukjkjhkne', '412408', 7219251512),
(16, 'pranya', 'nandaram', 'ghadge', '25/06/1995', 'female', 'aarti25695@gmail.com', 'pukjkjhkne', '412408', 7219251512),
(17, 'pranya', 'nandaram', 'ghadge', '25/06/1995', 'female', 'aarti25695@gmail.com', 'pukjkjhkne', '412408', 7219251512),
(18, 'pranya', 'nandaram', 'ghadge', '25/06/1995', 'female', 'aarti25695@gmail.com', 'pukjkjhkne', '412408', 7219251512),
(19, 'pranya', 'nandaram', 'ghadge', '25/06/1995', 'female', 'aarti25695@gmail.com', 'pukjkjhkne', '412408', 7219251512),
(20, 'aarav', 'nandaram', 'ghadge', '25/06/1995', 'female', 'aarti25695@gmail.com', 'pukjkjhkne', '412408', 7219251512),
(21, 'aarav', 'nandaram', 'ghadge', '25/06/1995', 'female', 'aarti25695@gmail.com', 'pukjkjhkne', '412408', 7219251512),
(22, 'aarav', 'nandaram', 'ghadge', '25/06/1995', 'female', 'aarti25695@gmail.com', 'pukjkjhkne', '412408', 7219251512),
(23, 'aarav', 'nandaram', 'ghadge', '25/06/1995', 'female', 'aarti25695@gmail.com', 'pukjkjhkne', '412408', 7219251512),
(24, 'aarav', 'nandaram', 'ghadge', '25/06/1995', 'female', 'aarti25695@gmail.com', 'pukjkjhkne', '412408', 7219251512),
(25, 'aarav', 'nandaram', 'ghadge', '25/06/1995', 'female', 'aarti25695@gmail.com', 'pukjkjhkne', '412408', 7219251512),
(26, 'arnav', 'nandaram', 'ghadge', '25/06/1995', 'female', 'aartik25695@gmail.com', 'pukjkjhkne', '412408', 7219251513),
(27, 'aarti', 'nandaram', 'kale', '25/06/1995', 'female', 'kale@gmail.com', 'pune', '412408', 7219251512),
(28, 'asd', 'asd', 'asd', '1/1/2020', 'female', 'kale@gmail.com', 'asdasd', 'fsdfsdf', 423432423423),
(29, 'aishwarya', 'pqr', 'mallad', '25/06/1995', 'female', 'aishwarya@gmail.com', 'pune', '412408', 6574890876),
(30, 'aaru', 'nandaram', 'kale', '25/06/1995', 'female', 'kalearti@gmail.com', 'pune', '412408', 72192514512),
(31, 'aaru', 'nandaram', 'kale', '25/06/1995', 'female', 'kaleaarti@gmail.com', 'pune', '412408', 7219251510);

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `ac_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `device_token` varchar(260) NOT NULL,
  `device_type` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`ac_id`, `user_id`, `device_token`, `device_type`) VALUES
(1, 28, 'cWV59ufNT8SKOYeEzScvdv:APA91bHo0X6U9R2PvDd9qBZlTc8lKE9WGxgIeWt01HN4ml2n3AXOxdiF4m2oRkttw3tL2soRQQZVbjg9QTh0qqzLlmAB7e-TW8_tOU0k6xVSpFe-iHknSjjk_XZvhat0PgVKNIkw0K-G', 'android'),
(2, 36, 'eQ6j9QATQ76ha4bHUf_bVD:APA91bHPxVemNqPDk32w1au1-s8iYpkpoclvcHYOHNJN1cCgEoN49iLDL7tvYCKf--_omj8AkHqcF_2U6mUyXBA2S5AS5Y8j0eg2bE8HVFYl1FcjYM-kZNUf7nSOlXoKxSPdaEOQ8PUw', 'android'),
(3, 66, 'fHuyT5_QQQC936jzIJqbfN:APA91bF3QRTQJGPrBw06eiTeu1A_zxtRESCrbsy0HGP3wcldqOSycOx8r1EL5_FF_0uoCjvoYioHOy7gm4RulEqFR3GQdeOO999fUAKVyLVK8ENOE_0A3xuwRo7Yz4RH6qyt_LdHGZq1', 'android'),
(4, 1, 'fk-7_NZLQgmrMnGuxzSDKq:APA91bEfYi8upfwIcKLhY1J2xsDf5CzFemEOMcsCbZZU6tPS_GPU_mBdS8hBnIRxsaQtrplbU58mlVH6n7qQ4c7vlgg8xlWIHJ5t2ai1uzlfe2vZ1l0Y6HJZFnRvZeGydzWF_vL9oNkj', 'android'),
(5, 53, 'eqHrNUHUQHy78HOh7RTLx9:APA91bF6LQVSpux0naifB0WaVuG4X_Q90sVUSn_4MDOgLEapFmkZCxlwm6HEuYWnJlZakPOX7kgEHfcVlTwF-d4CO8Oas_t_-64i-0HsLwvDqp-H8bLBtufQAF2VKThcGYYsj4sZbAkW', 'android'),
(6, 20, 'fjLmw3DgTVGIEXhBRzAwaB:APA91bF3KcZJVz-V5OQaNOv9kaG8nUNrskMnnD9FWnILRq3lvRBvz3oR_HK1ApIYKB6eA9YKpFRTXOnUUqjptX59_upz4M3EsPIODful9naRrm5IiR20qTSqSoUfY_t-vnuHK1M6g4R9', 'android'),
(7, 100, 'cbjXZl_DQG-7uIg8i4hiKT:APA91bGV-_nD5336s4WDZs2ybjO6_stYyJO84o7nBqnlEyGpo81uFxbsGzXJFmUlPrUYM2lCuQpz1URIHmUVxgJmVUUd-GldF4S878uIxP9kJ5sVfV5FJ2DEQmJ4_LfWeF-eeGAcztqC', 'android'),
(8, 101, 'dM3Btbu2QceMbqLgcsS-P5:APA91bGqZnA_9euHT0kCTP1k_ePkJc0eBjbYxEOq0Gbn-YoJuv7-6p_4H4VevPU6pUhqutm47ZoDyyLQIwCc9jaT_NexQPfRLFswhrxsbBTlIfC_Gkf3A14josuPMtxXFaqV07XOTx6i', 'android'),
(9, 99, 'cbjXZl_DQG-7uIg8i4hiKT:APA91bGV-_nD5336s4WDZs2ybjO6_stYyJO84o7nBqnlEyGpo81uFxbsGzXJFmUlPrUYM2lCuQpz1URIHmUVxgJmVUUd-GldF4S878uIxP9kJ5sVfV5FJ2DEQmJ4_LfWeF-eeGAcztqC', 'android'),
(10, 102, 'cgtxBd0iQMa-kjJutVWIsl:APA91bH6vK1ubg2rgXsd3Wil_kXCLPXjjOogcdCdvRqaVUubTU-luuPTa6XpSCJM2M8j3vUUziV1JJAwCzEerfTgsRXmTt1ksZ8Gd2K03Dbpss452uwAWTpRSKD9CeYzpdG3CdfjVrLA', 'android'),
(11, 103, 'dO9foYdVSxiTqf9ubRSN1C:APA91bGUwPY958qUb8jAFz-rJZhrDLEg6pUu4i6benkMeFcJoccSfbgCAKG3zMr1xSkKQNEaVng_XtMCS2dHnWIQF-jSon1fRwDtSfy3ZU8M4Htc-vDorG0YTfrNv-bHJ7QjCUVlRJ5u', 'android'),
(12, 68, 'cI6rR-L2QzOqNKLINJoqVd:APA91bEaS8a5XZKAtPw1kX0X0kP-Dx8z32MdG05OL0RwiSLKCFNTWpSw6lJeIg2XacYE5V8leGi5apy-8EaweYCZGzNndGvE07Um_PzhJNZGQTDOPqvFN1cXQ5dlApeh8y4Eae1UCftA', 'android'),
(13, 16, 'evNxBIC5TqSRhmz6c3MUCh:APA91bHEYUFB2xWygM5WEnZ2rbP0ryOKByeStfj8Q_vqd9ageCUF1izsy3PMhLALA_oaKE-bCIqNxY0DlzxxQ2N9g212gdVXvPmFwv559jWYKva1rpXbhhSSizkqLht1d2FKDjwlCfwZ', 'android'),
(14, 104, 'fE2oQP8aT3OhCNJIx5jVid:APA91bFr2PkA4PbACs0lPGJ0At1ATvrzaHu94ZfijwF-ZKkVuzKEXSWj-izr4M_DrrNhksb3q3rTNtzZpXzWcjFiqUY87OjwNFK9e_aCm4YHKwGQCaghf9hZYM07yV6NBnUW8AEVhqQ1', 'android'),
(15, 88, 'cIz79N_PRUu6ecHloSjKDM:APA91bGl8AAp120-Z-HQhDpaWXQfVNdXYwuUmjTa-YVSs5RG5xz9Lrqv8f-w2--bBo2fJTdo_El1z3KxCKrxulkxbdSWx_qTbAsute0IiE14uxiQdhAjnAIVexnpMoWlWjcvvPM0D7KZ', 'android'),
(16, 84, 'fccZPCBeQ7ix9who18phEE:APA91bES56uBVKPlcX-aDltAnrTJl88wV-8m0M4YeXbFYMFuot1Vx1WpGVWeToMSYeZswDdNu0UNe_DDuJs5N_ttocJiZU4cJvdoaNP3e9fABN7ZlJ0PEVHXYE7qv6KsppAXXi-p7d26', 'android'),
(17, 115, 'cQKpcJXNQnSmp80YrFnSNq:APA91bEN54_pJwqudfnD3MM8PMfd-iz4HNkoqnwvoB95F6F-BTnepcbbnfkezvX1js9D3_mMX573qC1UYFmmykbp5oHJYsgT3An0Y5GXimBNF_bglAenH3jr8b3H9fkaWDQc6jskO5va', 'android'),
(18, 114, 'fug7C6IzTBOiypn7Xfk-u8:APA91bFoYHSajJlPVxBICiW95lVkzX02x54dverlrZEhWI-tdH7hYVJRtaaJ3lT6aQlListi-KowehR0-SkDGxvHOakWBnFho6PI58f3LEOVFzJ3FYXFJbPKPTT74T56Wu5-Bhkuwo81', 'android'),
(19, 119, 'e-GPJolaQ1uUFB3mnVJ9hT:APA91bFWACBoyMbVmIDW0IiweauyHjyKjiaGL_IEWzK2W1jYTw0DEOhng9wno6UF0j3aDa5fmzqJSu1bVqr_XH89cxTWYmdOcmPyV2oqqTyj1f38-V7kgYltgwF6ozwlGR2hfyxInfcu', 'android'),
(20, 120, 'cB6VgtNyQ2G_io2npLd3Du:APA91bF6JLaJkWwt_XFa1VDGSwzx2RskuVdf8v5Eb9gdPnPRWYJiPa4LF7Buj6L02Pb_t6jYvVM6s_EJsoId3Qh8EH1I7wQ0vJeo_t27tDWCmWXWUl27AcINe5v8O7pO0ZeEyNyJ2MKX', 'android'),
(21, 122, 'c04QMFIGSs6d5DfpEjN01R:APA91bHZoBykBmMTfLReV3omuufJfJMsTen2p1NH1icgjL9dH0pvdxp0JVvIO6OoMQ8hWuH4Viv-v2yIPxYLZq4BrW_0h_VPEsZzIIXCXgwSKcWwDbc71GiNHcasnzdB1qxg0q4lC4wJ', 'android');

-- --------------------------------------------------------

--
-- Table structure for table `admin_details`
--

CREATE TABLE `admin_details` (
  `id` int(10) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email_id` varchar(256) NOT NULL,
  `password` varchar(256) NOT NULL,
  `mobile_number` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_details`
--

INSERT INTO `admin_details` (`id`, `first_name`, `last_name`, `email_id`, `password`, `mobile_number`) VALUES
(1, 'demo', 'account', 'demo@gmail.com', '12345678', '13467756888'),
(2, 'Jdczbb', '', 'dud@gmail.com', '12345678', '13456665445'),
(3, 'Sushil', 'Deokar', 'sushil.deokar@rslsolution.com', '12345678', '9850145236'),
(4, 'Djskj', 'sdfsdfsd', 'sheh@gmail.com', '12345678', '24562726'),
(5, 'Djdidk', 'sdfsd', 'sgsg@gmail.com', '12344555t', '354456663'),
(6, 'Dhduueu', 'sdsf', 'dgeg@gmail.com', '12345678', '123565545'),
(7, 'Oibch', 'xcvx', 'qee@gmail.com', '1234qwer', '9999885622587888888'),
(8, 'Offdi', 'weww', 'dgdheh@gmail.com', 'xwddd', '856565656'),
(9, 'Fred', 'xcv', 'Flintstone', '2345', '64876795'),
(10, 'poonam', 'ghevande', 'dkhfkh@gmail.com', '68768', '7547854'),
(11, 'shubham', 'sawant', 'shubhamsawant8055@gmail.com', 'shubham@12', '9874859632'),
(12, 'zxc', 'sdfs', 'zxc@gmail.com', '1234567890', '321654987'),
(13, 'G', 'ssdf', 'rhy', 'rgu', '523'),
(14, 'Akash', 'Lohar', 'skyward5129@gmail.com', '5129sky00', '8605568001'),
(15, 'david', 'parkar', 'admin@gmail.com', 'admin', '07748261423'),
(16, 'swapnil', 'machale', 'swapnilmachale38@gmail.com', '', '12345678'),
(17, 'samadhan', 'vidhate', 'samadhan.vidhate1@gmail.com', 'Sam12345678', '9860731823'),
(18, 'Sushil ', 'Deokar', 'sushildeokar12@gmail.com', 'Sushil12345', '9021900488'),
(19, 'Shripad', 'Kulkarni', 'shripad@gmail.com', 'Sripad123456', '9021900488'),
(20, '32432asdad', '', '', '', ''),
(21, 'samadhan', 'vidhate', 'samadhan.vidhate2@gmail.com', 'Sam@pta123', '9865321457'),
(22, 'shubh', 'sawant', 'sawantshubhamv@gmail.com', 'Shubham@1234', '123456789'),
(23, 'shubham', 'sawant', 'ksb98600@gmail.com', 'Shubham@1234', '9922252501');

-- --------------------------------------------------------

--
-- Table structure for table `AO`
--

CREATE TABLE `AO` (
  `ao_id` int(11) NOT NULL,
  `ao_list` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `AO`
--

INSERT INTO `AO` (`ao_id`, `ao_list`) VALUES
(1, 'NCFE'),
(2, 'YMCA'),
(3, 'FOCUS');

-- --------------------------------------------------------

--
-- Table structure for table `assessment_attempt`
--

CREATE TABLE `assessment_attempt` (
  `a_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `attempt` varchar(16) NOT NULL,
  `attempt_count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assessment_attempt`
--

INSERT INTO `assessment_attempt` (`a_id`, `user_id`, `unit_id`, `attempt`, `attempt_count`) VALUES
(1, 115, 528, 'true', 3),
(2, 115, 529, 'true', 1),
(3, 115, 510, 'true', 3),
(4, 115, 532, 'true', 1),
(5, 115, 533, 'true', 1),
(6, 115, 534, 'true', 1),
(7, 115, 535, 'true', 2),
(8, 119, 510, 'true', 2),
(9, 119, 532, 'true', 4),
(10, 119, 529, 'true', 4),
(11, 122, 536, 'true', 4),
(12, 119, 533, 'true', 3),
(13, 119, 534, 'true', 2),
(14, 119, 535, 'true', 2),
(15, 119, 528, 'true', 2),
(16, 119, 530, 'true', 2),
(17, 119, 495, 'true', 2),
(18, 122, 538, 'true', 4),
(19, 122, 539, 'true', 4),
(20, 119, 540, 'true', 2),
(21, 122, 541, 'true', 4),
(22, 115, 530, 'true', 1),
(23, 122, 542, 'true', 4),
(24, 115, 543, 'true', 2),
(25, 122, 544, 'true', 4),
(26, 122, 545, 'true', 3),
(27, 122, 530, 'true', 2),
(28, 122, 548, 'true', 2);

-- --------------------------------------------------------

--
-- Table structure for table `assessment_expandable`
--

CREATE TABLE `assessment_expandable` (
  `ae_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `prompt` varchar(160) NOT NULL,
  `done_text` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assessment_expandable`
--

INSERT INTO `assessment_expandable` (`ae_id`, `unit_id`, `title`, `prompt`, `done_text`) VALUES
(1, 225, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(2, 283, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(3, 283, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(4, 252, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(5, 224, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(6, 248, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(7, 1, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(8, 1, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(9, 1, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(10, 304, 'Expandable Listjllllllllllllllllllllllllllllllllll', 'Select an item to see more', 'Ok, I\'m donekooooooooooooooooo'),
(11, 248, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(12, 1, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(13, 226, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(14, 458, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(15, 470, 'Assessment criteria', 'Select an item to see more', 'Ok, I\'m done'),
(16, 475, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(17, 476, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(18, 483, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(19, 489, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(20, 490, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(21, 491, 'Rajasthan political crisis | Supreme Court to hear', 'Select an item to see more', 'Ok, I\'m done'),
(22, 478, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(23, 497, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(24, 487, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(25, 510, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(26, 511, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(27, 512, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(28, 513, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(29, 514, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(30, 516, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(31, 517, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(32, 518, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(33, 520, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(34, 519, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(35, 519, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(36, 527, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(37, 529, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(38, 531, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(39, 536, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(40, 537, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(41, 542, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(42, 544, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(43, 545, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(44, 545, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(45, 545, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done'),
(46, 548, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done');

-- --------------------------------------------------------

--
-- Table structure for table `assessment_expandable_answers`
--

CREATE TABLE `assessment_expandable_answers` (
  `aexans_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `attempt_count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assessment_expandable_answers`
--

INSERT INTO `assessment_expandable_answers` (`aexans_id`, `user_id`, `unit_id`, `question_id`, `attempt_count`) VALUES
(1, 115, 510, 25, 2),
(2, 119, 529, 37, 1),
(3, 122, 536, 39, 1),
(4, 122, 536, 39, 2),
(5, 119, 529, 37, 2),
(6, 122, 536, 39, 3),
(7, 119, 529, 37, 3),
(8, 122, 542, 41, 2),
(9, 122, 544, 42, 3),
(10, 122, 545, 43, 1),
(11, 122, 545, 44, 1),
(12, 122, 545, 45, 1),
(13, 122, 545, 43, 2),
(14, 122, 545, 44, 2),
(15, 122, 545, 45, 2),
(16, 122, 542, 41, 3),
(17, 122, 548, 46, 1);

-- --------------------------------------------------------

--
-- Table structure for table `assessment_expandable_list`
--

CREATE TABLE `assessment_expandable_list` (
  `ael_id` int(11) NOT NULL,
  `ae_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assessment_expandable_list`
--

INSERT INTO `assessment_expandable_list` (`ael_id`, `ae_id`, `title`, `content`) VALUES
(1, 1, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.'),
(2, 1, 'Product Y', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.'),
(7, 2, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.'),
(8, 2, 'Product Y', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.'),
(9, 3, 'Product Z', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.'),
(10, 3, 'Product Y', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.'),
(11, 4, 'Client name', 'Type answer here'),
(12, 4, 'Date of birth', 'Type answer here'),
(13, 4, 'Contact number', 'Type answer here'),
(14, 4, 'Next of kin', 'Type answer here'),
(23, 5, 'Client name', 'Type answer here'),
(24, 5, 'Date of birth', 'Type answer here'),
(25, 5, 'Contact number', 'Type answer here'),
(26, 5, 'Next of kin', 'Type answer here'),
(27, 6, 'Client name', 'Type answer here'),
(28, 6, 'Date of birth', 'Type answer here'),
(29, 6, 'Contact number', 'Type answer here'),
(30, 6, 'Next of kin', 'Type answer here'),
(31, 7, 'Client name', 'Type answer here'),
(32, 7, 'Date of birth', 'Type answer here'),
(33, 7, 'Contact number', 'Type answer here'),
(34, 7, 'Next of kin', 'Type answer here'),
(35, 8, 'Client name', 'Type answer here'),
(36, 8, 'Date of birth', 'Type answer here'),
(37, 8, 'Contact number', 'Type answer here'),
(38, 8, 'Next of kin', 'Type answer here'),
(39, 9, 'Client name', 'Type answer here'),
(40, 9, 'Date of birth', 'Type answer here'),
(41, 9, 'Contact number', 'Type answer here'),
(42, 9, 'Next of kin', 'Type answer here'),
(47, 10, 'Client namekkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk', 'Type answer herek;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;plppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppppp'),
(48, 10, 'Date of birthjijijijijijijijijijijijijijijijijijij', 'Type answer herjkjkjkjkjkjkjkjkjkjkjkjkjkjkjkjkjkjkjkjkjkjkjkjkjkjkjkjkjkjkjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjje'),
(49, 10, 'Contact number', 'Type answer here'),
(50, 10, 'Next of kin', 'Type answer here'),
(55, 11, 'Client name', 'Type answer here'),
(56, 11, 'Date of birth', 'Type answer here'),
(57, 11, 'Contact number', 'Type answer here'),
(58, 11, 'Next of kin', 'Type answer here'),
(59, 12, 'Client name', 'Type answer here'),
(60, 12, 'Date of birth', 'Type answer here'),
(61, 12, 'Contact number', 'Type answer here'),
(62, 12, 'Next of kin', 'Type answer here'),
(71, 13, 'Client name', 'Type answer here'),
(72, 13, 'Date of birth', 'Type answer here'),
(73, 13, 'Contact number', 'Type answer here'),
(74, 13, 'Next of kin', 'Type answer here'),
(75, 14, 'Client name', 'Type answer here'),
(76, 14, 'Date of birth', 'Type answer here'),
(77, 14, 'Contact number', 'Type answer here'),
(78, 14, 'Next of kin', 'Type answer here'),
(83, 15, 'Prepared the environment and checked equipment is ', 'choose one option'),
(84, 15, 'Prepared the environment and checked equipment is ', 'choose one option'),
(85, 15, 'Prepared the environment and checked equipment is ', 'choose one option'),
(86, 15, 'Prepared the environment and checked equipment is ', 'choose one option'),
(87, 15, 'Prepared the environment and checked equipment is ', 'choose one option'),
(88, 16, 'Client name', 'Type answer here'),
(89, 16, 'Date of birth', 'Type answer here'),
(90, 16, 'Contact number', 'Type answer here'),
(91, 16, 'Next of kin', 'Type answer here'),
(92, 17, 'Client name', 'Type answer here'),
(93, 17, 'Date of birth', 'Type answer here'),
(94, 17, 'Contact number', 'Type answer here'),
(95, 17, 'Next of kin', 'Type answer here'),
(96, 18, 'Client name', 'Type answer here'),
(97, 18, 'Date of birth', 'Type answer here'),
(98, 18, 'Contact number', 'Type answer here'),
(99, 18, 'Next of kin', 'Type answer here'),
(100, 19, 'Client name', 'Type answer here'),
(101, 19, 'Date of birth', 'Type answer here'),
(102, 19, 'Contact number', 'Type answer here'),
(103, 19, 'Next of kin', 'Type answer here'),
(116, 20, 'Client name', 'Type answer here'),
(117, 20, 'Date of birth', 'Type answer here'),
(118, 20, 'Contact number', 'Type answer here'),
(119, 20, 'Next of kin', 'Type answer here'),
(124, 21, 'DYCM :- Sachin Pilot', 'Rajasthan political crisis | Supreme Court to hear on July 23 Speaker’s plea against High Court order to defer anti-defection proceedings'),
(125, 21, 'CM :- Ashok Gehlot', 'Rajasthan political crisis | Supreme Court to hear on July 23 Speaker’s plea against High Court order to defer anti-defection proceedings'),
(126, 21, 'IC president : Rahul Gandhi', 'Rajasthan political crisis | Supreme Court to hear on July 23 Speaker’s plea against High Court order to defer anti-defection proceedings'),
(127, 21, 'BJP President :- Amit shaha', 'Rajasthan political crisis | Supreme Court to hear on July 23 Speaker’s plea against High Court order to defer anti-defection proceedings'),
(128, 22, 'Client name', 'Type answer here'),
(129, 22, 'Date of birth', 'Type answer here'),
(130, 22, 'Contact number', 'Type answer here'),
(131, 22, 'Next of kin', 'Type answer here'),
(132, 23, 'Client name', 'Type answer here'),
(133, 23, 'Date of birth', 'Type answer here'),
(134, 23, 'Contact number', 'Type answer here'),
(135, 23, 'Next of kin', 'Type answer here'),
(136, 24, 'Client name', 'Type answer here'),
(137, 24, 'Date of birth', 'Type answer here'),
(138, 24, 'Contact number', 'Type answer here'),
(139, 24, 'Next of kin', 'Type answer here'),
(140, 25, 'Client name', 'Type answer here'),
(141, 25, 'Date of birth', 'Type answer here'),
(142, 25, 'Contact number', 'Type answer here'),
(143, 25, 'Next of kin', 'Type answer here'),
(144, 26, 'Client name', 'Type answer here'),
(145, 26, 'Date of birth', 'Type answer here'),
(146, 26, 'Contact number', 'Type answer here'),
(147, 26, 'Next of kin', 'Type answer here'),
(148, 27, 'Client name', 'Type answer here'),
(149, 27, 'Date of birth', 'Type answer here'),
(150, 27, 'Contact number', 'Type answer here'),
(151, 27, 'Next of kin', 'Type answer here'),
(152, 28, 'Client name', 'Type answer here'),
(153, 28, 'Date of birth', 'Type answer here'),
(154, 28, 'Contact number', 'Type answer here'),
(155, 28, 'Next of kin', 'Type answer here'),
(156, 29, 'Client name', 'Type answer here'),
(157, 29, 'Date of birth', 'Type answer here'),
(158, 29, 'Contact number', 'Type answer here'),
(159, 29, 'Next of kin', 'Type answer here'),
(160, 30, 'Client name', 'Type answer here'),
(161, 30, 'Date of birth', 'Type answer here'),
(162, 30, 'Contact number', 'Type answer here'),
(163, 30, 'Next of kin', 'Type answer here'),
(164, 31, 'Client name', 'Type answer here'),
(165, 31, 'Date of birth', 'Type answer here'),
(166, 31, 'Contact number', 'Type answer here'),
(167, 31, 'Next of kin', 'Type answer here'),
(168, 32, 'Client name', 'Type answer here'),
(169, 32, 'Date of birth', 'Type answer here'),
(170, 32, 'Contact number', 'Type answer here'),
(171, 32, 'Next of kin', 'Type answer here'),
(172, 33, 'Client name', 'Type answer here'),
(173, 33, 'Date of birth', 'Type answer here'),
(174, 33, 'Contact number', 'Type answer here'),
(175, 33, 'Next of kin', 'Type answer here'),
(176, 34, 'Client name', 'Type answer here'),
(177, 34, 'Date of birth', 'Type answer here'),
(178, 34, 'Contact number', 'Type answer here'),
(179, 34, 'Next of kin', 'Type answer here'),
(184, 35, 'Your name', 'Type answer here'),
(185, 35, 'Date of birth', 'Type answer here'),
(186, 35, 'Contact number', 'Type answer here'),
(187, 35, 'Address', 'Type answer here'),
(188, 36, 'Client name', 'Type answer here'),
(189, 36, 'Date of birth', 'Type answer here'),
(190, 36, 'Contact number', 'Type answer here'),
(191, 36, 'Next of kin', 'Type answer here'),
(192, 37, 'Client name', 'Type answer here'),
(193, 37, 'Date of birth', 'Type answer here'),
(194, 37, 'Contact number', 'Type answer here'),
(195, 37, 'Next of kin', 'Type answer here'),
(196, 38, 'Client name', 'Type answer here'),
(197, 38, 'Date of birth', 'Type answer here'),
(198, 38, 'Contact number', 'Type answer here'),
(199, 38, 'Next of kin', 'Type answer here'),
(200, 39, 'Client name', 'Type answer here'),
(201, 39, 'Date of birth', 'Type answer here'),
(202, 39, 'Contact number', 'Type answer here'),
(203, 39, 'Next of kin', 'Type answer here'),
(208, 40, 'clear the assessment?', 'Type answer here'),
(209, 40, 'clear the assessment?', 'Type answer here'),
(210, 40, 'clear the assessment?', 'Type answer here'),
(211, 40, 'clear the assessment?', 'Type answer here'),
(212, 41, 'Client name', 'Type answer here'),
(213, 41, 'Date of birth', 'Type answer here'),
(214, 41, 'Contact number', 'Type answer here'),
(215, 41, 'Next of kin', 'Type answer here'),
(220, 42, 'Client name', 'Type answer here'),
(221, 42, 'Date of birth', 'Type answer here'),
(222, 42, 'Contact number', 'Type answer here'),
(223, 42, 'Next of kin', 'Type answer here'),
(224, 43, 'Client name', 'Type answer here'),
(225, 43, 'Date of birth', 'Type answer here'),
(226, 43, 'Contact number', 'Type answer here'),
(227, 43, 'Next of kin', 'Type answer here'),
(228, 44, 'Client name', 'Type answer here'),
(229, 44, 'Date of birth', 'Type answer here'),
(230, 44, 'Contact number', 'Type answer here'),
(231, 44, 'Next of kin', 'Type answer here'),
(236, 45, 'Client name', 'Type answer here'),
(237, 45, 'Date of birth', 'Type answer here'),
(238, 45, 'Contact number', 'Type answer here'),
(239, 45, 'Next of kin', 'Type answer here'),
(244, 46, 'Client name', 'Type answer here'),
(245, 46, 'Date of birth', 'Type answer here'),
(246, 46, 'Contact number', 'Type answer here'),
(247, 46, 'Next of kin', 'Type answer here');

-- --------------------------------------------------------

--
-- Table structure for table `assessment_multiple_choice`
--

CREATE TABLE `assessment_multiple_choice` (
  `id` int(30) NOT NULL,
  `title` varchar(256) NOT NULL,
  `unit_id` int(10) NOT NULL,
  `done_text` varchar(100) NOT NULL,
  `prompt` varchar(256) NOT NULL,
  `answer` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assessment_multiple_choice`
--

INSERT INTO `assessment_multiple_choice` (`id`, `title`, `unit_id`, `done_text`, `prompt`, `answer`) VALUES
(1, 'new What are some of the problems with long stints of elearning?', 176, 'Ok, I\'m done', 'Tap on the correct answer', ''),
(2, 'What are some of the problems with long stints of elearning?', 176, 'Ok, I\'m done', 'Tap on the correct answer', ''),
(3, 'What are some of the problems with long stints of elearning?', 177, 'Ok, I\'m done', 'Tap on the correct answer', ''),
(4, 'What are some of the problems with long stints of elearning?', 174, 'Ok, I\'m done', 'Tap on the correct answer', ''),
(5, 'What are some of the problems with long stints of elearning?', 225, 'Ok, I\'m done', 'Tap on the correct answer', ''),
(6, 'What are some of the problems with long stints of elearning?', 208, 'Ok, I\'m done', 'Tap on the correct answer', ''),
(7, 'What are some of the problems with long stints of elearning?', 209, 'Ok, I\'m done', 'Tap on the correct answer', ''),
(8, 'What are some of the problems with long stints of elearning?', 210, 'Ok, I\'m done', 'Tap on the correct answer', ''),
(9, 'What are some of the problems with long stints of elearning?', 212, 'Ok, I\'m done', 'Tap on the correct answer', ''),
(10, 'What are some of the problems with long stints of elearning?', 218, 'Ok, I\'m done', 'Tap on the correct answer', ''),
(11, 'What are some of the problems with long stints of elearning?', 224, 'Ok, I\'m done', 'Tap on the correct answer', 'This'),
(12, 'What most beneficial benefit of skipping?', 224, 'Ok, I\'m done', 'Tap on the correct answer', 'Weight Loss'),
(13, 'What are some of the problems with long stints of elearning?', 225, 'Ok, I\'m done', 'Tap on the correct answer', ''),
(14, 'What are some of the problems with long stints of elearning?', 226, 'Ok, I\'m done', 'Tap on the correct answer', ''),
(15, 'What are some of the problems with long stints of elearning?', 227, 'Ok, I\'m done', 'Tap on the correct answer', ''),
(16, 'What are some of the problems with long stints of elearning?', 1, 'Ok, I\'m done', 'Tap on the correct answer', ''),
(17, 'What are some of the problems with long stints of elearning?', 304, 'Ok, I\'m donejlkkkkkkkkkkkkkkkk', 'Tap on the correct answer', ''),
(18, 'What are some of the problems with long stints of elearning?', 248, 'Ok, I\'m done', 'Tap on the correct answer', 'This'),
(19, '1. The output of the requirement analysis and the ', 477, 'Ok, I\'m done', 'Tap on the correct answer', ''),
(20, ' DSDM stands for _____________', 477, 'Ok, I\'m done', 'Tap on the correct answer', ''),
(21, 'What are some of the problems with long stints of elearning?', 477, 'Ok, I\'m done', 'Tap on the correct answer', ''),
(22, 'What are some of the problems with long stints of elearning?', 478, 'Ok, I\'m done', 'Tap on the correct answer', ''),
(23, 'What are some of the problems with long stints of elearning?', 490, 'Ok, I\'m done', 'Tap on the correct answer', ''),
(24, 'What are some of the problems with long stints of elearning?', 458, 'Ok, I\'m done', 'Tap on the correct answer', ''),
(25, 'What are some of the problems with long stints of elearning?', 488, 'Ok, I\'m done', 'Tap on the correct answer', 'This'),
(26, 'What are some of the problems with long stints of elearning?', 491, 'Ok, I\'m done', 'Tap on the correct answer', ''),
(27, 'What are some of the problems with long stints of elearning?', 491, 'Ok, I\'m done', 'Tap on the correct answer', ''),
(28, 'What are some of the problems with long stints of elearning?', 459, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(29, 'What are some of the problems with long stints of elearning?', 495, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(30, 'What are some of the problems with long stints of elearning?', 324, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(31, 'What are some of the problems with long stints of elearning?', 510, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(32, 'What are some of the problems with long stints of elearning?', 512, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(33, 'What are some of the problems with long stints of elearning?', 520, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(34, 'What are some of the problems with long stints of elearning?', 519, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(35, 'What are some of the problems with long stints of elearning?', 519, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(36, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(37, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(38, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(39, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(40, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(41, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(42, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(43, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(44, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(45, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(46, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(47, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(48, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(49, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(50, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(51, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(52, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(53, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(54, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(55, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(56, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(57, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(58, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(59, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(60, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(61, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(62, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(63, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(64, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(65, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(66, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(67, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(68, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(69, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(70, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(71, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(72, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(73, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(74, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(75, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(76, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(77, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(78, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(79, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(80, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(81, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(82, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(83, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(84, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(85, 'What are some of the problems with long stints of elearning?', 524, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(86, 'What are some of the problems with long stints of elearning?', 525, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(87, 'A type of long bone', 525, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(88, 'A type of sesamoid bone', 525, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(89, 'A type of flat bone', 525, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(90, 'The University of Waterloo does NOT have a building of this name?', 525, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(91, 'What is the title of the book about Waterloo written by “Simon the Troll”?', 525, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(92, 'what are the skipping benefits?', 526, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(93, 'What are some of the problems with long stints of elearning?', 526, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(94, 'What are some of the problems with long stints of elearning?', 526, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(95, 'What are some of the problems with long stints of elearning?', 526, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(96, 'What are some of the problems with long stints of elearning?', 526, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(97, 'What are some of the problems with long stints of elearning?', 526, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(98, 'What are some of the problems with long stints of elearning?', 526, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(99, 'What are some of the problems with long stints of elearning?', 526, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(100, 'What are some of the problems with long stints of elearning?', 527, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(101, 'What are some of the problems with long stints of elearning?', 528, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(102, 'What are some of the problems with long stints of elearning?', 529, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(103, 'What are some of the problems with long stints of elearning?', 530, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(104, 'What are some of the problems with long stints of elearning?', 531, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(105, 'What are some of the problems with long stints of elearning?', 532, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(106, 'The University of Waterloo does NOT have a building of this name?', 532, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(107, 'What are some of the problems with long stints of elearning?', 533, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(108, 'What are some of the problems with long stints of elearning?', 534, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(109, 'What are some of the problems with long stints of elearning?', 535, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(110, 'What are  benefits of gym?', 536, 'Ok, I\'m done', 'Tap on the correct answer', 'to make good personality'),
(111, 'What are some of the problems with long stints of elearning?', 542, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(112, 'What are some of the problems with long stints of elearning?', 544, 'Ok, I\'m done', 'Tap on the correct answer', 'That'),
(113, 'What are some of the problems with long stints of elearning?', 548, 'Ok, I\'m done', 'Tap on the correct answer', 'That');

-- --------------------------------------------------------

--
-- Table structure for table `assessment_multiple_choice_options`
--

CREATE TABLE `assessment_multiple_choice_options` (
  `id` int(10) NOT NULL,
  `options` varchar(256) NOT NULL,
  `multiple_choice_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assessment_multiple_choice_options`
--

INSERT INTO `assessment_multiple_choice_options` (`id`, `options`, `multiple_choice_id`) VALUES
(5, ' All of the', 1),
(6, 'This', 1),
(7, 'That', 1),
(8, 'The other', 1),
(49, ' All of the', 2),
(50, 'This', 2),
(51, 'That', 2),
(52, 'The other', 2),
(53, ' All of the', 3),
(54, 'This', 3),
(55, 'That', 3),
(56, 'The other', 3),
(57, ' All of the', 4),
(58, 'This', 4),
(59, 'That', 4),
(60, 'The other', 4),
(61, ' All of the', 5),
(62, 'This', 5),
(63, 'That', 5),
(64, 'The other', 5),
(65, ' All of the', 6),
(66, 'This', 6),
(67, 'That', 6),
(68, 'The other', 6),
(69, ' All of the', 7),
(70, 'This', 7),
(71, 'That', 7),
(72, 'The other', 7),
(73, ' All of the', 8),
(74, 'This', 8),
(75, 'That', 8),
(76, 'The other', 8),
(77, ' All of the', 9),
(78, 'This', 9),
(79, 'That', 9),
(80, 'The other', 9),
(81, ' All of the', 10),
(82, 'This', 10),
(83, 'That', 10),
(84, 'The other', 10),
(93, ' All of the', 13),
(94, 'This', 13),
(95, 'That', 13),
(96, 'The other', 13),
(97, ' All of the', 14),
(98, 'This', 14),
(99, 'That', 14),
(100, 'The other', 14),
(101, ' All of the', 15),
(102, 'This', 15),
(103, 'That', 15),
(104, 'The other', 15),
(113, ' All of the', 16),
(114, 'This', 16),
(115, 'That', 16),
(116, 'The other', 16),
(121, ' All of thekjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj', 17),
(122, 'Thisjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj', 17),
(123, 'Thatklllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllll', 17),
(124, 'The otherkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk', 17),
(137, 'User Acceptance Test Cases', 19),
(138, 'User Rejection Test Cases', 19),
(139, 'Product Rejection Test Cases', 19),
(140, 'Product Acceptance Test Cases', 19),
(145, ' All of the', 21),
(146, 'This', 21),
(147, 'That', 21),
(148, 'The other', 21),
(157, 'Dynamic systems development method', 20),
(158, 'Dynamic Solution development method', 20),
(159, 'Database systems development method', 20),
(160, 'Database solution development method', 20),
(161, ' All of the', 22),
(162, 'This', 22),
(163, 'That', 22),
(164, 'The other', 22),
(165, ' All of the', 23),
(166, 'This', 23),
(167, 'That', 23),
(168, 'The other', 23),
(169, ' All of the', 24),
(170, 'This', 24),
(171, 'That', 24),
(172, 'The other', 24),
(177, ' All of the', 26),
(178, 'This', 26),
(179, 'That', 26),
(180, 'The other', 26),
(181, ' All of the', 27),
(182, 'This', 27),
(183, 'That', 27),
(184, 'The other', 27),
(189, ' All of the', 25),
(190, 'This', 25),
(191, 'That', 25),
(192, 'The other', 25),
(193, ' All of the', 18),
(194, 'This', 18),
(195, 'That', 18),
(196, 'The other', 18),
(197, ' All of the', 11),
(198, 'This', 11),
(199, 'That', 11),
(200, 'The other', 11),
(201, ' Leg strength.', 12),
(202, 'Stamina.', 12),
(203, 'Weight Loss', 12),
(204, 'Coordination.', 12),
(205, ' All of the', 28),
(206, 'This', 28),
(207, 'That', 28),
(208, 'The other', 28),
(209, ' All of the', 29),
(210, 'This', 29),
(211, 'That', 29),
(212, 'The other', 29),
(213, ' All of the', 30),
(214, 'This', 30),
(215, 'That', 30),
(216, 'The other', 30),
(217, ' All of the', 31),
(218, 'This', 31),
(219, 'That', 31),
(220, 'The other', 31),
(221, ' All of the', 32),
(222, 'This', 32),
(223, 'That', 32),
(224, 'The other', 32),
(225, ' All of the', 33),
(226, 'This', 33),
(227, 'That', 33),
(228, 'The other', 33),
(229, ' All of the', 34),
(230, 'This', 34),
(231, 'That', 34),
(232, 'The other', 34),
(237, 'pune', 35),
(238, 'pimpri', 35),
(239, 'akurdi', 35),
(240, 'nigdi', 35),
(245, ' All of the', 37),
(246, 'This', 37),
(247, 'That', 37),
(248, 'The other', 37),
(249, ' All of the', 38),
(250, 'This', 38),
(251, 'That', 38),
(252, 'The other', 38),
(253, ' All of the', 39),
(254, 'This', 39),
(255, 'That', 39),
(256, 'The other', 39),
(257, ' All of the', 40),
(258, 'This', 40),
(259, 'That', 40),
(260, 'The other', 40),
(261, ' All of the', 41),
(262, 'This', 41),
(263, 'That', 41),
(264, 'The other', 41),
(265, ' All of the', 42),
(266, 'This', 42),
(267, 'That', 42),
(268, 'The other', 42),
(269, ' All of the', 43),
(270, 'This', 43),
(271, 'That', 43),
(272, 'The other', 43),
(273, ' All of the', 44),
(274, 'This', 44),
(275, 'That', 44),
(276, 'The other', 44),
(277, ' All of the', 45),
(278, 'This', 45),
(279, 'That', 45),
(280, 'The other', 45),
(281, ' All of the', 46),
(282, 'This', 46),
(283, 'That', 46),
(284, 'The other', 46),
(285, ' All of the', 47),
(286, 'This', 47),
(287, 'That', 47),
(288, 'The other', 47),
(289, ' All of the', 48),
(290, 'This', 48),
(291, 'That', 48),
(292, 'The other', 48),
(293, ' All of the', 49),
(294, 'This', 49),
(295, 'That', 49),
(296, 'The other', 49),
(297, ' All of the', 50),
(298, 'This', 50),
(299, 'That', 50),
(300, 'The other', 50),
(301, ' All of the', 51),
(302, 'This', 51),
(303, 'That', 51),
(304, 'The other', 51),
(305, ' All of the', 52),
(306, 'This', 52),
(307, 'That', 52),
(308, 'The other', 52),
(309, ' All of the', 53),
(310, 'This', 53),
(311, 'That', 53),
(312, 'The other', 53),
(313, ' All of the', 54),
(314, 'This', 54),
(315, 'That', 54),
(316, 'The other', 54),
(317, ' All of the', 55),
(318, 'This', 55),
(319, 'That', 55),
(320, 'The other', 55),
(321, ' All of the', 56),
(322, 'This', 56),
(323, 'That', 56),
(324, 'The other', 56),
(325, ' All of the', 57),
(326, 'This', 57),
(327, 'That', 57),
(328, 'The other', 57),
(329, ' All of the', 58),
(330, 'This', 58),
(331, 'That', 58),
(332, 'The other', 58),
(333, ' All of the', 59),
(334, 'This', 59),
(335, 'That', 59),
(336, 'The other', 59),
(337, ' All of the', 60),
(338, 'This', 60),
(339, 'That', 60),
(340, 'The other', 60),
(341, ' All of the', 61),
(342, 'This', 61),
(343, 'That', 61),
(344, 'The other', 61),
(345, ' All of the', 62),
(346, 'This', 62),
(347, 'That', 62),
(348, 'The other', 62),
(349, ' All of the', 63),
(350, 'This', 63),
(351, 'That', 63),
(352, 'The other', 63),
(353, ' All of the', 64),
(354, 'This', 64),
(355, 'That', 64),
(356, 'The other', 64),
(357, ' All of the', 65),
(358, 'This', 65),
(359, 'That', 65),
(360, 'The other', 65),
(361, ' All of the', 66),
(362, 'This', 66),
(363, 'That', 66),
(364, 'The other', 66),
(365, ' All of the', 67),
(366, 'This', 67),
(367, 'That', 67),
(368, 'The other', 67),
(369, ' All of the', 68),
(370, 'This', 68),
(371, 'That', 68),
(372, 'The other', 68),
(373, ' All of the', 69),
(374, 'This', 69),
(375, 'That', 69),
(376, 'The other', 69),
(377, ' All of the', 70),
(378, 'This', 70),
(379, 'That', 70),
(380, 'The other', 70),
(381, ' All of the', 71),
(382, 'This', 71),
(383, 'That', 71),
(384, 'The other', 71),
(385, ' All of the', 72),
(386, 'This', 72),
(387, 'That', 72),
(388, 'The other', 72),
(389, ' All of the', 73),
(390, 'This', 73),
(391, 'That', 73),
(392, 'The other', 73),
(393, ' All of the', 74),
(394, 'This', 74),
(395, 'That', 74),
(396, 'The other', 74),
(397, ' All of the', 75),
(398, 'This', 75),
(399, 'That', 75),
(400, 'The other', 75),
(401, ' All of the', 76),
(402, 'This', 76),
(403, 'That', 76),
(404, 'The other', 76),
(405, ' All of the', 77),
(406, 'This', 77),
(407, 'That', 77),
(408, 'The other', 77),
(409, ' All of the', 78),
(410, 'This', 78),
(411, 'That', 78),
(412, 'The other', 78),
(413, ' All of the', 79),
(414, 'This', 79),
(415, 'That', 79),
(416, 'The other', 79),
(417, ' All of the', 80),
(418, 'This', 80),
(419, 'That', 80),
(420, 'The other', 80),
(421, ' All of the', 81),
(422, 'This', 81),
(423, 'That', 81),
(424, 'The other', 81),
(425, ' All of the', 82),
(426, 'This', 82),
(427, 'That', 82),
(428, 'The other', 82),
(429, ' All of the', 83),
(430, 'This', 83),
(431, 'That', 83),
(432, 'The other', 83),
(433, ' All of the', 84),
(434, 'This', 84),
(435, 'That', 84),
(436, 'The other', 84),
(437, ' All of the', 85),
(438, 'This', 85),
(439, 'That', 85),
(440, 'The other', 85),
(445, 'first', 36),
(446, 'This', 36),
(447, 'third', 36),
(448, 'The other', 36),
(449, ' All of the', 86),
(450, 'This', 86),
(451, 'That', 86),
(452, 'The other', 86),
(477, ' All of the', 93),
(478, 'This', 93),
(479, 'That', 93),
(480, 'The other', 93),
(481, 'Femur', 87),
(482, 'Carpals', 87),
(483, 'Scapula', 87),
(484, 'Sturnum', 87),
(485, 'Patella', 88),
(486, 'Patella', 88),
(487, 'Metacarpals', 88),
(488, 'Sternum', 89),
(489, 'Tibia', 89),
(490, 'Metacarpals', 89),
(491, 'Radius', 89),
(492, ' weight loss', 92),
(493, 'stamina', 92),
(494, 'relaxation', 92),
(495, 'The other', 92),
(496, 'B.C. Matthews Hall', 90),
(497, 'Carl A. Pollock Hall', 90),
(498, ' I.L. Neilson Hall', 90),
(499, 'Douglas Wright Engineering Building', 90),
(500, ' Dreaming in Technicolor', 91),
(501, 'Water Under the Bridge', 91),
(502, 'Of Mud and Dreams', 91),
(503, 'Images of Waterloo', 91),
(504, ' All of the', 94),
(505, 'This', 94),
(506, 'That', 94),
(507, 'The other', 94),
(508, ' All of the', 95),
(509, 'This', 95),
(510, 'That', 95),
(511, 'The other', 95),
(512, ' All of the', 96),
(513, 'This', 96),
(514, 'That', 96),
(515, 'The other', 96),
(516, ' All of the', 97),
(517, 'This', 97),
(518, 'That', 97),
(519, 'The other', 97),
(520, ' All of the', 98),
(521, 'This', 98),
(522, 'That', 98),
(523, 'The other', 98),
(528, ' All of the', 99),
(529, 'This', 99),
(530, 'That', 99),
(531, 'The other', 99),
(532, ' All of the', 100),
(533, 'This', 100),
(534, 'That', 100),
(535, 'The other', 100),
(536, ' All of the', 101),
(537, 'This', 101),
(538, 'That', 101),
(539, 'The other', 101),
(540, ' All of the', 102),
(541, 'This', 102),
(542, 'That', 102),
(543, 'The other', 102),
(544, ' All of the', 103),
(545, 'This', 103),
(546, 'That', 103),
(547, 'The other', 103),
(548, ' All of the', 104),
(549, 'This', 104),
(550, 'That', 104),
(551, 'The other', 104),
(552, ' All of the', 105),
(553, 'This', 105),
(554, 'That', 105),
(555, 'The other', 105),
(560, 'B.C. Matthews Hall', 106),
(561, 'Carl A. Pollock Hall', 106),
(562, 'I.L. Neilson Hall', 106),
(563, 'Douglas Wright Engineering Building', 106),
(564, ' All of the', 107),
(565, 'This', 107),
(566, 'That', 107),
(567, 'The other', 107),
(568, ' All of the', 108),
(569, 'This', 108),
(570, 'That', 108),
(571, 'The other', 108),
(572, ' All of the', 109),
(573, 'This', 109),
(574, 'That', 109),
(575, 'The other', 109),
(580, 'health', 110),
(581, 'to make good personality', 110),
(582, 'strong', 110),
(583, 'stamina', 110),
(584, ' All of the', 111),
(585, 'This', 111),
(586, 'That', 111),
(587, 'The other', 111),
(592, ' All of the', 112),
(593, 'This', 112),
(594, 'That', 112),
(595, 'The other', 112),
(596, ' All of the', 113),
(597, 'This', 113),
(598, 'That', 113),
(599, 'The other', 113);

-- --------------------------------------------------------

--
-- Table structure for table `assessment_pool_questions`
--

CREATE TABLE `assessment_pool_questions` (
  `pq_id` int(11) NOT NULL,
  `title` varchar(256) NOT NULL,
  `done_text` varchar(125) NOT NULL,
  `prompt` varchar(256) NOT NULL,
  `pool_id` int(11) NOT NULL,
  `answer` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assessment_pool_questions`
--

INSERT INTO `assessment_pool_questions` (`pq_id`, `title`, `done_text`, `prompt`, `pool_id`, `answer`) VALUES
(1, 'What is the name given to the thigh bone?', 'OK, I am done', 'done', 1, ''),
(2, 'What is the name of thhe upper arm bone?', 'Next question', 'Type your response', 1, ''),
(3, 'pqr', '', '', 1, ''),
(4, 'A type of long bone', 'Ok, I\'m done', 'Select the correct answer', 2, ''),
(5, 'A type of sesamoid bone 1', 'Ok, I\'m done', 'Select the correct answer', 2, ''),
(6, 'A type of flat bone', 'Ok, I\'m done', 'Select the correct answer', 2, ''),
(7, 'A type of long bone', 'Ok, I\'m done', 'Select the correct answer', 4, ''),
(8, 'A type of sesamoid bone', 'Ok, I\'m done', 'Select the correct answer', 4, ''),
(9, 'A type of flat bone', 'Ok, I\'m done', 'Select the correct answer', 4, ''),
(10, 'A type of long bone', 'Ok, I\'m done', 'Select the correct answer', 5, 'Carpals'),
(11, 'A type of sesamoid bone', 'Ok, I\'m done', 'Select the correct answer', 5, 'Tibia'),
(12, 'A type of flat bone', 'Ok, I\'m done', 'Select the correct answer', 5, 'Metacarpals'),
(13, 'A type of long bone', 'Ok, I\'m done', 'Select the correct answer', 6, ''),
(14, 'A type of sesamoid bone', 'Ok, I\'m done', 'Select the correct answer', 6, ''),
(15, 'A type of flat bone', 'Ok, I\'m done', 'Select the correct answer', 6, ''),
(16, 'A type of long bone', 'Ok, I\'m done', 'Select the correct answer', 7, ''),
(17, 'A type of sesamoid bone', 'Ok, I\'m done', 'Select the correct answer', 7, ''),
(18, 'A type of flat bone', 'Ok, I\'m done', 'Select the correct answer', 7, ''),
(19, 'A type of long bone', 'Ok, I\'m done', 'Select the correct answer', 8, ''),
(20, 'A type of sesamoid bone', 'Ok, I\'m done', 'Select the correct answer', 8, ''),
(21, 'A type of flat bone', 'Ok, I\'m done', 'Select the correct answer', 8, 'Tibia'),
(22, 'A type of long bone', 'Ok, I\'m done', 'Select the correct answer', 9, ''),
(23, 'A type of sesamoid bone', 'Ok, I\'m done', 'Select the correct answer', 9, ''),
(24, 'A type of flat bone', 'Ok, I\'m done', 'Select the correct answer', 9, ''),
(25, 'A type of long bone', 'Ok, I\'m done', 'Select the correct answer', 10, ''),
(26, 'A type of sesamoid bone', 'Ok, I\'m done', 'Select the correct answer', 10, ''),
(27, 'A type of flat bone', 'Ok, I\'m done', 'Select the correct answer', 10, ''),
(28, 'A type of long bone', 'Ok, I\'m done', 'Select the correct answer', 11, ''),
(29, 'A type of sesamoid bone', 'Ok, I\'m done', 'Select the correct answer', 11, ''),
(30, 'A type of flat bone', 'Ok, I\'m done', 'Select the correct answer', 11, ''),
(31, 'A type of long bone', 'Ok, I\'m done', 'Select the correct answer', 12, ''),
(32, 'A type of sesamoid bone', 'Ok, I\'m done', 'Select the correct answer', 12, ''),
(33, 'A type of flat bone', 'Ok, I\'m done', 'Select the correct answer', 12, ''),
(34, 'A type of long bone', 'Ok, I\'m done', 'Select the correct answer', 13, ''),
(35, 'A type of sesamoid bone', 'Ok, I\'m done', 'Select the correct answer', 13, ''),
(36, 'A type of flat bone', 'Ok, I\'m done', 'Select the correct answer', 13, ''),
(37, 'A type of long bone', 'Ok, I\'m done', 'Select the correct answer', 14, 'Scapula'),
(38, 'A type of sesamoid bone', 'Ok, I\'m done', 'Select the correct answer', 14, 'Radius'),
(39, 'A type of flat bone', 'Ok, I\'m done', 'Select the correct answer', 14, 'Tibia'),
(40, 'A type of long bone', 'Ok, I\'m done', 'Select the correct answer', 15, 'Scapula'),
(41, 'A type of sesamoid bone', 'Ok, I\'m done', 'Select the correct answer', 15, 'Tibia'),
(42, 'A type of flat bone', 'Ok, I\'m done', 'Select the correct answer', 15, 'Metacarpals'),
(43, 'A type of long bone', 'Ok, I\'m done', 'Select the correct answer', 16, ''),
(44, 'A type of sesamoid bone', 'Ok, I\'m done', 'Select the correct answer', 16, ''),
(45, 'A type of flat bone', 'Ok, I\'m done', 'Select the correct answer', 16, ''),
(46, 'A type of long bone', 'Ok, I\'m done', 'Select the correct answer', 17, 'Carpals'),
(47, 'A type of sesamoid bone', 'Ok, I\'m done', 'Select the correct answer', 17, 'Radius'),
(48, 'A type of flat bone', 'Ok, I\'m done', 'Select the correct answer', 17, 'Tibia'),
(49, 'A type of long bone', 'Ok, I\'m done', 'Select the correct answer', 18, 'Carpals'),
(50, 'A type of sesamoid bone', 'Ok, I\'m done', 'Select the correct answer', 18, 'Radius'),
(51, 'A type of flat bone', 'Ok, I\'m done', 'Select the correct answer', 18, 'Tibia'),
(52, 'A type of long bone', 'Ok, I\'m done', 'Select the correct answer', 19, ''),
(53, 'A type of sesamoid bone', 'Ok, I\'m done', 'Select the correct answer', 19, ''),
(54, 'A type of flat bone', 'Ok, I\'m done', 'Select the correct answer', 19, ''),
(55, 'A type of long bone', 'Ok, I\'m done', 'Select the correct answer', 20, 'Carpals'),
(56, 'A type of sesamoid bone', 'Ok, I\'m done', 'Select the correct answer', 20, 'Radius'),
(57, 'A type of flat bone', 'Ok, I\'m done', 'Select the correct answer', 20, 'Tibia'),
(58, 'A type of long bone', 'Ok, I\'m done', 'Select the correct answer', 21, 'Carpals'),
(59, 'A type of sesamoid bone', 'Ok, I\'m done', 'Select the correct answer', 21, 'Radius'),
(60, 'A type of flat bone', 'Ok, I\'m done', 'Select the correct answer', 21, 'Tibia'),
(61, 'A type of long bone', 'Ok, I\'m done', 'Select the correct answer', 22, ''),
(62, 'A type of sesamoid bone', 'Ok, I\'m done', 'Select the correct answer', 22, ''),
(63, 'A type of flat bone', 'Ok, I\'m done', 'Select the correct answer', 22, ''),
(64, 'A type of long bone', 'Ok, I\'m done', 'Select the correct answer', 23, ''),
(65, 'A type of sesamoid bone', 'Ok, I\'m done', 'Select the correct answer', 23, ''),
(66, 'A type of flat bone', 'Ok, I\'m done', 'Select the correct answer', 23, ''),
(67, 'A type of long bone', 'Ok, I\'m done', 'Select the correct answer', 24, ''),
(68, 'A type of sesamoid bone', 'Ok, I\'m done', 'Select the correct answer', 24, ''),
(69, 'A type of flat bone', 'Ok, I\'m done', 'Select the correct answer', 24, ''),
(70, 'A type of long bone', 'Ok, I\'m done', 'Select the correct answer', 25, ''),
(71, 'A type of sesamoid bone', 'Ok, I\'m done', 'Select the correct answer', 25, ''),
(72, 'A type of flat bone', 'Ok, I\'m done', 'Select the correct answer', 25, ''),
(73, 'A type of long bone', 'Ok, I\'m done', 'Select the correct answer', 26, 'Carpals'),
(74, 'A type of sesamoid bone', 'Ok, I\'m done', 'Select the correct answer', 26, 'Radius'),
(75, 'A type of flat bone', 'Ok, I\'m done', 'Select the correct answer', 26, 'Tibia'),
(76, 'A type of long bone', 'Ok, I\'m done', 'Select the correct answer', 27, 'Carpals'),
(77, 'A type of sesamoid bone', 'Ok, I\'m done', 'Select the correct answer', 27, 'Radius'),
(78, 'A type of flat bone', 'Ok, I\'m done', 'Select the correct answer', 27, 'Tibia'),
(79, 'A type of long bone', 'Ok, I\'m done', 'Select the correct answer', 28, 'Carpals'),
(80, 'A type of sesamoid bone', 'Ok, I\'m done', 'Select the correct answer', 28, 'Radius'),
(81, 'A type of flat bone', 'Ok, I\'m done', 'Select the correct answer', 28, 'Tibia'),
(82, 'A type of long bone', 'Ok, I\'m done', 'Select the correct answer', 29, 'Carpals'),
(83, 'A type of sesamoid bone', 'Ok, I\'m done', 'Select the correct answer', 29, 'Radius'),
(84, 'A type of flat bone', 'Ok, I\'m done', 'Select the correct answer', 29, 'Tibia'),
(85, 'A type of long bone', 'Ok, I\'m done', 'Select the correct answer', 30, 'Carpals'),
(86, 'A type of sesamoid bone', 'Ok, I\'m done', 'Select the correct answer', 30, 'Radius'),
(87, 'A type of flat bone', 'Ok, I\'m done', 'Select the correct answer', 30, 'Tibia'),
(88, 'A type of long bone', 'Ok, I\'m done', 'Select the correct answer', 31, 'Carpals'),
(89, 'A type of sesamoid bone', 'Ok, I\'m done', 'Select the correct answer', 31, 'Radius'),
(90, 'A type of flat bone', 'Ok, I\'m done', 'Select the correct answer', 31, 'Tibia'),
(91, 'A type of long bone', 'Ok, I\'m done', 'Select the correct answer', 32, 'Carpals'),
(92, 'A type of sesamoid bone', 'Ok, I\'m done', 'Select the correct answer', 32, 'Radius'),
(93, 'A type of flat bone', 'Ok, I\'m done', 'Select the correct answer', 32, 'Tibia'),
(94, 'A type of long bone', 'Ok, I\'m done', 'Select the correct answer', 33, 'Carpals'),
(95, 'A type of sesamoid bone', 'Ok, I\'m done', 'Select the correct answer', 33, 'Radius'),
(96, 'A type of flat bone', 'Ok, I\'m done', 'Select the correct answer', 33, 'Tibia'),
(97, 'A type of long bone', 'Ok, I\'m done', 'Select the correct answer', 34, 'Carpals'),
(98, 'A type of sesamoid bone', 'Ok, I\'m done', 'Select the correct answer', 34, 'Radius'),
(99, 'A type of flat bone', 'Ok, I\'m done', 'Select the correct answer', 34, 'Tibia'),
(100, 'A type of long bone', 'Ok, I\'m done', 'Select the correct answer', 35, 'Carpals'),
(101, 'A type of sesamoid bone', 'Ok, I\'m done', 'Select the correct answer', 35, 'Radius'),
(102, 'A type of flat bone', 'Ok, I\'m done', 'Select the correct answer', 35, 'Tibia'),
(103, 'A type of long bone', 'Ok, I\'m done', 'Select the correct answer', 36, 'Carpals'),
(104, 'A type of sesamoid bone', 'Ok, I\'m done', 'Select the correct answer', 36, 'Radius'),
(105, 'A type of flat bone', 'Ok, I\'m done', 'Select the correct answer', 36, 'Tibia'),
(106, 'A type of long bone', 'Ok, I\'m done', 'Select the correct answer', 37, 'Carpals'),
(107, 'A type of sesamoid bone', 'Ok, I\'m done', 'Select the correct answer', 37, 'Radius'),
(108, 'A type of flat bone', 'Ok, I\'m done', 'Select the correct answer', 37, 'Tibia'),
(109, 'A type of long bone', 'Ok, I\'m done', 'Select the correct answer', 38, 'Carpals'),
(110, 'A type of sesamoid bone', 'Ok, I\'m done', 'Select the correct answer', 38, 'Radius'),
(111, 'A type of flat bone', 'Ok, I\'m done', 'Select the correct answer', 38, 'Tibia'),
(112, 'A type of long bone', 'Ok, I\'m done', 'Select the correct answer', 39, 'Carpals'),
(113, 'A type of sesamoid bone', 'Ok, I\'m done', 'Select the correct answer', 39, 'Radius'),
(114, 'A type of flat bone', 'Ok, I\'m done', 'Select the correct answer', 39, 'Tibia'),
(115, 'A type of long bone', 'Ok, I\'m done', 'Select the correct answer', 40, 'Carpals'),
(116, 'A type of sesamoid bone', 'Ok, I\'m done', 'Select the correct answer', 40, 'Radius'),
(117, 'A type of flat bone', 'Ok, I\'m done', 'Select the correct answer', 40, 'Tibia'),
(118, 'If 5x plus 32 equals 4 minus 2x what is the value of x ?', 'Ok, I\'m done', 'Select the correct answer', 41, 'Carpals'),
(119, 'Which of the following pathogens is most commonly implicated in acute bacterial rhinosinusitis?', 'Ok, I\'m done', 'Select the correct answer', 41, 'Radius'),
(120, 'Appropriate auxiliary labelling for clarithromycin suspension includes which of the following?', 'Ok, I\'m done', 'Select the correct answer', 41, 'Tibia');

-- --------------------------------------------------------

--
-- Table structure for table `assessment_question_result`
--

CREATE TABLE `assessment_question_result` (
  `ar_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `question_type` varchar(50) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer` varchar(256) DEFAULT NULL,
  `answer_flag` varchar(16) DEFAULT NULL,
  `score` varchar(25) DEFAULT NULL,
  `attempt_count` int(11) NOT NULL,
  `option_id` varchar(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assessment_question_result`
--

INSERT INTO `assessment_question_result` (`ar_id`, `user_id`, `unit_id`, `question_type`, `question_id`, `answer`, `answer_flag`, `score`, `attempt_count`, `option_id`) VALUES
(1, 115, 510, 'free_text', 84, 'Bdndj', '', '1', 2, ''),
(2, 115, 510, 'ass_multiple_choice', 31, 'That', 'true', '1', 2, '219'),
(3, 115, 510, 'ass_expandable', 25, NULL, 'true', '1', 2, NULL),
(4, 115, 510, 'question_pool', 20, 'Carpals', 'true', '1', 2, '360'),
(5, 115, 535, 'ass_multiple_choice', 109, 'This', 'false', '1', 1, '573'),
(6, 119, 529, 'free_text', 100, 'Bsjs', '', '1', 1, ''),
(7, 119, 529, 'ass_multiple_choice', 102, 'That', 'true', '1', 1, '542'),
(8, 119, 529, 'ass_expandable', 37, NULL, 'true', '1', 1, NULL),
(9, 119, 529, 'question_pool', 30, 'Carpals', 'true', '1', 1, '480'),
(10, 122, 536, 'ass_multiple_choice', 110, 'to make good personality', 'true', '1', 1, '581'),
(11, 122, 536, 'ass_expandable', 39, NULL, 'true', '1', 1, NULL),
(12, 122, 536, 'question_pool', 32, 'Sternum', 'false', '1', 1, '511'),
(13, 122, 536, 'free_text', 104, 'To make stamina  and good health , loose weight', '', '1', 1, ''),
(14, 122, 536, 'ass_multiple_choice', 110, 'to make good personality', 'true', '1', 2, '581'),
(15, 122, 536, 'ass_expandable', 39, NULL, 'true', '1', 2, NULL),
(17, 122, 536, 'question_pool', 32, 'Carpals', 'true', '1', 2, '504'),
(21, 122, 536, 'free_text', 104, 'Stamina loose weight\n', '', '1', 2, ''),
(22, 119, 510, 'free_text', 84, 'Gfj', '', '1', 2, ''),
(23, 119, 510, 'ass_multiple_choice', 31, 'That', 'true', '1', 2, '219'),
(24, 119, 532, 'ass_multiple_choice', 105, 'This', 'false', '1', 2, '553'),
(25, 119, 532, 'ass_multiple_choice', 106, 'B.C. Matthews Hall', 'false', '1', 2, '560'),
(26, 119, 533, 'ass_multiple_choice', 107, 'That', 'true', '1', 1, '566'),
(27, 119, 534, 'ass_multiple_choice', 108, 'That', 'true', '1', 1, '570'),
(28, 119, 535, 'ass_multiple_choice', 109, 'That', 'true', '1', 1, '574'),
(33, 119, 532, 'ass_multiple_choice', 105, 'That', 'true', '1', 3, '554'),
(34, 119, 532, 'ass_multiple_choice', 106, 'B.C. Matthews Hall', 'false', '1', 3, '560'),
(35, 119, 529, 'free_text', 100, 'Hsjjs', '', '1', 3, ''),
(36, 119, 529, 'ass_multiple_choice', 102, 'That', 'true', '1', 3, '542'),
(37, 119, 529, 'ass_expandable', 37, NULL, 'true', '1', 3, NULL),
(38, 119, 529, 'question_pool', 30, 'Femur', 'false', '1', 3, '479'),
(39, 119, 528, 'ass_multiple_choice', 101, 'That', 'true', '1', 1, '538'),
(40, 119, 528, 'free_text', 99, 'Bzjsj', '', '1', 1, ''),
(41, 119, 530, 'ass_multiple_choice', 103, 'That', 'true', '1', 1, '546'),
(42, 119, 530, 'free_text', 101, 'Vhj', '', '1', 1, ''),
(43, 119, 533, 'ass_multiple_choice', 107, 'That', 'true', '1', 2, '566'),
(44, 119, 495, 'ass_multiple_choice', 29, 'That', 'true', '1', 1, '211'),
(45, 119, 495, 'question_pool', 17, 'Tibia', 'true', '1', 1, '332'),
(46, 119, 495, 'free_text', 81, 'Hdjdj', '', '1', 1, ''),
(47, 122, 536, 'ass_multiple_choice', 110, 'strong', 'false', '1', 3, '582'),
(48, 122, 536, 'ass_expandable', 39, NULL, 'true', '1', 3, NULL),
(49, 122, 536, 'question_pool', 92, 'Tibia', 'true', '1', 3, '508'),
(50, 122, 536, 'free_text', 104, 'Fghhhj\n', '', '1', 3, ''),
(51, 122, 538, 'question_pool', 95, 'Tibia', 'false', '1', 1, '520'),
(52, 122, 538, 'question_pool', 95, 'Metacarpals', 'false', '1', 2, '521'),
(53, 122, 538, 'question_pool', 95, 'Radius', 'true', '1', 3, '522'),
(54, 122, 539, 'question_pool', 98, 'Patella', 'false', '1', 1, '535'),
(55, 122, 539, 'question_pool', 97, 'Scapula', 'false', '1', 2, '533'),
(56, 119, 533, 'ass_multiple_choice', 107, 'That', 'true', '1', 3, '566'),
(57, 119, 533, 'question_pool', 101, 'Tibia', 'false', '1', 3, '548'),
(58, 119, 540, 'question_pool', 105, 'Metacarpals', 'false', '1', 1, '565'),
(59, 122, 541, 'question_pool', 108, 'Sternum', 'false', '1', 1, '575'),
(60, 122, 541, 'question_pool', 110, 'Patella', 'false', '1', 1, '583'),
(61, 122, 541, 'question_pool', 108, 'Tibia', 'true', '1', 2, '576'),
(62, 122, 541, 'question_pool', 109, 'Scapula', 'false', '1', 2, '581'),
(63, 122, 541, 'question_pool', 106, 'Carpals', 'true', '1', 3, '568'),
(64, 122, 541, 'question_pool', 110, 'Metacarpals', 'false', '1', 3, '585'),
(65, 115, 528, 'ass_multiple_choice', 101, 'That', 'true', '1', 1, '538'),
(66, 115, 528, 'free_text', 99, 'Bdnd', '', '1', 1, ''),
(67, 115, 528, 'ass_multiple_choice', 101, 'That', 'true', '1', 2, '538'),
(68, 115, 528, 'free_text', 99, 'Ndn', '', '1', 2, ''),
(69, 122, 542, 'free_text', 105, 'Bbsnsnsnsn', '', '1', 2, ''),
(70, 122, 542, 'ass_multiple_choice', 111, ' All of the', 'false', '1', 2, '584'),
(71, 122, 542, 'ass_expandable', 41, NULL, 'true', '1', 2, NULL),
(72, 122, 542, 'question_pool', 112, 'Carpals', 'true', '1', 2, '592'),
(73, 115, 543, 'question_pool', 116, 'Radius', 'true', '1', 1, '610'),
(74, 115, 543, 'question_pool', 120, 'Take with plenty of fluids', 'false', '1', 1, '636'),
(75, 122, 539, 'question_pool', 97, 'Carpals', 'true', '1', 3, '532'),
(76, 122, 544, 'ass_multiple_choice', 112, 'The other', 'false', '1', 1, '595'),
(77, 122, 544, 'ass_multiple_choice', 112, ' All of the', 'false', '1', 2, '592'),
(78, 122, 544, 'ass_multiple_choice', 112, 'That', 'true', '1', 3, '594'),
(79, 122, 544, 'ass_expandable', 42, NULL, 'true', '1', 3, NULL),
(80, 122, 545, 'ass_expandable', 43, NULL, 'true', '1', 1, NULL),
(81, 122, 545, 'ass_expandable', 44, NULL, 'true', '1', 1, NULL),
(82, 122, 545, 'ass_expandable', 45, NULL, 'true', '1', 1, NULL),
(83, 122, 545, 'ass_expandable', 43, NULL, 'true', '1', 2, NULL),
(84, 122, 545, 'ass_expandable', 44, NULL, 'true', '1', 2, NULL),
(85, 122, 545, 'ass_expandable', 45, NULL, 'true', '1', 2, NULL),
(86, 122, 530, 'ass_multiple_choice', 103, ' All of the', 'false', '1', 1, '544'),
(87, 122, 530, 'free_text', 101, 'Vxvmuguf\n', '', '1', 1, ''),
(88, 122, 542, 'free_text', 105, 'Sfdbhfjg\n', '', '1', 3, ''),
(89, 122, 542, 'ass_multiple_choice', 111, 'This', 'false', '1', 3, '585'),
(90, 122, 542, 'ass_expandable', 41, NULL, 'true', '1', 3, NULL),
(91, 122, 542, 'question_pool', 113, 'Tibia', 'false', '1', 3, '596'),
(92, 122, 548, 'ass_multiple_choice', 113, 'That', 'true', '1', 1, '598'),
(93, 122, 548, 'ass_expandable', 46, NULL, 'true', '1', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `assessment_result_by_assessor`
--

CREATE TABLE `assessment_result_by_assessor` (
  `ass_res_id` int(11) NOT NULL,
  `assessor_id` int(11) NOT NULL,
  `learner_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `question_type` varchar(125) NOT NULL,
  `assessor_response` varchar(16) NOT NULL,
  `attempt_count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assessment_result_by_assessor`
--

INSERT INTO `assessment_result_by_assessor` (`ass_res_id`, `assessor_id`, `learner_id`, `unit_id`, `question_id`, `question_type`, `assessor_response`, `attempt_count`) VALUES
(1, 114, 115, 510, 84, 'free_text', '1', 2),
(2, 16, 119, 529, 100, 'free_text', '1', 1),
(3, 120, 122, 536, 104, 'free_text', '1', 1),
(4, 120, 122, 536, 104, 'free_text', '1', 2),
(5, 120, 122, 536, 104, 'free_text', '1', 3),
(6, 16, 119, 528, 99, 'free_text', '1', 1),
(7, 16, 119, 530, 101, 'free_text', '1', 1),
(8, 16, 119, 495, 81, 'free_text', '1', 1),
(9, 114, 115, 528, 99, 'free_text', '1', 1),
(10, 114, 115, 528, 99, 'free_text', '1', 2),
(11, 120, 122, 542, 105, 'free_text', '1', 2),
(12, 120, 122, 530, 101, 'free_text', '1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `assessor_expandable_ans`
--

CREATE TABLE `assessor_expandable_ans` (
  `ass_ex_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `assessor_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assessor_expandable_ans`
--

INSERT INTO `assessor_expandable_ans` (`ass_ex_id`, `question_id`, `assessor_id`, `unit_id`, `user_id`) VALUES
(1, 40, 120, 537, 122),
(2, 26, 16, 511, 119);

-- --------------------------------------------------------

--
-- Table structure for table `assessor_expandable_option_ans`
--

CREATE TABLE `assessor_expandable_option_ans` (
  `ass_exOpt_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `pqrd_option` varchar(25) NOT NULL,
  `ass_ex_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assessor_expandable_option_ans`
--

INSERT INTO `assessor_expandable_option_ans` (`ass_exOpt_id`, `option_id`, `pqrd_option`, `ass_ex_id`) VALUES
(1, 208, 'P', 1),
(2, 209, 'P', 1),
(3, 210, 'P', 1),
(4, 211, 'P', 1);

-- --------------------------------------------------------

--
-- Table structure for table `assign_ao_regdate`
--

CREATE TABLE `assign_ao_regdate` (
  `aoreg_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `ao_id` int(11) NOT NULL,
  `date_registered` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assign_ao_regdate`
--

INSERT INTO `assign_ao_regdate` (`aoreg_id`, `user_id`, `course_id`, `ao_id`, `date_registered`) VALUES
(1, 1, 1, 1, '6/16/2020'),
(2, 1, 69, 1, '6/15/2020'),
(3, 29, 2, 1, '6/8/2020'),
(4, 28, 77, 2, '6/15/2020'),
(5, 28, 81, 1, '6/16/2020'),
(6, 28, 1, 2, '7/23/2020'),
(7, 28, 76, 2, '7/14/2020');

-- --------------------------------------------------------

--
-- Table structure for table `ass_expandable_option_answers`
--

CREATE TABLE `ass_expandable_option_answers` (
  `aexopt_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `option_answer` text NOT NULL,
  `aexans_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ass_expandable_option_answers`
--

INSERT INTO `ass_expandable_option_answers` (`aexopt_id`, `option_id`, `option_answer`, `aexans_id`) VALUES
(1, 140, 'Shubham', 1),
(2, 141, '12/12/1995', 1),
(3, 142, '987373755', 1),
(4, 143, 'Hsn', 1),
(5, 192, 'Hsh', 2),
(6, 193, '10/8/1999', 2),
(7, 194, '726728282', 2),
(8, 195, 'Bsjsj', 2),
(9, 200, 'Can yaman', 3),
(10, 201, '7. November', 3),
(11, 202, '6789999766', 3),
(12, 203, 'Dfffgghhhhhh', 3),
(13, 200, 'Can', 4),
(14, 201, '5 nov', 4),
(15, 202, '256889', 4),
(16, 203, 'Dfghhhh', 4),
(17, 192, 'Abc', 5),
(18, 193, '10/01/1996', 5),
(19, 194, '7353738266', 5),
(20, 195, 'Gsh', 5),
(21, 200, 'Poo', 6),
(22, 201, 'Gghj', 6),
(23, 202, 'Fhjk', 6),
(24, 203, 'Ghgddgghh', 6),
(25, 192, 'Shubham', 7),
(26, 193, '11/11/1988', 7),
(27, 194, '3836373642', 7),
(28, 195, 'Gsh', 7),
(29, 212, 'Can yaman', 8),
(30, 213, '6 December', 8),
(31, 214, '12334556677', 8),
(32, 215, 'Ghahshsshhshsh', 8),
(33, 224, 'Ppooonam', 10),
(34, 225, 'Grghtjyky', 10),
(35, 226, 'Gdnfkgky', 10),
(36, 227, 'Dgfjjyky', 10),
(37, 224, 'Ppooonam', 11),
(38, 225, 'Grghtjyky', 11),
(39, 226, 'Gdnfkgky', 11),
(40, 227, 'Dgfjjyky', 11),
(41, 228, 'Teyrjtjyky', 11),
(42, 229, 'Yejttiyky', 11),
(43, 230, 'Yrjtjykyky', 11),
(44, 231, 'Rutjkylulukuky', 11),
(45, 224, 'Ppooonam', 12),
(46, 225, 'Grghtjyky', 12),
(47, 226, 'Gdnfkgky', 12),
(48, 227, 'Dgfjjyky', 12),
(49, 228, 'Teyrjtjyky', 12),
(50, 229, 'Yejttiyky', 12),
(51, 230, 'Yrjtjykyky', 12),
(52, 231, 'Rutjkylulukuky', 12),
(53, 236, 'Utoylupulu', 12),
(54, 237, 'Yrrukykyjg', 12),
(55, 238, 'Urrujfkgky', 12),
(56, 239, 'Jfkgkggk', 12),
(57, 224, 'Xuss ns', 13),
(58, 225, 'Bznzjjz', 13),
(59, 226, 'Hzhzjjzjzj', 13),
(60, 227, 'Hajzjzjjzjs', 13),
(61, 224, 'Xuss ns', 14),
(62, 225, 'Bznzjjz', 14),
(63, 226, 'Hzhzjjzjzj', 14),
(64, 227, 'Hajzjzjjzjs', 14),
(65, 228, 'Hhshsjzj', 14),
(66, 229, 'Hjzjjzjzzj', 14),
(67, 230, 'Hshzjsj', 14),
(68, 231, 'Hzhzjjjz', 14),
(69, 224, 'Xuss ns', 15),
(70, 225, 'Bznzjjz', 15),
(71, 226, 'Hzhzjjzjzj', 15),
(72, 227, 'Hajzjzjjzjs', 15),
(73, 228, 'Hhshsjzj', 15),
(74, 229, 'Hjzjjzjzzj', 15),
(75, 230, 'Hshzjsj', 15),
(76, 231, 'Hzhzjjjz', 15),
(77, 236, 'Jsjsjsj', 15),
(78, 237, 'Jzjzjzjzzj', 15),
(79, 238, 'Jzjzjzj', 15),
(80, 239, 'Jzjzzjzjzj', 15),
(81, 212, 'Xvbbb', 16),
(82, 213, ' Z fnfnt', 16),
(83, 214, 'Svnfmymy', 16),
(84, 215, 'Ernmymy', 16),
(85, 244, 'Dbngmhlh', 17),
(86, 245, 'Hdjgkh', 17),
(87, 246, 'Dhkglh', 17),
(88, 247, 'Dbng', 17);

-- --------------------------------------------------------

--
-- Table structure for table `ass_pool_question_options`
--

CREATE TABLE `ass_pool_question_options` (
  `opt_id` int(11) NOT NULL,
  `pque_id` int(11) NOT NULL,
  `pool_id` int(11) NOT NULL,
  `options` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ass_pool_question_options`
--

INSERT INTO `ass_pool_question_options` (`opt_id`, `pque_id`, `pool_id`, `options`) VALUES
(7, 3, 1, '333'),
(8, 3, 1, '313'),
(9, 3, 1, '323'),
(18, 6, 2, 'Sternum'),
(19, 6, 2, 'Tibia'),
(20, 6, 2, 'Metacarpals'),
(21, 6, 2, 'Radius'),
(25, 909, 3, 'Femur'),
(26, 909, 3, 'Carpals'),
(27, 909, 3, 'Scapula'),
(28, 909, 3, 'Sturnum'),
(29, 28, 3, 'Patella'),
(30, 28, 3, 'Tibia'),
(31, 28, 3, 'Metacarpals'),
(32, 28, 3, 'Radius'),
(33, 32, 3, 'Sternum'),
(34, 32, 3, 'Tibia'),
(35, 32, 3, 'Metacarpals'),
(36, 32, 3, 'Radius'),
(40, 7, 4, 'Femur'),
(41, 7, 4, 'Carpals'),
(42, 7, 4, 'Scapula'),
(43, 7, 4, 'Sturnum'),
(44, 8, 4, 'Patella'),
(45, 8, 4, 'Tibia'),
(46, 8, 4, 'Metacarpals'),
(47, 8, 4, 'Radius'),
(48, 9, 4, 'Sternum'),
(49, 9, 4, 'Tibia'),
(50, 9, 4, 'Metacarpals'),
(51, 9, 4, 'Radius'),
(76, 4, 2, 'Femur'),
(77, 4, 2, 'Carpals'),
(78, 4, 2, 'Scapula'),
(79, 4, 2, 'Sturnum'),
(96, 5, 2, 'Patella1'),
(97, 5, 2, 'Tibia1'),
(98, 5, 2, 'Metacarpals1'),
(111, 13, 6, 'Femur'),
(112, 13, 6, 'Carpals'),
(113, 13, 6, 'Scapula'),
(114, 13, 6, 'Sturnum'),
(115, 14, 6, 'Patella'),
(116, 14, 6, 'Tibia'),
(117, 14, 6, 'Metacarpals'),
(118, 14, 6, 'Radius'),
(119, 15, 6, 'Sternum'),
(120, 15, 6, 'Tibia'),
(121, 15, 6, 'Metacarpals'),
(122, 15, 6, 'Radius'),
(123, 1, 1, 'Tibia'),
(124, 1, 1, 'Femur'),
(125, 1, 1, 'Ulna'),
(126, 1, 1, 'Humerus'),
(127, 2, 1, 'Femur'),
(128, 2, 1, 'Humerus'),
(129, 2, 1, 'Ulna'),
(130, 2, 1, 'Tibia'),
(131, 16, 7, 'Femur'),
(132, 16, 7, 'Carpals'),
(133, 16, 7, 'Scapula'),
(134, 16, 7, 'Sturnum'),
(135, 17, 7, 'Patella'),
(136, 17, 7, 'Tibia'),
(137, 17, 7, 'Metacarpals'),
(138, 17, 7, 'Radius'),
(139, 18, 7, 'Sternum'),
(140, 18, 7, 'Tibia'),
(141, 18, 7, 'Metacarpals'),
(142, 18, 7, 'Radius'),
(155, 22, 9, 'Femur'),
(156, 22, 9, 'Carpals'),
(157, 22, 9, 'Scapula'),
(158, 22, 9, 'Sturnum'),
(159, 23, 9, 'Patella'),
(160, 23, 9, 'Tibia'),
(161, 23, 9, 'Metacarpals'),
(162, 23, 9, 'Radius'),
(163, 24, 9, 'Sternum'),
(164, 24, 9, 'Tibia'),
(165, 24, 9, 'Metacarpals'),
(166, 24, 9, 'Radius'),
(167, 25, 10, 'Femur'),
(168, 25, 10, 'Carpals'),
(169, 25, 10, 'Scapula'),
(170, 25, 10, 'Sturnum'),
(171, 26, 10, 'Patella'),
(172, 26, 10, 'Tibia'),
(173, 26, 10, 'Metacarpals'),
(174, 26, 10, 'Radius'),
(175, 27, 10, 'Sternum'),
(176, 27, 10, 'Tibia'),
(177, 27, 10, 'Metacarpals'),
(178, 27, 10, 'Radius'),
(183, 29, 11, 'Patella'),
(184, 29, 11, 'Tibia'),
(185, 29, 11, 'Metacarpals'),
(186, 29, 11, 'Radius'),
(187, 30, 11, 'Sternum'),
(188, 30, 11, 'Tibia'),
(189, 30, 11, 'Metacarpals'),
(190, 30, 11, 'Radius'),
(191, 28, 11, 'Patella'),
(192, 28, 11, 'Tibia'),
(193, 28, 11, 'Metacarpals'),
(194, 28, 11, 'Radius'),
(195, 28, 11, 'Femur'),
(196, 28, 11, 'Carpals'),
(197, 28, 11, 'Scapula'),
(198, 28, 11, 'Sturnum'),
(203, 32, 12, 'Patella'),
(204, 32, 12, 'Tibia'),
(205, 32, 12, 'Metacarpals'),
(206, 32, 12, 'Radius'),
(207, 33, 12, 'Sternum'),
(208, 33, 12, 'Tibia'),
(209, 33, 12, 'Metacarpals'),
(210, 33, 12, 'Radius'),
(211, 34, 13, 'Femur'),
(212, 34, 13, 'Carpals'),
(213, 34, 13, 'Scapula'),
(214, 34, 13, 'Sturnum'),
(215, 35, 13, 'Patella'),
(216, 35, 13, 'Tibia'),
(217, 35, 13, 'Metacarpals'),
(218, 35, 13, 'Radius'),
(219, 36, 13, 'Sternum'),
(220, 36, 13, 'Tibia'),
(221, 36, 13, 'Metacarpals'),
(222, 36, 13, 'Radius'),
(235, 31, 12, 'Femur'),
(236, 31, 12, 'Carpals'),
(237, 31, 12, 'Scapula'),
(238, 31, 12, 'Sturnum'),
(243, 38, 14, 'Patella'),
(244, 38, 14, 'Tibia'),
(245, 38, 14, 'Metacarpals'),
(246, 38, 14, 'Radius'),
(247, 39, 14, 'Sternum'),
(248, 39, 14, 'Tibia'),
(249, 39, 14, 'Metacarpals'),
(250, 39, 14, 'Radius'),
(251, 37, 14, 'Femur'),
(252, 37, 14, 'Carpals'),
(253, 37, 14, 'Scapula'),
(254, 37, 14, 'Sturnum'),
(255, 19, 8, 'Femur'),
(256, 19, 8, 'Carpals'),
(257, 19, 8, 'Scapula'),
(258, 19, 8, 'Sturnum'),
(259, 20, 8, 'Patella'),
(260, 20, 8, 'Tibia'),
(261, 20, 8, 'Metacarpals'),
(262, 20, 8, 'Radius'),
(267, 21, 8, 'Sternum'),
(268, 21, 8, 'Tibia'),
(269, 21, 8, 'Metacarpals'),
(270, 21, 8, 'Radius'),
(287, 40, 15, 'Femur'),
(288, 40, 15, 'Carpals'),
(289, 40, 15, 'Scapula'),
(290, 40, 15, 'Sturnum'),
(291, 41, 15, 'Patella'),
(292, 41, 15, 'Tibia'),
(293, 41, 15, 'Metacarpals'),
(294, 41, 15, 'Radius'),
(295, 42, 15, 'Sternum'),
(296, 42, 15, 'Tibia'),
(297, 42, 15, 'Metacarpals'),
(298, 42, 15, 'Radius'),
(299, 12, 5, 'Sternum'),
(300, 12, 5, 'Tibia'),
(301, 12, 5, 'Metacarpals'),
(302, 12, 5, 'Radius'),
(303, 10, 5, 'Femur'),
(304, 10, 5, 'Carpals'),
(305, 10, 5, 'Scapula'),
(306, 10, 5, 'Sturnum'),
(307, 11, 5, 'Patella'),
(308, 11, 5, 'Tibia'),
(309, 11, 5, 'Metacarpals'),
(310, 11, 5, 'Radius'),
(311, 43, 16, 'Femur'),
(312, 43, 16, 'Carpals'),
(313, 43, 16, 'Scapula'),
(314, 43, 16, 'Sturnum'),
(315, 44, 16, 'Patella'),
(316, 44, 16, 'Tibia'),
(317, 44, 16, 'Metacarpals'),
(318, 44, 16, 'Radius'),
(319, 45, 16, 'Sternum'),
(320, 45, 16, 'Tibia'),
(321, 45, 16, 'Metacarpals'),
(322, 45, 16, 'Radius'),
(323, 46, 17, 'Femur'),
(324, 46, 17, 'Carpals'),
(325, 46, 17, 'Scapula'),
(326, 46, 17, 'Sturnum'),
(327, 47, 17, 'Patella'),
(328, 47, 17, 'Tibia'),
(329, 47, 17, 'Metacarpals'),
(330, 47, 17, 'Radius'),
(331, 48, 17, 'Sternum'),
(332, 48, 17, 'Tibia'),
(333, 48, 17, 'Metacarpals'),
(334, 48, 17, 'Radius'),
(335, 49, 18, 'Femur'),
(336, 49, 18, 'Carpals'),
(337, 49, 18, 'Scapula'),
(338, 49, 18, 'Sturnum'),
(339, 50, 18, 'Patella'),
(340, 50, 18, 'Tibia'),
(341, 50, 18, 'Metacarpals'),
(342, 50, 18, 'Radius'),
(343, 51, 18, 'Sternum'),
(344, 51, 18, 'Tibia'),
(345, 51, 18, 'Metacarpals'),
(346, 51, 18, 'Radius'),
(347, 52, 19, 'Femur'),
(348, 52, 19, 'Carpals'),
(349, 52, 19, 'Scapula'),
(350, 52, 19, 'Sturnum'),
(351, 53, 19, 'Patella'),
(352, 53, 19, 'Tibia'),
(353, 53, 19, 'Metacarpals'),
(354, 53, 19, 'Radius'),
(355, 54, 19, 'Sternum'),
(356, 54, 19, 'Tibia'),
(357, 54, 19, 'Metacarpals'),
(358, 54, 19, 'Radius'),
(359, 55, 20, 'Femur'),
(360, 55, 20, 'Carpals'),
(361, 55, 20, 'Scapula'),
(362, 55, 20, 'Sturnum'),
(363, 56, 20, 'Patella'),
(364, 56, 20, 'Tibia'),
(365, 56, 20, 'Metacarpals'),
(366, 56, 20, 'Radius'),
(367, 57, 20, 'Sternum'),
(368, 57, 20, 'Tibia'),
(369, 57, 20, 'Metacarpals'),
(370, 57, 20, 'Radius'),
(371, 58, 21, 'Femur'),
(372, 58, 21, 'Carpals'),
(373, 58, 21, 'Scapula'),
(374, 58, 21, 'Sturnum'),
(375, 59, 21, 'Patella'),
(376, 59, 21, 'Tibia'),
(377, 59, 21, 'Metacarpals'),
(378, 59, 21, 'Radius'),
(379, 60, 21, 'Sternum'),
(380, 60, 21, 'Tibia'),
(381, 60, 21, 'Metacarpals'),
(382, 60, 21, 'Radius'),
(383, 61, 22, 'Femur'),
(384, 61, 22, 'Carpals'),
(385, 61, 22, 'Scapula'),
(386, 61, 22, 'Sturnum'),
(387, 62, 22, 'Patella'),
(388, 62, 22, 'Tibia'),
(389, 62, 22, 'Metacarpals'),
(390, 62, 22, 'Radius'),
(391, 63, 22, 'Sternum'),
(392, 63, 22, 'Tibia'),
(393, 63, 22, 'Metacarpals'),
(394, 63, 22, 'Radius'),
(395, 64, 23, 'Femur'),
(396, 64, 23, 'Carpals'),
(397, 64, 23, 'Scapula'),
(398, 64, 23, 'Sturnum'),
(399, 65, 23, 'Patella'),
(400, 65, 23, 'Tibia'),
(401, 65, 23, 'Metacarpals'),
(402, 65, 23, 'Radius'),
(403, 66, 23, 'Sternum'),
(404, 66, 23, 'Tibia'),
(405, 66, 23, 'Metacarpals'),
(406, 66, 23, 'Radius'),
(407, 67, 24, 'Femur'),
(408, 67, 24, 'Carpals'),
(409, 67, 24, 'Scapula'),
(410, 67, 24, 'Sturnum'),
(411, 68, 24, 'Patella'),
(412, 68, 24, 'Tibia'),
(413, 68, 24, 'Metacarpals'),
(414, 68, 24, 'Radius'),
(415, 69, 24, 'Sternum'),
(416, 69, 24, 'Tibia'),
(417, 69, 24, 'Metacarpals'),
(418, 69, 24, 'Radius'),
(419, 70, 25, 'Femur'),
(420, 70, 25, 'Carpals'),
(421, 70, 25, 'Scapula'),
(422, 70, 25, 'Sturnum'),
(423, 71, 25, 'Patella'),
(424, 71, 25, 'Tibia'),
(425, 71, 25, 'Metacarpals'),
(426, 71, 25, 'Radius'),
(427, 72, 25, 'Sternum'),
(428, 72, 25, 'Tibia'),
(429, 72, 25, 'Metacarpals'),
(430, 72, 25, 'Radius'),
(431, 73, 26, 'Femur'),
(432, 73, 26, 'Carpals'),
(433, 73, 26, 'Scapula'),
(434, 73, 26, 'Sturnum'),
(435, 74, 26, 'Patella'),
(436, 74, 26, 'Tibia'),
(437, 74, 26, 'Metacarpals'),
(438, 74, 26, 'Radius'),
(439, 75, 26, 'Sternum'),
(440, 75, 26, 'Tibia'),
(441, 75, 26, 'Metacarpals'),
(442, 75, 26, 'Radius'),
(443, 76, 27, 'Femur'),
(444, 76, 27, 'Carpals'),
(445, 76, 27, 'Scapula'),
(446, 76, 27, 'Sturnum'),
(447, 77, 27, 'Patella'),
(448, 77, 27, 'Tibia'),
(449, 77, 27, 'Metacarpals'),
(450, 77, 27, 'Radius'),
(451, 78, 27, 'Sternum'),
(452, 78, 27, 'Tibia'),
(453, 78, 27, 'Metacarpals'),
(454, 78, 27, 'Radius'),
(455, 79, 28, 'Femur'),
(456, 79, 28, 'Carpals'),
(457, 79, 28, 'Scapula'),
(458, 79, 28, 'Sturnum'),
(459, 80, 28, 'Patella'),
(460, 80, 28, 'Tibia'),
(461, 80, 28, 'Metacarpals'),
(462, 80, 28, 'Radius'),
(463, 81, 28, 'Sternum'),
(464, 81, 28, 'Tibia'),
(465, 81, 28, 'Metacarpals'),
(466, 81, 28, 'Radius'),
(467, 82, 29, 'Femur'),
(468, 82, 29, 'Carpals'),
(469, 82, 29, 'Scapula'),
(470, 82, 29, 'Sturnum'),
(471, 83, 29, 'Patella'),
(472, 83, 29, 'Tibia'),
(473, 83, 29, 'Metacarpals'),
(474, 83, 29, 'Radius'),
(475, 84, 29, 'Sternum'),
(476, 84, 29, 'Tibia'),
(477, 84, 29, 'Metacarpals'),
(478, 84, 29, 'Radius'),
(479, 85, 30, 'Femur'),
(480, 85, 30, 'Carpals'),
(481, 85, 30, 'Scapula'),
(482, 85, 30, 'Sturnum'),
(483, 86, 30, 'Patella'),
(484, 86, 30, 'Tibia'),
(485, 86, 30, 'Metacarpals'),
(486, 86, 30, 'Radius'),
(487, 87, 30, 'Sternum'),
(488, 87, 30, 'Tibia'),
(489, 87, 30, 'Metacarpals'),
(490, 87, 30, 'Radius'),
(491, 88, 31, 'Femur'),
(492, 88, 31, 'Carpals'),
(493, 88, 31, 'Scapula'),
(494, 88, 31, 'Sturnum'),
(495, 89, 31, 'Patella'),
(496, 89, 31, 'Tibia'),
(497, 89, 31, 'Metacarpals'),
(498, 89, 31, 'Radius'),
(499, 90, 31, 'Sternum'),
(500, 90, 31, 'Tibia'),
(501, 90, 31, 'Metacarpals'),
(502, 90, 31, 'Radius'),
(503, 91, 32, 'Femur'),
(504, 91, 32, 'Carpals'),
(505, 91, 32, 'Scapula'),
(506, 91, 32, 'Sturnum'),
(507, 92, 32, 'Patella'),
(508, 92, 32, 'Tibia'),
(509, 92, 32, 'Metacarpals'),
(510, 92, 32, 'Radius'),
(511, 93, 32, 'Sternum'),
(512, 93, 32, 'Tibia'),
(513, 93, 32, 'Metacarpals'),
(514, 93, 32, 'Radius'),
(519, 95, 33, 'Patella'),
(520, 95, 33, 'Tibia'),
(521, 95, 33, 'Metacarpals'),
(522, 95, 33, 'Radius'),
(523, 96, 33, 'Sternum'),
(524, 96, 33, 'Tibia'),
(525, 96, 33, 'Metacarpals'),
(526, 96, 33, 'Radius'),
(527, 94, 33, 'fisstt'),
(528, 94, 33, 'neet'),
(529, 94, 33, 'clean'),
(530, 94, 33, 'Stunner'),
(531, 97, 34, 'Femur'),
(532, 97, 34, 'Carpals'),
(533, 97, 34, 'Scapula'),
(534, 97, 34, 'Sturnum'),
(535, 98, 34, 'Patella'),
(536, 98, 34, 'Tibia'),
(537, 98, 34, 'Metacarpals'),
(538, 98, 34, 'Radius'),
(539, 99, 34, 'Sternum'),
(540, 99, 34, 'Tibia'),
(541, 99, 34, 'Metacarpals'),
(542, 99, 34, 'Radius'),
(543, 100, 35, 'Femur'),
(544, 100, 35, 'Carpals'),
(545, 100, 35, 'Scapula'),
(546, 100, 35, 'Sturnum'),
(547, 101, 35, 'Patella'),
(548, 101, 35, 'Tibia'),
(549, 101, 35, 'Metacarpals'),
(550, 101, 35, 'Radius'),
(551, 102, 35, 'Sternum'),
(552, 102, 35, 'Tibia'),
(553, 102, 35, 'Metacarpals'),
(554, 102, 35, 'Radius'),
(555, 103, 36, 'Femur'),
(556, 103, 36, 'Carpals'),
(557, 103, 36, 'Scapula'),
(558, 103, 36, 'Sturnum'),
(559, 104, 36, 'Patella'),
(560, 104, 36, 'Tibia'),
(561, 104, 36, 'Metacarpals'),
(562, 104, 36, 'Radius'),
(563, 105, 36, 'Sternum'),
(564, 105, 36, 'Tibia'),
(565, 105, 36, 'Metacarpals'),
(566, 105, 36, 'Radius'),
(567, 106, 37, 'Femur'),
(568, 106, 37, 'Carpals'),
(569, 106, 37, 'Scapula'),
(570, 106, 37, 'Sturnum'),
(571, 107, 37, 'Patella'),
(572, 107, 37, 'Tibia'),
(573, 107, 37, 'Metacarpals'),
(574, 107, 37, 'Radius'),
(575, 108, 37, 'Sternum'),
(576, 108, 37, 'Tibia'),
(577, 108, 37, 'Metacarpals'),
(578, 108, 37, 'Radius'),
(579, 109, 38, 'Femur'),
(580, 109, 38, 'Carpals'),
(581, 109, 38, 'Scapula'),
(582, 109, 38, 'Sturnum'),
(583, 110, 38, 'Patella'),
(584, 110, 38, 'Tibia'),
(585, 110, 38, 'Metacarpals'),
(586, 110, 38, 'Radius'),
(587, 111, 38, 'Sternum'),
(588, 111, 38, 'Tibia'),
(589, 111, 38, 'Metacarpals'),
(590, 111, 38, 'Radius'),
(591, 112, 39, 'Femur'),
(592, 112, 39, 'Carpals'),
(593, 112, 39, 'Scapula'),
(594, 112, 39, 'Sturnum'),
(595, 113, 39, 'Patella'),
(596, 113, 39, 'Tibia'),
(597, 113, 39, 'Metacarpals'),
(598, 113, 39, 'Radius'),
(599, 114, 39, 'Sternum'),
(600, 114, 39, 'Tibia'),
(601, 114, 39, 'Metacarpals'),
(602, 114, 39, 'Radius'),
(603, 115, 40, 'Femur'),
(604, 115, 40, 'Carpals'),
(605, 115, 40, 'Scapula'),
(606, 115, 40, 'Sturnum'),
(607, 116, 40, 'Patella'),
(608, 116, 40, 'Tibia'),
(609, 116, 40, 'Metacarpals'),
(610, 116, 40, 'Radius'),
(611, 117, 40, 'Sternum'),
(612, 117, 40, 'Tibia'),
(613, 117, 40, 'Metacarpals'),
(614, 117, 40, 'Radius'),
(627, 118, 41, 'Negative 4'),
(628, 118, 41, 'Negative 3'),
(629, 118, 41, '+4'),
(630, 118, 41, '+7'),
(631, 119, 41, 'E. coli'),
(632, 119, 41, ' S. aureus'),
(633, 119, 41, ' S. pneumoniae'),
(634, 119, 41, 'N. meningitidis'),
(635, 120, 41, 'Shake well before using'),
(636, 120, 41, 'Take with plenty of fluids'),
(637, 120, 41, 'Avoid prolonged exposure to sunlight'),
(638, 120, 41, 'Keep refrigerated');

-- --------------------------------------------------------

--
-- Table structure for table `ass_submitdate_bylearner`
--

CREATE TABLE `ass_submitdate_bylearner` (
  `sd_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `attempt_count` int(11) NOT NULL,
  `attempt_date` varchar(25) NOT NULL,
  `assessor_check_date` varchar(25) DEFAULT NULL,
  `assessor_id` int(11) NOT NULL,
  `complete_status` varchar(11) NOT NULL DEFAULT 'false'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ass_submitdate_bylearner`
--

INSERT INTO `ass_submitdate_bylearner` (`sd_id`, `user_id`, `unit_id`, `attempt_count`, `attempt_date`, `assessor_check_date`, `assessor_id`, `complete_status`) VALUES
(1, 115, 510, 2, '11/8/2020', '11/8/2020', 114, 'true'),
(2, 115, 535, 1, '11/8/2020', NULL, 0, 'true'),
(3, 119, 529, 1, '11/8/2020', '11/8/2020', 16, 'true'),
(4, 122, 536, 1, '11/8/2020', '11/8/2020', 120, 'true'),
(5, 119, 529, 2, '11/8/2020', NULL, 0, 'true'),
(6, 122, 536, 2, '11/8/2020', '11/8/2020', 120, 'true'),
(7, 119, 532, 2, '11/8/2020', NULL, 0, 'true'),
(8, 119, 533, 1, '11/8/2020', NULL, 0, 'true'),
(9, 119, 534, 1, '11/8/2020', '12/8/2020', 16, 'true'),
(10, 119, 535, 1, '11/8/2020', NULL, 0, 'true'),
(11, 122, 536, 3, '11/8/2020', '11/8/2020', 120, 'true'),
(12, 119, 532, 3, '11/8/2020', NULL, 0, 'true'),
(13, 119, 529, 3, '11/8/2020', NULL, 0, 'true'),
(14, 119, 528, 1, '11/8/2020', '11/8/2020', 16, 'true'),
(15, 119, 530, 1, '11/8/2020', '11/8/2020', 16, 'true'),
(16, 119, 533, 2, '11/8/2020', NULL, 0, 'true'),
(17, 119, 495, 1, '11/8/2020', '11/8/2020', 16, 'true'),
(18, 122, 538, 1, '11/8/2020', NULL, 0, 'true'),
(19, 122, 538, 2, '11/8/2020', NULL, 0, 'true'),
(20, 122, 538, 3, '11/8/2020', '12/8/2020', 120, 'true'),
(21, 122, 539, 1, '11/8/2020', NULL, 0, 'true'),
(22, 122, 539, 2, '12/8/2020', '12/8/2020', 120, 'true'),
(23, 119, 540, 1, '12/8/2020', '12/8/2020', 16, 'true'),
(24, 122, 541, 1, '12/8/2020', '12/8/2020', 120, 'true'),
(25, 122, 541, 2, '12/8/2020', '12/8/2020', 120, 'true'),
(26, 122, 541, 3, '12/8/2020', '12/8/2020', 120, 'true'),
(27, 115, 528, 1, '12/8/2020', '12/8/2020', 114, 'true'),
(28, 115, 528, 2, '12/8/2020', '12/8/2020', 114, 'true'),
(29, 122, 542, 1, '12/8/2020', NULL, 0, 'true'),
(30, 122, 542, 2, '12/8/2020', '14/8/2020', 120, 'true'),
(31, 115, 543, 1, '12/8/2020', '12/8/2020', 114, 'true'),
(32, 122, 539, 3, '12/8/2020', '13/8/2020', 120, 'true'),
(33, 122, 544, 1, '13/8/2020', NULL, 0, 'true'),
(34, 122, 544, 2, '13/8/2020', NULL, 0, 'true'),
(35, 122, 544, 3, '13/8/2020', '14/8/2020', 120, 'true'),
(36, 122, 545, 1, '14/8/2020', '14/8/2020', 120, 'true'),
(37, 122, 545, 2, '14/8/2020', '19/8/2020', 120, 'true'),
(38, 122, 530, 1, '19/8/2020', '19/8/2020', 120, 'true'),
(39, 122, 542, 3, '19/8/2020', NULL, 0, 'true'),
(40, 122, 548, 1, '19/8/2020', NULL, 0, 'true');

-- --------------------------------------------------------

--
-- Table structure for table `circle_the_answer`
--

CREATE TABLE `circle_the_answer` (
  `id` int(30) NOT NULL,
  `title` varchar(100) NOT NULL,
  `correct_answer` varchar(125) NOT NULL,
  `correct_reinforcement` varchar(125) NOT NULL,
  `incorrect_reinforcement` varchar(125) NOT NULL,
  `prompt` varchar(50) NOT NULL,
  `done_text` varchar(50) NOT NULL,
  `lesson_id` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `circle_the_answer`
--

INSERT INTO `circle_the_answer` (`id`, `title`, `correct_answer`, `correct_reinforcement`, `incorrect_reinforcement`, `prompt`, `done_text`, `lesson_id`) VALUES
(1, 'introduction of php', 'var_dump', 'That is correct!', 'Not quite..', 'All Done', 'Ok, Im done', '283'),
(2, 'Php', 'print_r', 'fdfsdf', 'fsdfsdf', 'sfsdfsdf', 'sfsdf', '285'),
(3, 'java', 'echo', 'fgdfgdf', 'bnvnbv', 'uikuyk', 'uiuyi', '283'),
(4, 'python', 'set', 'hgfhfg', 'hfgh', 'nbnbvn', 'nmnbm', '285'),
(5, 'android', 'hfghfg', 'hgj', 'jhgjgh', 'jhg', 'ghjghj', '245'),
(6, 'php', 'var_dump', 'srfsdrf', 'serser', 'drfsdr', 'srser', '283'),
(8, 'Circle the Answer', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', 'Circle the correct answer', 'Ok, I\'m done', '283'),
(9, 'Circle the Answer', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', 'Circle the correct answer', 'Ok, I\'m done', '283'),
(10, 'Circle the answer', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', 'Circle the correct answer', 'Ok, I\'m done', '319'),
(11, 'Circle the answer', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', 'Circle the correct answer', 'Ok, I\'m done', '319'),
(15, 'Circle the answer', 'This', 'That\'s correct!', 'Not quite...', 'Circle the correct answer', 'Ok, I\'m done', '319'),
(16, 'Circle the answer', 'This', 'That\'s correct!', 'Not quite...', 'Circle the correct answer', 'Ok, I\'m done', '319'),
(17, 'Circle the answer', 'This', 'That\'s correct!', 'Not quite...', 'Circle the correct answer', 'Ok, I\'m done', '319'),
(18, 'Circle the answer', 'This', 'That\'s correct!', 'Not quite...', 'Circle the correct answer', 'Ok, I\'m done', '319'),
(19, 'Circle the answer', 'This', 'That\'s correct!', 'Not quite...', 'Circle the correct answer', 'Ok, I\'m done', '313'),
(20, 'Circle the answer', 'This', 'That\'s correct!', 'Not quite...', 'Circle the correct answer', 'Ok, I\'m done', '313'),
(21, 'Circle the Answer', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', 'Circle the correct answer', 'Ok, I\'m done', '283'),
(27, 'Circle the Answer', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', 'Circle the correct answer', 'Ok, I\'m done', '283'),
(28, 'Circle the Answer', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', 'Circle the correct answer', 'Ok, I\'m done', '283'),
(29, 'Circle the answer', 'This', 'That\'s correct!', 'Not quite...', 'Circle the correct answer', 'Ok, I\'m done', '330'),
(30, 'Circle the answer', 'This', 'That\'s correct!', 'Not quite...', 'Circle the correct answer', 'Ok, I\'m done', '16'),
(31, 'Circle the answer', 'This', 'That\'s correct!', 'Not quite...', 'Circle the correct answer', 'Ok, I\'m done', '16'),
(32, 'Circle the answer', 'This', 'That\'s correct!', 'Not quite...', 'Circle the correct answer', 'Ok, I\'m done', '16'),
(33, 'Circle the answer', 'This', 'That\'s correct!', 'Not quite...', 'Circle the correct answer', 'Ok, I\'m done', '16'),
(34, 'Circle the answer', 'This', 'That\'s correct!', 'Not quite...', 'Circle the correct answer', 'Ok, I\'m done', '16'),
(35, 'Circle the answer', 'This', 'That\'s correct!', 'Not quite...', 'Circle the correct answer', 'Ok, I\'m done', '16'),
(36, 'Circle the answer', 'This', 'That\'s correct!', 'Not quite...', 'Circle the correct answer', 'Ok, I\'m done', '338'),
(39, 'Circle the answer', 'This', 'That\'s correct!', 'Not quite...', 'Circle the correct answer', 'Ok, I\'m done', '338'),
(41, 'Circle the answer', 'This', 'That\'s correct!', 'Not quite...', 'Circle the correct answer', 'Ok, I\'m done', '338'),
(45, 'Circle the answer', 'This', 'That\'s correct!', 'Not quite...', 'Circle the correct answer', 'Ok, I\'m done', '338'),
(46, 'Circle the answer', 'This', 'That\'s correct!', 'Not quite...', 'Circle the correct answer', 'Ok, I\'m done', '341'),
(47, 'Circle the answer', 'This', 'That\'s correct!', 'Not quite...', 'Circle the correct answer', 'Ok, I\'m done', '338'),
(48, 'Circle the answer', 'This', 'That\'s correct!', 'Not quite...', 'Circle the correct answer', 'Ok, I\'m done', '338'),
(49, 'Circle the answer', 'This', 'That\'s correct!', 'Not quite...', 'Circle the correct answer', 'Ok, I\'m done', '338'),
(50, 'Circle the answer', 'This', 'That\'s correct!', 'Not quite...', 'Circle the correct answer', 'Ok, I\'m done', '338'),
(51, 'Circle the answer', 'This', 'That\'s correct!', 'Not quite...', 'Circle the correct answer', 'Ok, I\'m done', '354'),
(56, 'Circle the answer', 'This', 'That\'s correct!', 'Not quite...', 'Circle the correct answer', 'Ok, I\'m done', '378'),
(57, 'Circle the answer', 'This', 'That\'s correct!', 'Not quite...', 'Circle the correct answer', 'Ok, I\'m done', '388');

-- --------------------------------------------------------

--
-- Table structure for table `circle_the_answer_options`
--

CREATE TABLE `circle_the_answer_options` (
  `id` int(30) NOT NULL,
  `options` varchar(100) NOT NULL,
  `circle_the_answer_id` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `circle_the_answer_options`
--

INSERT INTO `circle_the_answer_options` (`id`, `options`, `circle_the_answer_id`) VALUES
(1, 'var_dump', '6'),
(2, 'echo ', '6'),
(7, ' All of the', '10'),
(8, 'This', '10'),
(9, 'That', '10'),
(10, 'The other', '10'),
(11, ' All of the below', '9'),
(12, 'This', '9'),
(13, 'That', '9'),
(14, 'The other', '9'),
(15, ' All of the below', '19'),
(16, 'This', '19'),
(17, 'That', '19'),
(18, 'The other', '19'),
(19, ' All of the', '11'),
(20, 'This', '11'),
(21, 'That', '11'),
(22, 'The other', '11'),
(35, ' All of the', '15'),
(36, 'This', '15'),
(37, 'That', '15'),
(38, 'The other', '15'),
(39, ' All of the', '16'),
(40, 'This', '16'),
(41, 'That', '16'),
(42, 'The other', '16'),
(43, ' All of the', '17'),
(44, 'This', '17'),
(45, 'That', '17'),
(46, 'The other', '17'),
(47, ' All of the', '18'),
(48, 'This', '18'),
(49, 'That', '18'),
(50, 'The other', '18'),
(51, ' All of the', '19'),
(52, 'This', '19'),
(53, 'That', '19'),
(54, 'The other', '19'),
(55, ' All of the', '20'),
(56, 'This', '20'),
(57, 'That', '20'),
(58, 'The other', '20'),
(59, ' All of the below', '21'),
(60, 'This', '21'),
(61, 'That', '21'),
(62, 'The other', '21'),
(63, ' All of the below', '22'),
(64, 'This', '22'),
(65, 'That', '22'),
(66, 'The other', '22'),
(67, ' All of the below', '27'),
(68, 'This', '27'),
(69, 'That', '27'),
(70, 'The other', '27'),
(71, ' All of the below', '28'),
(72, 'This', '28'),
(73, 'That', '28'),
(74, 'The other', '28'),
(75, ' All of the', '29'),
(76, 'This', '29'),
(77, 'That', '29'),
(78, 'The other', '29'),
(79, ' All of the', '30'),
(80, 'This', '30'),
(81, 'That', '30'),
(82, 'The other', '30'),
(83, ' All of the', '31'),
(84, 'This', '31'),
(85, 'That', '31'),
(86, 'The other', '31'),
(87, ' All of the', '32'),
(88, 'This', '32'),
(89, 'That', '32'),
(90, 'The other', '32'),
(91, ' All of the', '33'),
(92, 'This', '33'),
(93, 'That', '33'),
(94, 'The other', '33'),
(95, ' All of the', '34'),
(96, 'This', '34'),
(97, 'That', '34'),
(98, 'The other', '34'),
(99, ' All of the', '35'),
(100, 'This', '35'),
(101, 'That', '35'),
(102, 'The other', '35'),
(103, ' All of the', '36'),
(104, 'This', '36'),
(105, 'That', '36'),
(106, 'The other', '36'),
(115, ' All of the', '39'),
(116, 'This', '39'),
(117, 'That', '39'),
(118, 'The other', '39'),
(123, ' All of the', '41'),
(124, 'This', '41'),
(125, 'That', '41'),
(126, 'The other', '41'),
(139, ' All of the', '45'),
(140, 'This', '45'),
(141, 'That', '45'),
(142, 'The other', '45'),
(143, ' All of the', '46'),
(144, 'This', '46'),
(145, 'That', '46'),
(146, 'The other', '46'),
(147, ' All of the', '47'),
(148, 'This', '47'),
(149, 'That', '47'),
(150, 'The other', '47'),
(151, ' All of the', '48'),
(152, 'This', '48'),
(153, 'That', '48'),
(154, 'The other', '48'),
(155, ' All of the', '49'),
(156, 'This', '49'),
(157, 'That', '49'),
(158, 'The other', '49'),
(159, ' All of the', '50'),
(160, 'This', '50'),
(161, 'That', '50'),
(162, 'The other', '50'),
(163, ' All of the', '51'),
(164, 'This', '51'),
(165, 'That', '51'),
(166, 'The other', '51'),
(183, ' All of the', '56'),
(184, 'This', '56'),
(185, 'That', '56'),
(186, 'The other', '56'),
(187, ' All of the', '57'),
(188, 'This', '57'),
(189, 'That', '57'),
(190, 'The other', '57');

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `id` int(11) NOT NULL,
  `client_name` varchar(125) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`id`, `client_name`) VALUES
(1, 'Myra Ponting'),
(2, 'Ehddbb'),
(3, 'Gsgsgs'),
(5, 'Fuifigkv'),
(6, 'Fuifigkv'),
(8, 'Ffgg');

-- --------------------------------------------------------

--
-- Table structure for table `component`
--

CREATE TABLE `component` (
  `id` int(11) NOT NULL,
  `component_title` varchar(125) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `component`
--

INSERT INTO `component` (`id`, `component_title`) VALUES
(1, 'CV Exercise'),
(2, 'Resistance Exercise'),
(3, 'Selecting Stretch');

-- --------------------------------------------------------

--
-- Table structure for table `corss_word_details`
--

CREATE TABLE `corss_word_details` (
  `id` int(30) NOT NULL,
  `cross_word_id` int(50) DEFAULT NULL,
  `words` varchar(50) DEFAULT NULL,
  `answer_text` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `corss_word_details`
--

INSERT INTO `corss_word_details` (`id`, `cross_word_id`, `words`, `answer_text`) VALUES
(1, 1, 'java', NULL),
(2, 1, 'python', NULL),
(3, 1, 'php', NULL),
(4, 1, 'react native', NULL),
(5, 1, 'android', NULL),
(6, 1, 'oracle', NULL),
(7, 2, 'monali', NULL),
(8, 2, 'rajveer', NULL),
(9, 2, 'arnav', NULL),
(10, 2, 'shubham', NULL),
(11, 2, 'poonam', NULL),
(12, 2, 'khushi', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `id` int(30) NOT NULL,
  `course_name` varchar(100) NOT NULL,
  `image` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'Unpublished',
  `from_date` varchar(50) NOT NULL DEFAULT '00-00-0000',
  `to_date` varchar(50) NOT NULL DEFAULT '0000-00-00',
  `due_date` varchar(50) NOT NULL DEFAULT '0000-00-00',
  `course_type` varchar(50) DEFAULT NULL,
  `sequence_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `course_name`, `image`, `description`, `status`, `from_date`, `to_date`, `due_date`, `course_type`, `sequence_id`) VALUES
(1, 'LEVEL 2', 'course_images/15902998801646794435.png', 'FITNESS INSTRUCTOR', 'Unpublished', '1/29/2020', '2/29/2020', '', NULL, 0),
(2, 'LEVEL 3 ', 'course_images/15898163481657429126.png', 'PERSONAL TRAINER', 'Unpublished', '11/11/2019', '11/11/2019', '', NULL, 1),
(76, 'LEVEL 4 ', 'course_images/1589792783455135646.png', 'STRENGTH AND CONDITIONING SPECIALIST', 'Unpublished', '5/18/2020', '5/18/2020', '', NULL, 2),
(77, 'LEVEL 4 LOWER BACK PAIN ', 'course_images/1589816502818546685.png', 'hbnjv', 'Published', '5/18/2020', '5/18/2020', '', NULL, 5),
(81, 'Assessment Pathway Guidance', 'course_images/1589969620839182694.png', '', 'Unpublished', '5/20/2020', '5/20/2020', '', NULL, 4),
(82, 'ADMIN', '', 'ADMIN', 'Unpublished', '5/22/2020', '5/22/2020', '', NULL, 6),
(86, 'LEVEL 3 EXERCISE REFERRAL SPECIALIST', '', 'EXERCISE REFERRAL SPECIALIST', 'Unpublished', '6/16/2020', '6/16/2020', '', NULL, 3),
(88, 'Level 1', '', 'NEW COURSE', 'Unpublished', '6/30/2020', '6/30/2020', '0000-00-00', NULL, 7),
(89, 'LEVEL 7', '', 'level 7', 'Unpublished', '7/2/2020', '7/2/2020', '', NULL, 10),
(92, '@Ash New Course update', '', 'Title test up', 'Unpublished', '7/2/2020', '7/2/2020', '', NULL, 9),
(100, 'Python for beginner', '', 'Python for beginner', 'Unpublished', '7/9/2020', '7/9/2020', '', NULL, 12),
(113, 'Unit testing', '', 'sbdjdjk', 'Published', '7/10/2020', '7/10/2020', '', NULL, 15),
(115, 'shubham\'s dssfffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff', '', 'shubhamasf asfsa fasfsfufuisifdsufusgdfusgfdfugsfgyudgfusgfgsdfjsfsgdfhgdsgfhgsdfhgdsfgshdgfhgsdgfshdgfhgsdfgsdhfgsdgfhgsdfgsdfgsdgfsdgfhgsdsdsdsdsdsd', 'Published', '7/20/2020', '7/20/2020', '', NULL, 16),
(116, 'Manua\'l Testing', 'course_images/15953153591451738417.png', 'Manual Testing', 'Published', '7/21/2020', '12/31/2020', '', NULL, 17),
(117, 'Aish Testing', 'course_images/1595333887925481231.png', 'Testing', 'Unpublished', '7/21/2020', '7/31/2020', '', NULL, 18),
(118, 'Publish vgvCourse', '', 'Publish C    bjbourse', 'Unpublished', '7/25/2020', '7/25/2020', '', NULL, 19),
(120, 'AS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS AS', 'course_images/15954798881454342401.png', 'DESASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS AS', 'Unpublished', '7/21/2020', '7/21/2020', '', NULL, 20),
(121, 'Test Course\'s...', 'course_images/15955001281479930999.png', 'Course for testing\\\'s....', 'Unpublished', '7/31/2020', '7/31/2020', '', NULL, 21),
(123, 'demo course\'s', 'course_images/15962031761455893375.png', 'demo course\'s', 'Published', '7/31/2020', '7/31/2020', '', NULL, 22),
(124, 'test course\'s', 'course_images/15967053341881091393.png', 'test course\'s', 'Unpublished', '7/31/2020', '7/31/2020', '', NULL, 23),
(125, 'Test Demonstartion', '', 'do your best', 'Published', '8/8/2020', '8/8/2020', '0000-00-00', NULL, 24),
(126, 'new course', '', 'new course', 'Published', '8/11/2020', '8/11/2020', '', NULL, 25),
(127, 'demo course', 'course_images/15971250301481624121.png', 'demo course', 'Published', '8/11/2020', '8/11/2020', '', NULL, 26),
(128, 'Can yaman course', 'course_images/15972232301055680920.png', 'shshhsh', 'Published', '8/11/2020', '9/4/2020', '', NULL, 27),
(129, 'testttt', 'course_images/1597308505452218744.png', 'xxxxx', 'Published', '8/13/2020', '8/13/2020', '', NULL, 28);

-- --------------------------------------------------------

--
-- Table structure for table `course_type`
--

CREATE TABLE `course_type` (
  `id` int(10) NOT NULL,
  `course_type` varchar(256) DEFAULT NULL,
  `plan` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course_type`
--

INSERT INTO `course_type` (`id`, `course_type`, `plan`) VALUES
(1, 'type 1 of ASH', '1'),
(2, 'type two', '1'),
(3, 'Gold', '2'),
(5, 'PINK @  Lables', '2'),
(7, 'yellow', '1'),
(8, 'PLATlatinum', '1'),
(9, 'Platinum Diploma', '1'),
(10, 'PINK LABEL DIPLOMA', '1'),
(11, 'BLUE DIPLOMA', '1'),
(12, 'SIMI COURSE', '1'),
(13, 'DPP package', NULL),
(17, 'Latest package', NULL),
(20, 'Software Testing', NULL),
(25, 'bnvnb', NULL),
(26, 'Test', NULL),
(27, 'test new\'s', NULL),
(28, 'demo\'s', NULL),
(29, 'test\'s', NULL),
(30, '\"csc\" center\'s - l/o', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cpd`
--

CREATE TABLE `cpd` (
  `cpd_id` int(11) NOT NULL,
  `course_type_id` int(11) NOT NULL,
  `cpd` varchar(125) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cpd`
--

INSERT INTO `cpd` (`cpd_id`, `course_type_id`, `cpd`) VALUES
(3, 2, 'Level 1'),
(5, 3, 'Level 2'),
(6, 7, 'Level 1'),
(9, 5, 'Suspension Training'),
(10, 5, 'Boxing'),
(11, 8, 'Level 1'),
(13, 9, 'Suspension training'),
(14, 9, 'Kettlebells'),
(19, 9, 'muscles'),
(20, 8, 'health'),
(22, 2, 'Level\'s 3'),
(24, 5, 'Testing level'),
(32, 11, 'Q1'),
(33, 11, 'Q2'),
(34, 12, 'level 1'),
(38, 30, 'fdgsdafsssssssssssssssssssssssssssssssssssssssssss'),
(39, 8, 'Test Demonstration'),
(40, 30, 'level 1'),
(41, 17, 'Can yaman course'),
(42, 17, 'testttt'),
(43, 17, 'new course');

-- --------------------------------------------------------

--
-- Table structure for table `cpd_course`
--

CREATE TABLE `cpd_course` (
  `q_id` int(11) NOT NULL,
  `cpd_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `course_type_id` int(11) NOT NULL,
  `status` varchar(16) NOT NULL DEFAULT 'Unpublished'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cpd_course`
--

INSERT INTO `cpd_course` (`q_id`, `cpd_id`, `course_id`, `course_type_id`, `status`) VALUES
(3, 3, 1, 2, 'Unpublished'),
(5, 3, 38, 2, 'Unpublished'),
(6, 3, 69, 2, 'Unpublished'),
(7, 3, 70, 2, 'Published'),
(8, 5, 75, 3, 'Unpublished'),
(9, 5, 68, 3, 'Unpublished'),
(10, 5, 2, 3, 'Unpublished'),
(11, 6, 77, 7, 'Unpublished'),
(13, 6, 78, 7, 'Unpublished'),
(14, 3, 84, 2, 'Unpublished'),
(15, 11, 77, 8, 'Published'),
(16, 11, 81, 8, 'Unpublished'),
(17, 3, 87, 2, 'Unpublished'),
(19, 20, 1, 8, 'Unpublished'),
(20, 20, 76, 8, 'Unpublished'),
(21, 22, 1, 2, 'Unpublished'),
(22, 11, 87, 8, 'Unpublished'),
(23, 6, 1, 7, 'Unpublished'),
(25, 9, 77, 5, 'Unpublished'),
(26, 10, 92, 5, 'Unpublished'),
(39, 3, 115, 2, 'Unpublished'),
(43, 32, 116, 11, 'Published'),
(46, 3, 100, 2, 'Unpublished'),
(47, 3, 92, 2, 'Unpublished'),
(48, 3, 89, 2, 'Unpublished'),
(49, 11, 115, 8, 'Published'),
(50, 32, 117, 11, 'Unpublished'),
(51, 33, 117, 11, 'Published'),
(52, 34, 115, 12, 'Unpublished'),
(53, 34, 123, 12, 'Published'),
(54, 34, 124, 12, 'Unpublished'),
(56, 39, 125, 8, 'Published'),
(57, 38, 115, 30, 'Published'),
(58, 38, 126, 30, 'Published'),
(59, 40, 127, 30, 'Published'),
(60, 40, 123, 30, 'Published'),
(61, 41, 128, 17, 'Published'),
(62, 42, 129, 17, 'Published'),
(63, 43, 126, 17, 'Published');

-- --------------------------------------------------------

--
-- Table structure for table `crossword_data`
--

CREATE TABLE `crossword_data` (
  `id` int(10) NOT NULL,
  `words` varchar(100) NOT NULL,
  `crossword_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `crossword_data`
--

INSERT INTO `crossword_data` (`id`, `words`, `crossword_id`) VALUES
(42, 'productX', 5),
(43, 'productY', 5),
(44, 'productZ', 5),
(48, 'productX', 7),
(49, 'productY', 7),
(50, 'productZ', 7),
(51, 'productX', 8),
(52, 'productY', 8),
(53, 'productZ', 8),
(54, 'productX', 9),
(55, 'productY', 9),
(56, 'productZ', 9),
(60, 'productX', 11),
(61, 'productY', 11),
(62, 'productZ', 11),
(63, 'productX', 12),
(64, 'productY', 12),
(65, 'productZ', 12),
(96, 'productX', 13),
(97, 'productY', 13),
(98, 'productZ', 13),
(115, 'femur', 15),
(116, 'Humerus', 15),
(117, 'Tibia', 15),
(118, 'Radius', 15),
(119, 'Metacarpals', 15),
(120, 'Metatarsals', 15),
(121, 'productX', 16),
(122, 'productY', 16),
(123, 'productZ', 16),
(130, 'productX', 17),
(131, 'productY', 17),
(132, 'productZ', 17),
(139, 'productX', 18),
(140, 'productY', 18),
(141, 'productZ', 18),
(142, 'productX', 19),
(143, 'productY', 19),
(144, 'productZ', 19),
(163, 'Femur', 20),
(164, 'Humerus', 20),
(165, 'Metacarpals', 20),
(166, 'Metatarsals', 20),
(167, 'Tibia', 20),
(168, 'fibula', 20),
(214, 'productX', 21),
(215, 'productY', 21),
(216, 'productZ', 21),
(217, 'productX', 1),
(218, 'productY', 1),
(219, 'productZ', 1),
(220, 'productX', 22),
(221, 'productY', 22),
(222, 'productZ', 22),
(232, 'Femur', 23),
(233, 'Humerus', 23),
(234, 'Ulna', 23),
(235, 'Tibia', 23),
(236, 'Fibula', 23),
(237, 'Radius', 23),
(244, 'productX', 25),
(245, 'productY', 25),
(246, 'productZ', 25),
(247, 'productX', 26),
(248, 'productY', 26),
(249, 'productZ', 26),
(250, 'productX', 24),
(251, 'productY', 24),
(252, 'productZ', 24),
(253, 'productX', 27),
(254, 'productY', 27),
(255, 'productZ', 27),
(276, 'ProductZ', 28),
(283, 'productX', 30),
(284, 'productY', 30),
(285, 'productZ', 30),
(309, 'DO', 33),
(310, 'productY', 33),
(311, 'productZ', 33),
(315, 'productX', 34),
(316, 'productY', 34),
(317, 'productZ', 34),
(330, 'productX', 35),
(331, 'productY', 35),
(332, 'productZ', 35),
(336, 'productX', 37),
(337, 'productY', 37),
(338, 'productZ', 37),
(342, 'productX', 39),
(343, 'productY', 39),
(344, 'productZ', 39);

-- --------------------------------------------------------

--
-- Table structure for table `cross_word`
--

CREATE TABLE `cross_word` (
  `id` int(30) NOT NULL,
  `title` varchar(256) NOT NULL,
  `prompt` varchar(50) NOT NULL,
  `done_text` varchar(50) NOT NULL,
  `correct_reinforcement` varchar(60) NOT NULL,
  `incorrect_reinforcement` varchar(60) NOT NULL,
  `time_limit` varchar(60) NOT NULL,
  `lesson_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cross_word`
--

INSERT INTO `cross_word` (`id`, `title`, `prompt`, `done_text`, `correct_reinforcement`, `incorrect_reinforcement`, `time_limit`, `lesson_id`) VALUES
(1, 'Find all of the products that make up our range.', 'Find the words in the grid', 'Ok, Im done', 'Thats correct!', 'Not quite...', '30000', 1),
(5, 'Find all of the products that make up our range', 'Find the words in the grid', 'Ok, I\'m done', 'That\'s correct!', 'Not quite...', '10000', 285),
(7, 'Find all of the products that make up our range', 'Find the words in the grid', 'Ok, I\'m done', 'That\'s correct!', 'Not quite...', '10000', 286),
(8, 'Find all of the products that make up our range', 'Find the words in the grid', 'Ok, I\'m done', 'That\'s correct!', 'Not quite...', '10000', 219),
(9, 'Find all of the products that make up our range', 'Find the words in the grid', 'Ok, I\'m done', 'That\'s correct!', 'Not quite...', '10000', 292),
(11, 'Find all of the products that make up our range', 'Find the words in the grid', 'Ok, I\'m done', 'That\'s correct!', 'Not quite...', '10000', 290),
(12, 'Find all of the products that make up our range', 'Find the words in the grid', 'Ok, I\'m done', 'That\'s correct!', 'Not quite...', '10000', 294),
(13, 'Find all of the products that make up our range', 'Find the words in the grid', 'Ok, I\'m done', 'That\'s correct!', 'Not quite...', '10000', 295),
(15, 'Can you find the long bones?', 'Find the words in the grid', 'Ok, I\'m done', 'That\'s correct!', 'Not quite...', '10000', 298),
(16, 'Find all of the products that make up our range', 'Find the words in the grid', 'Ok, I\'m done', 'That\'s correct!', 'Not quite...', '10000', 298),
(17, 'Find all of the products that make up our range', 'Find the words in the grid', 'Ok, I\'m done', 'That\'s correct!', 'Not quite...', '10000', 15),
(18, 'Find all of the products that make up our range', 'Find the words in the grid', 'Ok, I\'m done', 'That\'s correct!', 'Not quite...', '10000', 303),
(19, 'Find all of the products that make up our range', 'Find the words in the grid', 'Ok, I\'m done', 'That\'s correct!', 'Not quite...', '10000', 1),
(20, 'Long bones', 'Find the words in the grid', 'Ok, I\'m done', 'That\'s correct!', 'Not quite...', '1000', 1),
(21, 'Find all of the products that make up our range', 'Find the words in the grid', 'Ok, I\'m done', 'That\'s correct!', 'Not quite...', '10000', 322),
(22, 'Find all of the products that make up our range', 'Find the words in the grid', 'Ok, I\'m done', 'That\'s correct!', 'Not quite...', '10000', 327),
(23, 'Find the name of the long bones', 'Find the words in the grid', 'Ok, I\'m done', 'That\'s correct!', 'Not quite...', '200000', 329),
(24, 'Find all of the products that make up our range', 'Find the words in the grid', 'Ok, I\'m done', 'That\'s correct!', 'Not quite...', '10000', 338),
(25, 'Find all of the products that make up our range', 'Find the words in the grid', 'Ok, I\'m done', 'That\'s correct!', 'Not quite...', '10000', 351),
(26, 'Find all of the products that make up our range', 'Find the words in the grid', 'Ok, I\'m done', 'That\'s correct!', 'Not quite...', '10000', 338),
(27, 'Find all of the products that make up our range', 'Find the words in the grid', 'Ok, I\'m done', 'That\'s correct!', 'Not quite...', '10000', 338),
(28, 'Find all of the products that make up our range', 'Find the words in the grid', 'Ok, I\'m done', 'That\'s correct!', 'Not quite...', '10000', 364),
(30, 'Find all of the products that make up our range', 'Find the words in the grid', 'Ok, I\'m done', 'That\'s correct!', 'Not quite...', '10000', 354),
(33, 'Find all of the products that make up our range', 'Find the words in the grid', 'Ok, I\'m done', 'That\'s correct!', 'Not quite...', '10000', 388),
(34, 'Find all of the products that make up our range', 'Find the words in the grid', 'Ok, I\'m done', 'That\'s correct!', 'Not quite...', '10000', 396),
(35, 'Find all of the products that make up our range', 'Find the words in the grid', 'Ok, I\'m done', 'That\'s correct!', 'Not quite...', '10000', 365),
(37, 'Find all of the products that make up our range', 'Find the words in the grid', 'Ok, I\'m done', 'That\'s correct!', 'Not quite...', '10000', 401),
(39, 'Find all of the products that make up our range', 'Find the words in the grid', 'Ok, I\'m done', 'That\'s correct!', 'Not quite...', '10000', 347);

-- --------------------------------------------------------

--
-- Table structure for table `cv_exercise`
--

CREATE TABLE `cv_exercise` (
  `cv_id` int(11) NOT NULL,
  `exercise_type_id` int(11) NOT NULL,
  `activity` varchar(125) NOT NULL,
  `equipment_id` int(11) NOT NULL,
  `MHR` varchar(11) NOT NULL,
  `RPE` varchar(11) NOT NULL,
  `traning_zone` varchar(125) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cv_exercise`
--

INSERT INTO `cv_exercise` (`cv_id`, `exercise_type_id`, `activity`, `equipment_id`, `MHR`, `RPE`, `traning_zone`, `user_id`) VALUES
(1, 1, 'new activity', 1, '25', '11', 'new traning zone', 28),
(2, 1, 'second activity', 2, '25', '11', 'new traning zone', 28),
(3, 2, 'Sbffzbfzbbfs', 3, 'Babdaadbsbf', 'Dansjst', ' Czzc z cznfzgnxgn', 28),
(4, 2, 'Svjffjf', 4, 'Dhrjktkgjf', 'Dbdbnfmgm', 'Xbmglhlhmgfrj', 28),
(5, 3, 'Gchvjbjb', 1, 'Cghcjbkb', ' Ghcjbbjjb', 'Vgxhvkbkn', 28),
(6, 3, 'Dtsggdsfgddg', 3, 'VdDbfsfzb', 'Grhaargrga', 'C ,C X.vxgmxgm', 28),
(7, 3, 'Fg. G.h', 2, 'Dtyhf', 'Xgbchc', 'Cbbnvvn', 27),
(8, 3, 'Udkfjf', 4, 'Hdjxfj', 'Jxjxjckc', 'Hxncmvmv', 28);

-- --------------------------------------------------------

--
-- Table structure for table `drag_the_Match`
--

CREATE TABLE `drag_the_Match` (
  `dtm_id` int(30) NOT NULL,
  `lesson_id` int(50) NOT NULL,
  `title` varchar(50) NOT NULL,
  `done_text` varchar(30) NOT NULL,
  `prompt` varchar(160) NOT NULL,
  `correct_reinforcement` varchar(125) NOT NULL,
  `incorrect_reinforcement` varchar(125) NOT NULL,
  `answer_text` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `drag_the_Match`
--

INSERT INTO `drag_the_Match` (`dtm_id`, `lesson_id`, `title`, `done_text`, `prompt`, `correct_reinforcement`, `incorrect_reinforcement`, `answer_text`) VALUES
(12, 364, 'Match the product with its name', 'Ok, I\'m done', 'Drag the images to their labels', 'That\'s correct!', 'Not quite...', 'The first is Product X, then Product Y and thirdly Product Z.'),
(13, 362, 'Match the product with its name', 'Ok, I\'m done', 'Drag the images to their labels', 'That\'s correct!', 'Not quite...', 'The first is Product X, then Product Y and thirdly Product Z.'),
(14, 376, 'Match the product with its name', 'Ok, I\'m done', 'Drag the images to their labels', 'That\'s correct!', 'Not quite...', 'The first is Product X, then Product Y and thirdly Product Z.'),
(15, 388, 'Match the product with its name', 'Ok, I\'m done', 'Drag the images to their labels', 'That\'s correct!', 'Not quite...', 'The first is Product X, then Product Y and thirdly Product Z.'),
(16, 388, 'Match the product with its name', 'Ok, I\'m done', 'Drag the images to their labels', 'That\'s correct!', 'Not quite...', 'The first is Product X, then Product Y and thirdly Product Z.'),
(17, 396, 'Match the product with its name', 'Ok, I\'m done', 'Drag the images to their labels', 'That\'s correct!', 'Not quite...', 'The first is Product X, then Product Y and thirdly Product Z.'),
(18, 348, 'Match the product with its name', 'Ok, I\'m done', 'Drag the images to their labels', 'That\'s correct!', 'Not quite...', 'The first is Product X, then Product Y and thirdly Product Z.'),
(19, 348, 'Match the product with its name', 'Ok, I\'m done', 'Drag the images to their labels', 'That\'s correct!', 'Not quite...', 'The first is Product X, then Product Y and thirdly Product Z.'),
(20, 365, 'Match the product with its name', 'Ok, I\'m done', 'Drag the images to their labels', 'That\'s correct!', 'Not quite...', 'The first is Product X, then Product Y and thirdly Product Z.'),
(21, 347, 'Match the product with its name', 'Ok, I\'m done', 'Drag the images to their labels', 'That\'s correct!', 'Not quite...', 'The first is Product X, then Product Y and thirdly Product Z.'),
(22, 397, 'Match the product with its name', 'Ok, I\'m done', 'Drag the images to their labels', 'That\'s correct!', 'Not quite...', 'The first is Product X, then Product Y and thirdly Product Z.'),
(24, 401, 'Match the product with its name', 'Ok, I\'m done', 'Drag the images to their labels', 'That\'s correct!', 'Not quite...', 'The first is Product X, then Product Y and thirdly Product Z.');

-- --------------------------------------------------------

--
-- Table structure for table `drag_to_match_options`
--

CREATE TABLE `drag_to_match_options` (
  `dtmopt_id` int(11) NOT NULL,
  `left_content` varchar(225) NOT NULL,
  `right_content` varchar(225) NOT NULL,
  `dtm_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `drag_to_match_options`
--

INSERT INTO `drag_to_match_options` (`dtmopt_id`, `left_content`, `right_content`, `dtm_id`) VALUES
(36, 'drag_to_match_images/15936842531614642519.png', 'Product X', 12),
(37, 'drag_to_match_images/15936842531228192124.png', 'Product Y', 12),
(38, 'drag_to_match_images/1593684253961218871.png', 'Product Z', 12),
(42, 'drag_to_match_images/1594100108791681815.png', 'Product X', 13),
(43, 'drag_to_match_images/15941001082017458321.png', 'Product Y', 13),
(44, 'drag_to_match_images/1594100108233992159.png', 'Product Z', 13),
(48, 'drag_to_match_images/1594100478223937387.png', 'Product X', 14),
(49, 'drag_to_match_images/15941004781995057109.png', 'Product Y', 14),
(50, 'drag_to_match_images/15941004781245211825.png', 'Product Z', 14),
(51, 'drag_to_match_images/1595492172807397334.png', 'Product X', 15),
(52, 'drag_to_match_images/15954921721255537600.png', 'Product Y', 15),
(53, 'drag_to_match_images/15954921721986195688.png', 'Product Z', 15),
(54, 'drag_to_match_images/1595492173936218227.png', 'Product X', 16),
(55, 'drag_to_match_images/15954921731330852223.png', 'Product Y', 16),
(56, 'drag_to_match_images/15954921731850718448.png', 'Product Z', 16),
(60, 'drag_to_match_images/15956730851033383165.png', 'Product X', 17),
(61, 'drag_to_match_images/1595673085876385135.png', 'Product Y', 17),
(62, 'drag_to_match_images/15956730851641059153.png', 'Product Z', 17),
(63, 'drag_to_match_images/1595863531742418261.png', 'Product X', 18),
(64, 'drag_to_match_images/1595863680937813127.png', 'Product X', 19),
(65, 'drag_to_match_images/15959977381294750876.png', 'Product X', 20),
(66, 'drag_to_match_images/1595997738266434772.png', 'Product Y', 20),
(67, 'drag_to_match_images/1595997738634790237.png', 'Product Z', 20),
(68, 'drag_to_match_images/15960277181443637450.png', 'Product X', 21),
(69, 'drag_to_match_images/159602771874064676.png', 'Product Y', 21),
(70, 'drag_to_match_images/1596027718349810302.png', 'Product Z', 21),
(71, 'drag_to_match_images/1596094608203317168.png', 'Product X', 22),
(72, 'drag_to_match_images/15960946081639489888.png', 'Product Y', 22),
(73, 'drag_to_match_images/1596094608196155825.png', 'Product Z', 22),
(80, 'drag_to_match_images/15967071771248939996.png', 'Product X', 24),
(81, 'drag_to_match_images/15962051301653214598.png', 'Product Y', 24),
(82, 'drag_to_match_images/15962051301028166248.png', 'Product Z', 24);

-- --------------------------------------------------------

--
-- Table structure for table `equipment`
--

CREATE TABLE `equipment` (
  `id` int(11) NOT NULL,
  `equipment_name` varchar(125) NOT NULL,
  `exercise_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `equipment`
--

INSERT INTO `equipment` (`id`, `equipment_name`, `exercise_type_id`) VALUES
(1, 'Rowing machine', 1),
(2, 'Upright cycle', 1),
(3, 'Recumbent cycle', 1),
(4, 'Treadmill', 1),
(5, 'Cross-trainer', 1),
(6, 'Stepper', 1),
(7, 'Rowing machine', 2),
(8, 'leg extension', 4),
(9, 'leg curl', 4),
(10, 'Upright cycle', 2),
(11, 'Recumbent cycle', 2),
(12, 'Stepper', 2),
(13, 'Rowing machine', 3),
(14, 'Upright cycle', 3),
(15, 'Recumbent cycle', 3),
(16, 'Stepper', 3);

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `e_id` int(11) NOT NULL,
  `title` varchar(125) NOT NULL,
  `description` text NOT NULL,
  `event_date` varchar(16) NOT NULL,
  `event_time` varchar(11) NOT NULL,
  `noti_send_date` varchar(16) NOT NULL,
  `location` varchar(225) NOT NULL,
  `course_type_id` varchar(11) DEFAULT NULL,
  `user_group` varchar(125) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`e_id`, `title`, `description`, `event_date`, `event_time`, `noti_send_date`, `location`, `course_type_id`, `user_group`) VALUES
(1, 'Yoga class', 'ashdsgs fgfs fjsgfs fsgfsdgf ds fuyd fuysd', '9/5/2020', '06:00 AM', '5/5/2020', 'Walhekar chinchwad pune 411033', '0', ''),
(2, 'Yoga class', 'ashdsgs fgfs fjsgfs fsgfsdgf ds fuyd fuysd', '15/5/2020', '06:00 AM', '05/12/2020', 'Walhekar chinchwad pune 411033', '0', ''),
(3, 'Zumba class', 'hfhf r4 ashdsgs fgfs fjsgfs ', '5/6/2020', '07:00 AM', '6/3/2020', 'Koregav park pune', '0', ''),
(4, 'Gym new', 'Gym is opened for all only for today 24*7', '3/6/2020', '07:00 AM', '2/6/2020', 'Ravet pune', '0', ''),
(5, 'Gym de', 'Gym is opened for all only for today 24*7', '15/4/2020', '07:00 AM', '12/4/2020', 'Hinjewadi pune', '0', ''),
(6, 'Gym', 'Gym is opened for all only for today 24*7', '15/4/2020', '20:00', '12/4/2020', 'pune', '0', ''),
(8, 'Smile club', 'Smile club for 3 days only', '15/7/2020', '07:00 AM', '12/7/2020', 'Koregav Park', '8', 'learners'),
(9, 'yoga', 'yoga class', '7/3/2020', '11:00', '7/2/2020', 'pune', '2', 'learners'),
(10, 'Swapnil', 'Swapnil Sadashiv Machale', '7/3/2020', '10:30', '7/2/2020', 'Pune', '1', 'learners'),
(11, 'shadi', 'event till monady s', '7/12/2020', '23:02', '7/11/2020', 'Kaveri C, Block B6, Sidhivinayak Nagari, Transport Nagar, Nigdi, Pimpri-Chinchwad, Maharashtra 411044, India', '', 'all_users'),
(12, 'zumba', '', '7/28/2020', '11:01', '7/31/2020', 'zumba', '', 'learners'),
(13, 'zumba', '', '8/14/2020', '11:11', '8/1/2020', 'pune', '', 'all_users'),
(14, 'yoga', '', '8/20/2020', '10:01', '8/12/2020', 'pune', '', 'all_users');

-- --------------------------------------------------------

--
-- Table structure for table `exercise`
--

CREATE TABLE `exercise` (
  `rex_id` int(11) NOT NULL,
  `activity` varchar(125) NOT NULL,
  `exercise_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exercise`
--

INSERT INTO `exercise` (`rex_id`, `activity`, `exercise_type_id`) VALUES
(1, 'leg extension', 4),
(2, 'leg curl', 4),
(3, 'leg press', 4),
(4, 'total hip machine', 4),
(5, 'seated adductor', 4),
(6, 'seated abductor', 4),
(7, 'Abdominal machine', 5),
(8, 'Torso twister', 5);

-- --------------------------------------------------------

--
-- Table structure for table `exercise_type`
--

CREATE TABLE `exercise_type` (
  `id` int(11) NOT NULL,
  `exercise_type` varchar(125) NOT NULL,
  `component_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exercise_type`
--

INSERT INTO `exercise_type` (`id`, `exercise_type`, `component_id`) VALUES
(1, 'Warm up CV1', 1),
(2, 'Main CV CV2', 1),
(3, 'Cool down CV3', 1),
(4, 'RM- Lower body', 2),
(5, 'RM- Abdominal and lower back', 2),
(6, 'RM- Upper body', 2),
(7, 'RM- Arms', 2),
(8, 'BW- Lower body', 2),
(9, 'BW- Abdominal and lower back', 2),
(10, 'BW- Upper body', 2),
(11, 'BW- Arms', 2),
(12, 'FW- Lower body', 2),
(13, 'FW- Abdominal and lower back', 2),
(14, 'FW- Upper body', 2),
(15, 'FW- Arms', 2),
(16, 'Warm up Stretch', 3),
(17, 'Cool down Stretch', 3);

-- --------------------------------------------------------

--
-- Table structure for table `exit_lesson`
--

CREATE TABLE `exit_lesson` (
  `id` int(30) NOT NULL,
  `title` varchar(50) NOT NULL,
  `content` varchar(50) NOT NULL,
  `button_cta` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `expandable_details`
--

CREATE TABLE `expandable_details` (
  `id` int(30) NOT NULL,
  `quetion` varchar(100) NOT NULL,
  `question_details` varchar(150) NOT NULL,
  `expandable_list_id` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `expandable_details`
--

INSERT INTO `expandable_details` (`id`, `quetion`, `question_details`, `expandable_list_id`) VALUES
(11, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 3),
(12, 'Product Y', 'Crystal clear display performs better than any of our competitors.', 3),
(13, 'Product Z', 'The newest feature offers a new level of feeddback for the user.', 3),
(14, 'Appendicular skeletondfgdfg', 'The appendicular skeleton (126 bondfgfdges) containing the arm, pelvic and leg bones, also known as  the upper and lower extremities.', 4),
(15, 'Product Xdfgdfgfdg', 'Our brilliant new dfgfdgsleek design allows the user to have unparalleled comfort when using the device.', 4),
(16, 'Product Xdfgdfgdf', 'Our brilliant new sdfgfdgfdgleek design allows the user to have unparalleled comfort when using the device.', 4),
(17, 'Appendicular skeleton', 'The appendicular skeleton (126 bones) containing the arm, pelvic and leg bones, also known as  the upper and lower extremities.', 5),
(18, 'Product Xsdfdsfs', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 5),
(28, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 7),
(29, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 8),
(31, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 9),
(32, 'Product y', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 9),
(39, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 16),
(44, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 19),
(45, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 20),
(46, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 21),
(47, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 22),
(274, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 39),
(275, 'Product Y', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 39),
(326, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 40),
(327, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 41),
(328, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 42),
(329, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 43),
(330, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 44),
(331, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 45),
(332, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 46),
(336, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 50),
(396, 'product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 1),
(397, 'Product Y', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 1),
(433, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 61),
(437, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 63),
(447, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 67),
(448, 'product Y', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 67),
(457, 'Appendicular skeleton', 'The appendicular section (126 bones) contains the arm, pelvic and leg bones, also known as the upper and lower extremities.', 68),
(458, 'Axial skeleton', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 68),
(464, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 69),
(465, 'product Y', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 69),
(466, 'Product Z', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 69),
(467, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 73),
(471, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 75),
(472, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 76),
(474, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 78),
(475, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 79),
(477, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 80),
(483, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 85),
(507, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 102),
(509, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 104),
(510, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 105),
(511, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 106),
(514, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 109),
(516, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 110),
(518, 'Product X test', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device. ,,,,', 88),
(519, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 111),
(521, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 113),
(522, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 114),
(523, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 115),
(524, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 116),
(525, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 117),
(526, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 118),
(527, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 119),
(528, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 120),
(530, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 121),
(532, 'Product X', 'Our brilliant new sleek design allows the user to have unparalleled comfort when using the device.', 122);

-- --------------------------------------------------------

--
-- Table structure for table `expandable_list`
--

CREATE TABLE `expandable_list` (
  `id` int(30) NOT NULL,
  `title` varchar(50) NOT NULL,
  `prompt` varchar(150) NOT NULL,
  `done_text` varchar(50) NOT NULL,
  `lesson_id` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `expandable_list`
--

INSERT INTO `expandable_list` (`id`, `title`, `prompt`, `done_text`, `lesson_id`) VALUES
(1, 'Learn more about our product', 'Select an item to see now', 'Ok, I am ready to move on', 1),
(3, 'Learn more about our product rangee', 'Ok, I am Done', 'done_text', 1),
(4, 'Learn more about our product range', 'Select an item to see more', 'Ok, I am ready to move on', 1),
(5, 'Learn more about our product range', 'Select an item to see more', 'Ok, I am ready to move on', 1),
(7, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 1),
(8, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 1),
(9, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 5),
(16, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 217),
(19, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 239),
(20, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 238),
(21, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 241),
(22, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 241),
(39, 'What is the capital of Wales?', 'Select an item to see more', 'Ok, I\'m done', 298),
(40, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 3),
(41, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 7),
(42, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 10),
(43, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 11),
(44, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 12),
(45, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 14),
(46, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 124),
(50, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 1),
(61, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 1),
(63, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 332),
(67, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 16),
(68, 'The bones of the axial and appendicular skeleton', 'Select an item to see more', 'Ok, I\'m done', 16),
(69, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 338),
(73, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 338),
(75, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 338),
(76, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 338),
(78, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 123),
(79, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 355),
(80, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 360),
(85, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 348),
(88, 'Expandable List testing', 'Select an item to see more', 'Ok, I\'m done', 364),
(102, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 354),
(104, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 380),
(105, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 389),
(106, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 388),
(109, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 392),
(110, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 396),
(111, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 365),
(113, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 401),
(114, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 402),
(115, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 403),
(116, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 394),
(117, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 418),
(118, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 419),
(119, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 420),
(120, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 422),
(121, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 429),
(122, 'Expandable List', 'Select an item to see more', 'Ok, I\'m done', 436);

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `f_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `feedback_from` int(11) NOT NULL,
  `feedback_to` int(11) NOT NULL,
  `feedback_text` text NOT NULL,
  `date_time_sent` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`f_id`, `unit_id`, `feedback_from`, `feedback_to`, `feedback_text`, `date_time_sent`) VALUES
(1, 226, 36, 28, 'Need improvement', '2020-09-09 11:11:11'),
(2, 224, 36, 28, 'ufhoidu', ''),
(3, 226, 36, 28, 'ufhoidu', '2020-07-07 12:34:22'),
(4, 224, 36, 28, 'ufhoidu', ''),
(5, 224, 36, 28, 'ufhoidu', ''),
(6, 224, 36, 28, 'ufhoidu', ''),
(7, 224, 36, 28, 'ufhoidu', ''),
(8, 224, 36, 28, 'ufhoidu', ''),
(9, 224, 36, 28, 'ufhoidu', ''),
(10, 224, 36, 28, 'JDFKH', ''),
(11, 224, 36, 28, 'JDFKH', ''),
(12, 225, 36, 28, 'Ddf', ''),
(13, 224, 36, 28, 'wSS', ''),
(14, 225, 36, 28, 'Vbababba', ''),
(15, 225, 36, 28, 'Vbababba', ''),
(16, 227, 36, 28, 'Cbffbgj', ''),
(17, 226, 36, 28, 'Need better improvement ', '2020-07-23 12:03:32'),
(18, 226, 36, 28, 'Need better improvement ', '2020-07-23 12:04:16'),
(19, 226, 36, 28, 'You are doing good now', '2020-07-23 12:06:52'),
(20, 224, 36, 28, 'Hio', '2020-07-24 10:25:44'),
(21, 224, 36, 28, 'Hii', '2020-07-24 10:41:09'),
(22, 224, 36, 28, 'Hello', '2020-07-24 10:41:42'),
(23, 224, 36, 28, 'Hii', '2020-07-24 10:46:39'),
(24, 224, 36, 28, 'Hello', '2020-07-24 10:46:50'),
(25, 224, 36, 28, 'How ', '2020-07-24 10:47:00'),
(26, 224, 36, 28, 'Hui', '2020-07-24 10:49:36'),
(27, 224, 36, 28, 'Hui', '2020-07-24 10:49:54'),
(28, 224, 36, 28, 'Hui', '2020-07-24 10:49:58'),
(29, 224, 36, 28, 'Hiii', '2020-07-24 10:50:06'),
(30, 224, 36, 28, 'Hiii', '2020-07-24 10:53:56'),
(31, 224, 36, 28, 'Hiii', '2020-07-24 10:53:59'),
(32, 224, 36, 28, 'Hii', '2020-07-24 11:10:12'),
(33, 224, 36, 28, 'Hiigii', '2020-07-24 11:10:25'),
(34, 470, 36, 1, 'Hii', '2020-07-26 18:06:28'),
(35, 470, 36, 1, 'Hii', '2020-07-26 18:06:35'),
(36, 470, 36, 1, 'Hello', '2020-07-26 18:06:43'),
(37, 470, 36, 1, 'Hello', '2020-07-26 18:06:45'),
(38, 470, 36, 1, 'Great', '2020-07-27 12:52:48'),
(39, 225, 36, 1, 'Hii', '2020-07-27 13:08:39'),
(40, 225, 36, 1, 'Hiii', '2020-07-27 13:09:37'),
(41, 225, 36, 1, 'Hiiihello', '2020-07-27 13:09:43'),
(42, 225, 36, 1, 'Hiiihello', '2020-07-27 13:09:44'),
(43, 225, 36, 1, 'Hiiihello', '2020-07-27 13:09:45'),
(44, 225, 36, 1, 'Hiiihello', '2020-07-27 13:09:45'),
(45, 225, 36, 1, 'Hiiihello', '2020-07-27 13:11:33'),
(46, 225, 36, 1, 'Hiiiii', '2020-07-27 13:13:02'),
(47, 225, 36, 1, 'Hi', '2020-07-27 13:14:46'),
(48, 225, 36, 1, 'Hiello', '2020-07-27 13:15:11'),
(49, 225, 36, 1, 'Hii', '2020-07-27 13:15:26'),
(50, 226, 36, 1, 'Hiii', '2020-07-27 13:52:46'),
(51, 226, 36, 1, 'Hiii', '2020-07-27 13:52:49'),
(52, 226, 36, 1, 'Hiii', '2020-07-27 13:52:50'),
(53, 226, 36, 1, 'Hiii', '2020-07-27 13:52:50'),
(54, 225, 36, 1, 'Hiii', '2020-07-28 03:59:04'),
(55, 225, 36, 1, 'Hii', '2020-07-28 04:00:38'),
(56, 224, 36, 1, 'Hii', '2020-07-28 09:00:56'),
(57, 225, 36, 1, 'Hiii', '2020-07-28 09:09:06'),
(58, 225, 36, 1, 'Hiii', '2020-07-28 09:10:04'),
(59, 224, 36, 28, 'Good', '2020-07-29 13:16:24'),
(60, 224, 36, 28, 'Good', '2020-07-29 13:16:32'),
(61, 224, 36, 28, 'Good', '2020-07-29 13:17:49'),
(62, 224, 36, 28, 'Good', '2020-07-29 13:19:58'),
(63, 224, 36, 28, 'Hii', '2020-07-29 13:23:39'),
(64, 470, 36, 1, 'Hiii', '2020-07-29 22:17:50'),
(65, 227, 36, 1, 'Hii', '2020-07-29 23:11:35'),
(66, 227, 36, 28, 'Hiii', '2020-07-30 02:19:53'),
(67, 227, 36, 28, 'How are you', '2020-07-30 02:20:04'),
(68, 226, 36, 1, 'Hii', '2020-07-31 13:29:05'),
(69, 226, 36, 1, 'Heelo', '2020-07-31 13:29:09'),
(70, 225, 36, 1, 'Hii', '2020-07-31 15:00:32'),
(71, 225, 36, 1, 'Need improvement', '2020-07-31 15:10:13'),
(72, 495, 16, 103, 'Hello', '2020-08-01 12:50:19'),
(73, 470, 16, 58, 'Hello', '2020-08-01 13:02:47'),
(74, 510, 16, 103, 'Hello', '2020-08-01 13:06:03'),
(75, 510, 16, 103, 'Jdndkdldlodkdnd djdkdnbdjd xidndndkdkdk dudndnods dudjndndnd uxdbbdjdjd dudhdbdudn dhdjdjbdud dudndndhdd djdjdn', '2020-08-01 13:07:46'),
(76, 512, 16, 103, 'Hello', '2020-08-01 13:17:32'),
(77, 495, 16, 68, 'Hiii', '2020-08-02 09:30:12'),
(78, 226, 16, 58, 'Hiiii', '2020-08-02 09:38:08'),
(79, 226, 16, 58, 'Need improvement', '2020-08-02 11:59:09'),
(80, 516, 16, 103, 'Hiiii', '2020-08-02 13:01:56'),
(81, 511, 16, 68, 'Hello', '2020-08-02 13:02:41'),
(82, 470, 36, 28, 'Hii', '2020-08-02 15:59:47'),
(83, 470, 36, 28, 'Hii', '2020-08-02 16:00:17'),
(84, 470, 36, 28, '', '2020-08-02 16:00:17'),
(85, 511, 36, 88, 'Hii', '2020-08-02 16:01:07'),
(86, 458, 36, 28, 'Hiii', '2020-08-02 16:18:04'),
(87, 495, 36, 1, 'Need improvement', '2020-08-05 12:09:17'),
(88, 495, 36, 88, 'Hii', '2020-08-06 13:08:35'),
(89, 524, 36, 28, 'Need improvement', '2020-08-09 08:57:25'),
(90, 510, 36, 88, 'Need improvement', '2020-08-09 11:00:03'),
(91, 526, 36, 28, 'Good', '2020-08-09 13:47:45'),
(92, 526, 36, 28, 'Hii', '2020-08-09 13:48:00'),
(93, 526, 36, 1, 'Hii', '2020-08-10 21:04:08'),
(94, 495, 36, 28, 'Need improvement', '2020-08-11 09:49:18'),
(95, 528, 114, 115, 'Good', '2020-08-12 13:01:31');

-- --------------------------------------------------------

--
-- Table structure for table `first_last_slide`
--

CREATE TABLE `first_last_slide` (
  `id` int(10) NOT NULL,
  `title` text,
  `sub_title` text,
  `button_text` varchar(256) NOT NULL,
  `lesson_id` int(10) DEFAULT NULL,
  `default_status` varchar(100) DEFAULT NULL,
  `unit_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `first_last_slide`
--

INSERT INTO `first_last_slide` (`id`, `title`, `sub_title`, `button_text`, `lesson_id`, `default_status`, `unit_id`) VALUES
(2, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 1, 'exit', NULL),
(3, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 2, 'exit', NULL),
(4, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 3, 'exit', NULL),
(5, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 5, 'exit', NULL),
(14, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 244, 'start', NULL),
(15, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 244, 'exit', NULL),
(16, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 245, 'start', NULL),
(17, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 245, 'exit', NULL),
(20, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 247, 'start', NULL),
(21, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 247, 'exit', NULL),
(22, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 248, 'start', NULL),
(23, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 248, 'exit', NULL),
(24, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 249, 'start', NULL),
(25, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 249, 'exit', NULL),
(26, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 250, 'start', NULL),
(27, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 250, 'exit', NULL),
(28, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 251, 'start', NULL),
(29, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 251, 'exit', NULL),
(30, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson0000000', 252, 'start', NULL),
(31, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 252, 'exit', NULL),
(36, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 255, 'start', NULL),
(37, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 255, 'exit', NULL),
(54, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 264, 'start', NULL),
(55, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 264, 'exit', NULL),
(56, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 265, 'start', NULL),
(57, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 265, 'exit', NULL),
(58, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 266, 'start', NULL),
(59, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 266, 'exit', NULL),
(60, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 267, 'start', NULL),
(61, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 267, 'exit', NULL),
(67, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 270, 'exit', NULL),
(69, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 271, 'exit', NULL),
(71, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 272, 'exit', NULL),
(75, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 274, 'exit', NULL),
(77, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 275, 'exit', NULL),
(79, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 276, 'exit', NULL),
(81, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 277, 'exit', NULL),
(83, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 278, 'exit', NULL),
(88, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 281, 'start', NULL),
(89, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 281, 'exit', NULL),
(122, 'Biomechaics', 'Learn biomechanics', 'Ok, let\'s go!', 298, 'start', NULL),
(123, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 298, 'exit', NULL),
(132, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 303, 'start', NULL),
(133, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 303, 'exit', NULL),
(134, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 304, 'start', NULL),
(135, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 304, 'exit', NULL),
(157, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 110),
(158, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 110),
(159, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 111),
(160, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 111),
(161, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 112),
(162, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 112),
(163, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 113),
(164, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 110),
(165, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 114),
(166, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 114),
(167, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 116),
(168, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 116),
(169, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 117),
(170, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 117),
(171, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 118),
(172, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 118),
(173, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 119),
(174, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 119),
(175, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 120),
(176, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 120),
(177, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 122),
(178, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 122),
(179, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 123),
(180, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 123),
(181, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 124),
(182, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 124),
(183, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 125),
(184, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 125),
(185, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 126),
(186, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 126),
(187, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 128),
(188, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 128),
(189, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 129),
(190, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 129),
(191, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 130),
(192, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 130),
(193, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 131),
(194, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 131),
(195, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 132),
(196, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 132),
(197, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 134),
(198, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 134),
(199, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 135),
(200, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 135),
(201, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 136),
(202, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 136),
(203, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 137),
(204, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 137),
(205, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 138),
(206, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 138),
(207, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 140),
(208, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 140),
(209, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 141),
(210, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 141),
(211, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 142),
(212, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 142),
(213, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 143),
(214, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 143),
(215, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 144),
(216, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 144),
(217, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 146),
(218, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 146),
(219, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 147),
(220, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 147),
(221, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 148),
(222, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 148),
(223, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 149),
(224, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 149),
(225, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 150),
(226, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 150),
(227, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 152),
(228, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 152),
(229, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 153),
(230, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 153),
(231, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 154),
(232, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 154),
(233, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 155),
(234, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 155),
(235, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 156),
(236, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 156),
(237, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 158),
(238, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 158),
(239, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 159),
(240, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 159),
(241, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 160),
(242, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 160),
(243, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 161),
(244, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 161),
(245, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 162),
(246, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 162),
(247, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 305, 'start', NULL),
(248, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 305, 'exit', NULL),
(249, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 306, 'start', NULL),
(250, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 306, 'exit', NULL),
(251, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 164),
(252, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 164),
(253, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 165),
(254, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 165),
(255, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 166),
(256, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 166),
(257, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 167),
(258, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 167),
(259, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 168),
(260, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 168),
(261, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 170),
(262, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 170),
(263, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 171),
(264, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 171),
(265, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 172),
(266, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 172),
(267, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 173),
(268, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 173),
(269, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 174),
(270, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 174),
(283, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 176),
(284, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 176),
(285, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 177),
(286, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 177),
(287, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 178),
(288, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 178),
(289, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 179),
(290, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 179),
(291, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 180),
(292, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 180),
(305, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 182),
(306, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 182),
(307, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 183),
(308, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 183),
(309, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 184),
(310, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 184),
(311, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 185),
(312, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 185),
(313, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 186),
(314, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 186),
(333, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 188),
(334, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 188),
(335, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 189),
(336, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 189),
(337, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 190),
(338, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 190),
(339, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 191),
(340, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 191),
(341, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 192),
(342, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 192),
(343, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 188),
(344, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 188),
(345, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 189),
(346, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 189),
(347, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 190),
(348, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 190),
(349, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 191),
(350, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 191),
(351, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 192),
(352, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 192),
(353, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 194),
(354, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 194),
(355, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 195),
(356, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 195),
(357, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 196),
(358, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 196),
(359, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 197),
(360, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 197),
(361, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 198),
(362, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 198),
(367, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 200),
(368, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 200),
(369, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 201),
(370, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 201),
(371, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 202),
(372, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 202),
(373, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 203),
(374, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 203),
(375, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 204),
(376, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 204),
(381, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 206),
(382, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 206),
(383, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 207),
(384, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 207),
(385, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 208),
(386, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 208),
(387, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 209),
(388, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 209),
(389, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 210),
(390, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 210),
(391, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 212),
(392, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 212),
(393, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 213),
(394, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 213),
(395, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 214),
(396, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 214),
(397, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 215),
(398, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 215),
(399, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 216),
(400, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 216),
(401, 'UNIT 1 ASSESSMENT', 'There is one correct answer for each question', 'Ok, let\'s go!', NULL, 'start', 218),
(402, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 218),
(403, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 219),
(404, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 219),
(405, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 220),
(406, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 220),
(407, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 221),
(408, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 221),
(409, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 222),
(410, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 222),
(411, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 332, 'start', NULL),
(412, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 332, 'exit', NULL),
(417, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 224),
(418, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 224),
(419, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 225),
(420, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 225),
(421, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 226),
(422, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 226),
(423, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 227),
(424, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 227),
(425, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 228),
(426, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 228),
(427, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 335, 'start', NULL),
(428, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 335, 'exit', NULL),
(433, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 338, 'start', NULL),
(434, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 338, 'exit', NULL),
(437, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 227),
(438, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 230),
(439, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 231),
(440, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 231),
(441, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 232),
(442, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 232),
(443, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 233),
(444, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 233),
(445, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 234),
(446, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 234),
(447, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 236),
(448, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 236),
(449, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 237),
(450, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 237),
(451, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 238),
(452, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 238),
(453, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 239),
(454, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 239),
(455, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 240),
(456, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 240),
(457, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 242),
(458, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 242),
(459, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 243),
(460, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 243),
(461, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 244),
(462, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 244),
(463, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 245),
(464, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 245),
(465, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 246),
(466, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 246),
(475, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 248),
(476, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 248),
(477, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 249),
(478, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 249),
(479, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 250),
(480, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 250),
(481, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 251),
(482, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 251),
(483, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 252),
(484, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 252),
(489, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 346, 'start', NULL),
(490, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 346, 'exit', NULL),
(491, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 347, 'start', NULL),
(492, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 347, 'exit', NULL),
(493, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 348, 'start', NULL),
(494, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 348, 'exit', NULL),
(501, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 254),
(502, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 254),
(503, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 255),
(504, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 255),
(505, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 256),
(506, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 256),
(507, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 257),
(508, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 257),
(509, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 258),
(510, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 258),
(511, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 260),
(512, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 260),
(513, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 261),
(514, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 261),
(515, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 262),
(516, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 262),
(517, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 263),
(518, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 263),
(519, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 264),
(520, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 264),
(521, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 267),
(522, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 267),
(523, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 268),
(524, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 268),
(525, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 269),
(526, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 269),
(527, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 270),
(528, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 270),
(529, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 271),
(530, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 271),
(533, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 274),
(534, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 274),
(535, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 275),
(536, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 275),
(537, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 276),
(538, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 276),
(539, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 277),
(540, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 277),
(541, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 278),
(542, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 278),
(545, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 354, 'start', NULL),
(546, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 354, 'exit', NULL),
(547, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 355, 'start', NULL),
(548, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 355, 'exit', NULL),
(549, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 356, 'start', NULL),
(550, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 356, 'exit', NULL),
(551, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 357, 'start', NULL),
(552, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 357, 'exit', NULL),
(553, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 283),
(554, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 283),
(555, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 358, 'start', NULL),
(556, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 358, 'exit', NULL),
(557, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 359, 'start', NULL),
(558, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 359, 'exit', NULL),
(559, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 360, 'start', NULL),
(560, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 360, 'exit', NULL),
(561, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 284),
(562, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 284),
(563, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 285),
(564, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 285),
(565, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 286),
(566, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 286),
(567, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 287),
(568, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 287),
(569, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 288),
(570, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 288),
(571, 'Welcome to the course', 'This course is great', 'Ok, let\'s go!', 361, 'start', NULL),
(572, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 361, 'exit', NULL),
(573, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 290),
(574, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 290),
(575, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 291),
(576, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 291),
(577, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 292),
(578, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 292),
(579, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 293),
(580, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 293),
(581, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 294),
(582, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 294),
(583, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 362, 'start', NULL),
(584, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 362, 'exit', NULL),
(585, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 296),
(586, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 296),
(587, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 297),
(588, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 297),
(589, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 298),
(590, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 298),
(591, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 299),
(592, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 299),
(593, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 300),
(594, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 300),
(595, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 302),
(596, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 302),
(597, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 303),
(598, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 303),
(599, 'A title slidehjjjjjjjjjjjjjjjj', 'An optional subtitle.jnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn', 'Ok, let\'s go!jkkkkkkkkkkkkkkkk', NULL, 'start', 304),
(600, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 304),
(603, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 305),
(604, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 305),
(605, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 306),
(606, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 306),
(607, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 307),
(608, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 307),
(609, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 308),
(610, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 308),
(611, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 309),
(612, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 309),
(613, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 310),
(614, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 310),
(615, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 312),
(616, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 312),
(617, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 313),
(618, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 313),
(619, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 314),
(620, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 314),
(621, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 315),
(622, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 315),
(623, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 316),
(624, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 316),
(625, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 318),
(626, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 318),
(627, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 319),
(628, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 319),
(629, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 320),
(630, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 320),
(631, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 321),
(632, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 321),
(633, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 322),
(634, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 322),
(635, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 324),
(636, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 324),
(637, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 325),
(638, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 325),
(639, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 326),
(640, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 326),
(641, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 327),
(642, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 327),
(643, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 328),
(644, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 328),
(645, 'A title slide of testing ', '', 'Ok, let\'s go!', 364, 'start', NULL),
(646, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 364, 'exit', NULL),
(647, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 330),
(648, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 330),
(649, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 331),
(650, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 331),
(651, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 332),
(652, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 332),
(653, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 333),
(654, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 333),
(655, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 334),
(656, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 334),
(657, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 336),
(658, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 336),
(659, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 337),
(660, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 337),
(661, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 338),
(662, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 338),
(663, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 339),
(664, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 339),
(665, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 340),
(666, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 340),
(667, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 365, 'start', NULL),
(668, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 365, 'exit', NULL),
(669, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 342),
(670, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 342),
(671, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 343),
(672, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 343),
(673, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 344),
(674, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 344),
(675, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 345),
(676, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 345),
(677, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 346),
(678, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 346),
(679, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 348),
(680, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 348),
(681, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 349),
(682, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 349),
(683, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 350),
(684, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 350),
(685, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 351),
(686, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 351),
(687, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 352),
(688, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 352),
(689, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 354),
(690, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 354),
(691, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 355),
(692, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 355),
(693, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 356),
(694, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 356),
(695, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 357),
(696, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 357),
(697, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 358),
(698, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 358),
(699, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 360),
(700, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 360),
(701, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 361),
(702, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 361),
(703, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 362),
(704, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 362),
(705, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 363),
(706, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 363),
(707, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 364),
(708, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 364),
(709, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 366),
(710, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 366),
(711, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 367),
(712, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 367),
(713, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 368),
(714, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 368),
(715, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 369),
(716, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 369),
(717, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 370),
(718, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 370),
(719, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 372),
(720, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 372),
(721, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 373),
(722, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 373),
(723, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 374),
(724, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 374),
(725, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 375),
(726, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 375),
(727, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 376),
(728, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 376),
(729, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 378),
(730, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 378),
(731, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 379),
(732, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 379),
(733, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 380),
(734, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 380);
INSERT INTO `first_last_slide` (`id`, `title`, `sub_title`, `button_text`, `lesson_id`, `default_status`, `unit_id`) VALUES
(735, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 381),
(736, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 381),
(737, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 382),
(738, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 382),
(739, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 384),
(740, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 384),
(741, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 385),
(742, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 385),
(743, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 386),
(744, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 386),
(745, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 387),
(746, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 387),
(747, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 388),
(748, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 388),
(749, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 390),
(750, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 390),
(751, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 391),
(752, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 391),
(753, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 392),
(754, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 392),
(755, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 393),
(756, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 393),
(757, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 394),
(758, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 394),
(759, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 396),
(760, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 396),
(761, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 397),
(762, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 397),
(763, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 398),
(764, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 398),
(765, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 399),
(766, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 399),
(767, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 400),
(768, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 400),
(769, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 402),
(770, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 402),
(771, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 403),
(772, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 403),
(773, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 404),
(774, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 404),
(775, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 405),
(776, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 405),
(777, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 406),
(778, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 406),
(779, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 408),
(780, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 408),
(781, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 409),
(782, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 409),
(783, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 410),
(784, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 410),
(785, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 411),
(786, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 411),
(787, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 412),
(788, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 412),
(789, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 414),
(790, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 414),
(791, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 415),
(792, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 415),
(793, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 416),
(794, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 416),
(795, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 417),
(796, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 417),
(797, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 418),
(798, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 418),
(799, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 420),
(800, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 420),
(801, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 421),
(802, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 421),
(803, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 422),
(804, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 422),
(805, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 423),
(806, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 423),
(807, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 424),
(808, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 424),
(809, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 426),
(810, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 426),
(811, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 427),
(812, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 427),
(813, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 428),
(814, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 428),
(815, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 429),
(816, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 429),
(817, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 430),
(818, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 430),
(819, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 366, 'start', NULL),
(820, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 366, 'exit', NULL),
(821, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 367, 'start', NULL),
(822, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 367, 'exit', NULL),
(823, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 368, 'start', NULL),
(824, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 368, 'exit', NULL),
(825, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 369, 'start', NULL),
(826, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 369, 'exit', NULL),
(827, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 432),
(828, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 432),
(829, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 433),
(830, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 433),
(831, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 434),
(832, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 434),
(833, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 435),
(834, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 435),
(835, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 436),
(836, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 436),
(837, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 438),
(838, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 438),
(839, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 439),
(840, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 439),
(841, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 440),
(842, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 440),
(843, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 441),
(844, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 441),
(845, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 442),
(846, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 442),
(847, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 443),
(848, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 443),
(849, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 445),
(850, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 445),
(851, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 446),
(852, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 446),
(853, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 447),
(854, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 447),
(855, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 448),
(856, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 448),
(857, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 449),
(858, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 449),
(859, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 370, 'start', NULL),
(860, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 370, 'exit', NULL),
(861, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 371, 'start', NULL),
(862, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 371, 'exit', NULL),
(863, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 372, 'start', NULL),
(864, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 372, 'exit', NULL),
(865, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 373, 'start', NULL),
(866, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 373, 'exit', NULL),
(867, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 374, 'start', NULL),
(868, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 374, 'exit', NULL),
(869, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 375, 'start', NULL),
(870, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 375, 'exit', NULL),
(871, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 376, 'start', NULL),
(872, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 376, 'exit', NULL),
(873, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 451),
(874, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 451),
(875, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 452),
(876, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 452),
(877, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 453),
(878, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 453),
(879, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 454),
(880, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 454),
(881, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 455),
(882, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 455),
(883, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 457),
(884, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 457),
(885, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 458),
(886, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 458),
(887, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 459),
(888, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 459),
(889, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 460),
(890, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 460),
(891, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 461),
(892, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 461),
(893, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 462),
(894, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 462),
(895, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 464),
(896, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 464),
(897, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 465),
(898, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 465),
(899, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 466),
(900, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 466),
(901, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 467),
(902, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 467),
(903, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 468),
(904, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 468),
(905, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 377, 'start', NULL),
(906, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 377, 'exit', NULL),
(908, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 376, 'exit', NULL),
(909, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 377, 'start', NULL),
(910, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 377, 'exit', NULL),
(913, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 458),
(914, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 458),
(915, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 459),
(916, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 459),
(917, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 460),
(918, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 460),
(919, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 461),
(920, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 461),
(921, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 462),
(922, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 462),
(923, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 379, 'start', NULL),
(924, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 379, 'exit', NULL),
(925, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 380, 'start', NULL),
(926, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 380, 'exit', NULL),
(927, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 381, 'start', NULL),
(928, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 381, 'exit', NULL),
(931, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 383, 'start', NULL),
(932, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 383, 'exit', NULL),
(933, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 464),
(934, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 464),
(935, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 384, 'start', NULL),
(936, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 384, 'exit', NULL),
(937, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 465),
(938, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 465),
(939, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 466),
(940, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 466),
(941, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 467),
(942, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 467),
(943, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 468),
(944, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 468),
(945, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 469),
(946, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 469),
(947, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 470),
(948, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 470),
(949, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 385, 'start', NULL),
(950, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 385, 'exit', NULL),
(951, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 471),
(952, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 471),
(953, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 472),
(954, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 472),
(955, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 473),
(956, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 473),
(957, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 474),
(958, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 474),
(961, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 475),
(962, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 475),
(963, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 476),
(964, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 476),
(965, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 477),
(966, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 477),
(967, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 478),
(968, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 478),
(969, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 479),
(970, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 479),
(971, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 480),
(972, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 480),
(973, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 481),
(974, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 481),
(975, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 482),
(976, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 482),
(977, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 483),
(978, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 483),
(979, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 484),
(980, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 484),
(981, 'DSDM fixes cost, quality and time ', 'An optional DSDM fixes cost, quality and time at the outset and uses the MoSCow priorisubtitle.', 'Ok, let\'s go!', 387, 'start', NULL),
(982, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 387, 'exit', NULL),
(983, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 485),
(984, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 485),
(985, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 486),
(986, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 486),
(987, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 388, 'start', NULL),
(988, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 388, 'exit', NULL),
(989, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 487),
(990, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 487),
(991, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 488),
(992, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 488),
(993, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 489),
(994, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 489),
(995, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 490),
(996, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 490),
(997, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 389, 'start', NULL),
(998, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 389, 'exit', NULL),
(999, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 491),
(1000, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 491),
(1005, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 392, 'start', NULL),
(1006, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 392, 'exit', NULL),
(1007, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 492),
(1008, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 492),
(1009, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 393, 'start', NULL),
(1010, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 393, 'exit', NULL),
(1011, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 493),
(1012, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 493),
(1013, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 494),
(1014, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 494),
(1015, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 495),
(1016, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 495),
(1017, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 394, 'start', NULL),
(1018, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 394, 'exit', NULL),
(1020, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 395, 'exit', NULL),
(1021, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 396, 'start', NULL),
(1022, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 396, 'exit', NULL),
(1023, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 496),
(1024, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 496),
(1025, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 397, 'start', NULL),
(1026, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 397, 'exit', NULL),
(1027, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 497),
(1028, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 497),
(1031, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 498),
(1032, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 498),
(1033, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 499),
(1034, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 499),
(1035, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 500),
(1036, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 500),
(1037, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 501),
(1038, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 501),
(1039, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 502),
(1040, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 502),
(1041, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 503),
(1042, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 503),
(1043, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 504),
(1044, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 504),
(1045, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 505),
(1046, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 505),
(1047, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 506),
(1048, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 506),
(1049, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 507),
(1050, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 507),
(1051, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 508),
(1052, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 508),
(1053, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 509),
(1054, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 509),
(1055, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 510),
(1056, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 510),
(1059, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 511),
(1060, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 511),
(1061, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 400, 'start', NULL),
(1062, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 400, 'exit', NULL),
(1063, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 512),
(1064, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 512),
(1065, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 513),
(1066, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 513),
(1067, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 401, 'start', NULL),
(1068, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 401, 'exit', NULL),
(1069, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 514),
(1070, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 514),
(1071, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 402, 'start', NULL),
(1072, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 402, 'exit', NULL),
(1073, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 515),
(1074, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 515),
(1075, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 516),
(1076, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 516),
(1077, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 403, 'start', NULL),
(1078, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 403, 'exit', NULL),
(1079, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 517),
(1080, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 517),
(1083, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 518),
(1084, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 518),
(1085, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 405, 'start', NULL),
(1086, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 405, 'exit', NULL),
(1087, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 406, 'start', NULL),
(1088, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 406, 'exit', NULL),
(1089, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 407, 'start', NULL),
(1090, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 407, 'exit', NULL),
(1091, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 519),
(1092, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 519),
(1093, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 408, 'start', NULL),
(1094, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 408, 'exit', NULL),
(1095, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 520),
(1096, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 520),
(1097, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 409, 'start', NULL),
(1098, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 409, 'exit', NULL),
(1099, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 521),
(1100, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 521),
(1101, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 410, 'start', NULL),
(1102, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 410, 'exit', NULL),
(1103, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 522),
(1104, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 522),
(1105, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 523),
(1106, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 523),
(1109, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 412, 'start', NULL),
(1110, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 412, 'exit', NULL),
(1111, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 524),
(1112, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 524),
(1113, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 413, 'start', NULL),
(1114, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 413, 'exit', NULL),
(1115, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 525),
(1116, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 525),
(1117, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 414, 'start', NULL),
(1118, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 414, 'exit', NULL),
(1119, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 526),
(1120, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 526),
(1121, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 415, 'start', NULL),
(1122, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 415, 'exit', NULL),
(1125, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 527),
(1126, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 527),
(1127, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 417, 'start', NULL),
(1128, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 417, 'exit', NULL),
(1129, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 528),
(1130, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 528),
(1131, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 418, 'start', NULL),
(1132, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 418, 'exit', NULL),
(1133, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 529),
(1134, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 529),
(1135, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 419, 'start', NULL),
(1136, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 419, 'exit', NULL),
(1137, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 530),
(1138, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 530),
(1139, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 420, 'start', NULL),
(1140, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 420, 'exit', NULL),
(1141, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 531),
(1142, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 531),
(1143, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 421, 'start', NULL),
(1144, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 421, 'exit', NULL),
(1145, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 532),
(1146, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 532),
(1147, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 422, 'start', NULL),
(1148, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 422, 'exit', NULL),
(1149, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 533),
(1150, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 533),
(1151, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 423, 'start', NULL),
(1152, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 423, 'exit', NULL),
(1153, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 534),
(1154, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 534),
(1155, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 424, 'start', NULL),
(1156, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 424, 'exit', NULL),
(1157, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 535),
(1158, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 535),
(1159, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 425, 'start', NULL),
(1160, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 425, 'exit', NULL),
(1161, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 536),
(1162, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 536),
(1163, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 426, 'start', NULL),
(1164, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 426, 'exit', NULL),
(1165, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 537),
(1166, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 537),
(1167, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 427, 'start', NULL),
(1168, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 427, 'exit', NULL),
(1169, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 538),
(1170, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 538),
(1171, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 428, 'start', NULL),
(1172, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 428, 'exit', NULL),
(1173, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 539),
(1174, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 539),
(1175, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 429, 'start', NULL),
(1176, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 429, 'exit', NULL),
(1177, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 540),
(1178, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 540),
(1179, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 430, 'start', NULL),
(1180, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 430, 'exit', NULL),
(1181, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 541),
(1182, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 541),
(1183, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 431, 'start', NULL),
(1184, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 431, 'exit', NULL),
(1185, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 542),
(1186, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 542),
(1187, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 432, 'start', NULL),
(1188, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 432, 'exit', NULL),
(1189, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 543),
(1190, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 543),
(1191, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 433, 'start', NULL),
(1192, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 433, 'exit', NULL),
(1193, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 544),
(1194, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 544),
(1195, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 434, 'start', NULL),
(1196, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 434, 'exit', NULL),
(1197, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 545),
(1198, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 545),
(1199, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 435, 'start', NULL),
(1200, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 435, 'exit', NULL),
(1201, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 546),
(1202, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 546),
(1203, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 547),
(1204, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 547),
(1205, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 436, 'start', NULL),
(1206, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 436, 'exit', NULL),
(1207, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', NULL, 'start', 548),
(1208, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', NULL, 'exit', 548),
(1209, 'A title slide', 'An optional subtitle.', 'Ok, let\'s go!', 437, 'start', NULL),
(1210, 'That\'s it!', 'Nice work. You\'ve completed this lesson.', 'Exit Lesson', 437, 'exit', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `freetext_feedback_by_assessor`
--

CREATE TABLE `freetext_feedback_by_assessor` (
  `ftfeed_id` int(11) NOT NULL,
  `assessor_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `question_type` varchar(125) NOT NULL,
  `assessor_feedback` text NOT NULL,
  `feedback_datetime` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `freetext_feedback_by_assessor`
--

INSERT INTO `freetext_feedback_by_assessor` (`ftfeed_id`, `assessor_id`, `unit_id`, `user_id`, `question_id`, `question_type`, `assessor_feedback`, `feedback_datetime`) VALUES
(1, 36, 225, 28, 1, 'ass_expandable', 'refer jdbfdbjkdc sd3r3d3d4fdvv vcvc', ''),
(2, 36, 225, 28, 1, 'ass_expandable', 'hdf d refer jdbfdbjkdc sd3r3d3d4fdvv vcvc', ''),
(3, 36, 227, 28, 28, 'free_text', 'Dfggg', ''),
(4, 36, 224, 28, 54, 'free_text', 'Vhj', ''),
(5, 36, 224, 28, 54, 'free_text', 'Xghj', ''),
(6, 36, 224, 28, 54, 'free_text', 'Hello', '2020-07-29 13:26:19'),
(7, 36, 224, 28, 54, 'free_text', 'Hiii', '2020-07-29 13:34:00'),
(8, 36, 227, 1, 28, 'free_text', 'Hii', '2020-07-29 23:09:50'),
(9, 36, 227, 1, 28, 'free_text', 'Heelo', '2020-07-29 23:09:57'),
(10, 36, 226, 1, 27, 'free_text', 'Hii', '2020-07-29 23:21:29'),
(11, 36, 225, 1, 17, 'free_text', 'Hiii', '2020-07-30 09:26:21'),
(12, 36, 226, 1, 27, 'free_text', 'Hello', '2020-07-30 10:03:21'),
(13, 36, 226, 1, 27, 'free_text', 'Need improvement', '2020-07-31 13:29:27'),
(14, 36, 225, 1, 26, 'free_text', 'Hiii', '2020-07-31 15:00:58'),
(15, 36, 225, 1, 26, 'free_text', '', '2020-07-31 15:01:01'),
(16, 36, 225, 1, 17, 'free_text', 'Give answers', '2020-07-31 15:10:34'),
(17, 36, 495, 1, 81, 'free_text', 'Hiii', '2020-07-31 20:15:01'),
(18, 16, 495, 103, 81, 'free_text', 'Hiiii', '2020-08-01 13:22:04'),
(19, 16, 495, 68, 81, 'free_text', 'Hello', '2020-08-01 13:26:09'),
(20, 16, 510, 68, 84, 'free_text', 'Hello', '2020-08-02 13:13:53'),
(21, 36, 521, 28, 94, 'free_text', 'Need improvement', '2020-08-05 15:25:33'),
(22, 36, 521, 28, 94, 'free_text', '', '2020-08-05 15:26:18'),
(23, 120, 536, 122, 104, 'free_text', 'Need improvement', '2020-08-11 19:25:37'),
(24, 120, 536, 122, 104, 'free_text', 'Good', '2020-08-11 19:47:18'),
(25, 120, 536, 122, 104, 'free_text', 'Need improvement', '2020-08-11 20:28:06'),
(26, 114, 528, 115, 99, 'free_text', 'Good', '2020-08-12 13:01:11'),
(27, 120, 542, 122, 105, 'free_text', 'Good ', '2020-08-12 14:17:13');

-- --------------------------------------------------------

--
-- Table structure for table `free_text`
--

CREATE TABLE `free_text` (
  `id` int(30) NOT NULL,
  `title` varchar(50) NOT NULL,
  `done_text` varchar(50) NOT NULL,
  `prompt` varchar(50) NOT NULL,
  `lesson_id` int(50) NOT NULL,
  `unit_id` int(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `free_text`
--

INSERT INTO `free_text` (`id`, `title`, `done_text`, `prompt`, `lesson_id`, `unit_id`) VALUES
(1, 'text1', 'Ok i am done..!', 'Type your response', 283, 0),
(2, 'What are some?', 'Ok i am done..!', 'Type your response', 0, 134),
(4, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 158),
(5, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 164),
(6, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 164),
(8, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 319, 0),
(9, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 313, 0),
(10, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 319, 0),
(11, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 182),
(12, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 330, 0),
(13, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 176),
(14, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 177),
(15, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 174),
(16, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 174),
(17, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 225),
(18, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 207),
(19, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 208),
(20, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 209),
(21, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 210),
(22, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 212),
(23, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 16, 0),
(25, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 336, 0),
(26, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 225),
(27, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 226),
(28, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 227),
(29, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 228),
(30, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 248),
(31, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 232),
(32, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 233),
(33, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 249),
(34, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 338, 0),
(35, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 338, 0),
(37, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 351, 0),
(38, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 341, 0),
(39, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 341, 0),
(40, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 341, 0),
(41, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 341, 0),
(42, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 341, 0),
(43, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 341, 0),
(44, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 230),
(45, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 227),
(46, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 349, 0),
(47, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 338, 0),
(48, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 338, 0),
(49, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 271),
(50, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 278),
(51, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 338, 0),
(52, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 355, 0),
(53, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 283),
(54, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 224),
(55, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 348, 0),
(56, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 1),
(57, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 362, 0),
(58, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 362, 0),
(59, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 362, 0),
(60, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 362, 0),
(61, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 362, 0),
(62, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 362, 0),
(63, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 362, 0),
(64, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 363, 0),
(65, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 304),
(66, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 248),
(67, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 364, 0),
(68, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 354, 0),
(69, 'What are some?', 'Ok, I\'m done', 'helloonmn ', 0, 478),
(70, 'What are some?', 'Ok, I\'m done', 'WOO Test test', 0, 483),
(71, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 488),
(72, 'What are some?', 'Ok, I\'m done', 'This is prompt', 0, 478),
(73, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 490),
(74, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 347, 0),
(75, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 387, 0),
(76, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 388, 0),
(77, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 491),
(78, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 491),
(79, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 396, 0),
(80, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 365, 0),
(81, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 495),
(82, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 497),
(83, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 399, 0),
(84, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 510),
(85, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 512),
(86, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 401, 0),
(87, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 402, 0),
(88, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 514),
(89, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 403, 0),
(90, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 516),
(91, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 517),
(92, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 518),
(93, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 520),
(94, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 521),
(95, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 519),
(96, 'LOWER BACK PAIN', 'Ok, I\'m done', 'Type your response', 0, 519),
(97, 'How would you improve this lesson?', 'Ok, I\'m done', 'Type your response', 414, 0),
(98, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 527),
(99, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 528),
(100, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 529),
(101, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 530),
(102, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 531),
(103, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 531),
(104, 'What  are benefits of skipping?', 'Ok, I\'m done', 'Type your response', 0, 536),
(105, 'What are some?', 'Ok, I\'m done', 'Type your response', 0, 542);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `group_id` int(10) NOT NULL,
  `group_name` varchar(100) NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`group_id`, `group_name`, `status`) VALUES
(1, 'All Users', 'false'),
(2, 'group 1', 'true'),
(3, 'group 2', 'true'),
(6, 'group 3', 'true'),
(7, 'group 5', 'true');

-- --------------------------------------------------------

--
-- Table structure for table `group_courses`
--

CREATE TABLE `group_courses` (
  `id` int(10) NOT NULL,
  `group_id` int(10) NOT NULL,
  `course_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `group_courses`
--

INSERT INTO `group_courses` (`id`, `group_id`, `course_id`) VALUES
(165, 3, 1),
(166, 3, 4),
(167, 1, 1),
(168, 1, 2),
(169, 1, 4),
(170, 1, 5),
(171, 1, 7),
(172, 1, 9),
(173, 1, 10),
(174, 1, 11),
(175, 1, 12),
(176, 1, 13),
(177, 1, 14),
(178, 1, 15),
(179, 1, 16),
(180, 1, 17),
(181, 1, 18),
(182, 1, 25),
(183, 1, 26),
(184, 1, 28),
(185, 1, 29),
(216, 2, 1),
(217, 2, 4),
(218, 2, 11),
(219, 2, 12),
(220, 2, 13),
(221, 2, 14),
(236, 7, 32),
(237, 6, 1),
(238, 6, 2),
(239, 6, 4),
(240, 6, 5),
(241, 6, 7),
(242, 6, 9),
(243, 6, 10);

-- --------------------------------------------------------

--
-- Table structure for table `group_users`
--

CREATE TABLE `group_users` (
  `id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `group_id` int(10) NOT NULL,
  `user_role` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `group_users`
--

INSERT INTO `group_users` (`id`, `user_id`, `group_id`, `user_role`) VALUES
(79, 1, 3, 'learner'),
(80, 2, 3, 'learner'),
(81, 3, 3, 'learner'),
(82, 4, 3, 'learner'),
(83, 22, 3, 'mentor'),
(84, 21, 3, 'iqa'),
(85, 1, 1, 'learner'),
(86, 2, 1, 'learner'),
(87, 3, 1, 'learner'),
(88, 21, 1, 'mentor'),
(89, 21, 1, 'iqa'),
(114, 1, 2, 'learner'),
(115, 22, 2, 'mentor'),
(116, 20, 2, 'iqa'),
(129, 33, 7, 'learner'),
(130, 33, 7, 'mentor'),
(131, 33, 7, 'iqa'),
(132, 4, 6, 'learner'),
(133, 5, 6, 'learner'),
(134, 22, 6, 'mentor'),
(135, 21, 6, 'iqa'),
(136, 22, 6, 'iqa');

-- --------------------------------------------------------

--
-- Table structure for table `image_coordinates`
--

CREATE TABLE `image_coordinates` (
  `id` int(30) NOT NULL,
  `x_coordinate` varchar(100) NOT NULL,
  `y_coordinate` varchar(100) NOT NULL,
  `image_id` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `image_coordinates`
--

INSERT INTO `image_coordinates` (`id`, `x_coordinate`, `y_coordinate`, `image_id`) VALUES
(1, 'dgdfg', 'gfdgfd', 3);

-- --------------------------------------------------------

--
-- Table structure for table `image_waypoint`
--

CREATE TABLE `image_waypoint` (
  `id` int(30) NOT NULL,
  `image` varchar(30) NOT NULL,
  `edit_region` varchar(150) NOT NULL,
  `caption` varchar(150) NOT NULL,
  `narration` varchar(150) NOT NULL,
  `appearance` varchar(100) NOT NULL,
  `light_box` varchar(100) NOT NULL,
  `text_color` varchar(30) NOT NULL,
  `done_text` varchar(50) NOT NULL,
  `upload_file` varchar(50) NOT NULL,
  `lesson_id` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `image_waypoint`
--

INSERT INTO `image_waypoint` (`id`, `image`, `edit_region`, `caption`, `narration`, `appearance`, `light_box`, `text_color`, `done_text`, `upload_file`, `lesson_id`) VALUES
(1, 'demo.jpg', 'hello', 'Good afternoon', 'how are you', 'I am fine', 'hello', 'red', 'hii', 'demo1.png', 0),
(2, 'demo2.jpg', '', 'hello', '', '', '', '', 'i am done', 'demo7.jpg', 0),
(3, 'demo5.jpg', 'fgdf', 'dgdfg', 'dgdfg', 'dgdfg', 'gfdg', 'gfdgd', 'dgdfg', 'dgfdg', 283);

-- --------------------------------------------------------

--
-- Table structure for table `insert_video`
--

CREATE TABLE `insert_video` (
  `id` int(30) NOT NULL,
  `video` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `insert_video`
--

INSERT INTO `insert_video` (`id`, `video`) VALUES
(1, 'www'),
(2, 'www'),
(3, 'www'),
(4, 'www'),
(5, 'www'),
(6, 'www'),
(7, 'www'),
(8, 'www'),
(9, 'www'),
(10, ''),
(11, ''),
(12, 'demo_video/'),
(13, 'demo_video/'),
(14, 'demo_video/'),
(15, 'demo_video/'),
(16, 'demo_video/'),
(17, 'demo_video/'),
(18, 'demo_video/'),
(19, 'demo_video/'),
(20, 'demo_video/'),
(21, 'demo_video/'),
(22, 'demo_video/'),
(23, 'demo_video/'),
(24, 'demo_video/'),
(25, 'demo_video/'),
(26, 'demo_video/'),
(27, 'demo_video/'),
(28, 'demo_video/'),
(29, 'demo_video/');

-- --------------------------------------------------------

--
-- Table structure for table `iqareport_iqa_list`
--

CREATE TABLE `iqareport_iqa_list` (
  `list_id` int(11) NOT NULL,
  `iqa_id` int(11) NOT NULL,
  `assessor_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `action` varchar(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iqareport_iqa_list`
--

INSERT INTO `iqareport_iqa_list` (`list_id`, `iqa_id`, `assessor_id`, `course_id`, `user_id`, `action`) VALUES
(1, 22, 120, 128, 122, 'true'),
(2, 22, 16, 123, 119, 'true'),
(3, 22, 16, 127, 119, 'false'),
(4, 22, 114, 123, 115, 'false'),
(5, 22, 120, 122, 122, 'true'),
(6, 22, 16, 126, 119, 'false'),
(7, 22, 16, 115, 119, 'false'),
(8, 22, 120, 123, 119, 'true'),
(9, 22, 114, 126, 115, 'false');

-- --------------------------------------------------------

--
-- Table structure for table `iqa_feedback`
--

CREATE TABLE `iqa_feedback` (
  `if_id` int(11) NOT NULL,
  `iqa_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `assessor_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `development_points` text NOT NULL,
  `referral_actions` text NOT NULL,
  `fdatetime` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iqa_feedback`
--

INSERT INTO `iqa_feedback` (`if_id`, `iqa_id`, `user_id`, `course_id`, `assessor_id`, `comment`, `development_points`, `referral_actions`, `fdatetime`) VALUES
(1, 22, 28, 77, 36, 'comment ttttttt tttttttt  tttt', 'development_points dsfds dsgds g', 'referral_actions sd3r3d3d4fdvv vcvc', '2020-07-07 11:12:13'),
(2, 22, 28, 77, 36, '1comment ttttttt tttttttt  tttt', '1development_points dsfds dsgds g', '1referral_actions sd3r3d3d4fdvv vcvc', ''),
(3, 22, 28, 77, 36, 'your result is correct', 'your result is correct', 'your result is correct', ''),
(4, 22, 28, 77, 36, 'good', 'good', 'good', '2020-08-02 10:46:53'),
(5, 22, 28, 77, 36, 'good', 'good', 'good1', '2020-08-02 10:51:07'),
(6, 22, 28, 77, 36, 'good', 'good', 'good', '2020-08-02 10:51:22'),
(7, 22, 28, 77, 36, 'good', 'good', 'good', '2020-08-11 09:48:55'),
(8, 22, 115, 126, 114, 'Good', '', '', '2020-08-11 11:08:39'),
(9, 22, 115, 115, 114, 'good', '', '', '2020-08-11 11:14:12'),
(10, 22, 68, 124, 16, 'asdsad', 'asd', 'asd', '2020-08-11 11:32:43'),
(11, 22, 103, 124, 16, 'asd', 'asd', 'asd', '2020-08-11 11:33:18'),
(12, 22, 122, 128, 120, 'good', '', '', '2020-08-12 14:45:20'),
(13, 22, 119, 126, 16, 'good', '', '', '2020-08-12 15:47:31');

-- --------------------------------------------------------

--
-- Table structure for table `iqa_submitted_report`
--

CREATE TABLE `iqa_submitted_report` (
  `sr_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `view` varchar(11) NOT NULL,
  `result` varchar(11) NOT NULL,
  `lr_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='unit_id, view, result';

--
-- Dumping data for table `iqa_submitted_report`
--

INSERT INTO `iqa_submitted_report` (`sr_id`, `unit_id`, `view`, `result`, `lr_id`, `user_id`) VALUES
(1, 536, 'true', 'true', 1, 122),
(2, 537, 'true', 'true', 1, 122),
(3, 538, 'true', 'true', 1, 122),
(4, 539, 'true', 'true', 1, 122),
(5, 541, 'true', 'true', 1, 122),
(6, 542, 'true', 'true', 1, 122),
(7, 528, 'true', 'true', 2, 119),
(8, 530, 'true', 'true', 2, 119),
(9, 511, 'false', 'false', 3, 119),
(10, 532, 'true', 'true', 3, 119),
(11, 533, 'true', 'true', 3, 119),
(12, 534, 'true', 'true', 3, 119),
(13, 535, 'false', 'false', 3, 119),
(14, 540, 'false', 'false', 3, 119),
(15, 543, 'false', 'false', 3, 119);

-- --------------------------------------------------------

--
-- Table structure for table `learner_report_by_iqa`
--

CREATE TABLE `learner_report_by_iqa` (
  `lr_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `valid` varchar(11) NOT NULL,
  `authentic` varchar(11) NOT NULL,
  `reliable` varchar(11) NOT NULL,
  `sufficient` varchar(11) NOT NULL,
  `all_of_this` varchar(11) NOT NULL,
  `iqa_id` int(11) NOT NULL,
  `result_generate_date` varchar(35) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `learner_report_by_iqa`
--

INSERT INTO `learner_report_by_iqa` (`lr_id`, `user_id`, `course_id`, `valid`, `authentic`, `reliable`, `sufficient`, `all_of_this`, `iqa_id`, `result_generate_date`) VALUES
(1, 122, 128, 'true', 'true', 'true', 'true', 'true', 22, '2020-08-12'),
(2, 119, 126, 'true', 'true', 'true', 'true', 'true', 22, '2020-08-12'),
(3, 119, 123, 'true', 'true', 'true', 'false', 'false', 22, '2020-08-12');

-- --------------------------------------------------------

--
-- Table structure for table `learner_type_master`
--

CREATE TABLE `learner_type_master` (
  `id` int(10) NOT NULL,
  `learner_type` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `learner_type_master`
--

INSERT INTO `learner_type_master` (`id`, `learner_type`) VALUES
(1, 'Self-study'),
(2, 'Flexi'),
(3, 'Full time');

-- --------------------------------------------------------

--
-- Table structure for table `lesson`
--

CREATE TABLE `lesson` (
  `id` int(30) NOT NULL,
  `lesson_name` varchar(200) NOT NULL,
  `course_id` int(30) DEFAULT NULL,
  `image` varchar(50) NOT NULL,
  `description` varchar(150) NOT NULL,
  `section_id` int(10) NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'Unpublished',
  `sequence_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lesson`
--

INSERT INTO `lesson` (`id`, `lesson_name`, `course_id`, `image`, `description`, `section_id`, `status`, `sequence_id`) VALUES
(1, 'Bones of the skeleton', 1, '', 'lesson', 1, 'Published', 0),
(2, ' Function of the skeleton', 1, '', '', 1, 'Published', 0),
(3, 'General features of a long  bone', 1, '', '', 1, 'Published', 0),
(4, 'Bone classifications', 1, '', '', 1, 'Published', 0),
(5, ' Bone formation across a  lifespan', 1, '', '', 1, 'Published', 0),
(6, ' Osteoporosis', 1, '', '', 1, 'Published', 0),
(7, 'Types of joint', 1, '', '', 1, 'Published', 0),
(8, 'Types of synovial joint', 1, '', '', 1, 'Published', 0),
(9, 'Features of a synovial joint', 1, '', '', 1, 'Published', 0),
(10, 'Cartilage and ligaments', 1, '', '', 1, 'Published', 0),
(11, 'the spine', 1, '', '', 1, 'Published', 0),
(12, 'Spine abnormalities', 1, '', '', 1, 'Published', 0),
(13, ' Positional terms', 1, '', '', 1, 'Published', 0),
(14, '  The effects of exercise on  the skeletal system', 1, '', '', 1, 'Published', 0),
(15, ' Joint actions', 1, '', '', 1, 'Published', 0),
(24, 'The basic structure and \r\nfunction of the nervous \r\nsystem', 1, '', '', 3, 'Unpublished', 3),
(25, 'Motor unit recruitment and \r\n‘all-or-nothing’ law', 1, 'section_images/15911047041194967916.png', '', 3, 'Unpublished', 1),
(26, 'The effects of exercise on \r\nthe neuromuscular system', 1, '', '', 3, 'Unpublished', 2),
(27, ' The function of the nervous system', 1, '', '', 4, 'Unpublished', 0),
(28, 'The structure of the nervous system', 1, '', '', 4, 'Unpublished', 0),
(29, 'Mechanism of breathing', 1, '', '', 4, 'Unpublished', 0),
(30, ' The passage of oxygen and \r\ncarbon dioxide through \r\nthe respiratory tract and \r\ndiffusion', 1, '', '', 4, 'Unpublished', 0),
(31, 'Respiratory volumes', 1, '', '', 4, 'Unpublished', 0),
(32, 'The effects of exercise on \r\nthe respiratory system', 1, '', '', 4, 'Unpublished', 0),
(33, 'The basic function of the \r\ncardiovascular system', 1, '', '', 5, 'Unpublished', 0),
(34, ' The heart', 1, '', '', 5, 'Unpublished', 0),
(35, 'The blood vessels', 1, '', '', 5, 'Unpublished', 0),
(36, ' The blood', 1, '', '', 5, 'Unpublished', 0),
(37, 'Pulmonary and systemic \r\ncirculation', 1, '', '', 5, 'Unpublished', 0),
(38, 'Mechanisms of venous \r\nreturn', 1, '', '', 5, 'Unpublished', 0),
(41, 'What and where does \r\nenergy come from?', 1, '', '', 6, 'Unpublished', 0),
(42, 'Adenosine Triphosphate \r\n(energy currency)', 1, '', '', 6, 'Unpublished', 0),
(43, 'The energy systems', 1, '', '', 6, 'Unpublished', 0),
(44, 'Effects of exercise on the \r\nenergy systems', 1, '', '', 6, 'Unpublished', 0),
(45, 'Lesson 2.1', 1, '', 'Forming effective working relationships with clients', 7, 'Published', 0),
(46, 'Presenting oneself and the \r\norganisation positively to \r\nclients', 1, '', '', 7, 'Published', 0),
(47, ' The importance of valuing \r\nequality and diversity \r\nwhen working with clients', 1, '', '', 7, 'Published', 0),
(48, 'Behaviour change', 1, '', '', 8, 'Unpublished', 0),
(49, 'Barriers to exercise and \r\nphysical activity', 1, '', '', 8, 'Unpublished', 0),
(50, 'Increasing motivation and \r\nclient adherence', 1, '', '', 8, 'Unpublished', 0),
(51, ' Customer service and \r\nclient care', 1, '', '', 9, 'Unpublished', 0),
(52, 'Principles of good customer service', 1, '', '', 9, 'Unpublished', 0),
(53, 'Handling customer \r\ncomplaints', 1, '', '', 9, 'Unpublished', 0),
(54, 'Health and Safety at Work \r\nAct 1974', 1, '', '', 10, 'Unpublished', 0),
(55, 'Types of emergencies that \r\nmay occur in a fitness \r\nenvironment', 1, '', '', 10, 'Unpublished', 0),
(56, 'The role of staff and external services play during an \r\nemergency', 1, '', '', 10, 'Unpublished', 0),
(57, 'Duty of care and professional role/boundaries in \r\nthe event of an emergency \r\nconcerning special population groups', 1, '', '', 10, 'Unpublished', 0),
(58, 'Health and safety documents that are relevant to \r\nthe fitness instructor', 1, '', '', 10, 'Unpublished', 0),
(59, 'Security procedures within \r\na fitness environment', 1, '', '', 10, 'Unpublished', 0),
(60, 'Risk management within a \r\nfitness environment', 1, '', '', 11, 'Unpublished', 0),
(61, 'How to perform a risk \r\nassessment', 1, '', '', 11, 'Unpublished', 0),
(62, 'Legislation concerning \r\nsafeguarding the welfare \r\nof children and vulnerable \r\nadults', 1, '', '', 12, 'Unpublished', 0),
(63, 'Disclosure and Barring \r\nService (DBS)', 1, '', '', 12, 'Unpublished', 0),
(64, 'Types of child abuse \r\nthat an instructor may \r\nencounter', 1, '', '', 12, 'Unpublished', 0),
(65, 'What organisations need \r\nto do to protect children \r\nand vulnerable adults from \r\nharm', 1, '', '', 12, 'Unpublished', 0),
(66, 'Protecting fitness professionals from accusations \r\nof abuse', 1, '', '', 12, 'Unpublished', 0),
(67, 'The health benefits of \r\nphysical activity', 1, '', '', 13, 'Unpublished', 0),
(68, 'Components of fitness', 1, '', '', 13, 'Unpublished', 0),
(69, 'Components of health-related fitness', 1, '', '', 13, 'Unpublished', 0),
(70, 'Components of skill-related \r\nfitness', 1, '', '', 13, 'Unpublished', 0),
(71, 'Factors affecting and individual’s health and fitness', 1, '', '', 13, 'Unpublished', 0),
(72, 'The effects of aerobic \r\ntraining on the body', 1, '', '', 13, 'Unpublished', 0),
(73, 'Blood pooling after \r\nexercise', 1, '', '', 13, 'Unpublished', 0),
(74, ' The effects of resistance \r\ntraining on the body', 1, '', '', 13, 'Unpublished', 0),
(75, 'The effects of delayed \r\nonset of muscle soreness \r\n(DOMS)', 1, '', '', 13, 'Unpublished', 0),
(76, ' The effects of flexibility \r\non the body', 1, '', '', 13, 'Unpublished', 0),
(77, 'Training principles', 1, '', '', 14, 'Unpublished', 0),
(78, 'Exercise guidelines for \r\nworking with pre and post \r\nnatal women', 1, '', '', 14, 'Unpublished', 0),
(79, 'Exercise guidelines for \r\nworking with older adults \r\n(50+)', 1, '', '', 14, 'Unpublished', 0),
(80, 'Exercise guidelines for \r\nworking with young people \r\n(14-16 years)', 1, '', '', 14, 'Unpublished', 0),
(81, 'Exercise guidelines for \r\nworking with disabled \r\npeople', 1, '', '', 14, 'Unpublished', 0),
(82, 'How to monitor exercise \r\nintensity', 1, '', '', 14, 'Unpublished', 0),
(83, 'How eating habits have \r\nchanged', 1, '', '', 15, 'Unpublished', 0),
(84, 'Current healthy eating \r\nguidelines', 1, '', '', 15, 'Unpublished', 0),
(85, 'The dietary role of nutrients: carbohydrates', 1, '', '', 15, 'Unpublished', 0),
(86, 'The dietary role of nutrients: fats', 1, '', '', 15, 'Unpublished', 0),
(87, 'The dietary role of nutrients: protein', 1, '', '', 15, 'Unpublished', 0),
(88, 'Hydration 198', 1, '', '', 15, 'Unpublished', 0),
(89, 'The role of water in the \r\nbody', 1, '', '', 15, 'Unpublished', 0),
(90, 'Energy balance equation', 1, '', '', 15, 'Unpublished', 0),
(91, 'Understand the role of a \r\nfitness instructor', 1, '', '', 16, 'Unpublished', 0),
(92, 'Understand how to collect information to plan gym-based exercise', 1, '', '', 16, 'Unpublished', 0),
(93, 'Factors affecting exercise \r\nprescription', 1, '', '', 16, 'Unpublished', 0),
(94, 'The basic function of the \r\n2.1 How to design a gymbased workout', 1, '', '', 17, 'Published', 0),
(95, 'How to plan a warm-up \r\ncomponent', 1, '', '', 17, 'Published', 0),
(96, 'How to plan an aerobic \r\ncomponent', 1, '', '', 17, 'Published', 0),
(97, 'How to plan a resistance \r\ncomponent', 1, '', '', 17, 'Published', 0),
(98, 'How to plan a cooldown \r\nand stretch component', 1, '', '', 17, 'Published', 0),
(99, 'Exercise library', 1, '', '', 17, 'Unpublished', 0),
(122, ' The basic function of the \r\ncardiovascular system', 2, '', '', 18, 'Published', 0),
(123, 'The heart', 2, '', '', 18, 'Published', 0),
(124, 'The blood vessels', 2, '', '', 18, 'Published', 0),
(125, 'The blood', 2, '', '', 18, 'Published', 0),
(126, 'The effects of disease on \r\nthe structure and function \r\nof blood vessels', 2, '', '', 18, 'Unpublished', 0),
(127, ' Pulmonary and systemic \r\ncirculation', 2, '', '', 18, 'Unpublished', 0),
(128, 'Blood flow and oxygen \r\nconsumption during \r\nexercise', 2, '', '', 18, 'Unpublished', 0),
(129, 'Mechanisms of venous \r\nreturn', 2, '', '', 18, 'Unpublished', 0),
(130, '  Blood pressure', 2, '', '', 18, 'Unpublished', 0),
(131, 'Valsalva manoeuvre', 2, '', '', 18, 'Unpublished', 0),
(132, 'The effects of exercise on \r\nthe cardiovascular system', 2, '', '', 18, 'Unpublished', 0),
(133, 'Lesson 2.1', 2, '', 'Spine', 19, 'Unpublished', 0),
(134, 'Stabilisation (control) \r\nmechanisms of the spine: \r\nactive (muscular)', 2, '', '', 19, 'Unpublished', 0),
(135, 'Stabilisation (control) \r\nmechanisms of the spine: \r\npassive (ligaments)', 2, '', '', 19, 'Unpublished', 0),
(136, 'Understanding good \r\nposture', 2, '', '', 19, 'Unpublished', 0),
(137, 'Abnormal postures', 2, '', '', 19, 'Unpublished', 0),
(138, ' Core training', 2, '', '', 19, 'Unpublished', 0),
(139, ' The basic structure of \r\nskeletal muscle and how \r\nthey contract', 2, '', '', 20, 'Unpublished', 0),
(140, 'Muscle fibre types', 2, '', '', 20, 'Unpublished', 0),
(141, ' The roles of muscles and \r\ntypes of muscle contractions', 2, '', '', 20, 'Unpublished', 0),
(142, 'Name, locate and explain \r\nthe function of skeletal \r\nmuscle involved in physical \r\nactivity', 2, '', '', 20, 'Unpublished', 0),
(143, 'Positional terms', 2, '', '', 20, 'Unpublished', 0),
(144, 'Anatomical planes and axes', 2, '', '', 20, 'Unpublished', 0),
(145, 'Comparison between male \r\nand female skeletons', 2, '', '', 20, 'Unpublished', 0),
(146, 'Effects of exercise on the \r\nmusculoskeletal system', 2, '', '', 20, 'Unpublished', 0),
(147, 'Joint actions', 2, '', '', 20, 'Unpublished', 0),
(149, 'Basic structure and function of the nervous system', 2, '', '', 21, 'Unpublished', 0),
(150, 'The structure of a neuron', 2, '', '', 21, 'Unpublished', 0),
(151, 'How do neurons function?', 2, '', '', 21, 'Unpublished', 0),
(152, 'Motor unit recruitment and \r\n‘all-or-none’ law', 2, '', '', 21, 'Unpublished', 0),
(153, 'Proprioceptors, the stretch \r\nreflex, reciprocal inhibition \r\nand flexibility training', 2, '', '', 21, 'Unpublished', 0),
(154, 'The effects of exercise on \r\nthe neuromuscular system', 2, '', '', 21, 'Unpublished', 0),
(155, 'What is the endocrine \r\nsystem?', 2, '', '', 22, 'Unpublished', 0),
(156, 'Endocrine glands and their \r\nhormones', 2, '', '', 22, 'Unpublished', 0),
(157, 'Hormonal regulation of \r\nmetabolism during exercise', 2, '', '', 22, 'Unpublished', 0),
(158, 'effects of exercise on \r\ntestosterone and human \r\ngrowth hormone', 2, '', '', 22, 'Unpublished', 0),
(159, 'What and where does \r\nenergy come from?', 2, '', '', 23, 'Unpublished', 0),
(160, ' Adenosine Triphosphate \n(ATP or \'energy currency\')', 2, '', '', 23, 'Unpublished', 0),
(161, 'The energy systems', 2, '', '', 23, 'Unpublished', 0),
(162, 'What is cellular respiration?', 2, '', '', 23, 'Unpublished', 0),
(163, 'The effects of exercise on \r\nthe energy systems', 2, '', '', 23, 'Unpublished', 0),
(164, 'Introduction to nutrition', 2, '', '', 24, 'Unpublished', 0),
(165, 'function of the digestive  system', 2, '', '', 24, 'Unpublished', 0),
(166, 'structure of the digestive  system', 2, '', '', 24, 'Unpublished', 0),
(167, 'Macronutrients: Carbohydrates (CHO)', 2, '', '', 24, 'Unpublished', 0),
(168, 'Macronutrients: Protein', 2, '', '', 24, 'Unpublished', 0),
(169, 'Macronutrients: Fats (lipds)', 2, '', '', 24, 'Unpublished', 0),
(170, 'Micronutrients: Vitamins  and minerals', 2, '', '', 24, 'Unpublished', 0),
(171, 'Micronutrients: Supplementation', 2, '', '', 24, 'Unpublished', 0),
(172, 'The relationship between  nutrition Physical activity  and health', 2, '', '', 24, 'Unpublished', 0),
(173, ' Healthy eating: main food \r\ngroups', 2, '', '', 25, 'Unpublished', 0),
(174, 'Healthy eating: nutritional \r\nprinciples', 2, '', '', 25, 'Unpublished', 0),
(175, 'Portion sizes and food \r\npreparation', 2, '', '', 25, 'Unpublished', 0),
(176, 'Weight management: \r\nenergy expenditure', 2, '', '', 25, 'Unpublished', 0),
(177, ' Weight management: \r\nweight loss', 2, '', '', 25, 'Unpublished', 0),
(178, ' Understanding food labels', 2, '', '', 25, 'Unpublished', 0),
(179, 'Popular diets', 2, '', '', 25, 'Unpublished', 0),
(180, 'Religious and cultural \r\npractices', 2, '', '', 25, 'Unpublished', 0),
(181, ' Nutritional requirements \r\nand hydration needs of \r\nclients engaged in physical \r\nactivity', 2, '', '', 25, 'Unpublished', 0),
(182, ' Professionals and professional bodies involved in \r\nthe area of nutrition', 2, '', '', 25, 'Unpublished', 0),
(183, 'Professional boundaries', 2, '', '', 25, 'Unpublished', 0),
(184, ' Eating disorders', 2, '', '', 25, 'Unpublished', 0),
(190, 'Client consultation and \r\ninformation gathering', 2, '', '', 26, 'Unpublished', 0),
(191, 'Setting client goals', 2, '', '', 26, 'Unpublished', 0),
(192, ' Understanding client \r\nbehaviour, behavioural \r\nchange and the performance equation', 2, '', '', 26, 'Unpublished', 0),
(193, 'Motivational strategies', 2, '', '', 26, 'Unpublished', 0),
(194, 'How to analyse and interpret collected information \r\nso that client needs and \r\nnutritional goals can be \r\nidentified', 2, '', '', 26, 'Unpublished', 0),
(195, 'Gathering client-centred \r\ninformation', 2, '', '', 27, 'Unpublished', 0),
(196, 'Screening clients prior to \r\npersonal training', 2, '', '', 27, 'Unpublished', 0),
(197, ' Working with clients to \r\nagree goals, objectives, \r\nprogrammes and adaptations', 2, '', '', 27, 'Unpublished', 0),
(198, 'Setting client-centred goals', 2, '', '', 27, 'Unpublished', 0),
(199, 'Health appraisal', 2, '', '', 27, 'Unpublished', 0),
(200, 'Fitness appraisal', 2, '', '', 27, 'Unpublished', 0),
(201, 'Personal training resources', 2, '', '', 27, 'Unpublished', 0),
(202, 'Training principles', 2, '', '', 28, 'Unpublished', 0),
(203, 'Encouraging activities of \r\ndaily living', 2, '', '', 28, 'Unpublished', 0),
(204, 'Programming an effective \r\nwarm-up', 2, '', '', 28, 'Unpublished', 0),
(205, 'Programming aerobic \r\nexercise', 2, '', '', 28, 'Unpublished', 0),
(206, 'Programming strength', 2, '', '', 28, 'Unpublished', 0),
(207, ' Programming power \r\nexercise', 2, '', '', 28, 'Unpublished', 0),
(208, 'Programming muscle \r\nhypertrophy', 2, '', '', 28, 'Unpublished', 0),
(209, ' Programming muscular \r\nendurance', 2, '', '', 28, 'Unpublished', 0),
(210, 'Programming a cool-down \r\nand flexibility', 2, '', '', 28, 'Unpublished', 0),
(211, 'Adapt, review and progress exercise', 2, '', '', 28, 'Unpublished', 0),
(213, 'lesson', 5, '', '', 34, 'Unpublished', 0),
(216, 'lesson demo', 4, '', '', 29, 'Unpublished', 0),
(219, 'demolesson1', 26, '', '', 39, 'Unpublished', 0),
(220, 'demovidio', 26, '', 'fsfsdf', 39, 'Unpublished', 0),
(229, 'test demo', 26, '', '', 39, 'Unpublished', 0),
(230, 'test demo1', 26, '', '', 39, 'Unpublished', 0),
(231, 'test lesson1', 26, '', '', 39, 'Unpublished', 0),
(232, 'lessondemo', 26, '', '', 39, 'Unpublished', 0),
(233, 'lessondemo1', 26, '', '', 39, 'Unpublished', 0),
(234, 'lessondemo3', 26, '', '', 39, 'Unpublished', 0),
(235, 'lessondemo4', 26, '', '', 39, 'Unpublished', 0),
(236, 'lessondemo5', 26, '', '', 39, 'Unpublished', 0),
(240, 'lessondemo8', 26, '', '', 39, 'Unpublished', 0),
(243, 'lessondemo11', 26, '', '', 39, 'Unpublished', 0),
(255, 'xyz', 2, '', '', 24, 'Unpublished', 0),
(256, 'demo lesson22', 25, '', '', 41, 'Published', 0),
(257, 'demo12', 25, '', '', 42, 'Unpublished', 0),
(258, 'test lesson', 25, '', '', 42, 'Unpublished', 0),
(259, 'test lesson11', 25, '', '', 42, 'Unpublished', 0),
(261, 'sadasdsadsa', 25, '', '', 44, 'Unpublished', 0),
(262, 'asdsad', 25, '', '', 45, 'Unpublished', 0),
(263, 'demo lesson 12', 25, '', '', 41, 'Published', 0),
(268, 'lesson test', 25, '', '', 45, 'Unpublished', 0),
(269, 'New Test Lesson', 25, '', '', 41, 'Unpublished', 0),
(273, '1212', 18, '', '', 38, 'Unpublished', 0),
(279, '21', 26, '', '', 48, 'Unpublished', 0),
(280, '11', 18, '', '', 38, 'Unpublished', 0),
(281, 'abc', 1, '', '', 3, 'Unpublished', 0),
(282, 'test', 28, '', '', 49, 'Published', 0),
(283, 'test12', 28, '', '', 53, 'Published', 0),
(284, 'lesson12', 9, '', '', 55, 'Unpublished', 0),
(285, '1213', 29, '', 'asd', 56, 'Published', 0),
(286, 'lessonsadas', 12, '', '', 57, 'Published', 0),
(287, 'arrti', 32, '', '', 58, 'Published', 0),
(288, '213213', 33, '', '', 59, 'Unpublished', 0),
(289, 'lessontest', 32, '', '', 60, 'Unpublished', 0),
(290, 'ewr453245', 33, '', '', 59, 'Published', 0),
(291, 'lesson 11', 5, '', '', 61, 'Unpublished', 0),
(292, 'sundaram', 28, '', '', 50, 'Published', 0),
(293, 'lesson 1', 28, '', '', 62, 'Published', 0),
(294, 'lesson for testing', 38, '', 'Lesson Description.!@@#%', 63, 'Published', 0),
(295, 'lesson 12', 40, '', 'testdsad', 64, 'Published', 0),
(296, 'lesson2', 40, '', '', 64, 'Unpublished', 0),
(297, 'i9kopkpo', 42, '', '', 66, 'Unpublished', 0),
(298, 'Biomechanics', 1, '', 'Lear biomechanics of movement', 1, 'Published', 0),
(299, 'lesson1', 40, '', '', 64, 'Unpublished', 0),
(300, 'lesson01', 40, '', '', 64, 'Unpublished', 0),
(301, 'nbhg', 40, '', '', 64, 'Unpublished', 0),
(302, 'lesson-1', 44, '', '', 67, 'Unpublished', 0),
(303, 'Lesson 6.1', 1, '', 'Introduction to NAMSET', 68, 'Unpublished', 0),
(304, 'demo lesson', 53, '', '', 69, 'Published', 0),
(307, 'Bones of the skeletons', 68, '', '', 78, 'Published', 0),
(308, 'Muscle action', 68, '', '', 79, 'Published', 0),
(309, 'The basic structure of the nervous system', 68, '', '', 80, 'Published', 0),
(310, 'The function of the nervous systems', 68, '', '', 81, 'Published', 0),
(311, 'Adenosine Triphosphate', 68, '', '', 82, 'Published', 0),
(312, 'The blood vessel', 68, '', '', 83, 'Published', 0),
(313, 'Forming effective working', 69, '', '', 84, 'Published', 0),
(314, 'Behaviour changes', 69, '', '', 85, 'Published', 0),
(315, 'Handling customer complaint', 69, '', '', 86, 'Published', 0),
(316, 'Health and Safety at Work', 69, '', '', 87, 'Published', 0),
(317, 'How to perform a risk assessments', 69, '', '', 88, 'Published', 0),
(318, 'Disclosure and Barring Service', 69, '', '', 89, 'Published', 0),
(319, 'Pulmonary and systemic circulations', 70, '', '', 90, 'Published', 0),
(320, 'Abnormal posture', 70, '', '', 91, 'Published', 0),
(321, 'Muscle fibre type', 70, '', '', 92, 'Published', 0),
(322, 'Basic structure and function', 70, '', '', 93, 'Published', 0),
(323, 'Endocrine glands and their hormone', 70, '', '', 94, 'Published', 0),
(324, 'Adenosine Triphosphates', 70, '', '', 95, 'Published', 0),
(325, 'dsad', 67, '', '', 98, 'Unpublished', 0),
(326, 'dsadsadsa', 67, '', '', 99, 'Unpublished', 0),
(327, 'losson no 1', 67, '', '', 100, 'Unpublished', 0),
(328, 'Strength', 28, '', 'a look at strength', 49, 'Unpublished', 0),
(329, 'Muscle tissue', 67, '', '', 103, 'Unpublished', 0),
(330, 'Bone formation', 1, '', 'Learn hw bones form', 104, 'Unpublished', 0),
(331, 'leg press', 68, '', '', 82, 'Published', 0),
(332, 'muscle', 76, '', 'The aim', 111, 'Published', 0),
(333, 'Bone function', 1, '', '', 104, 'Unpublished', 0),
(334, 'Boned formation', 1, '', '', 104, 'Unpublished', 0),
(336, 'personal Trainer', 1, '', 'The aim', 114, 'Unpublished', 0),
(337, 'Cycling', 1, '', 'The aim', 115, 'Unpublished', 0),
(338, 'Lesson 1.1', 1, 'lesson_images/15922219802133905345.png', 'Bones of the skeleton', 2, 'Published', 0),
(339, '1.2 ', 69, '', '', 84, 'Published', 0),
(340, 'gym lesson 2', 78, '', '', 119, 'Published', 0),
(341, 'gym lesson 1', 78, '', '', 118, 'Published', 0),
(342, 'gym lesson 3', 78, '', '', 120, 'Published', 0),
(343, 'gym lesson 4', 78, '', '', 121, 'Published', 0),
(344, 'gym lesson 5', 78, '', '', 122, 'Published', 0),
(345, 'gym lesson 6', 78, '', '', 123, 'Published', 0),
(346, 'Lesson 1.2', 76, '', '', 111, 'Unpublished', 0),
(347, 'sfgsgbsdgsddddddd', 77, '', 'hfhfkkfh', 117, 'Published', 0),
(348, 'lesson new', 81, '', 'hhfjhfk', 117, 'Published', 0),
(349, 'gym lesson', 78, '', '', 118, 'Unpublished', 0),
(350, 'Gym lesson 7', 78, '', '', 118, 'Unpublished', 0),
(351, 'gym lesson 8', 78, '', '', 118, 'Published', 0),
(352, 'lesson1111', 84, '', '', 131, 'Unpublished', 1),
(353, 'LESSON11', 85, '', '', 132, 'Unpublished', 1),
(354, 'lesson 1', 81, '', 'Anatomy 1', 135, 'Published', 1),
(355, 'Lesson 1.2', 1, '', 'Functions of the skeleton', 2, 'Published', 1),
(358, 'fyuy', 76, '', 'chgdhh', 128, 'Published', 1),
(359, 'Lesson 1.3', 1, '', 'Joint structure', 2, 'Published', 2),
(360, 'L1', 1, '', 'Muscle', 126, 'Published', 1),
(361, '1.1', 86, '', 'pathology', 139, 'Unpublished', 1),
(362, 'skipping lesson', 77, '', 'erer', 140, 'Published', 1),
(363, 'cxvcncvnvnbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb', 77, '', 'gvjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj', 141, 'Published', 1),
(364, 'lesson 1 ch up', 92, '', 'lesson  title 1 test up', 142, 'Published', 1),
(365, 'test lesson 2', 92, '', 'test 2nd lesson description', 142, 'Unpublished', 2),
(367, 'lesson 5', 92, '', 'lesson 5', 144, 'Unpublished', 1),
(370, 'sfdssdf', 92, '', 'sdfsdf', 142, 'Unpublished', 3),
(371, 'hnbvnbvnb', 92, '', 'fgvgbcbgc', 146, 'Published', 1),
(372, 'lesson up', 92, '', 'lesson uppp', 146, 'Published', 2),
(373, 'lesson newww', 92, '', 'new lesson', 146, 'Published', 3),
(374, 'lesson for another section', 92, '', 'escription lesson for another section', 147, 'Published', 1),
(375, 'lesson 2 for another section', 92, '', 'lesson 2 for another section', 147, 'Published', 2),
(376, ' lesssss', 77, '', 'fggfg', 148, 'Published', 1),
(377, 'uiu', 77, '', 'uiu', 149, 'Published', 1),
(378, 'aa', 77, '', 'addd', 150, 'Published', 1),
(379, 'gd', 113, '', 'gjhj', 153, 'Published', 1),
(380, 'ert', 113, '', 'rrr', 152, 'Published', 1),
(381, 'dfvf', 77, '', 'fffg', 154, 'Published', 1),
(382, 'yyyuy', 77, '', 'yyuyuy', 155, 'Published', 1),
(383, 'lrr', 89, '', 'sd', 156, 'Unpublished', 1),
(384, 'SD', 77, '', 'DD', 157, 'Published', 1),
(385, 'aaa', 77, '', 'xxx', 158, 'Published', 1),
(386, 'lesson 1', 87, '', 'lesson 1', 159, 'Published', 1),
(387, 'Theory', 116, '', 'Theory', 161, 'Published', 1),
(388, 'Lesson latest', 117, '', 'Description lesson', 160, 'Unpublished', 1),
(389, 'lesson 1', 77, '', 'lesson1', 140, 'Published', 2),
(390, 'Adhoc Lesson', 116, '', 'Adhoc Lesson', 163, 'Unpublished', 1),
(391, 'Adhoc', 116, '', 'Adhoc', 163, 'Unpublished', 2),
(392, 'lesson 0.1', 77, '', 'lesson 0.1', 117, 'Published', 1),
(393, 'aa', 77, '', 'sss', 166, 'Published', 1),
(394, 'lesson 1.1.1 sdaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa sadddddddddddddddddddddddddddddddddddddddddddddddd', 115, '', 'lesson 1.1.1sa saddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd', 168, 'Published', 1),
(396, 'lesson 1.1', 92, '', 'lesson 1.1', 170, 'Unpublished', 1),
(397, 'lesson 1', 81, '', 'lesson 1', 129, 'Published', 1),
(398, 'hkhkhkhkhkhkhkhkhkhkhkhkhkhkhkhkhkhkhkhkhkhkhkhkhkhkhkhkhkhkhkhkhkhkhkhkhkhkhkhkhkhkhkhkhkhkhkhkhkhk', 122, '', 'sssssssssssssssssssssssssssssssssssssssssssssshbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbkj;ljkalkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk', 171, 'Published', 1),
(399, 'lesson 1.1.1', 123, 'lesson_images/15962032471273570095.png', 'lesson 1.1.1', 172, 'Published', 1),
(400, 'lesson 2.2', 123, '', 'lesson 2.2', 173, 'Published', 1),
(401, 'lesson 1.1', 124, '', 'lesson 1.1', 174, 'Published', 1),
(402, '33', 124, '', '22', 175, 'Published', 1),
(403, 'lesson', 115, '', 'my lesson', 176, 'Published', 1),
(404, 'aaa', 115, '', 'aaa', 177, 'Published', 1),
(405, 'as', 115, '', 'sdsdf', 178, 'Published', 1),
(406, 'demo', 77, '', 'demo', 166, 'Published', 2),
(407, 'sad', 77, '', 'sad', 179, 'Published', 1),
(408, 'asd', 77, '', 'sad', 180, 'Published', 1),
(409, 'asd', 115, '', 'asd', 181, 'Published', 1),
(410, 'vvv', 77, '', 'aaa', 182, 'Published', 1),
(411, 'dzxc\'s', 124, '', 'xc.x\'d', 183, 'Unpublished', 1),
(412, 'ad\'hoc', 116, '', 'ad\'hoc', 161, 'Unpublished', 2),
(413, 'demo lesson', 125, '', 'aa', 184, 'Published', 1),
(414, 'demo lesson', 125, '', 'sss', 185, 'Published', 1),
(415, 'neww one', 125, '', 'gg', 186, 'Published', 1),
(416, 'lesson', 115, '', 'lesson', 187, 'Published', 1),
(417, 'new lesson', 115, '', 'ss', 188, 'Published', 1),
(418, 'lesson', 126, '', 'lesson', 189, 'Published', 1),
(419, 'lesson', 127, 'lesson_images/15971251301817101802.png', 'lesson', 190, 'Published', 1),
(420, 'lesson 1.1.1', 126, '', 'lesson 1.1.1', 191, 'Published', 1),
(421, 'new lesson', 115, '', 'new lesson', 192, 'Published', 1),
(422, 'lesson3.1.1', 123, '', 'lesson3.1.1', 193, 'Published', 1),
(423, 'lesson', 123, '', 'lesson', 194, 'Published', 1),
(424, 'lesson 1', 123, '', 'lesson 1', 195, 'Published', 1),
(425, 'lesson 1', 123, '', 'lesson 1', 196, 'Published', 1),
(426, 'lessonn', 128, '', 'hhhhhhhh', 197, 'Published', 1),
(427, 'lesson task', 128, '', 'cncnnc', 198, 'Published', 1),
(428, 'aaa', 128, '', 'aaaaa', 199, 'Published', 1),
(429, 'ss', 128, '', 'sss', 200, 'Published', 1),
(430, 'lesson 1', 123, '', 'lesson 1', 201, 'Published', 1),
(431, 'another lesson', 128, '', 'jhkj', 202, 'Published', 1),
(432, 'vvvv', 128, '', 'hhhh', 203, 'Published', 1),
(433, 'lesson1', 123, '', 'lesson1', 204, 'Published', 1),
(434, 'ewg', 129, '', 'ee', 205, 'Published', 1),
(435, 'wffff', 129, '', '2323', 206, 'Published', 1),
(436, 'kkk', 129, '', 'kkk', 207, 'Published', 1),
(437, 'ss', 128, '', 'ddd', 208, 'Published', 1);

-- --------------------------------------------------------

--
-- Table structure for table `lesson_attempt`
--

CREATE TABLE `lesson_attempt` (
  `id` int(30) NOT NULL,
  `lesson_id` int(50) NOT NULL,
  `user_id` int(30) NOT NULL,
  `attempte` varchar(50) NOT NULL,
  `attempt_count` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lesson_attempt`
--

INSERT INTO `lesson_attempt` (`id`, `lesson_id`, `user_id`, `attempte`, `attempt_count`) VALUES
(1, 419, 115, 'true', NULL),
(2, 400, 119, 'true', NULL),
(3, 419, 119, 'true', NULL),
(4, 399, 119, 'true', NULL),
(5, 422, 119, 'true', NULL),
(6, 423, 119, 'true', NULL),
(7, 424, 119, 'true', NULL),
(8, 425, 119, 'true', NULL),
(9, 426, 122, 'true', NULL),
(10, 427, 122, 'true', NULL),
(11, 418, 119, 'true', NULL),
(12, 420, 119, 'true', NULL),
(13, 431, 122, 'true', NULL),
(14, 429, 122, 'true', NULL),
(15, 418, 115, 'true', NULL),
(16, 420, 115, 'true', NULL),
(17, 432, 122, 'true', NULL),
(18, 428, 122, 'true', NULL),
(19, 394, 119, 'true', NULL),
(20, 434, 122, 'true', NULL),
(21, 435, 122, 'true', NULL),
(22, 436, 122, 'true', NULL),
(23, 418, 122, 'true', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lesson_question_relation`
--

CREATE TABLE `lesson_question_relation` (
  `id` int(30) NOT NULL,
  `lesson_id` int(30) DEFAULT '0',
  `question_id` int(30) NOT NULL,
  `question_type` varchar(150) NOT NULL,
  `unit_id` int(10) DEFAULT NULL,
  `sequence_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lesson_question_relation`
--

INSERT INTO `lesson_question_relation` (`id`, `lesson_id`, `question_id`, `question_type`, `unit_id`, `sequence_id`) VALUES
(1, 1, 1, 'scrollable', NULL, 1),
(2, 2, 2, 'scrollable', NULL, 1),
(3, 3, 3, 'scrollable', NULL, 1),
(5, 1, 1, 'expandable', NULL, 0),
(7, 1, 1, 'multiple_choice', NULL, 0),
(8, 1, 1, 'true_false', NULL, 0),
(11, 1, 1, 'slider', NULL, 0),
(25, 5, 25, 'scrollable', NULL, 0),
(26, 5, 9, 'expandable', NULL, 0),
(27, 5, 5, 'multiple_choice1', NULL, 0),
(28, 5, 6, 'multiple_choice1', NULL, 0),
(61, 15, 14, 'start_lesson', NULL, 0),
(63, 245, 16, 'start_lesson', NULL, 0),
(67, 247, 20, 'start_lesson', NULL, 0),
(69, 248, 22, 'start_lesson', NULL, 0),
(71, 249, 24, 'start_lesson', NULL, 0),
(73, 250, 26, 'start_lesson', NULL, 0),
(75, 251, 28, 'start_lesson', NULL, 0),
(77, 252, 30, 'start_lesson', NULL, 0),
(90, 255, 36, 'start_lesson', NULL, 0),
(94, 242, 9, 'video_slide', NULL, 0),
(95, 217, 30, 'scrollable', NULL, 0),
(96, 217, 16, 'expandable', NULL, 0),
(97, 217, 11, 'multiple_choice', NULL, 0),
(98, 217, 6, 'slider', NULL, 0),
(99, 217, 10, 'video_slide', NULL, 0),
(134, 264, 54, 'start_lesson', NULL, 0),
(136, 265, 56, 'start_lesson', NULL, 0),
(138, 266, 58, 'start_lesson', NULL, 0),
(142, 267, 60, 'start_lesson', NULL, 0),
(149, 239, 19, 'expandable', NULL, 0),
(154, 238, 20, 'expandable', NULL, 0),
(156, 238, 12, 'video_slide', NULL, 0),
(159, 237, 13, 'video_slide', NULL, 0),
(180, 241, 21, 'expandable', NULL, 0),
(181, 241, 22, 'expandable', NULL, 0),
(244, 281, 88, 'start_lesson', NULL, 0),
(259, 1, 1, 'crossword', NULL, 0),
(260, 1, 2, 'crossword', NULL, 0),
(281, 1, 6, 'circle_the_answer', NULL, 0),
(282, 285, 6, 'circle_the_answer', NULL, 0),
(345, 298, 122, 'start_lesson', NULL, 0),
(346, 298, 39, 'expandable', NULL, 0),
(347, 298, 40, 'multiple_choice', NULL, 0),
(348, 298, 66, 'scrollable', NULL, 0),
(349, 298, 24, 'slider', NULL, 0),
(350, 298, 26, 'video_slide', NULL, 0),
(351, 298, 34, 'true_false', NULL, 0),
(352, 298, 15, 'crossword', NULL, 0),
(353, 298, 31, 'memory', NULL, 0),
(354, 298, 35, 'true_false', NULL, 0),
(355, 298, 16, 'crossword', NULL, 0),
(368, 3, 67, 'scrollable', NULL, 2),
(369, 3, 40, 'expandable', NULL, 0),
(370, 4, 68, 'scrollable', NULL, 1),
(371, 4, 42, 'multiple_choice', NULL, 0),
(372, 4, 28, 'video_slide', NULL, 0),
(373, 6, 69, 'scrollable', NULL, 0),
(374, 6, 29, 'video_slide', NULL, 0),
(375, 7, 25, 'slider', NULL, 0),
(376, 7, 41, 'expandable', NULL, 0),
(377, 8, 30, 'video_slide', NULL, 0),
(378, 8, 37, 'true_false', NULL, 0),
(379, 9, 38, 'true_false', NULL, 0),
(380, 9, 43, 'multiple_choice', NULL, 0),
(381, 9, 26, 'slider', NULL, 0),
(382, 10, 27, 'slider', NULL, 0),
(383, 10, 42, 'expandable', NULL, 0),
(384, 10, 44, 'multiple_choice', NULL, 0),
(385, 11, 31, 'video_slide', NULL, 2),
(386, 11, 43, 'expandable', NULL, 1),
(387, 12, 70, 'scrollable', NULL, 0),
(388, 12, 44, 'expandable', NULL, 0),
(389, 13, 71, 'scrollable', NULL, 0),
(390, 13, 32, 'video_slide', NULL, 0),
(391, 14, 45, 'expandable', NULL, 0),
(392, 14, 39, 'true_false', NULL, 0),
(393, 15, 33, 'video_slide', NULL, 0),
(394, 15, 17, 'crossword', NULL, 0),
(395, 45, 28, 'slider', NULL, 0),
(397, 46, 73, 'scrollable', NULL, 0),
(398, 46, 45, 'multiple_choice', NULL, 0),
(399, 47, 46, 'multiple_choice', NULL, 0),
(400, 47, 32, 'memory', NULL, 0),
(401, 47, 34, 'video_slide', NULL, 0),
(402, 122, 35, 'video_slide', NULL, 0),
(403, 122, 74, 'scrollable', NULL, 0),
(405, 124, 46, 'expandable', NULL, 0),
(406, 124, 47, 'multiple_choice', NULL, 0),
(407, 125, 36, 'video_slide', NULL, 0),
(408, 2, 37, 'video_slide', NULL, 0),
(414, 303, 132, 'start_lesson', NULL, 0),
(415, 303, 18, 'crossword', NULL, 0),
(417, 1, 77, 'scrollable', NULL, 2),
(419, 1, 48, 'multiple_choice', NULL, 0),
(420, 1, 29, 'slider', NULL, 0),
(421, 1, 39, 'video_slide', NULL, 0),
(422, 1, 40, 'true_false', NULL, 0),
(423, 1, 19, 'crossword', NULL, 0),
(424, 1, 33, 'memory', NULL, 0),
(425, 1, 78, 'scrollable', NULL, 3),
(426, 1, 50, 'expandable', NULL, 0),
(427, 1, 41, 'true_false', NULL, 0),
(428, 1, 49, 'multiple_choice', NULL, 0),
(429, 1, 30, 'slider', NULL, 0),
(430, 1, 40, 'video_slide', NULL, 0),
(431, 1, 20, 'crossword', NULL, 0),
(432, 1, 34, 'memory', NULL, 0),
(433, 304, 134, 'start_lesson', NULL, 0),
(444, NULL, 157, 'start_lesson', 110, 0),
(445, NULL, 159, 'start_lesson', 111, 0),
(446, NULL, 161, 'start_lesson', 112, 0),
(447, NULL, 163, 'start_lesson', 113, 0),
(448, NULL, 165, 'start_lesson', 114, 0),
(449, NULL, 167, 'start_lesson', 116, 0),
(450, NULL, 169, 'start_lesson', 117, 0),
(451, NULL, 171, 'start_lesson', 118, 0),
(452, NULL, 173, 'start_lesson', 119, 0),
(453, NULL, 175, 'start_lesson', 120, 0),
(454, NULL, 177, 'start_lesson', 122, 0),
(455, NULL, 179, 'start_lesson', 123, 0),
(456, NULL, 181, 'start_lesson', 124, 0),
(457, NULL, 183, 'start_lesson', 125, 0),
(458, NULL, 185, 'start_lesson', 126, 0),
(459, NULL, 187, 'start_lesson', 128, 0),
(460, NULL, 189, 'start_lesson', 129, 0),
(461, NULL, 191, 'start_lesson', 130, 0),
(462, NULL, 193, 'start_lesson', 131, 0),
(463, NULL, 195, 'start_lesson', 132, 0),
(464, NULL, 197, 'start_lesson', 134, 0),
(465, NULL, 199, 'start_lesson', 135, 0),
(466, NULL, 201, 'start_lesson', 136, 0),
(467, NULL, 203, 'start_lesson', 137, 0),
(468, NULL, 205, 'start_lesson', 138, 0),
(469, 0, 1, 'ass_multiple_choice', 134, 0),
(470, 0, 2, 'free_text', 134, 0),
(471, 0, 3, 'free_text', 134, 0),
(472, 0, 207, 'start_lesson', 140, 0),
(473, 0, 209, 'start_lesson', 141, 0),
(474, 0, 211, 'start_lesson', 142, 0),
(475, 0, 213, 'start_lesson', 143, 0),
(476, 0, 215, 'start_lesson', 144, 0),
(477, 0, 217, 'start_lesson', 146, 0),
(478, 0, 219, 'start_lesson', 147, 0),
(479, 0, 221, 'start_lesson', 148, 0),
(480, 0, 223, 'start_lesson', 149, 0),
(481, 0, 225, 'start_lesson', 150, 0),
(482, 0, 227, 'start_lesson', 152, 0),
(483, 0, 229, 'start_lesson', 153, 0),
(484, 0, 231, 'start_lesson', 154, 0),
(485, 0, 233, 'start_lesson', 155, 0),
(486, 0, 235, 'start_lesson', 156, 0),
(487, 0, 237, 'start_lesson', 158, 0),
(488, 0, 239, 'start_lesson', 159, 0),
(489, 0, 241, 'start_lesson', 160, 0),
(490, 0, 243, 'start_lesson', 161, 0),
(491, 0, 245, 'start_lesson', 162, 0),
(492, 305, 247, 'start_lesson', NULL, 0),
(493, 306, 249, 'start_lesson', NULL, 0),
(494, 0, 4, 'free_text', 158, 0),
(495, 0, 251, 'start_lesson', 164, 0),
(496, 0, 253, 'start_lesson', 165, 0),
(497, 0, 255, 'start_lesson', 166, 0),
(498, 0, 257, 'start_lesson', 167, 0),
(499, 0, 259, 'start_lesson', 168, 0),
(500, 0, 5, 'free_text', 164, 0),
(501, 0, 2, 'multiple_choice', 164, 0),
(502, 0, 6, 'free_text', 164, 0),
(503, 0, 261, 'start_lesson', 170, 0),
(504, 0, 263, 'start_lesson', 171, 0),
(505, 0, 265, 'start_lesson', 172, 0),
(506, 0, 267, 'start_lesson', 173, 0),
(507, 0, 269, 'start_lesson', 174, 0),
(526, 0, 283, 'start_lesson', 176, 0),
(527, 0, 285, 'start_lesson', 177, 0),
(528, 0, 287, 'start_lesson', 178, 0),
(529, 0, 289, 'start_lesson', 179, 0),
(530, 0, 291, 'start_lesson', 180, 0),
(550, 0, 305, 'start_lesson', 182, 0),
(551, 0, 307, 'start_lesson', 183, 0),
(552, 0, 309, 'start_lesson', 184, 0),
(553, 0, 311, 'start_lesson', 185, 0),
(554, 0, 313, 'start_lesson', 186, 0),
(588, 0, 333, 'start_lesson', 188, 0),
(589, 0, 335, 'start_lesson', 189, 0),
(590, 0, 337, 'start_lesson', 190, 0),
(591, 0, 339, 'start_lesson', 191, 0),
(592, 0, 341, 'start_lesson', 192, 0),
(593, 0, 343, 'start_lesson', 188, 0),
(594, 0, 345, 'start_lesson', 189, 0),
(595, 0, 347, 'start_lesson', 190, 0),
(596, 0, 349, 'start_lesson', 191, 0),
(597, 0, 351, 'start_lesson', 192, 0),
(598, 0, 353, 'start_lesson', 194, 0),
(599, 0, 355, 'start_lesson', 195, 0),
(600, 0, 357, 'start_lesson', 196, 0),
(601, 0, 359, 'start_lesson', 197, 0),
(602, 0, 361, 'start_lesson', 198, 0),
(629, 1, 60, 'multiple_choice', NULL, 0),
(630, 1, 61, 'expandable', NULL, 0),
(641, 0, 11, 'free_text', 182, 1),
(642, 16, 87, 'scrollable', NULL, 4),
(656, 0, 13, 'free_text', 176, 2),
(657, 0, 1, 'ass_multiple_choice', 176, 0),
(658, 0, 2, 'ass_multiple_choice', 176, 0),
(659, 0, 3, 'ass_multiple_choice', 177, 0),
(660, 0, 14, 'free_text', 177, 3),
(661, 0, 15, 'free_text', 174, 4),
(662, 0, 4, 'ass_multiple_choice', 174, 0),
(663, 0, 16, 'free_text', 174, 5),
(664, 0, 17, 'free_text', 225, 6),
(665, 0, 5, 'ass_multiple_choice', 225, 0),
(666, 0, 18, 'free_text', 207, 7),
(667, 0, 6, 'ass_multiple_choice', 208, 0),
(668, 0, 19, 'free_text', 208, 8),
(669, 0, 20, 'free_text', 209, 9),
(670, 0, 7, 'ass_multiple_choice', 209, 0),
(671, 0, 21, 'free_text', 210, 10),
(672, 0, 8, 'ass_multiple_choice', 210, 0),
(673, 0, 391, 'start_lesson', 212, 1),
(674, 0, 393, 'start_lesson', 213, 1),
(675, 0, 395, 'start_lesson', 214, 1),
(676, 0, 397, 'start_lesson', 215, 1),
(677, 0, 399, 'start_lesson', 216, 1),
(678, 0, 22, 'free_text', 212, 11),
(679, 0, 9, 'ass_multiple_choice', 212, 0),
(680, 0, 401, 'start_lesson', 218, 1),
(681, 0, 403, 'start_lesson', 219, 1),
(682, 0, 405, 'start_lesson', 220, 1),
(683, 0, 407, 'start_lesson', 221, 1),
(684, 0, 409, 'start_lesson', 222, 1),
(685, 0, 10, 'ass_multiple_choice', 218, 0),
(686, 16, 50, 'video_slide', NULL, 6),
(687, 16, 15, 'peer_authoring', NULL, 7),
(688, 16, 10, 'reorder', NULL, 8),
(689, 16, 11, 'reorder', NULL, 5),
(690, 332, 90, 'scrollable', NULL, 0),
(691, 332, 51, 'video_slide', NULL, 2),
(693, 332, 16, 'peer_authoring', NULL, 4),
(694, 332, 63, 'expandable', NULL, 5),
(696, 332, 53, 'true_false', NULL, 6),
(697, 332, 17, 'peer_authoring', NULL, 7),
(701, 332, 12, 'reorder', NULL, 8),
(702, 332, 54, 'true_false', NULL, 9),
(707, 16, 52, 'video_slide', NULL, 9),
(708, 332, 17, 'reorder', NULL, 10),
(709, 16, 30, 'circle_the_answer', NULL, 10),
(710, 16, 31, 'circle_the_answer', NULL, 11),
(711, 16, 23, 'free_text', 0, 12),
(713, 16, 91, 'scrollable', NULL, 13),
(714, 16, 32, 'circle_the_answer', NULL, 3),
(715, 16, 33, 'circle_the_answer', NULL, 14),
(716, 16, 92, 'scrollable', NULL, 15),
(717, 16, 93, 'scrollable', NULL, 16),
(718, 16, 94, 'scrollable', NULL, 17),
(719, 16, 95, 'scrollable', NULL, 18),
(720, 0, 417, 'start_lesson', 224, 1),
(721, 0, 419, 'start_lesson', 225, 1),
(722, 0, 421, 'start_lesson', 226, 1),
(724, 0, 425, 'start_lesson', 228, 1),
(728, 16, 67, 'expandable', NULL, 19),
(729, 16, 41, 'memory', NULL, 20),
(732, 16, 68, 'expandable', NULL, 1),
(733, 16, 36, 'slider', NULL, 0),
(734, 16, 34, 'circle_the_answer', NULL, 21),
(735, 16, 35, 'circle_the_answer', NULL, 22),
(736, 16, 64, 'multiple_choice', NULL, 2),
(737, 338, 37, 'slider', NULL, 3),
(738, 338, 69, 'expandable', NULL, 4),
(739, 338, 65, 'multiple_choice', NULL, 5),
(740, 338, 55, 'true_false', NULL, 6),
(741, 0, 437, 'start_lesson', 227, 1),
(742, 0, 439, 'start_lesson', 231, 1),
(743, 0, 441, 'start_lesson', 232, 1),
(744, 0, 443, 'start_lesson', 233, 1),
(745, 0, 445, 'start_lesson', 234, 1),
(746, 0, 447, 'start_lesson', 236, 1),
(747, 0, 449, 'start_lesson', 237, 1),
(748, 0, 451, 'start_lesson', 238, 1),
(749, 0, 453, 'start_lesson', 239, 1),
(750, 0, 455, 'start_lesson', 240, 1),
(751, 0, 11, 'ass_multiple_choice', 224, 6),
(752, 0, 457, 'start_lesson', 242, 1),
(753, 0, 459, 'start_lesson', 243, 1),
(754, 0, 461, 'start_lesson', 244, 1),
(755, 0, 463, 'start_lesson', 245, 1),
(756, 0, 465, 'start_lesson', 246, 1),
(757, 0, 12, 'ass_multiple_choice', 224, 5),
(758, 0, 13, 'ass_multiple_choice', 225, 0),
(759, 0, 26, 'free_text', 225, 12),
(760, 0, 27, 'free_text', 226, 13),
(761, 0, 14, 'ass_multiple_choice', 226, 0),
(762, 0, 28, 'free_text', 227, 14),
(763, 0, 29, 'free_text', 228, 15),
(770, 0, 15, 'ass_multiple_choice', 227, 0),
(771, 0, 30, 'free_text', 231, 16),
(772, 0, 31, 'free_text', 232, 17),
(773, 0, 32, 'free_text', 233, 18),
(776, 0, 475, 'start_lesson', 248, 1),
(777, 0, 477, 'start_lesson', 249, 1),
(778, 0, 479, 'start_lesson', 250, 1),
(779, 0, 481, 'start_lesson', 251, 1),
(780, 0, 483, 'start_lesson', 252, 1),
(781, 0, 33, 'free_text', 234, 19),
(786, 338, 53, 'video_slide', NULL, 7),
(787, 348, 100, 'scrollable', NULL, 0),
(790, 0, 501, 'start_lesson', 254, 1),
(791, 0, 503, 'start_lesson', 255, 1),
(792, 0, 505, 'start_lesson', 256, 1),
(793, 0, 507, 'start_lesson', 257, 1),
(794, 0, 509, 'start_lesson', 258, 1),
(795, 338, 102, 'scrollable', NULL, 0),
(796, 338, 24, 'crossword', NULL, 22),
(797, 338, 42, 'memory', NULL, 8),
(799, 338, 73, 'expandable', NULL, 9),
(800, 338, 103, 'scrollable', NULL, 11),
(802, 338, 18, 'reorder', NULL, 12),
(803, 0, 34, 'free_text', 0, 13),
(804, 0, 35, 'free_text', 0, 14),
(805, 338, 36, 'circle_the_answer', NULL, 13),
(808, 45, 70, 'multiple_choice', NULL, 1),
(809, 45, 71, 'multiple_choice', NULL, 2),
(810, 45, 72, 'multiple_choice', NULL, 3),
(818, 0, 37, 'free_text', 0, 15),
(819, 0, 45, 'free_text', 230, 2),
(822, 338, 104, 'scrollable', NULL, 14),
(823, 338, 75, 'expandable', NULL, 15),
(824, 338, 73, 'multiple_choice', NULL, 16),
(825, 338, 41, 'slider', NULL, 17),
(826, 338, 54, 'video_slide', NULL, 18),
(827, 338, 57, 'true_false', NULL, 19),
(828, 338, 26, 'crossword', NULL, 20),
(829, 338, 43, 'memory', NULL, 21),
(831, 338, 42, 'slider', NULL, 23),
(832, 338, 21, 'reorder', NULL, 10),
(833, 338, 22, 'reorder', NULL, 24),
(834, 338, 39, 'circle_the_answer', NULL, 25),
(836, 338, 41, 'circle_the_answer', NULL, 26),
(840, 338, 47, 'free_text', NULL, 27),
(841, 338, 48, 'free_text', NULL, 2),
(842, 338, 45, 'circle_the_answer', NULL, 28),
(850, 338, 76, 'expandable', NULL, 29),
(851, 0, 511, 'start_lesson', 260, 1),
(852, 0, 513, 'start_lesson', 261, 1),
(853, 0, 515, 'start_lesson', 262, 1),
(854, 0, 517, 'start_lesson', 263, 1),
(855, 0, 519, 'start_lesson', 264, 1),
(856, 0, 521, 'start_lesson', 267, 1),
(857, 0, 523, 'start_lesson', 268, 1),
(858, 0, 525, 'start_lesson', 269, 1),
(859, 0, 527, 'start_lesson', 270, 1),
(860, 0, 529, 'start_lesson', 271, 1),
(861, 0, 49, 'free_text', 271, 2),
(863, 0, 533, 'start_lesson', 274, 1),
(864, 0, 535, 'start_lesson', 275, 1),
(865, 0, 537, 'start_lesson', 276, 1),
(866, 0, 539, 'start_lesson', 277, 1),
(867, 0, 541, 'start_lesson', 278, 1),
(872, 0, 50, 'free_text', 278, 2),
(875, 354, 545, 'start_lesson', NULL, 1),
(876, 355, 547, 'start_lesson', NULL, 1),
(877, 356, 549, 'start_lesson', NULL, 1),
(878, 357, 551, 'start_lesson', NULL, 1),
(879, 356, 106, 'scrollable', NULL, 2),
(880, 338, 26, 'reorder', NULL, 30),
(881, 338, 27, 'crossword', NULL, 31),
(882, 338, 47, 'circle_the_answer', NULL, 32),
(883, 122, 58, 'true_false', NULL, 1),
(884, 122, 44, 'memory', NULL, 2),
(885, 123, 59, 'true_false', NULL, 1),
(886, 123, 78, 'expandable', NULL, 2),
(889, 122, 1, 'vimeo_video', NULL, 3),
(890, 327, 1, 'list_slide', NULL, 1),
(892, 338, 48, 'circle_the_answer', NULL, 33),
(893, 338, 51, 'free_text', NULL, 34),
(894, 338, 27, 'reorder', NULL, 35),
(896, 355, 3, 'list_slide', NULL, 2),
(897, 355, 79, 'expandable', NULL, 3),
(898, 355, 3, 'vimeo_video', NULL, 4),
(899, 0, 1, 'ass_expandable', 248, 2),
(900, 0, 553, 'start_lesson', 283, 1),
(901, 0, 2, 'ass_expandable', 283, 2),
(902, 0, 3, 'ass_expandable', 283, 3),
(903, 355, 52, 'free_text', NULL, 5),
(904, 0, 1, 'question_pool', 1, 1),
(907, 358, 555, 'start_lesson', NULL, 1),
(908, 0, 2, 'question_pool', 283, 4),
(909, 0, 3, 'question_pool', 283, 5),
(910, 0, 4, 'question_pool', 283, 6),
(911, 0, 53, 'free_text', 283, 7),
(912, 338, 43, 'slider', NULL, 36),
(913, 359, 557, 'start_lesson', NULL, 1),
(914, 347, 107, 'scrollable', NULL, 7),
(915, 338, 49, 'circle_the_answer', NULL, 37),
(916, 360, 559, 'start_lesson', NULL, 1),
(917, 360, 80, 'expandable', NULL, 2),
(918, 0, 561, 'start_lesson', 284, 1),
(919, 0, 563, 'start_lesson', 285, 1),
(920, 0, 565, 'start_lesson', 286, 1),
(921, 0, 567, 'start_lesson', 287, 1),
(922, 0, 569, 'start_lesson', 288, 1),
(923, 361, 571, 'start_lesson', NULL, 1),
(924, 361, 55, 'video_slide', NULL, 2),
(925, 361, 60, 'true_false', NULL, 3),
(926, 0, 573, 'start_lesson', 290, 1),
(927, 0, 575, 'start_lesson', 291, 1),
(928, 0, 577, 'start_lesson', 292, 1),
(929, 0, 579, 'start_lesson', 293, 1),
(930, 0, 581, 'start_lesson', 294, 1),
(935, 338, 24, 'peer_authoring', NULL, 38),
(936, 0, 54, 'free_text', 224, 2),
(937, 0, 4, 'ass_expandable', 252, 2),
(938, 0, 5, 'ass_expandable', 224, 3),
(939, 0, 5, 'question_pool', 224, 4),
(940, 0, 6, 'question_pool', 252, 3),
(941, 362, 583, 'start_lesson', NULL, 1),
(942, 362, 4, 'list_slide', NULL, 2),
(943, 362, 4, 'vimeo_video', NULL, 3),
(944, 362, 75, 'multiple_choice', NULL, 4),
(946, 338, 50, 'circle_the_answer', NULL, 39),
(947, 347, 5, 'list_slide', NULL, 10),
(948, 347, 5, 'vimeo_video', NULL, 13),
(949, 0, 6, 'ass_expandable', 248, 3),
(951, 348, 6, 'list_slide', NULL, 1),
(952, 348, 55, 'free_text', NULL, 2),
(953, 348, 76, 'multiple_choice', NULL, 3),
(954, 348, 85, 'expandable', NULL, 4),
(955, 0, 16, 'ass_multiple_choice', 1, 2),
(996, 348, 14, 'multiple_choice_image', NULL, 6),
(997, 0, 56, 'free_text', 1, 3),
(998, 0, 7, 'ass_expandable', 1, 4),
(999, 347, 77, 'multiple_choice', NULL, 3),
(1000, 347, 78, 'multiple_choice', NULL, 5),
(1001, 0, 8, 'ass_expandable', 1, 5),
(1002, 338, 7, 'list_slide', NULL, 40),
(1003, 338, 6, 'vimeo_video', NULL, 1),
(1018, 303, 57, 'video_slide', NULL, 1),
(1020, 0, 9, 'ass_expandable', 1, 6),
(1021, 0, 585, 'start_lesson', 296, 1),
(1022, 0, 587, 'start_lesson', 297, 1),
(1023, 0, 589, 'start_lesson', 298, 1),
(1024, 0, 591, 'start_lesson', 299, 1),
(1025, 0, 593, 'start_lesson', 300, 1),
(1026, 347, 15, 'multiple_choice_image', NULL, 4),
(1027, 348, 7, 'vimeo_video', NULL, 8),
(1028, 0, 595, 'start_lesson', 302, 1),
(1029, 0, 597, 'start_lesson', 303, 1),
(1030, 348, 58, 'video_slide', NULL, 9),
(1032, 362, 57, 'free_text', NULL, 5),
(1033, 362, 58, 'free_text', NULL, 6),
(1034, 362, 59, 'free_text', NULL, 7),
(1035, 362, 60, 'free_text', NULL, 8),
(1036, 362, 61, 'free_text', NULL, 9),
(1037, 362, 62, 'free_text', NULL, 10),
(1038, 362, 63, 'free_text', NULL, 11),
(1039, 0, 599, 'start_lesson', 304, 1),
(1045, 0, 65, 'free_text', 304, 2),
(1046, 0, 17, 'ass_multiple_choice', 304, 3),
(1047, 0, 10, 'ass_expandable', 304, 4),
(1048, 0, 7, 'question_pool', 304, 5),
(1049, 0, 603, 'start_lesson', 305, 1),
(1050, 0, 605, 'start_lesson', 306, 1),
(1051, 0, 607, 'start_lesson', 307, 1),
(1052, 0, 609, 'start_lesson', 308, 1),
(1053, 0, 611, 'start_lesson', 309, 1),
(1054, 0, 613, 'start_lesson', 310, 1),
(1055, 0, 615, 'start_lesson', 312, 1),
(1056, 0, 617, 'start_lesson', 313, 1),
(1057, 0, 619, 'start_lesson', 314, 1),
(1058, 0, 621, 'start_lesson', 315, 1),
(1059, 0, 623, 'start_lesson', 316, 1),
(1060, 0, 66, 'free_text', 248, 4),
(1061, 0, 18, 'ass_multiple_choice', 248, 5),
(1062, 0, 8, 'question_pool', 248, 6),
(1063, 0, 11, 'ass_expandable', 248, 7),
(1064, 0, 625, 'start_lesson', 318, 1),
(1065, 0, 627, 'start_lesson', 319, 1),
(1066, 0, 629, 'start_lesson', 320, 1),
(1067, 0, 631, 'start_lesson', 321, 1),
(1068, 0, 633, 'start_lesson', 322, 1),
(1069, 0, 635, 'start_lesson', 324, 1),
(1070, 0, 637, 'start_lesson', 325, 1),
(1071, 0, 639, 'start_lesson', 326, 1),
(1072, 0, 641, 'start_lesson', 327, 1),
(1073, 0, 643, 'start_lesson', 328, 1),
(1074, 364, 645, 'start_lesson', NULL, 1),
(1075, 364, 88, 'expandable', NULL, 2),
(1090, 364, 110, 'scrollable', NULL, 12),
(1091, 364, 44, 'slider', NULL, 13),
(1092, 364, 80, 'multiple_choice', NULL, 14),
(1093, 364, 60, 'video_slide', NULL, 15),
(1094, 364, 61, 'true_false', NULL, 16),
(1095, 364, 28, 'crossword', NULL, 17),
(1096, 364, 45, 'memory', NULL, 18),
(1097, 364, 16, 'multiple_choice_image', NULL, 19),
(1098, 364, 25, 'peer_authoring', NULL, 20),
(1099, 364, 28, 'reorder', NULL, 21),
(1100, 364, 12, 'drag_to_match', NULL, 22),
(1101, 364, 67, 'free_text', NULL, 23),
(1102, 364, 8, 'list_slide', NULL, 24),
(1103, 364, 8, 'vimeo_video', NULL, 25),
(1104, 0, 647, 'start_lesson', 330, 1),
(1105, 0, 649, 'start_lesson', 331, 1),
(1106, 0, 651, 'start_lesson', 332, 1),
(1107, 0, 653, 'start_lesson', 333, 1),
(1108, 0, 655, 'start_lesson', 334, 1),
(1109, 0, 657, 'start_lesson', 336, 1),
(1110, 0, 659, 'start_lesson', 337, 1),
(1111, 0, 661, 'start_lesson', 338, 1),
(1112, 0, 663, 'start_lesson', 339, 1),
(1113, 0, 665, 'start_lesson', 340, 1),
(1115, 365, 667, 'start_lesson', NULL, 1),
(1116, 0, 669, 'start_lesson', 342, 1),
(1117, 0, 671, 'start_lesson', 343, 1),
(1118, 0, 673, 'start_lesson', 344, 1),
(1119, 0, 675, 'start_lesson', 345, 1),
(1120, 0, 677, 'start_lesson', 346, 1),
(1121, 0, 679, 'start_lesson', 348, 1),
(1122, 0, 681, 'start_lesson', 349, 1),
(1123, 0, 683, 'start_lesson', 350, 1),
(1124, 0, 685, 'start_lesson', 351, 1),
(1125, 0, 687, 'start_lesson', 352, 1),
(1126, 0, 689, 'start_lesson', 354, 1),
(1127, 0, 691, 'start_lesson', 355, 1),
(1128, 0, 693, 'start_lesson', 356, 1),
(1129, 0, 695, 'start_lesson', 357, 1),
(1130, 0, 697, 'start_lesson', 358, 1),
(1131, 0, 699, 'start_lesson', 360, 1),
(1132, 0, 701, 'start_lesson', 361, 1),
(1133, 0, 703, 'start_lesson', 362, 1),
(1134, 0, 705, 'start_lesson', 363, 1),
(1135, 0, 707, 'start_lesson', 364, 1),
(1136, 0, 709, 'start_lesson', 366, 1),
(1137, 0, 711, 'start_lesson', 367, 1),
(1138, 0, 713, 'start_lesson', 368, 1),
(1139, 0, 715, 'start_lesson', 369, 1),
(1140, 0, 717, 'start_lesson', 370, 1),
(1141, 0, 12, 'ass_expandable', 1, 7),
(1142, 0, 719, 'start_lesson', 372, 1),
(1143, 0, 721, 'start_lesson', 373, 1),
(1144, 0, 723, 'start_lesson', 374, 1),
(1145, 0, 725, 'start_lesson', 375, 1),
(1146, 0, 727, 'start_lesson', 376, 1),
(1147, 0, 729, 'start_lesson', 378, 1),
(1148, 0, 731, 'start_lesson', 379, 1),
(1149, 0, 733, 'start_lesson', 380, 1),
(1150, 0, 735, 'start_lesson', 381, 1),
(1151, 0, 737, 'start_lesson', 382, 1),
(1152, 0, 739, 'start_lesson', 384, 1),
(1153, 0, 741, 'start_lesson', 385, 1),
(1154, 0, 743, 'start_lesson', 386, 1),
(1155, 0, 745, 'start_lesson', 387, 1),
(1156, 0, 747, 'start_lesson', 388, 1),
(1157, 0, 749, 'start_lesson', 390, 1),
(1158, 0, 751, 'start_lesson', 391, 1),
(1159, 0, 753, 'start_lesson', 392, 1),
(1160, 0, 755, 'start_lesson', 393, 1),
(1161, 0, 757, 'start_lesson', 394, 1),
(1162, 0, 759, 'start_lesson', 396, 1),
(1163, 0, 761, 'start_lesson', 397, 1),
(1164, 0, 763, 'start_lesson', 398, 1),
(1165, 0, 765, 'start_lesson', 399, 1),
(1166, 0, 767, 'start_lesson', 400, 1),
(1167, 0, 769, 'start_lesson', 402, 1),
(1168, 0, 771, 'start_lesson', 403, 1),
(1169, 0, 773, 'start_lesson', 404, 1),
(1170, 0, 775, 'start_lesson', 405, 1),
(1171, 0, 777, 'start_lesson', 406, 1),
(1172, 0, 779, 'start_lesson', 408, 1),
(1173, 0, 781, 'start_lesson', 409, 1),
(1174, 0, 783, 'start_lesson', 410, 1),
(1175, 0, 785, 'start_lesson', 411, 1),
(1176, 0, 787, 'start_lesson', 412, 1),
(1177, 0, 789, 'start_lesson', 414, 1),
(1178, 0, 791, 'start_lesson', 415, 1),
(1179, 0, 793, 'start_lesson', 416, 1),
(1180, 0, 795, 'start_lesson', 417, 1),
(1181, 0, 797, 'start_lesson', 418, 1),
(1182, 0, 799, 'start_lesson', 420, 1),
(1183, 0, 801, 'start_lesson', 421, 1),
(1184, 0, 803, 'start_lesson', 422, 1),
(1185, 0, 805, 'start_lesson', 423, 1),
(1186, 0, 807, 'start_lesson', 424, 1),
(1187, 0, 809, 'start_lesson', 426, 1),
(1188, 0, 811, 'start_lesson', 427, 1),
(1189, 0, 813, 'start_lesson', 428, 1),
(1190, 0, 815, 'start_lesson', 429, 1),
(1191, 0, 817, 'start_lesson', 430, 1),
(1192, 366, 819, 'start_lesson', NULL, 1),
(1193, 367, 821, 'start_lesson', NULL, 1),
(1194, 368, 823, 'start_lesson', NULL, 1),
(1195, 369, 825, 'start_lesson', NULL, 1),
(1196, 0, 827, 'start_lesson', 432, 1),
(1197, 0, 829, 'start_lesson', 433, 1),
(1198, 0, 831, 'start_lesson', 434, 1),
(1199, 0, 833, 'start_lesson', 435, 1),
(1200, 0, 835, 'start_lesson', 436, 1),
(1201, 0, 837, 'start_lesson', 438, 1),
(1202, 0, 839, 'start_lesson', 439, 1),
(1203, 0, 841, 'start_lesson', 440, 1),
(1204, 0, 843, 'start_lesson', 441, 1),
(1205, 0, 845, 'start_lesson', 442, 1),
(1206, 0, 847, 'start_lesson', 443, 1),
(1207, 0, 849, 'start_lesson', 445, 1),
(1208, 0, 851, 'start_lesson', 446, 1),
(1209, 0, 853, 'start_lesson', 447, 1),
(1210, 0, 855, 'start_lesson', 448, 1),
(1211, 0, 857, 'start_lesson', 449, 1),
(1212, 354, 112, 'scrollable', NULL, 2),
(1213, 354, 102, 'expandable', NULL, 3),
(1214, 354, 81, 'multiple_choice', NULL, 4),
(1215, 354, 45, 'slider', NULL, 5),
(1216, 354, 61, 'video_slide', NULL, 6),
(1217, 354, 62, 'true_false', NULL, 7),
(1219, 354, 46, 'memory', NULL, 8),
(1220, 354, 17, 'multiple_choice_image', NULL, 9),
(1221, 354, 68, 'free_text', NULL, 10),
(1222, 354, 9, 'vimeo_video', NULL, 11),
(1223, 354, 51, 'circle_the_answer', NULL, 12),
(1224, 354, 30, 'crossword', NULL, 13),
(1225, 370, 859, 'start_lesson', NULL, 1),
(1227, 371, 861, 'start_lesson', NULL, 1),
(1228, 372, 863, 'start_lesson', NULL, 1),
(1229, 373, 865, 'start_lesson', NULL, 1),
(1230, 374, 867, 'start_lesson', NULL, 1),
(1231, 375, 869, 'start_lesson', NULL, 1),
(1232, 376, 871, 'start_lesson', NULL, 1),
(1233, 0, 873, 'start_lesson', 451, 1),
(1234, 0, 875, 'start_lesson', 452, 1),
(1235, 0, 877, 'start_lesson', 453, 1),
(1236, 0, 879, 'start_lesson', 454, 1),
(1237, 0, 881, 'start_lesson', 455, 1),
(1238, 0, 883, 'start_lesson', 457, 1),
(1239, 0, 885, 'start_lesson', 458, 1),
(1240, 0, 887, 'start_lesson', 459, 1),
(1241, 0, 889, 'start_lesson', 460, 1),
(1242, 0, 891, 'start_lesson', 461, 1),
(1243, 0, 893, 'start_lesson', 462, 1),
(1244, 0, 895, 'start_lesson', 464, 1),
(1245, 0, 897, 'start_lesson', 465, 1),
(1246, 0, 899, 'start_lesson', 466, 1),
(1247, 0, 901, 'start_lesson', 467, 1),
(1248, 0, 903, 'start_lesson', 468, 1),
(1249, 0, 9, 'question_pool', 1, 8),
(1250, 377, 905, 'start_lesson', NULL, 1),
(1252, 362, 13, 'drag_to_match', NULL, 12),
(1253, 376, 14, 'drag_to_match', NULL, 2),
(1254, 347, 63, 'true_false', NULL, 12),
(1256, 376, 30, 'reorder', NULL, 3),
(1257, 376, 26, 'peer_authoring', NULL, 4),
(1258, 347, 27, 'peer_authoring', NULL, 11),
(1259, 377, 909, 'start_lesson', NULL, 1),
(1260, 0, 10, 'question_pool', 226, 14),
(1261, 0, 13, 'ass_expandable', 226, 15),
(1263, 377, 47, 'memory', NULL, 2),
(1264, 0, 913, 'start_lesson', 458, 1),
(1265, 0, 915, 'start_lesson', 459, 1),
(1266, 0, 917, 'start_lesson', 460, 1),
(1267, 0, 919, 'start_lesson', 461, 1),
(1268, 0, 921, 'start_lesson', 462, 1),
(1269, 379, 923, 'start_lesson', NULL, 1),
(1270, 380, 925, 'start_lesson', NULL, 1),
(1271, 380, 48, 'memory', NULL, 2),
(1272, 0, 14, 'ass_expandable', 458, 2),
(1273, 377, 64, 'true_false', NULL, 3),
(1274, 347, 49, 'memory', NULL, 0),
(1275, 381, 927, 'start_lesson', NULL, 1),
(1278, 383, 931, 'start_lesson', NULL, 1),
(1279, 383, 31, 'reorder', NULL, 2),
(1280, 383, 65, 'true_false', NULL, 3),
(1281, 383, 82, 'multiple_choice', NULL, 4),
(1282, 0, 933, 'start_lesson', 464, 1),
(1283, 384, 935, 'start_lesson', NULL, 1),
(1284, 0, 937, 'start_lesson', 465, 1),
(1285, 0, 939, 'start_lesson', 466, 1),
(1286, 0, 941, 'start_lesson', 467, 1),
(1287, 0, 943, 'start_lesson', 468, 1),
(1288, 0, 945, 'start_lesson', 469, 1),
(1289, 0, 947, 'start_lesson', 470, 1),
(1290, 385, 949, 'start_lesson', NULL, 1),
(1291, 0, 15, 'ass_expandable', 470, 2),
(1292, 0, 951, 'start_lesson', 471, 1),
(1293, 0, 953, 'start_lesson', 472, 1),
(1294, 0, 955, 'start_lesson', 473, 1),
(1295, 380, 113, 'scrollable', NULL, 3),
(1296, 380, 104, 'expandable', NULL, 4),
(1297, 380, 46, 'slider', NULL, 5),
(1298, 0, 957, 'start_lesson', 474, 1),
(1300, 0, 961, 'start_lesson', 475, 1),
(1301, 0, 11, 'question_pool', 475, 2),
(1302, 0, 16, 'ass_expandable', 475, 3),
(1303, 0, 963, 'start_lesson', 476, 1),
(1304, 0, 17, 'ass_expandable', 476, 2),
(1305, 0, 965, 'start_lesson', 477, 1),
(1306, 0, 19, 'ass_multiple_choice', 477, 2),
(1307, 0, 967, 'start_lesson', 478, 1),
(1308, 0, 69, 'free_text', 478, 2),
(1309, 0, 969, 'start_lesson', 479, 1),
(1310, 0, 971, 'start_lesson', 480, 1),
(1311, 0, 973, 'start_lesson', 481, 1),
(1312, 0, 975, 'start_lesson', 482, 1),
(1313, 0, 977, 'start_lesson', 483, 1),
(1314, 0, 70, 'free_text', 483, 2),
(1315, 0, 979, 'start_lesson', 484, 1),
(1316, 0, 18, 'ass_expandable', 483, 3),
(1317, 0, 20, 'ass_multiple_choice', 477, 3),
(1318, 0, 21, 'ass_multiple_choice', 477, 4),
(1319, 387, 981, 'start_lesson', NULL, 1),
(1320, 0, 983, 'start_lesson', 485, 1),
(1321, 387, 114, 'scrollable', NULL, 2),
(1322, 387, 115, 'scrollable', NULL, 3),
(1323, 387, 47, 'slider', NULL, 4),
(1324, 387, 83, 'multiple_choice', NULL, 5),
(1325, 387, 66, 'true_false', NULL, 6),
(1327, 0, 985, 'start_lesson', 486, 1),
(1328, 388, 987, 'start_lesson', NULL, 0),
(1329, 0, 989, 'start_lesson', 487, 1),
(1330, 0, 991, 'start_lesson', 488, 1),
(1331, 0, 993, 'start_lesson', 489, 1),
(1332, 0, 19, 'ass_expandable', 489, 2),
(1333, 0, 71, 'free_text', 488, 2),
(1334, 0, 72, 'free_text', 478, 3),
(1335, 0, 22, 'ass_multiple_choice', 478, 4),
(1336, 0, 995, 'start_lesson', 490, 1),
(1337, 0, 23, 'ass_multiple_choice', 490, 2),
(1338, 0, 73, 'free_text', 490, 3),
(1339, 0, 20, 'ass_expandable', 490, 4),
(1340, 0, 12, 'question_pool', 490, 5),
(1341, 0, 24, 'ass_multiple_choice', 458, 3),
(1342, 389, 997, 'start_lesson', NULL, 1),
(1343, 389, 84, 'multiple_choice', NULL, 2),
(1344, 389, 62, 'video_slide', NULL, 3),
(1345, 389, 105, 'expandable', NULL, 4),
(1346, 0, 13, 'question_pool', 458, 4),
(1347, 387, 63, 'video_slide', NULL, 8),
(1348, 388, 85, 'multiple_choice', NULL, 1),
(1355, 347, 74, 'free_text', NULL, 9),
(1358, 387, 75, 'free_text', NULL, 17),
(1359, 387, 9, 'list_slide', NULL, 18),
(1362, 388, 106, 'expandable', NULL, 2),
(1369, 388, 76, 'free_text', NULL, 3),
(1370, 387, 57, 'memory', NULL, 27),
(1371, 388, 116, 'scrollable', NULL, 4),
(1374, 387, 19, 'multiple_choice_image', NULL, 29),
(1375, 388, 48, 'slider', NULL, 5),
(1376, 348, 49, 'slider', NULL, 11),
(1379, 387, 32, 'reorder', NULL, 30),
(1380, 388, 67, 'true_false', NULL, 6),
(1381, 388, 65, 'video_slide', NULL, 7),
(1383, 0, 25, 'ass_multiple_choice', 488, 3),
(1384, 0, 999, 'start_lesson', 491, 1),
(1385, 0, 77, 'free_text', 491, 2),
(1386, 0, 78, 'free_text', 491, 3),
(1387, 0, 21, 'ass_expandable', 491, 4),
(1388, 0, 26, 'ass_multiple_choice', 491, 5),
(1389, 0, 27, 'ass_multiple_choice', 491, 6),
(1390, 380, 86, 'multiple_choice', NULL, 6),
(1391, 388, 33, 'crossword', NULL, 9),
(1392, 388, 60, 'memory', NULL, 10),
(1393, 388, 20, 'multiple_choice_image', NULL, 11),
(1394, 388, 68, 'true_false', NULL, 12),
(1395, 388, 21, 'multiple_choice_image', NULL, 13),
(1398, 388, 33, 'reorder', NULL, 14),
(1399, 388, 15, 'drag_to_match', NULL, 15),
(1400, 388, 16, 'drag_to_match', NULL, 16),
(1401, 388, 87, 'multiple_choice', NULL, 17),
(1402, 388, 10, 'list_slide', NULL, 18),
(1403, 388, 11, 'vimeo_video', NULL, 19),
(1404, 392, 1005, 'start_lesson', NULL, 1),
(1405, 392, 88, 'multiple_choice', NULL, 2),
(1406, 392, 109, 'expandable', NULL, 3),
(1407, 0, 14, 'question_pool', 488, 4),
(1408, 0, 15, 'question_pool', 224, 7),
(1409, 0, 1007, 'start_lesson', 492, 1),
(1410, 393, 1009, 'start_lesson', NULL, 1),
(1411, 0, 16, 'question_pool', 492, 2),
(1412, 0, 1011, 'start_lesson', 493, 1),
(1413, 0, 1013, 'start_lesson', 494, 1),
(1414, 379, 89, 'multiple_choice', NULL, 2),
(1415, 379, 50, 'slider', NULL, 3),
(1416, 0, 28, 'ass_multiple_choice', 459, 2),
(1417, 0, 1015, 'start_lesson', 495, 1),
(1418, 394, 1017, 'start_lesson', NULL, 0),
(1419, 394, 90, 'multiple_choice', NULL, 3),
(1421, 0, 29, 'ass_multiple_choice', 495, 2),
(1422, 0, 17, 'question_pool', 495, 3),
(1424, 0, 22, 'ass_expandable', 478, 5),
(1425, 0, 30, 'ass_multiple_choice', 324, 2),
(1426, 0, 18, 'question_pool', 324, 3),
(1427, 396, 1021, 'start_lesson', NULL, 0),
(1428, 396, 117, 'scrollable', NULL, 1),
(1429, 396, 110, 'expandable', NULL, 2),
(1430, 396, 91, 'multiple_choice', NULL, 3),
(1431, 396, 52, 'slider', NULL, 4),
(1432, 396, 66, 'video_slide', NULL, 5),
(1433, 396, 69, 'true_false', NULL, 6),
(1434, 396, 34, 'crossword', NULL, 7),
(1435, 396, 61, 'memory', NULL, 8),
(1436, 396, 22, 'multiple_choice_image', NULL, 9),
(1437, 396, 28, 'peer_authoring', NULL, 10),
(1438, 396, 34, 'reorder', NULL, 13),
(1439, 396, 17, 'drag_to_match', NULL, 11),
(1440, 396, 79, 'free_text', NULL, 15),
(1441, 396, 11, 'list_slide', NULL, 12),
(1442, 396, 12, 'vimeo_video', NULL, 14),
(1443, 348, 18, 'drag_to_match', NULL, 12),
(1444, 348, 19, 'drag_to_match', NULL, 13),
(1445, 365, 118, 'scrollable', NULL, 2),
(1446, 365, 111, 'expandable', NULL, 3),
(1447, 365, 92, 'multiple_choice', NULL, 4),
(1448, 365, 53, 'slider', NULL, 5),
(1449, 365, 67, 'video_slide', NULL, 6),
(1450, 365, 70, 'true_false', NULL, 7),
(1451, 365, 35, 'crossword', NULL, 8),
(1452, 365, 62, 'memory', NULL, 9),
(1453, 365, 23, 'multiple_choice_image', NULL, 10),
(1454, 365, 29, 'peer_authoring', NULL, 11),
(1455, 365, 35, 'reorder', NULL, 12),
(1456, 365, 20, 'drag_to_match', NULL, 13),
(1457, 365, 80, 'free_text', NULL, 14),
(1458, 365, 12, 'list_slide', NULL, 15),
(1459, 365, 13, 'vimeo_video', NULL, 16),
(1460, 347, 54, 'slider', NULL, 6),
(1461, 347, 21, 'drag_to_match', NULL, 8),
(1462, 0, 1023, 'start_lesson', 496, 1),
(1463, 0, 81, 'free_text', 495, 4),
(1465, 397, 1025, 'start_lesson', NULL, 0),
(1466, 397, 22, 'drag_to_match', NULL, 2),
(1467, 397, 30, 'peer_authoring', NULL, 1),
(1468, 0, 1027, 'start_lesson', 497, 1),
(1470, 0, 82, 'free_text', 497, 2),
(1471, 0, 23, 'ass_expandable', 497, 3),
(1472, 0, 19, 'question_pool', 497, 4),
(1473, 0, 1031, 'start_lesson', 498, 1),
(1474, 0, 1033, 'start_lesson', 499, 1),
(1475, 0, 1035, 'start_lesson', 500, 1),
(1476, 0, 1037, 'start_lesson', 501, 1),
(1477, 0, 1039, 'start_lesson', 502, 1),
(1478, 0, 1041, 'start_lesson', 503, 1),
(1479, 0, 1043, 'start_lesson', 504, 1),
(1480, 0, 1045, 'start_lesson', 505, 1),
(1481, 0, 1047, 'start_lesson', 506, 1),
(1482, 0, 1049, 'start_lesson', 507, 1),
(1483, 0, 24, 'ass_expandable', 487, 2),
(1484, 0, 1051, 'start_lesson', 508, 1),
(1485, 0, 1053, 'start_lesson', 509, 1),
(1486, 0, 1055, 'start_lesson', 510, 1),
(1503, 0, 84, 'free_text', 510, 2),
(1504, 0, 31, 'ass_multiple_choice', 510, 3),
(1505, 0, 25, 'ass_expandable', 510, 4),
(1506, 0, 20, 'question_pool', 510, 5),
(1507, 0, 1059, 'start_lesson', 511, 1),
(1508, 400, 1061, 'start_lesson', NULL, 1),
(1509, 0, 26, 'ass_expandable', 511, 2),
(1510, 0, 1063, 'start_lesson', 512, 1),
(1511, 0, 85, 'free_text', 512, 2),
(1512, 0, 32, 'ass_multiple_choice', 512, 3),
(1513, 0, 27, 'ass_expandable', 512, 4),
(1514, 0, 21, 'question_pool', 512, 5),
(1515, 0, 1065, 'start_lesson', 513, 1),
(1516, 0, 28, 'ass_expandable', 513, 2),
(1517, 401, 1067, 'start_lesson', NULL, 0),
(1518, 401, 120, 'scrollable', NULL, 2),
(1519, 401, 113, 'expandable', NULL, 1),
(1520, 401, 94, 'multiple_choice', NULL, 3),
(1521, 401, 56, 'slider', NULL, 5),
(1522, 401, 69, 'video_slide', NULL, 6),
(1523, 401, 72, 'true_false', NULL, 7),
(1524, 401, 37, 'crossword', NULL, 8),
(1525, 401, 64, 'memory', NULL, 9),
(1526, 401, 25, 'multiple_choice_image', NULL, 10),
(1527, 401, 32, 'peer_authoring', NULL, 4),
(1528, 401, 37, 'reorder', NULL, 11),
(1529, 401, 24, 'drag_to_match', NULL, 12),
(1530, 401, 86, 'free_text', NULL, 13),
(1531, 401, 14, 'list_slide', NULL, 14),
(1532, 401, 15, 'vimeo_video', NULL, 15),
(1533, 388, 57, 'circle_the_answer', NULL, 20),
(1534, 400, 121, 'scrollable', NULL, 2),
(1535, 400, 65, 'memory', NULL, 3),
(1536, 0, 1069, 'start_lesson', 514, 1),
(1537, 402, 1071, 'start_lesson', NULL, 1),
(1538, 402, 122, 'scrollable', NULL, 2),
(1539, 402, 114, 'expandable', NULL, 3),
(1540, 402, 95, 'multiple_choice', NULL, 4),
(1541, 402, 57, 'slider', NULL, 5),
(1542, 402, 73, 'true_false', NULL, 6),
(1543, 402, 58, 'slider', NULL, 7),
(1544, 402, 26, 'multiple_choice_image', NULL, 8),
(1546, 402, 38, 'reorder', NULL, 9),
(1547, 402, 87, 'free_text', NULL, 10),
(1548, 402, 16, 'vimeo_video', NULL, 11),
(1549, 402, 15, 'list_slide', NULL, 12),
(1550, 0, 88, 'free_text', 514, 2),
(1551, 0, 29, 'ass_expandable', 514, 3),
(1552, 0, 22, 'question_pool', 514, 4),
(1553, 0, 1073, 'start_lesson', 515, 1),
(1554, 0, 1075, 'start_lesson', 516, 1),
(1555, 403, 1077, 'start_lesson', NULL, 1),
(1556, 403, 123, 'scrollable', NULL, 2),
(1559, 403, 115, 'expandable', NULL, 3),
(1560, 403, 96, 'multiple_choice', NULL, 4),
(1561, 403, 74, 'true_false', NULL, 5),
(1562, 403, 59, 'slider', NULL, 6),
(1563, 403, 27, 'multiple_choice_image', NULL, 7),
(1564, 403, 70, 'video_slide', NULL, 8),
(1565, 403, 39, 'reorder', NULL, 9),
(1566, 403, 33, 'peer_authoring', NULL, 10),
(1567, 403, 34, 'peer_authoring', NULL, 11),
(1568, 403, 17, 'vimeo_video', NULL, 12),
(1569, 403, 16, 'list_slide', NULL, 13),
(1570, 403, 89, 'free_text', NULL, 14),
(1571, 0, 30, 'ass_expandable', 516, 2),
(1572, 0, 23, 'question_pool', 516, 3),
(1573, 0, 90, 'free_text', 516, 4),
(1574, 0, 1079, 'start_lesson', 517, 1),
(1576, 0, 91, 'free_text', 517, 2),
(1577, 0, 31, 'ass_expandable', 517, 3),
(1578, 0, 24, 'question_pool', 517, 4),
(1579, 0, 1083, 'start_lesson', 518, 1),
(1580, 405, 1085, 'start_lesson', NULL, 1),
(1581, 0, 92, 'free_text', 518, 2),
(1582, 0, 32, 'ass_expandable', 518, 3),
(1583, 0, 25, 'question_pool', 518, 4),
(1584, 347, 39, 'crossword', NULL, 2),
(1585, 347, 71, 'video_slide', NULL, 1),
(1586, 406, 1087, 'start_lesson', NULL, 1),
(1587, 394, 40, 'reorder', NULL, 2),
(1588, 407, 1089, 'start_lesson', NULL, 1),
(1589, 0, 1091, 'start_lesson', 519, 1),
(1590, 408, 1093, 'start_lesson', NULL, 1),
(1591, 0, 1095, 'start_lesson', 520, 1),
(1592, 409, 1097, 'start_lesson', NULL, 0),
(1593, 0, 33, 'ass_multiple_choice', 520, 2),
(1594, 0, 93, 'free_text', 520, 3),
(1595, 0, 33, 'ass_expandable', 520, 4),
(1596, 0, 26, 'question_pool', 520, 5),
(1597, 348, 97, 'multiple_choice', NULL, 14),
(1598, 348, 98, 'multiple_choice', NULL, 15),
(1599, 348, 99, 'multiple_choice', NULL, 16),
(1600, 0, 1099, 'start_lesson', 521, 1),
(1601, 410, 1101, 'start_lesson', NULL, 1),
(1602, 410, 41, 'reorder', NULL, 2),
(1603, 410, 66, 'memory', NULL, 3),
(1604, 0, 94, 'free_text', 521, 2),
(1605, 410, 60, 'slider', NULL, 4),
(1606, 0, 1103, 'start_lesson', 522, 1),
(1607, 0, 1105, 'start_lesson', 523, 1),
(1609, 412, 1109, 'start_lesson', NULL, 1),
(1612, 409, 126, 'scrollable', NULL, 5),
(1613, 409, 102, 'multiple_choice', NULL, 6),
(1624, 409, 77, 'true_false', NULL, 4),
(1625, 409, 78, 'true_false', NULL, 3),
(1626, 409, 44, 'reorder', NULL, 2),
(1627, 409, 45, 'reorder', NULL, 1),
(1628, 0, 1111, 'start_lesson', 524, 1),
(1629, 413, 1113, 'start_lesson', NULL, 1),
(1630, 0, 34, 'ass_multiple_choice', 519, 2),
(1631, 0, 35, 'ass_multiple_choice', 519, 3),
(1632, 0, 95, 'free_text', 519, 4),
(1633, 0, 96, 'free_text', 519, 5),
(1634, 0, 34, 'ass_expandable', 519, 6),
(1635, 0, 35, 'ass_expandable', 519, 7),
(1636, 0, 36, 'ass_multiple_choice', 524, 2),
(1637, 0, 37, 'ass_multiple_choice', 524, 3),
(1638, 0, 38, 'ass_multiple_choice', 524, 4),
(1639, 0, 39, 'ass_multiple_choice', 524, 5),
(1640, 0, 40, 'ass_multiple_choice', 524, 6),
(1641, 0, 41, 'ass_multiple_choice', 524, 7),
(1642, 0, 42, 'ass_multiple_choice', 524, 8),
(1643, 0, 43, 'ass_multiple_choice', 524, 9),
(1644, 0, 44, 'ass_multiple_choice', 524, 10),
(1645, 0, 45, 'ass_multiple_choice', 524, 11),
(1646, 0, 46, 'ass_multiple_choice', 524, 12),
(1647, 0, 47, 'ass_multiple_choice', 524, 13),
(1648, 0, 48, 'ass_multiple_choice', 524, 14),
(1649, 0, 49, 'ass_multiple_choice', 524, 15),
(1650, 0, 50, 'ass_multiple_choice', 524, 16),
(1651, 0, 51, 'ass_multiple_choice', 524, 17),
(1652, 0, 52, 'ass_multiple_choice', 524, 18),
(1653, 0, 53, 'ass_multiple_choice', 524, 19),
(1654, 0, 54, 'ass_multiple_choice', 524, 20),
(1655, 0, 55, 'ass_multiple_choice', 524, 21),
(1656, 0, 56, 'ass_multiple_choice', 524, 22),
(1657, 0, 57, 'ass_multiple_choice', 524, 23),
(1658, 0, 58, 'ass_multiple_choice', 524, 24),
(1659, 0, 59, 'ass_multiple_choice', 524, 25),
(1660, 0, 60, 'ass_multiple_choice', 524, 26),
(1661, 0, 61, 'ass_multiple_choice', 524, 27),
(1662, 0, 62, 'ass_multiple_choice', 524, 28),
(1663, 0, 63, 'ass_multiple_choice', 524, 29),
(1664, 0, 64, 'ass_multiple_choice', 524, 30),
(1665, 0, 65, 'ass_multiple_choice', 524, 31),
(1666, 0, 66, 'ass_multiple_choice', 524, 32),
(1667, 0, 67, 'ass_multiple_choice', 524, 33),
(1668, 0, 68, 'ass_multiple_choice', 524, 34),
(1669, 0, 69, 'ass_multiple_choice', 524, 35),
(1670, 0, 70, 'ass_multiple_choice', 524, 36),
(1671, 0, 71, 'ass_multiple_choice', 524, 37),
(1672, 0, 72, 'ass_multiple_choice', 524, 38),
(1673, 0, 73, 'ass_multiple_choice', 524, 39),
(1674, 0, 74, 'ass_multiple_choice', 524, 40),
(1675, 0, 75, 'ass_multiple_choice', 524, 41),
(1676, 0, 76, 'ass_multiple_choice', 524, 42),
(1677, 0, 77, 'ass_multiple_choice', 524, 43),
(1678, 0, 78, 'ass_multiple_choice', 524, 44),
(1679, 0, 79, 'ass_multiple_choice', 524, 45),
(1680, 0, 80, 'ass_multiple_choice', 524, 46),
(1681, 0, 81, 'ass_multiple_choice', 524, 47),
(1682, 0, 82, 'ass_multiple_choice', 524, 48),
(1683, 0, 83, 'ass_multiple_choice', 524, 49),
(1684, 0, 84, 'ass_multiple_choice', 524, 50),
(1685, 0, 85, 'ass_multiple_choice', 524, 51),
(1686, 413, 104, 'multiple_choice', NULL, 2),
(1687, 413, 105, 'multiple_choice', NULL, 3),
(1688, 413, 106, 'multiple_choice', NULL, 4),
(1689, 0, 1115, 'start_lesson', 525, 1),
(1690, 0, 86, 'ass_multiple_choice', 525, 2),
(1691, 0, 87, 'ass_multiple_choice', 525, 3),
(1692, 0, 88, 'ass_multiple_choice', 525, 4),
(1693, 0, 89, 'ass_multiple_choice', 525, 5),
(1694, 0, 90, 'ass_multiple_choice', 525, 6),
(1695, 0, 91, 'ass_multiple_choice', 525, 7),
(1696, 414, 1117, 'start_lesson', NULL, 1),
(1697, 414, 97, 'free_text', NULL, 2),
(1698, 0, 1119, 'start_lesson', 526, 1),
(1699, 415, 1121, 'start_lesson', NULL, 1),
(1700, 415, 79, 'true_false', NULL, 2),
(1701, 415, 17, 'list_slide', NULL, 3),
(1702, 0, 92, 'ass_multiple_choice', 526, 2),
(1703, 0, 93, 'ass_multiple_choice', 526, 3),
(1704, 0, 27, 'question_pool', 526, 4),
(1705, 0, 28, 'question_pool', 526, 5),
(1707, 0, 94, 'ass_multiple_choice', 526, 6),
(1708, 0, 95, 'ass_multiple_choice', 526, 7),
(1709, 0, 96, 'ass_multiple_choice', 526, 8),
(1710, 0, 97, 'ass_multiple_choice', 526, 9),
(1711, 0, 98, 'ass_multiple_choice', 526, 10),
(1712, 0, 99, 'ass_multiple_choice', 526, 11),
(1713, 415, 63, 'slider', NULL, 4),
(1714, 394, 107, 'multiple_choice', NULL, 4),
(1715, 394, 72, 'video_slide', NULL, 1),
(1716, 394, 116, 'expandable', NULL, 6),
(1717, 394, 127, 'scrollable', NULL, 7),
(1718, 0, 1125, 'start_lesson', 527, 1),
(1719, 417, 1127, 'start_lesson', NULL, 1),
(1720, 0, 100, 'ass_multiple_choice', 527, 2),
(1721, 0, 36, 'ass_expandable', 527, 3),
(1722, 0, 98, 'free_text', 527, 4),
(1723, 0, 29, 'question_pool', 527, 5),
(1724, 0, 1129, 'start_lesson', 528, 1),
(1725, 0, 101, 'ass_multiple_choice', 528, 2),
(1726, 0, 99, 'free_text', 528, 3),
(1727, 418, 1131, 'start_lesson', NULL, 1),
(1728, 418, 117, 'expandable', NULL, 2),
(1729, 418, 128, 'scrollable', NULL, 3),
(1730, 0, 1133, 'start_lesson', 529, 1),
(1731, 0, 100, 'free_text', 529, 2),
(1732, 0, 102, 'ass_multiple_choice', 529, 3),
(1733, 0, 37, 'ass_expandable', 529, 4),
(1734, 0, 30, 'question_pool', 529, 5),
(1735, 419, 1135, 'start_lesson', NULL, 1),
(1736, 419, 129, 'scrollable', NULL, 2),
(1737, 419, 118, 'expandable', NULL, 3),
(1738, 419, 108, 'multiple_choice', NULL, 4),
(1739, 419, 73, 'video_slide', NULL, 5),
(1740, 419, 80, 'true_false', NULL, 6),
(1741, 419, 30, 'multiple_choice_image', NULL, 7),
(1742, 419, 31, 'multiple_choice_image', NULL, 8),
(1744, 0, 1137, 'start_lesson', 530, 1),
(1745, 420, 1139, 'start_lesson', NULL, 1),
(1746, 420, 119, 'expandable', NULL, 2),
(1747, 420, 109, 'multiple_choice', NULL, 3),
(1748, 0, 103, 'ass_multiple_choice', 530, 2),
(1749, 0, 101, 'free_text', 530, 3),
(1750, 0, 1141, 'start_lesson', 531, 1),
(1751, 421, 1143, 'start_lesson', NULL, 1),
(1752, 0, 102, 'free_text', 531, 2),
(1753, 0, 103, 'free_text', 531, 3),
(1754, 0, 104, 'ass_multiple_choice', 531, 4),
(1755, 0, 38, 'ass_expandable', 531, 5),
(1756, 0, 31, 'question_pool', 531, 6),
(1757, 0, 1145, 'start_lesson', 532, 1),
(1758, 422, 1147, 'start_lesson', NULL, 1),
(1759, 422, 110, 'multiple_choice', NULL, 2),
(1760, 422, 120, 'expandable', NULL, 3),
(1761, 422, 130, 'scrollable', NULL, 4),
(1762, 422, 74, 'video_slide', NULL, 5),
(1763, 422, 18, 'list_slide', NULL, 6),
(1764, 0, 105, 'ass_multiple_choice', 532, 2),
(1765, 0, 106, 'ass_multiple_choice', 532, 3),
(1766, 0, 1149, 'start_lesson', 533, 1),
(1767, 423, 1151, 'start_lesson', NULL, 1),
(1768, 0, 107, 'ass_multiple_choice', 533, 2),
(1769, 0, 1153, 'start_lesson', 534, 1),
(1770, 424, 1155, 'start_lesson', NULL, 1),
(1771, 0, 108, 'ass_multiple_choice', 534, 2),
(1772, 0, 1157, 'start_lesson', 535, 1),
(1773, 425, 1159, 'start_lesson', NULL, 1),
(1774, 0, 109, 'ass_multiple_choice', 535, 2),
(1775, 0, 1161, 'start_lesson', 536, 1),
(1776, 426, 1163, 'start_lesson', NULL, 1),
(1777, 0, 1165, 'start_lesson', 537, 1),
(1778, 427, 1167, 'start_lesson', NULL, 1),
(1779, 0, 110, 'ass_multiple_choice', 536, 2),
(1780, 0, 39, 'ass_expandable', 536, 3),
(1781, 0, 32, 'question_pool', 536, 4),
(1782, 0, 104, 'free_text', 536, 5),
(1783, 0, 40, 'ass_expandable', 537, 2),
(1784, 423, 131, 'scrollable', NULL, 2),
(1785, 426, 132, 'scrollable', NULL, 2),
(1786, 424, 111, 'multiple_choice', NULL, 2),
(1787, 426, 133, 'scrollable', NULL, 3),
(1788, 425, 75, 'video_slide', NULL, 2),
(1789, 427, 134, 'scrollable', NULL, 2),
(1790, 0, 1169, 'start_lesson', 538, 1),
(1791, 428, 1171, 'start_lesson', NULL, 1),
(1792, 428, 135, 'scrollable', NULL, 2),
(1793, 0, 33, 'question_pool', 538, 2),
(1794, 0, 1173, 'start_lesson', 539, 1),
(1795, 429, 1175, 'start_lesson', NULL, 1),
(1796, 0, 34, 'question_pool', 539, 2),
(1797, 0, 35, 'question_pool', 533, 3),
(1798, 0, 1177, 'start_lesson', 540, 1),
(1799, 430, 1179, 'start_lesson', NULL, 1),
(1800, 430, 112, 'multiple_choice', NULL, 2),
(1801, 0, 36, 'question_pool', 540, 2),
(1802, 0, 1181, 'start_lesson', 541, 1),
(1803, 431, 1183, 'start_lesson', NULL, 1),
(1804, 0, 37, 'question_pool', 541, 2),
(1805, 0, 38, 'question_pool', 541, 3),
(1806, 431, 136, 'scrollable', NULL, 2),
(1807, 429, 121, 'expandable', NULL, 2),
(1808, 0, 1185, 'start_lesson', 542, 1),
(1809, 432, 1187, 'start_lesson', NULL, 1),
(1810, 432, 137, 'scrollable', NULL, 2),
(1811, 0, 105, 'free_text', 542, 2),
(1812, 0, 111, 'ass_multiple_choice', 542, 3),
(1813, 0, 41, 'ass_expandable', 542, 4),
(1814, 0, 39, 'question_pool', 542, 5),
(1815, 0, 1189, 'start_lesson', 543, 1),
(1816, 0, 40, 'question_pool', 543, 2),
(1817, 0, 41, 'question_pool', 543, 3),
(1818, 433, 1191, 'start_lesson', NULL, 1),
(1819, 0, 1193, 'start_lesson', 544, 1),
(1820, 434, 1195, 'start_lesson', NULL, 1),
(1821, 0, 112, 'ass_multiple_choice', 544, 2),
(1822, 0, 42, 'ass_expandable', 544, 3),
(1823, 0, 1197, 'start_lesson', 545, 1),
(1824, 435, 1199, 'start_lesson', NULL, 1),
(1825, 435, 81, 'true_false', NULL, 2),
(1826, 0, 43, 'ass_expandable', 545, 2),
(1827, 0, 44, 'ass_expandable', 545, 3),
(1828, 0, 45, 'ass_expandable', 545, 4),
(1829, 0, 1201, 'start_lesson', 546, 1),
(1830, 0, 1203, 'start_lesson', 547, 1),
(1831, 436, 1205, 'start_lesson', NULL, 1),
(1832, 436, 122, 'expandable', NULL, 2),
(1833, 394, 64, 'slider', NULL, 8),
(1834, 394, 32, 'multiple_choice_image', NULL, 9),
(1835, 0, 1207, 'start_lesson', 548, 1),
(1836, 437, 1209, 'start_lesson', NULL, 1),
(1837, 437, 138, 'scrollable', NULL, 2),
(1838, 0, 113, 'ass_multiple_choice', 548, 2),
(1839, 0, 46, 'ass_expandable', 548, 3);

-- --------------------------------------------------------

--
-- Table structure for table `list_slide`
--

CREATE TABLE `list_slide` (
  `l_id` int(11) NOT NULL,
  `title` varchar(256) NOT NULL,
  `lesson_id` int(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `list_slide`
--

INSERT INTO `list_slide` (`l_id`, `title`, `lesson_id`) VALUES
(1, 'List slide ex', 327),
(3, 'A List Slide', 355),
(4, 'A List Slide', 362),
(5, 'A List Slide', 347),
(6, 'A List Slide', 348),
(7, 'A List Slide', 338),
(8, 'A List Slide', 364),
(9, 'A List Slide', 387),
(10, 'A List Slide', 388),
(11, 'A List Slide', 396),
(12, 'A List Slide', 365),
(13, 'A List Slide', 399),
(14, 'A List Slide', 401),
(15, 'A List Slide', 402),
(16, 'A List Slide', 403),
(17, 'A List Slide', 415),
(18, 'A List Slide', 422);

-- --------------------------------------------------------

--
-- Table structure for table `list_slide_options`
--

CREATE TABLE `list_slide_options` (
  `lopt_id` int(11) NOT NULL,
  `list_option` text NOT NULL,
  `list_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `list_slide_options`
--

INSERT INTO `list_slide_options` (`lopt_id`, `list_option`, `list_id`) VALUES
(3, 'list option 1', 1),
(4, 'list option 2', 1),
(15, 'Has a several points', 3),
(16, 'Displays each point with a bullet', 3),
(17, 'Is similar to a PowerPoint slide', 3),
(21, 'Has a several points', 4),
(22, 'Displays each point with a bullet', 4),
(23, 'Is similar to a PowerPoint slide', 4),
(24, 'Has a several points', 5),
(25, 'Displays each point with a bullet', 5),
(26, 'Is similar to a PowerPoint slide', 5),
(27, 'Has a several points', 6),
(28, 'Displays each point with a bullet', 6),
(29, 'Is similar to a PowerPoint slide', 6),
(30, 'Has a several points', 7),
(31, 'Displays each point with a bullet', 7),
(32, 'Is similar to a PowerPoint slide', 7),
(33, 'Has a several points', 8),
(34, 'Displays each point with a bullet', 8),
(35, 'Is similar to a PowerPoint slide', 8),
(36, 'Has a several points', 9),
(37, 'Displays each point with a bullet', 9),
(38, 'Is similar to a PowerPoint slide', 9),
(39, 'Has a several points', 10),
(40, 'Displays each point with a bullet', 10),
(41, 'Is similar to a PowerPoint slide', 10),
(48, 'Has a several points', 11),
(49, 'Displays each point with a bullet', 11),
(50, 'Is similar to a PowerPoint slide', 11),
(51, 'Has a several points', 12),
(52, 'Displays each point with a bullet', 12),
(53, 'Is similar to a PowerPoint slide', 12),
(54, 'Has a several points', 13),
(55, 'Displays each point with a bullet', 13),
(56, 'Is similar to a PowerPoint slide', 13),
(57, 'Has a several points', 14),
(58, 'Displays each point with a bullet', 14),
(59, 'Is similar to a PowerPoint slide', 14),
(60, 'Has a several points', 15),
(61, 'Displays each point with a bullet', 15),
(62, 'Is similar to a PowerPoint slide', 15),
(63, 'Has a several points', 16),
(64, 'Displays each point with a bullet', 16),
(65, 'Is similar to a PowerPoint slide', 16),
(69, 'Has a several points', 17),
(70, 'Displays each point with a bullet', 17),
(71, 'Is similar to a PowerPoint slide', 17),
(72, 'Has a several points', 18),
(73, 'Displays each point with a bullet', 18),
(74, 'Is similar to a PowerPoint slide', 18);

-- --------------------------------------------------------

--
-- Table structure for table `login_statistic`
--

CREATE TABLE `login_statistic` (
  `id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `login_datetime` datetime NOT NULL,
  `flag` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_statistic`
--

INSERT INTO `login_statistic` (`id`, `user_id`, `login_datetime`, `flag`) VALUES
(1, 15, '2019-11-29 10:03:56', 'login'),
(2, 15, '2019-11-29 10:21:30', 'logout'),
(3, 15, '2019-11-29 10:21:30', 'logout'),
(4, 15, '2019-11-29 10:21:30', 'logout'),
(5, 15, '2019-11-29 10:32:08', 'login'),
(6, 15, '2019-11-29 11:02:03', 'login'),
(7, 15, '2019-11-29 11:21:18', 'login'),
(8, 15, '2019-11-29 11:21:58', 'login'),
(9, 15, '2019-11-29 11:27:47', 'login'),
(10, 15, '2019-11-29 17:30:19', 'logout'),
(11, 15, '2019-11-29 12:00:31', 'login'),
(12, 15, '2019-11-29 17:30:36', 'logout'),
(13, 15, '2019-11-29 12:01:23', 'login'),
(14, 15, '2019-11-29 17:31:33', 'logout'),
(15, 15, '2019-11-29 12:01:43', 'login'),
(16, 15, '2019-11-30 18:53:24', 'logout'),
(17, 15, '2019-11-30 01:23:33', 'login'),
(18, 15, '2019-12-03 04:44:17', 'login'),
(19, 15, '2019-12-03 10:14:25', 'logout'),
(20, 19, '2019-12-03 06:42:55', 'login'),
(21, 19, '2019-12-03 12:17:14', 'logout'),
(22, 19, '2019-12-03 06:47:38', 'login'),
(23, 15, '2019-12-03 12:19:20', 'login'),
(24, 15, '2019-12-04 05:44:58', 'login'),
(25, 15, '2019-12-04 05:53:16', 'login'),
(26, 15, '2019-12-05 05:53:11', 'login'),
(27, 15, '2019-12-05 05:53:13', 'login'),
(28, 15, '2019-12-05 05:53:14', 'login'),
(29, 15, '2019-12-05 05:53:14', 'login'),
(30, 15, '2019-12-05 05:53:14', 'login'),
(31, 15, '2019-12-05 05:53:14', 'login'),
(32, 15, '2019-12-05 05:53:14', 'login'),
(33, 15, '2019-12-06 07:18:40', 'login'),
(34, 15, '2019-12-06 13:53:57', 'logout'),
(35, 15, '2019-12-06 08:25:59', 'login'),
(36, 15, '2019-12-06 13:58:45', 'logout'),
(37, 15, '2019-12-06 08:30:43', 'login'),
(38, 15, '2019-12-06 18:15:12', 'logout'),
(39, 15, '2019-12-06 19:21:35', 'logout'),
(40, 15, '2019-12-09 12:29:09', 'logout'),
(41, 15, '2019-12-13 11:55:14', 'logout'),
(42, 15, '2019-12-13 12:24:20', 'logout'),
(43, 15, '2019-12-13 12:29:36', 'logout'),
(44, 15, '2019-12-13 12:31:38', 'logout'),
(45, 15, '2019-12-13 13:34:56', 'logout'),
(46, 15, '2019-12-13 13:38:26', 'logout'),
(47, 15, '2019-12-13 13:41:32', 'logout'),
(48, 15, '2019-12-13 13:43:40', 'logout'),
(49, 15, '2019-12-13 13:51:19', 'logout'),
(50, 15, '2019-12-13 13:54:29', 'logout'),
(51, 15, '2019-12-13 13:57:26', 'logout'),
(52, 15, '2019-12-13 14:00:33', 'logout'),
(53, 15, '2019-12-13 15:10:43', 'logout'),
(54, 15, '2019-12-16 10:44:45', 'logout'),
(55, 15, '2019-12-16 06:34:38', 'login'),
(56, 15, '2019-12-16 06:34:53', 'login'),
(57, 15, '2019-12-17 16:50:00', 'logout'),
(58, 15, '2019-12-17 16:50:51', 'logout'),
(59, 15, '2019-12-18 12:51:39', 'logout'),
(60, 15, '2019-12-18 14:11:21', 'logout'),
(61, 15, '2019-12-19 13:40:05', 'logout'),
(62, 15, '2019-12-19 13:49:24', 'logout'),
(63, 15, '2019-12-19 13:55:12', 'logout'),
(64, 15, '2019-12-19 14:59:37', 'logout'),
(65, 15, '2019-12-19 19:35:03', 'logout'),
(66, 15, '2019-12-20 09:53:57', 'logout'),
(67, 15, '2019-12-20 21:27:16', 'logout'),
(68, 15, '2019-12-21 14:49:36', 'logout'),
(69, 15, '2019-12-21 15:58:08', 'logout'),
(70, 15, '2019-12-21 16:36:46', 'logout'),
(71, 15, '2019-12-21 16:38:55', 'logout'),
(72, 15, '2019-12-21 17:05:05', 'logout'),
(73, 15, '2019-12-23 10:01:21', 'logout'),
(74, 15, '2019-12-30 10:31:24', 'logout'),
(75, 15, '2019-12-30 10:33:40', 'logout'),
(76, 15, '2019-12-30 10:37:24', 'logout'),
(77, 15, '2019-12-30 10:37:51', 'logout'),
(78, 15, '2019-12-30 10:39:40', 'logout'),
(79, 15, '2019-12-30 10:43:04', 'logout'),
(80, 15, '2020-01-03 12:03:03', 'logout'),
(81, 15, '2020-01-06 10:35:46', 'logout'),
(82, 15, '2020-01-06 10:50:40', 'logout'),
(83, 15, '2020-01-06 11:29:18', 'logout'),
(84, 15, '2020-01-06 12:23:42', 'logout'),
(85, 15, '2020-01-06 12:45:31', 'logout'),
(86, 15, '2020-01-09 12:21:29', 'logout'),
(87, 15, '2020-01-15 11:43:11', 'logout'),
(88, 15, '2020-01-31 18:22:21', 'logout'),
(89, 15, '2020-01-31 18:52:03', 'logout'),
(90, 15, '2020-01-31 18:52:27', 'logout'),
(91, 15, '2020-02-03 10:00:52', 'logout'),
(92, 15, '2020-02-04 15:43:09', 'logout'),
(93, 15, '2020-02-07 10:44:25', 'logout'),
(94, 15, '2020-02-19 11:10:25', 'logout'),
(95, 15, '2020-02-19 11:14:29', 'logout'),
(96, 15, '2020-02-19 11:15:07', 'logout'),
(97, 15, '2020-02-19 12:32:58', 'logout'),
(98, 15, '2020-02-19 18:24:47', 'logout'),
(99, 15, '2020-02-24 10:30:51', 'logout'),
(100, 15, '2020-02-24 12:02:06', 'logout'),
(101, 15, '2020-02-24 12:51:56', 'logout'),
(102, 15, '2020-02-24 12:56:00', 'logout'),
(103, 15, '2020-02-25 09:54:30', 'logout'),
(104, 15, '2020-02-26 16:05:02', 'logout'),
(105, 15, '2020-02-27 09:51:55', 'logout'),
(106, 15, '2020-03-02 10:01:08', 'logout'),
(107, 15, '2020-03-02 10:02:42', 'logout'),
(108, 15, '2020-03-02 10:03:54', 'logout'),
(109, 15, '2020-03-03 17:52:05', 'logout'),
(110, 15, '2020-03-04 10:19:06', 'logout'),
(111, 15, '2020-03-05 10:48:40', 'logout'),
(112, 15, '2020-03-10 12:52:28', 'logout'),
(113, 15, '2020-03-10 12:55:07', 'logout'),
(114, 15, '2020-03-10 18:35:24', 'logout'),
(115, 15, '2020-03-10 18:39:50', 'logout'),
(116, 15, '2020-03-17 12:18:24', 'logout'),
(117, 15, '2020-03-17 12:22:13', 'logout'),
(118, 15, '2020-03-20 18:28:45', 'logout'),
(119, 15, '2020-04-08 10:17:04', 'logout'),
(120, 15, '2020-04-08 13:29:44', 'logout'),
(121, 15, '2020-04-08 13:58:16', 'logout'),
(122, 15, '2020-04-09 10:03:03', 'logout'),
(123, 15, '2020-04-09 14:26:47', 'logout'),
(124, 15, '2020-04-14 20:05:33', 'logout'),
(125, 3, '2020-05-14 18:01:34', 'logout'),
(126, 15, '2020-05-19 14:51:47', 'logout'),
(127, 15, '2020-05-20 13:08:27', 'logout'),
(128, 15, '2020-05-30 14:33:59', 'logout'),
(129, 15, '2020-05-30 15:01:11', 'logout'),
(130, 15, '2020-06-02 20:54:22', 'logout'),
(131, 15, '2020-07-01 17:56:32', 'logout'),
(132, 15, '2020-07-01 17:57:24', 'logout'),
(133, 15, '2020-07-01 17:59:14', 'logout'),
(134, 15, '2020-07-01 18:06:47', 'logout'),
(135, 15, '2020-07-01 18:07:20', 'logout'),
(136, 15, '2020-07-01 18:09:19', 'logout'),
(137, 15, '2020-07-01 19:27:36', 'logout'),
(138, 15, '2020-07-02 13:08:41', 'logout'),
(139, 15, '2020-07-03 12:51:58', 'logout'),
(140, 15, '2020-07-18 11:12:49', 'logout'),
(141, 15, '2020-07-28 01:23:47', 'logout'),
(142, 15, '2020-07-28 02:59:32', 'logout'),
(143, 15, '2020-08-10 14:06:22', 'logout'),
(144, 15, '2020-08-10 07:52:27', 'login'),
(145, 15, '2020-08-13 12:06:30', 'logout'),
(146, 15, '2020-08-13 13:13:18', 'logout');

-- --------------------------------------------------------

--
-- Table structure for table `memory`
--

CREATE TABLE `memory` (
  `id` int(30) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` varchar(150) NOT NULL,
  `prompt` varchar(50) NOT NULL,
  `done_text` varchar(50) NOT NULL,
  `time_limit` int(10) NOT NULL DEFAULT '0',
  `lesson_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `memory`
--

INSERT INTO `memory` (`id`, `title`, `description`, `prompt`, `done_text`, `time_limit`, `lesson_id`) VALUES
(31, 'Find the matching tiles', 'Use your memory to find the matching tiles. Tap on a tile to select it and then tap its matching pair.', 'Find the matching tiles', 'Ok, I\'m done', 20000, 298),
(32, 'Find the matching tiles', 'Use your memory to find the matching tiles. Tap on a tile to select it and then tap its matching pair.', 'Find the matching tiles', 'Ok, I\'m done', 20000, 47),
(33, 'Find the matching tiles', 'Use your memory to find the matching tiles. Tap on a tile to select it and then tap its matching pair.', 'Find the matching tiles', 'Ok, I\'m done', 20000, 1),
(34, 'Find the matching tiles', 'Use your memory to find the matching tiles. Tap on a tile to select it and then tap its matching pair.', 'Find the matching tiles', 'Ok, I\'m done', 20000, 1),
(41, 'Find the matching tiles', 'Use your memory to find the matching tiles. Tap on a tile to select it and then tap its matching pair.', 'Find a pair of matching tiles', 'Ok, I\'m done', 30000, 16),
(42, 'Find the matching tiles', 'Use your memory to find the matching tiles. Tap on a tile to select it and then tap its matching pair.', 'Find the matching tiles', 'Ok, I\'m done', 20000, 338),
(43, 'Find the matching tiles', 'Use your memory to find the matching tiles. Tap on a tile to select it and then tap its matching pair.', 'Find the matching tiles', 'Ok, I\'m done', 20000, 338),
(44, 'Find the matching tiles', 'Use your memory to find the matching tiles. Tap on a tile to select it and then tap its matching pair.', 'Find the matching tiles', 'Ok, I\'m done', 20000, 122),
(45, 'Find the matching tiles', 'Use your memory to find the matching tiles. Tap on a tile to select it and then tap its matching pair.', 'Find the matching tiles', 'Ok, I\'m done', 20000, 364),
(46, 'Find the matching tiles', 'Use your memory to find the matching tiles. Tap on a tile to select it and then tap its matching pair.', 'Find the matching tiles', 'Ok, I\'m done', 20000, 354),
(47, 'Find the matching tiles', 'Use your memory to find the matching tiles. Tap on a tile to select it and then tap its matching pair.', 'Find the matching tiles', 'Ok, I\'m done', 20000, 377),
(48, 'Find the matching tiles', 'Use your memory to find the matching tiles. Tap on a tile to select it and then tap its matching pair.', 'Find the matching tiles', 'Ok, I\'m done', 20000, 380),
(49, 'Find the matching tiles', 'Use your memory to find the matching tiles. Tap on a tile to select it and then tap its matching pair.', 'Find the matching tiles', 'Ok, I\'m done', 20000, 347),
(57, 'Find the matching numbers to number Seplls', 'Use your memory to find the matching tiles. Tap on a tile to select it and then tap its matching pair.', 'Find the matching tiles', 'Ok, I\'m done', 30000, 387),
(60, 'Find the matching tiles', 'Use your memory to find the matching tiles. Tap on a tile to select it and then tap its matching pair.', 'Find the matching tiles', 'Ok, I\'m done', 20000, 388),
(61, 'Find the matching tiles', 'Use your memory to find the matching tiles. Tap on a tile to select it and then tap its matching pair.', 'Find the matching tiles', 'Ok, I\'m done', 20000, 396),
(62, 'Find the matching tiles', 'Use your memory to find the matching tiles. Tap on a tile to select it and then tap its matching pair.', 'Find the matching tiles', 'Ok, I\'m done', 20000, 365),
(64, 'Find the matching tiles', 'Use your memory to find the matching tiles. Tap on a tile to select it and then tap its matching pair.', 'Find the matching tiles', 'Ok, I\'m done', 20000, 401),
(65, 'Find the matching tiles', 'Use your memory to find the matching tiles. Tap on a tile to select it and then tap its matching pair.', 'Find the matching tiles', 'Ok, I\'m done', 20000, 400),
(66, 'Find the matching tiles', 'Use your memory to find the matching tiles. Tap on a tile to select it and then tap its matching pair.', 'Find the matching tiles', 'Ok, I\'m done', 20000, 410);

-- --------------------------------------------------------

--
-- Table structure for table `memory_details`
--

CREATE TABLE `memory_details` (
  `id` int(10) NOT NULL,
  `left_content` text NOT NULL,
  `right_content` text NOT NULL,
  `memory_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `memory_details`
--

INSERT INTO `memory_details` (`id`, `left_content`, `right_content`, `memory_id`) VALUES
(152, 'Pig', 'Pink', 31),
(153, 'Cow', 'Brown', 31),
(154, 'Cat', 'Black', 31),
(155, 'Polar Bear', 'White', 31),
(236, 'Pig', 'Pink', 32),
(237, 'Cow', 'Brown', 32),
(238, 'Cat', 'Black', 32),
(239, 'Polar Bear', 'White', 32),
(244, 'Pig', 'Pink', 34),
(245, 'Cow', 'Brown', 34),
(246, 'Cat', 'Black', 34),
(247, 'Polar Bear', 'White', 34),
(272, '123456789012345678901234567890', 'Pink', 33),
(273, 'Cow', 'Brown', 33),
(274, 'Cat', 'Black', 33),
(275, 'Polar Bear', 'White', 33),
(328, 'Pig', 'Pink', 41),
(329, 'Cow', 'Brown', 41),
(330, 'Cat', 'Black', 41),
(331, 'Polar Bear', 'White', 41),
(336, 'Pig', 'Pink', 43),
(337, 'Cow', 'Brown', 43),
(338, 'Cat', 'Black', 43),
(339, 'Polar Bear', 'White', 43),
(340, 'Pig', 'Pink', 42),
(341, 'Cow', 'Brown', 42),
(342, 'Cat', 'Black', 42),
(343, 'Polar Bear', 'White', 42),
(344, 'Pig', 'Pink', 44),
(345, 'Cow', 'Brown', 44),
(346, 'Cat', 'Black', 44),
(347, 'Polar Bear', 'White', 44),
(348, 'Pig', 'Pink', 45),
(349, 'Cow', 'Brown', 45),
(350, 'Cat', 'Black', 45),
(351, 'Polar Bear', 'White', 45),
(352, 'Pig', 'Pink', 46),
(353, 'Cow', 'Brown', 46),
(354, 'Cat', 'Black', 46),
(355, 'Polar Bear', 'White', 46),
(356, 'Pig', 'Pink', 47),
(357, 'Cow', 'Brown', 47),
(358, 'Cat', 'Black', 47),
(359, 'Polar Bear', 'White', 47),
(368, 'Pig', 'Pink', 48),
(369, 'Cow', 'Brown', 48),
(370, 'Cat', 'Black', 48),
(371, 'Polar Bear', 'White', 48),
(372, 'Pig', 'Pink', 49),
(373, 'Cow', 'Brown', 49),
(374, 'Cat', 'Black', 49),
(375, 'Polar Bear', 'White', 49),
(432, 'Pig', 'Pink', 60),
(433, 'Cow', 'Brown', 60),
(438, 'Pig', 'Pink', 61),
(439, 'Cow', 'Brown', 61),
(440, 'Cat', 'Black', 61),
(441, 'Polar Bear', 'White', 61),
(442, 'Pig', 'Pink', 62),
(443, 'Cow', 'Brown', 62),
(444, 'Cat', 'Black', 62),
(445, 'Polar Bear', 'White', 62),
(446, 'One', '01', 57),
(447, 'Two', '02', 57),
(448, 'Three', '03', 57),
(449, 'Four', '04', 57),
(454, 'Pig', 'Pink', 64),
(455, 'Cow', 'Brown', 64),
(456, 'Cat', 'Black', 64),
(457, 'Polar Bear', 'White', 64),
(462, 'Pig', 'Pink', 65),
(463, 'Cow', 'Brown', 65),
(464, 'Cat', 'Black', 65),
(465, 'Polar Bear', 'White', 65),
(470, 'Pig', 'Pink', 66),
(471, 'Cow', 'Brown', 66),
(472, 'Cat', 'Black', 66),
(473, 'Polar Bear', 'White', 66);

-- --------------------------------------------------------

--
-- Table structure for table `multiple_choice`
--

CREATE TABLE `multiple_choice` (
  `id` int(30) NOT NULL,
  `title` varchar(50) NOT NULL,
  `subtitle` varchar(50) DEFAULT NULL,
  `answer` text NOT NULL,
  `answer_text` text NOT NULL,
  `correct_reinforcement` varchar(100) NOT NULL,
  `incorrect_reinforcement` varchar(100) NOT NULL,
  `core_message` text,
  `lesson_id` int(10) NOT NULL,
  `done_text` varchar(100) NOT NULL,
  `prompt` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `multiple_choice`
--

INSERT INTO `multiple_choice` (`id`, `title`, `subtitle`, `answer`, `answer_text`, `correct_reinforcement`, `incorrect_reinforcement`, `core_message`, `lesson_id`, `done_text`, `prompt`) VALUES
(1, 'Which of these are true?', '', 'This', 'Any answer that contains \"the above\" will be placed at the bottom of the list, random or not.', 'That is correct!', 'Not quite...', 'All of the above are true', 1, 'Ok, Im done', 'All Done'),
(5, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer. Any answer that contains \"the above\" will be placed at the bottom of the list, random or not.', 'That\'s correct!', 'Not quite...', NULL, 5, 'Ok, I\'m done', 'Select the correct answer'),
(6, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer. Any answer that contains \"the above\" will be placed at the bottom of the list, random or not.', 'That\'s correct!', 'Not quite...', NULL, 5, 'Ok, I\'m done', 'Select the correct answer'),
(11, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer. Any answer that contains \"the above\" will be placed at the bottom of the list, random or not.', 'That\'s correct!', 'Not quite...', NULL, 217, 'Ok, I\'m done', 'Select the correct answer'),
(40, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer. Any answer that contains \"the above\" will be placed at the bottom of the list, random or not.', 'That\'s correct!', 'Not quite...', NULL, 298, 'Ok, I\'m done', 'Select the correct answer'),
(42, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer. Any answer that contains \"the above\" will be placed at the bottom of the list, random or not.', 'That\'s correct!', 'Not quite...', NULL, 4, 'Ok, I\'m done', 'Select the correct answer'),
(43, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer. Any answer that contains \"the above\" will be placed at the bottom of the list, random or not.', 'That\'s correct!', 'Not quite...', NULL, 9, 'Ok, I\'m done', 'Select the correct answer'),
(44, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer. Any answer that contains \"the above\" will be placed at the bottom of the list, random or not.', 'That\'s correct!', 'Not quite...', NULL, 10, 'Ok, I\'m done', 'Select the correct answer'),
(45, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer. Any answer that contains \"the above\" will be placed at the bottom of the list, random or not.', 'That\'s correct!', 'Not quite...', NULL, 46, 'Ok, I\'m done', 'Select the correct answer'),
(46, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer. Any answer that contains \"the above\" will be placed at the bottom of the list, random or not.', 'That\'s correct!', 'Not quite...', NULL, 47, 'Ok, I\'m done', 'Select the correct answer'),
(47, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer. Any answer that contains \"the above\" will be placed at the bottom of the list, random or not.', 'That\'s correct!', 'Not quite...', NULL, 124, 'Ok, I\'m done', 'Select the correct answer'),
(48, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer. Any answer that contains \"the above\" will be placed at the bottom of the list, random or not.', 'That\'s correct!', 'Not quite...', NULL, 1, 'Ok, I\'m done', 'Select the correct answer'),
(49, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer. Any answer that contains \"the above\" will be placed at the bottom of the list, random or not.', 'That\'s correct!', 'Not quite...', NULL, 1, 'Ok, I\'m done', 'Select the correct answer'),
(60, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', NULL, 1, 'Ok, I\'m done', 'Select the correct answer'),
(64, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', NULL, 16, 'Ok, I\'m done', 'Select the correct answer'),
(65, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', NULL, 338, 'Ok, I\'m done', 'Select the correct answer'),
(70, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', NULL, 45, 'Ok, I\'m done', 'Select the correct answer'),
(71, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', NULL, 45, 'Ok, I\'m done', 'Select the correct answer'),
(72, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', NULL, 45, 'Ok, I\'m done', 'Select the correct answer'),
(73, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', NULL, 338, 'Ok, I\'m done', 'Select the correct answer'),
(75, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', NULL, 362, 'Ok, I\'m done', 'Select the correct answer'),
(76, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', NULL, 348, 'Ok, I\'m done', 'Select the correct answer'),
(77, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', NULL, 347, 'Ok, I\'m done', 'Select the correct answer'),
(78, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', NULL, 347, 'Ok, I\'m done', 'Select the correct answer'),
(80, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', NULL, 364, 'Ok, I\'m done', 'Select the correct answer'),
(81, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', NULL, 354, 'Ok, I\'m done', 'Select the correct answer'),
(82, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', NULL, 383, 'Ok, I\'m done', 'Select the correct answer'),
(83, '...is responsible for requirement gathering', NULL, 'Scribe', ' Scribe is a separate person responsible for the logging of the defects found', 'That\'s correct!', 'Not quite...', NULL, 387, 'Ok, I\'m done', 'Select the correct answer'),
(84, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', NULL, 389, 'Ok, I\'m done', 'Select the correct answer'),
(85, 'Ma', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', NULL, 388, 'Ok, I\'m done', 'Select the correct answer'),
(86, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', NULL, 380, 'Ok, I\'m done', 'Select the correct answer'),
(87, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', NULL, 388, 'Ok, I\'m done', 'Select the correct answer'),
(88, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', NULL, 392, 'Ok, I\'m done', 'Select the correct answer'),
(89, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', NULL, 379, 'Ok, I\'m done', 'Select the correct answer'),
(90, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', NULL, 394, 'Ok, I\'m done', 'Select the correct answer'),
(91, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', NULL, 396, 'Ok, I\'m done', 'Select the correct answer'),
(92, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', NULL, 365, 'Ok, I\'m done', 'Select the correct answer'),
(94, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', NULL, 401, 'Ok, I\'m done', 'Select the correct answer'),
(95, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', NULL, 402, 'Ok, I\'m done', 'Select the correct answer'),
(96, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', NULL, 403, 'Ok, I\'m done', 'Select the correct answer'),
(97, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', NULL, 348, 'Ok, I\'m done', 'Select the correct answer'),
(98, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', NULL, 348, 'Ok, I\'m done', 'Select the correct answer'),
(99, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', NULL, 348, 'Ok, I\'m done', 'Select the correct answer'),
(102, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', NULL, 409, 'Ok, I\'m done', 'Select the correct answer'),
(104, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', NULL, 413, 'Ok, I\'m done', 'Select the correct answer'),
(105, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', NULL, 413, 'Ok, I\'m done', 'Select the correct answer'),
(106, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', NULL, 413, 'Ok, I\'m done', 'Select the correct answer'),
(107, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', NULL, 394, 'Ok, I\'m done', 'Select the correct answer'),
(108, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', NULL, 419, 'Ok, I\'m done', 'Select the correct answer'),
(109, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', NULL, 420, 'Ok, I\'m done', 'Select the correct answer'),
(110, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', NULL, 422, 'Ok, I\'m done', 'Select the correct answer'),
(111, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', NULL, 424, 'Ok, I\'m done', 'Select the correct answer'),
(112, 'Multiple Choice', NULL, 'This', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer.', 'That\'s correct!', 'Not quite...', NULL, 430, 'Ok, I\'m done', 'Select the correct answer');

-- --------------------------------------------------------

--
-- Table structure for table `multiple_choice_image`
--

CREATE TABLE `multiple_choice_image` (
  `id` int(30) NOT NULL,
  `title` varchar(50) NOT NULL,
  `done_text` varchar(50) NOT NULL,
  `lesson_id` int(30) NOT NULL,
  `prompt` varchar(256) NOT NULL,
  `answer_text` text NOT NULL,
  `correct_reinforcement` varchar(125) NOT NULL,
  `incorrect_reinforcement` varchar(125) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `multiple_choice_image`
--

INSERT INTO `multiple_choice_image` (`id`, `title`, `done_text`, `lesson_id`, `prompt`, `answer_text`, `correct_reinforcement`, `incorrect_reinforcement`) VALUES
(14, 'Multiple Choice', 'Ok, I\'m done', 348, 'Select the correct answer', 'It\'s possible to show incorrect answers in the specific order followed by a correct answer. Any answer that contains \"the above\" will be placed at the bottom of the list, random or not', 'That\'s correct!', 'Not quite...'),
(15, 'Multiple Choice', 'Ok, I\'m done', 347, 'Select the correct answer', 'First image is correct.', 'That\'s correct!', 'Not quite...'),
(16, 'Multiple Choice', 'Ok, I\'m done', 364, 'Select the correct answer', 'First image is correct.', 'That\'s correct!', 'Not quite...'),
(17, 'Multiple Choice', 'Ok, I\'m done', 354, 'Select the correct answer', 'First image is correct.', 'That\'s correct!', 'Not quite...'),
(19, 'Cheese Burger', 'Ok, I\'m done', 387, 'Select the correct answer', 'Third image is correct.', 'That\'s correct!', 'Not quite...'),
(20, 'Multiple Choice', 'Ok, I\'m done', 388, 'Select the correct answer', 'First image is correct.', 'That\'s correct!', 'Not quite...'),
(21, 'Multiple Choice', 'Ok, I\'m done', 388, 'Select the correct answer', 'First image is correct.', 'That\'s correct!', 'Not quite...'),
(22, 'Multiple Choice', 'Ok, I\'m done', 396, 'Select the correct answer', 'First image is correct.', 'That\'s correct!', 'Not quite...'),
(23, 'Multiple Choice', 'Ok, I\'m done', 365, 'Select the correct answer', 'First image is correct.', 'That\'s correct!', 'Not quite...'),
(24, 'Multiple Choice', 'Ok, I\'m done', 399, 'Select the correct answer', 'First image is correct.', 'That\'s correct!', 'Not quite...'),
(25, 'Multiple Choice', 'Ok, I\'m done', 401, 'Select the correct answer', 'First image is correct.', 'That\'s correct!', 'Not quite...'),
(26, 'Multiple Choice', 'Ok, I\'m done', 402, 'Select the correct answer', 'First image is correct.', 'That\'s correct!', 'Not quite...'),
(27, 'Multiple Choice', 'Ok, I\'m done', 403, 'Select the correct answer', 'First image is correct.', 'That\'s correct!', 'Not quite...'),
(28, 'Multiple Choice', 'Ok, I\'m done', 404, 'Select the correct answer', 'First image is correct.', 'That\'s correct!', 'Not quite...'),
(29, 'Multiple Choice', 'Ok, I\'m done', 404, 'Select the correct answer', 'First image is correct.', 'That\'s correct!', 'Not quite...'),
(30, 'Multiple Choice', 'Ok, I\'m done', 419, 'Select the correct answer', 'First image is correct.', 'That\'s correct!', 'Not quite...'),
(31, 'Multiple Choice', 'Ok, I\'m done', 419, 'Select the correct answer', 'First image is correct.', 'That\'s correct!', 'Not quite...'),
(32, 'Multiple Choice', 'Ok, I\'m done', 394, 'Select the correct answer', 'First image is correct.', 'That\'s correct!', 'Not quite...');

-- --------------------------------------------------------

--
-- Table structure for table `multiple_choice_image_options`
--

CREATE TABLE `multiple_choice_image_options` (
  `id` int(11) NOT NULL,
  `option_image` varchar(356) NOT NULL,
  `answer` varchar(11) NOT NULL,
  `multiple_choice_image_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `multiple_choice_image_options`
--

INSERT INTO `multiple_choice_image_options` (`id`, `option_image`, `answer`, `multiple_choice_image_id`) VALUES
(69, 'multiple_choice_images/1593595461112679950.png', 'true', 15),
(70, 'multiple_choice_images/1593595461446504922.png', 'false', 15),
(71, 'multiple_choice_images/1593595461414783142.png', 'false', 15),
(72, 'multiple_choice_images/15936842431412215142.png', 'true', 16),
(73, 'multiple_choice_images/159368424366100953.png', 'false', 16),
(74, 'multiple_choice_images/1593684243979774106.png', 'false', 16),
(75, 'multiple_choice_images/15937495991969320801.png', 'true', 17),
(76, 'multiple_choice_images/15937495991454136877.png', 'false', 17),
(77, 'multiple_choice_images/159374959998942823.png', 'false', 17),
(84, 'multiple_choice_images/15954209731806579790.jpeg', 'false', 19),
(85, 'multiple_choice_images/15954209731964010790.jpeg', 'false', 19),
(86, 'multiple_choice_images/159542097337349280.jpeg', 'true', 19),
(87, 'multiple_choice_images/1593259702830869741.png', 'false', 14),
(88, 'multiple_choice_images/1595421135453739886.png', 'true', 14),
(89, 'multiple_choice_images/15954901971665045042.png', 'true', 20),
(90, 'multiple_choice_images/15954901971263828095.png', 'false', 20),
(91, 'multiple_choice_images/1595490197421543315.png', 'false', 20),
(92, 'multiple_choice_images/15954908631331450865.png', 'true', 21),
(93, 'multiple_choice_images/1595490863259180592.png', 'false', 21),
(94, 'multiple_choice_images/15954908631013862417.png', 'false', 21),
(98, 'multiple_choice_images/1595673071911953746.png', 'true', 22),
(99, 'multiple_choice_images/15956730711447008178.png', 'false', 22),
(100, 'multiple_choice_images/15956730712002048950.png', 'false', 22),
(101, 'multiple_choice_images/15959977021137858437.png', 'true', 23),
(102, 'multiple_choice_images/1595997702300966099.png', 'false', 23),
(103, 'multiple_choice_images/15959977021974395580.png', 'false', 23),
(104, 'multiple_choice_images/15962035141070172318.png', 'true', 24),
(105, 'multiple_choice_images/1596203514293545857.png', 'false', 24),
(106, 'multiple_choice_images/1596203514130824426.png', 'false', 24),
(110, 'multiple_choice_images/1596336096325226894.png', 'true', 26),
(111, 'multiple_choice_images/15963360961993174509.png', 'false', 26),
(112, 'multiple_choice_images/1596336096518246236.png', 'false', 26),
(113, 'multiple_choice_images/1596352149284666450.png', 'true', 27),
(114, 'multiple_choice_images/15963521491310554509.png', 'false', 27),
(115, 'multiple_choice_images/15963521492002042885.png', 'false', 27),
(116, 'multiple_choice_images/15967070201127004047.png', 'true', 25),
(117, 'multiple_choice_images/15962050731804086641.png', 'false', 25),
(118, 'multiple_choice_images/1596205073738220692.png', 'false', 25),
(122, 'multiple_choice_images/15967834431498305816.png', 'true', 29),
(123, 'multiple_choice_images/15967834431456438089.png', 'false', 29),
(124, 'multiple_choice_images/15967834431956797354.png', 'false', 29),
(131, 'multiple_choice_images/15967834791059800627.png', 'false', 28),
(132, 'multiple_choice_images/15967834981125133418.png', 'true', 28),
(133, 'multiple_choice_images/15967834381602326867.png', 'false', 28),
(137, 'multiple_choice_images/1597125161823262952.png', 'true', 30),
(138, 'multiple_choice_images/15971251611311641032.png', 'false', 30),
(139, 'multiple_choice_images/15971251461124506731.png', 'false', 30),
(140, 'multiple_choice_images/15971370671780048902.jpeg', 'true', 31),
(141, 'multiple_choice_images/1597137067873319449.jpeg', 'false', 31),
(142, 'multiple_choice_images/15971370671450984086.jpeg', 'false', 31),
(143, 'multiple_choice_images/15975828881346655302.jpeg', 'true', 32),
(144, 'multiple_choice_images/1597582888204281754.jpeg', 'false', 32),
(145, 'multiple_choice_images/1597582888327517323.jpeg', 'false', 32);

-- --------------------------------------------------------

--
-- Table structure for table `multiple_choice_options`
--

CREATE TABLE `multiple_choice_options` (
  `id` int(10) NOT NULL,
  `options` varchar(256) NOT NULL,
  `multiple_choice_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `multiple_choice_options`
--

INSERT INTO `multiple_choice_options` (`id`, `options`, `multiple_choice_id`) VALUES
(59, ' All of the', 5),
(60, 'This', 5),
(61, 'That', 5),
(62, 'The other', 5),
(63, ' All of the', 6),
(64, 'This', 6),
(65, 'That', 6),
(66, 'The other', 6),
(83, ' All of the', 11),
(84, 'This', 11),
(85, 'That', 11),
(86, 'The other', 11),
(243, ' All of the', 40),
(244, 'This', 40),
(245, 'That', 40),
(246, 'The other', 40),
(255, ' All of the', 42),
(256, 'This', 42),
(257, 'That', 42),
(258, 'The other', 42),
(259, ' All of the', 43),
(260, 'This', 43),
(261, 'That', 43),
(262, 'The other', 43),
(263, ' All of the', 44),
(264, 'This', 44),
(265, 'That', 44),
(266, 'The other', 44),
(267, ' All of the', 45),
(268, 'This', 45),
(269, 'That', 45),
(270, 'The other', 45),
(271, ' All of the', 46),
(272, 'This', 46),
(273, 'That', 46),
(274, 'The other', 46),
(275, ' All of the', 47),
(276, 'This', 47),
(277, 'That', 47),
(278, 'The other', 47),
(279, ' All of the', 48),
(280, 'This', 48),
(281, 'That', 48),
(282, 'The other', 48),
(291, ' bkbbkjbkljbkj', 49),
(292, 'This', 49),
(293, 'That', 49),
(294, 'The other', 49),
(324, 'All of the', 1),
(325, 'This', 1),
(326, 'That', 1),
(327, 'The other', 1),
(380, ' All of the', 60),
(381, 'This', 60),
(382, 'That', 60),
(383, 'The other', 60),
(404, ' All of the', 64),
(405, 'This', 64),
(406, 'That', 64),
(407, 'The other', 64),
(432, ' All of the', 70),
(433, 'This', 70),
(434, 'That', 70),
(435, 'The other', 70),
(436, ' All of the', 71),
(437, 'This', 71),
(438, 'That', 71),
(439, 'The other', 71),
(440, ' All of the', 72),
(441, 'This', 72),
(442, 'That', 72),
(443, 'The other', 72),
(444, ' All of the', 73),
(445, 'This', 73),
(446, 'That', 73),
(447, 'The other', 73),
(456, ' All of the', 75),
(457, 'This', 75),
(458, 'That', 75),
(459, 'The other', 75),
(460, ' All of the', 76),
(461, 'This', 76),
(462, 'That', 76),
(463, 'The other', 76),
(464, ' All of the', 65),
(465, 'This', 65),
(466, 'That', 65),
(467, 'The other', 65),
(468, ' All of the', 77),
(469, 'This', 77),
(470, 'That', 77),
(471, 'The other', 77),
(472, ' All of the', 78),
(473, 'This', 78),
(474, 'That', 78),
(475, 'The other', 78),
(496, ' All of the', 80),
(497, 'This', 80),
(498, 'That', 80),
(499, 'The other', 80),
(500, ' All of the', 81),
(501, 'This', 81),
(502, 'That', 81),
(503, 'The other', 81),
(508, ' All of the', 82),
(509, 'This', 82),
(510, 'That', 82),
(511, 'The other', 82),
(516, 'Visionary', 83),
(517, 'Executive Sponsor', 83),
(518, 'Scribe', 83),
(519, 'Project Manager', 83),
(520, ' All of the', 84),
(521, 'This', 84),
(522, 'That', 84),
(523, 'The other', 84),
(528, 'qqqqqqqqq  qqqqqqqqq  qqqqqqqqq  qqqqqqqqq  qqqqqqqqq  qqqqqqqqq  qqqqqqqqq  qqqqqqqqq  qqqqqqqqq  q', 85),
(529, 'This', 85),
(530, 'That', 85),
(531, 'The other', 85),
(532, ' All of the', 86),
(533, 'This', 86),
(534, 'That', 86),
(535, 'The other', 86),
(536, ' All of the', 87),
(537, 'This', 87),
(538, 'That', 87),
(539, 'The other', 87),
(540, ' All of the', 88),
(541, 'This', 88),
(542, 'That', 88),
(543, 'The other', 88),
(544, ' All of the', 89),
(545, 'This', 89),
(546, 'That', 89),
(547, 'The other', 89),
(548, ' All of the', 90),
(549, 'This', 90),
(550, 'That', 90),
(551, 'The other', 90),
(556, ' All of the', 91),
(557, 'This', 91),
(558, 'That', 91),
(559, 'The other', 91),
(560, ' All of the', 92),
(561, 'This', 92),
(562, 'That', 92),
(563, 'The other', 92),
(568, ' All of the', 94),
(569, 'This', 94),
(570, 'That', 94),
(571, 'The other', 94),
(572, ' All of the', 95),
(573, 'This', 95),
(574, 'That', 95),
(575, 'The other', 95),
(576, ' All of the', 96),
(577, 'This', 96),
(578, 'That', 96),
(579, 'The other', 96),
(580, ' All of the', 97),
(581, 'This', 97),
(582, 'That', 97),
(583, 'The other', 97),
(584, ' All of the', 98),
(585, 'This', 98),
(586, 'That', 98),
(587, 'The other', 98),
(588, ' All of the', 99),
(589, 'This', 99),
(590, 'That', 99),
(591, 'The other', 99),
(600, ' All of the', 102),
(601, 'This', 102),
(602, 'That', 102),
(603, 'The other', 102),
(615, ' All of the', 104),
(616, 'This', 104),
(617, 'That', 104),
(618, 'The other', 104),
(619, ' All of the', 105),
(620, 'This', 105),
(621, 'That', 105),
(622, 'The other', 105),
(627, ' All of the', 106),
(628, 'This', 106),
(629, 'That', 106),
(630, 'The other', 106),
(635, '1990', 107),
(636, '1995', 107),
(637, '1992', 107),
(638, '1993', 107),
(639, ' All of the', 108),
(640, 'This', 108),
(641, 'That', 108),
(642, 'The other', 108),
(643, ' All of the', 109),
(644, 'This', 109),
(645, 'That', 109),
(646, 'The other', 109),
(647, ' All of the', 110),
(648, 'This', 110),
(649, 'That', 110),
(650, 'The other', 110),
(651, ' All of the', 111),
(652, 'This', 111),
(653, 'That', 111),
(654, 'The other', 111),
(655, ' All of the', 112),
(656, 'This', 112),
(657, 'That', 112),
(658, 'The other', 112);

-- --------------------------------------------------------

--
-- Table structure for table `payment_plan_master`
--

CREATE TABLE `payment_plan_master` (
  `id` int(10) NOT NULL,
  `payment_plans` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_plan_master`
--

INSERT INTO `payment_plan_master` (`id`, `payment_plans`) VALUES
(1, 'Paid in full'),
(2, 'Payment plan');

-- --------------------------------------------------------

--
-- Table structure for table `peer_authoring`
--

CREATE TABLE `peer_authoring` (
  `id` int(30) NOT NULL,
  `title` varchar(50) NOT NULL,
  `done_text` varchar(30) NOT NULL,
  `lesson_id` int(50) NOT NULL,
  `upload_button_label` varchar(35) NOT NULL,
  `no_media_updated_message` varchar(65) NOT NULL,
  `prompt` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `peer_authoring`
--

INSERT INTO `peer_authoring` (`id`, `title`, `done_text`, `lesson_id`, `upload_button_label`, `no_media_updated_message`, `prompt`) VALUES
(1, 'Peer Authoring', 'Okay, I\'m done', 327, 'Upload now', 'No videos have been uploaded yet.', 'Scroll to see more'),
(2, 'How would you improve this lesson?', 'Ok, I\'m done', 307, 'Upload now', 'No videos have been uploaded yet.', 'Scroll to see more'),
(8, 'How would you improve this lesson.', 'Ok, I\'m done', 319, 'Upload now', 'No videos have been uploaded yet.', 'Scroll to see more'),
(9, 'How would you improve this lesson?', 'Ok, I\'m done', 319, 'Upload now', 'No videos have been uploaded yet.', 'Scroll to see more'),
(10, 'How would you improve this lesson?', 'Ok, I\'m done', 313, 'Upload now', 'No videos have been uploaded yet.', 'Scroll to see more'),
(11, 'How would you improve this lesson?', 'Ok, I\'m done', 313, 'Upload now', 'No videos have been uploaded yet.', 'Scroll to see more'),
(12, 'Peer Authoring', 'Ok, I\'m done', 327, 'Upload now', 'No videos have been uploaded yet.', 'Scroll to see more'),
(13, 'Peer Authoring', 'Ok, I\'m done', 283, 'Upload now', 'No videos have been uploaded yet.', 'Scroll to see more'),
(14, 'How would you improve this lesson?', 'Ok, I\'m done', 330, 'Upload now', 'No videos have been uploaded yet.', 'Scroll to see more'),
(15, 'Upload a video for peer review', 'Ok, I\'m done', 16, 'Upload now', 'Use the upload button', 'Scroll to see more'),
(16, 'How would you improve this lesson?', 'Ok, I\'m done', 332, 'Upload now', 'No videos have been uploaded yet.', 'Scroll to see more'),
(17, 'Send me your video', 'Ok, I\'m done', 332, 'Upload now', 'Come on, do it now', 'Scroll to see more'),
(18, 'How would you improve this lesson?', 'Ok, I\'m done', 337, 'Upload now', 'No videos have been uploaded yet.', 'Scroll to see more'),
(19, 'How would you improve this lesson?', 'Ok, I\'m done', 345, 'Upload now', 'No videos have been uploaded yet.', 'Scroll to see more'),
(22, 'How would you improve this lesson?', 'Ok, I\'m done', 351, 'Upload now', 'No videos have been uploaded yet.', 'Scroll to see more'),
(24, 'How would you improve this lesson?', 'Ok, I\'m done', 338, 'Upload now', 'No videos have been uploaded yet.', 'Scroll to see more'),
(25, 'How would you improve this lesson?', 'Ok, I\'m done', 364, 'Upload now', 'No videos have been uploaded yet.', 'Scroll to see more'),
(26, 'How would you improve this lesson?', 'Ok, I\'m done', 376, 'Upload now', 'No videos have been uploaded yet.', 'Scroll to see more'),
(27, 'How would you improve this lesson?', 'Ok, I\'m done', 347, 'Upload now', 'No videos have been uploaded yet.', 'Scroll to see more'),
(28, 'How would you improve this lesson?', 'Ok, I\'m done', 396, 'Upload now', 'No videos have been uploaded yet.', 'Scroll to see more'),
(29, 'How would you improve this lesson?', 'Ok, I\'m done', 365, 'Upload now', 'No videos have been uploaded yet.', 'Scroll to see more'),
(30, 'How would you improve this lesson?', 'Ok, I\'m done', 397, 'Upload now', 'No videos have been uploaded yet.', 'Scroll to see more'),
(32, 'How would you improve this lesson?', 'Ok, I\'m done', 401, 'Upload now', 'No videos have been uploaded yet.', 'Scroll to see more'),
(33, 'How would you improve this lesson?', 'Ok, I\'m done', 403, 'Upload now', 'No videos have been uploaded yet.', 'Scroll to see more'),
(34, 'How would you improve this lesson?', 'Ok, I\'m done', 403, 'Upload now', 'No videos have been uploaded yet.', 'Scroll to see more');

-- --------------------------------------------------------

--
-- Table structure for table `peer_authoring_answers`
--

CREATE TABLE `peer_authoring_answers` (
  `pans_id` int(11) NOT NULL,
  `lesson_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `peer_authoring_answers`
--

INSERT INTO `peer_authoring_answers` (`pans_id`, `lesson_id`, `user_id`, `question_id`) VALUES
(1, 397, 28, 30),
(2, 399, 68, 31),
(3, 401, 68, 32),
(4, 403, 28, 34),
(5, 403, 28, 33),
(6, 347, 28, 27),
(7, 376, 28, 26),
(8, 399, 119, 31);

-- --------------------------------------------------------

--
-- Table structure for table `peer_authoring_ans_videos`
--

CREATE TABLE `peer_authoring_ans_videos` (
  `paa_id` int(11) NOT NULL,
  `video_path` varchar(256) NOT NULL,
  `pans_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `peer_authoring_ans_videos`
--

INSERT INTO `peer_authoring_ans_videos` (`paa_id`, `video_path`, `pans_id`) VALUES
(1, 'peer_authoring_video/video_7880.mp4', 1),
(2, 'peer_authoring_video/video_9961.mp4', 1),
(3, 'peer_authoring_video/video_9960.mp4', 2),
(4, 'peer_authoring_video/video_7240.mp4', 3),
(5, 'peer_authoring_video/video_7871.mp4', 3),
(6, 'peer_authoring_video/video_3490.mp4', 4),
(7, 'peer_authoring_video/video_6900.mp4', 5),
(8, 'peer_authoring_video/video_4300.mp4', 8);

-- --------------------------------------------------------

--
-- Table structure for table `plans_master`
--

CREATE TABLE `plans_master` (
  `id` int(10) NOT NULL,
  `plan` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `plans_master`
--

INSERT INTO `plans_master` (`id`, `plan`) VALUES
(1, '100'),
(2, '200');

-- --------------------------------------------------------

--
-- Table structure for table `programme_card`
--

CREATE TABLE `programme_card` (
  `id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `text1` text NOT NULL,
  `text2` text NOT NULL,
  `unit_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `programme_card`
--

INSERT INTO `programme_card` (`id`, `client_id`, `text1`, `text2`, `unit_id`, `user_id`) VALUES
(1, 1, 'New What is Lorem Ipsum?Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Why do we use it?It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', 43, 1),
(2, 2, 'Fbdddddrrreeeerrrrrrrrrrrrrrdrdddrdrrddrdrerdrd\ndd\neeeeeerdrdddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddsdddddddsssaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaahgggggggggggggggggggggfggggggfffgggggggggggggggggggggggggtggfffffttfff', 'Fbdddddrrreeeerrrrrrrrrrrrrrdrdddrdrrddrdrerdrddddeeeeeerdrddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddsdddddddsssaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaahgggggggggggggggggggggfggggggfffgggggggggggggggggggggggggtggfffffttfff', 174, 4),
(3, 3, 'Vzzvgse\nBxxnxbxbdhdhshsyeyeyeyey\nFbdddddrrreeeerrrrrrrrrrrrrrdrdddrdrrddrdrerdrddddeeeeeerdrddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddsdddddddsssaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaahgggggggggggggggggggggfggggggfffgggggggggggggggggggggggggtggfffffttfff', 'Dhhddhdbhdd', 174, 7),
(5, 5, 'CjigobobFbdddddrrreeeerrrrrrrrrrrrrrdrdddrdrrddrdrerdrddddeeeeeerdrddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddsdddddddsssaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaahgggggggggggggggggggggfggggggfffgggggggggggggggggggggggggtggfffffttfff', 'Fbdddddrrreeeerrrrrrrrrrrrrrdrdddrdrrddrdrerdrddddeeeeeerdrddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddsdddddddsssaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaahgggggggggggggggggggggfggggggfffgggggggggggggggggggggggggtggfffffttfff\nF ug if I bnbgnhg', 174, 22),
(6, 6, 'CjigobobFbdddddrrreeeerrrrrrrrrrrrrrdrdddrdrrddrdrerdrddddeeeeeerdrddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddsdddddddsssaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaahgggggggggggggggggggggfggggggfffgggggggggggggggggggggggggtggfffffttfff', 'Fbdddddrrreeeerrrrrrrrrrrrrrdrdddrdrrddrdrerdrddddeeeeeerdrddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddsdddddddsssaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaahgggggggggggggggggggggfggggggfffgggggggggggggggggggggggggtggfffffttfff\nF ug if I bnbgnhg', 174, 29),
(8, 8, 'Gggggg', 'Hhh', 5, 28);

-- --------------------------------------------------------

--
-- Table structure for table `question_pool`
--

CREATE TABLE `question_pool` (
  `pool_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question_pool`
--

INSERT INTO `question_pool` (`pool_id`, `unit_id`) VALUES
(1, 1),
(2, 283),
(3, 283),
(4, 283),
(5, 224),
(6, 252),
(7, 304),
(8, 248),
(9, 1),
(10, 226),
(11, 475),
(12, 490),
(13, 458),
(14, 488),
(15, 224),
(16, 492),
(17, 495),
(18, 324),
(19, 497),
(20, 510),
(21, 512),
(22, 514),
(23, 516),
(24, 517),
(25, 518),
(26, 520),
(27, 526),
(28, 526),
(29, 527),
(30, 529),
(31, 531),
(32, 536),
(33, 538),
(34, 539),
(35, 533),
(36, 540),
(37, 541),
(38, 541),
(39, 542),
(40, 543),
(41, 543);

-- --------------------------------------------------------

--
-- Table structure for table `question_result`
--

CREATE TABLE `question_result` (
  `id` int(10) NOT NULL,
  `lesson_id` int(10) NOT NULL,
  `question_id` int(10) NOT NULL,
  `question_type` varchar(50) DEFAULT NULL,
  `answer` varchar(256) DEFAULT NULL,
  `answer_flag` varchar(10) DEFAULT NULL,
  `score` varchar(50) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question_result`
--

INSERT INTO `question_result` (`id`, `lesson_id`, `question_id`, `question_type`, `answer`, `answer_flag`, `score`, `user_id`) VALUES
(1, 400, 121, 'scrollable', '', 'true', '', 119),
(2, 400, 65, 'memory', '', 'true', '3000', 119),
(3, 419, 129, 'scrollable', '', 'true', '', 119),
(4, 419, 118, 'expandable', '', 'true', '', 119),
(5, 419, 108, 'multiple_choice', 'That', 'false', '1', 119),
(6, 419, 73, 'video_slide', '', 'true', '', 119),
(7, 419, 80, 'true_false', 'true', 'false', '', 119),
(8, 419, 30, 'multiple_choice_image', 'false', 'false', '1', 119),
(9, 419, 31, 'multiple_choice_image', 'true', 'true', '1', 119),
(10, 399, 119, 'scrollable', '', 'true', '', 119),
(11, 399, 112, 'expandable', '', 'true', '', 119),
(12, 399, 93, 'multiple_choice', 'This', 'true', '1', 119),
(13, 399, 55, 'slider', '', 'false', '', 119),
(14, 399, 68, 'video_slide', '', 'true', '', 119),
(15, 399, 71, 'true_false', 'true', 'true', '', 119),
(16, 399, 36, 'crossword', '', '1', '', 119),
(17, 399, 63, 'memory', '', 'true', '2000', 119),
(18, 399, 24, 'multiple_choice_image', 'true', 'true', '1', 119),
(19, 399, 36, 'reorder', '', 'true', '', 119),
(20, 399, 83, 'free_text', 'Hdj', 'true', '1', 119),
(21, 399, 13, 'list_slide', '', 'true', '', 119),
(22, 422, 110, 'multiple_choice', 'This', 'true', '1', 119),
(23, 422, 120, 'expandable', '', 'true', '', 119),
(24, 422, 130, 'scrollable', '', 'true', '', 119),
(25, 422, 74, 'video_slide', '', 'true', '', 119),
(26, 422, 18, 'list_slide', '', 'true', '', 119),
(27, 423, 131, 'scrollable', '', 'true', '', 119),
(28, 424, 111, 'multiple_choice', 'This', 'true', '1', 119),
(29, 425, 75, 'video_slide', '', 'true', '', 119),
(30, 426, 132, 'scrollable', '', 'true', '', 122),
(31, 426, 133, 'scrollable', '', 'true', '', 122),
(32, 427, 134, 'scrollable', '', 'true', '', 122),
(33, 418, 117, 'expandable', '', 'true', '', 119),
(34, 418, 128, 'scrollable', '', 'true', '', 119),
(35, 420, 119, 'expandable', '', 'true', '', 119),
(36, 420, 109, 'multiple_choice', 'This', 'true', '1', 119),
(37, 431, 136, 'scrollable', '', 'true', '', 122),
(38, 429, 121, 'expandable', '', 'true', '', 122),
(39, 418, 117, 'expandable', '', 'true', '', 115),
(40, 418, 128, 'scrollable', '', 'true', '', 115),
(41, 420, 119, 'expandable', '', 'true', '', 115),
(42, 420, 109, 'multiple_choice', 'This', 'true', '1', 115),
(43, 432, 137, 'scrollable', '', 'true', '', 122),
(44, 428, 135, 'scrollable', '', 'true', '', 122),
(45, 394, 72, 'video_slide', '', 'true', '', 119),
(46, 435, 81, 'true_false', 'true', 'false', '', 122),
(47, 436, 122, 'expandable', '', 'true', '', 122),
(48, 418, 117, 'expandable', '', 'true', '', 122),
(49, 418, 128, 'scrollable', '', 'true', '', 122);

-- --------------------------------------------------------

--
-- Table structure for table `regions_master`
--

CREATE TABLE `regions_master` (
  `id` int(10) NOT NULL,
  `regions` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `regions_master`
--

INSERT INTO `regions_master` (`id`, `regions`) VALUES
(1, 'North east'),
(2, 'North west'),
(3, 'Midlands'),
(4, 'East Anglia'),
(5, 'West London'),
(6, 'South London'),
(7, 'East London'),
(8, 'North London'),
(9, 'South east'),
(10, 'South'),
(11, 'South west'),
(12, 'South wales'),
(13, 'North wales'),
(14, 'Scotland'),
(15, 'Northern Ireland'),
(16, 'International');

-- --------------------------------------------------------

--
-- Table structure for table `registration_user`
--

CREATE TABLE `registration_user` (
  `id` int(30) NOT NULL,
  `email_id` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `con_password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registration_user`
--

INSERT INTO `registration_user` (`id`, `email_id`, `password`, `con_password`) VALUES
(1, 'admin12@gmail.com', '12345', '12345'),
(2, 'admin12@gmail.com', '12345', '12345'),
(3, 'admin12@gmail.com', '12345', '12345'),
(4, 'admin12@gmail.com', '12345', '12345'),
(5, 'admin55@gmail.com', '12345678', '12345678'),
(6, 'admin55@gmail.com', '12345678', '12345678'),
(7, 'aarti@gmail.com', 'aarti', 'aarti'),
(8, 'aartikale@gmail.com', 'aartikale', 'aartikale'),
(9, 'aartikale@gmail.com', 'aartikale', 'aartikale'),
(10, 'aartikale@gmail.com', 'aartikale', 'aartikale'),
(11, 'aartikale@gmail.com', 'aartikale', 'aartikale'),
(12, 'aartikale@gmail.com', 'aartikale', 'aartikale');

-- --------------------------------------------------------

--
-- Table structure for table `reordering`
--

CREATE TABLE `reordering` (
  `id` int(30) NOT NULL,
  `title` varchar(50) NOT NULL,
  `done_text` varchar(50) NOT NULL,
  `prompt` varchar(256) NOT NULL,
  `lesson_id` int(50) NOT NULL,
  `correct_reinforcement` varchar(256) DEFAULT NULL,
  `incorrect_reinforcement` varchar(256) DEFAULT NULL,
  `answer_text` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reordering`
--

INSERT INTO `reordering` (`id`, `title`, `done_text`, `prompt`, `lesson_id`, `correct_reinforcement`, `incorrect_reinforcement`, `answer_text`) VALUES
(1, 'demo', 'ok done', 'Put the items in the correct order', 283, 'That is correct!', 'Not quite', 'Tokyo has almost nine million inhabitants in the city alone, and 13 million in the Greater Tokyo region. In fact, the number of residents in the Japanese capital is greater then the combined population of the next three largest cities Yokohama, Osaka and Nagoya.\n'),
(2, 'Peer Authoring', 'Okay, I\'m done', 'Scroll to see more', 327, 'sdvdsv', 'csdvdvds', 'dvdvDSV'),
(3, 'Rank these cities by population', 'Ok, I\'m done', 'Put the items in the correct order', 307, 'That\'s correct!', 'Not quite...', 'Tokyo has almost nine million inhabitants in the city alone, and 13 million in the Greater Tokyo region'),
(4, 'Peer Authoring', 'Ok, I\'m done', 'Scroll to see more', 327, 'sdvdsv', 'csdvdvds', 'dvdvDSV'),
(6, 'Rank these cities by population', 'Ok, I\'m done', 'Put the items in the correct order', 313, 'That\'s correct!', 'Not quite...', 'Tokyo has almost nine million inhabitants in the city alone, and 13 million in the Greater Tokyo region'),
(7, 'Rank these cities by population', 'Ok, I\'m done', 'Put the items in the correct order', 319, 'That\'s correct!', 'Not quite...', 'Tokyo has almost nine million inhabitants in the city alone, and 13 million in the Greater Tokyo region'),
(8, 'Rank these cities by population', 'Ok, I\'m done', 'Put the items in the correct order', 330, 'That\'s correct!', 'Not quite...', 'Tokyo has almost nine million inhabitants in the city alone, and 13 million in the Greater Tokyo region'),
(9, 'Rank these cities by population', 'Ok, I\'m done', 'Put the items in the correct order', 330, 'That\'s correct!', 'Not quite...', 'Tokyo has almost nine million inhabitants in the city alone, and 13 million in the Greater Tokyo region'),
(10, 'Rank these cities by population', 'Ok, I\'m done', 'Put the items in the correct order', 16, 'That\'s correct!', 'Not quite...', 'Tokyo has almost nine million inhabitants in the city alone, and 13 million in the Greater Tokyo region'),
(11, 'Rank these cities by population', 'Ok, I\'m done', 'Put the items in the correct order', 16, 'That\'s correct!', 'Not quite...', 'Tokyo has almost nine million inhabitants in the city alone, and 13 million in the Greater Tokyo region'),
(12, 'Rank these cities by size of population', 'Ok, I\'m done', 'Put the items in the correct order', 332, 'That\'s correct!', 'Not quite...', 'Manchester has almost nine million inhabitants in the city alone, and 13 million in the Greater Tokyo region'),
(17, 'Rank these cities by size', 'Ok, I\'m done', 'Put the items in the correct order', 332, 'That\'s correct!', 'Not quite...', 'Tokyo has almost nine million inhabitants in the city alone, and 13 million in the Greater Tokyo region'),
(18, 'Rank these cities by population', 'Ok, I\'m done', 'Put the items in the correct order', 338, 'That\'s correct!', 'Not quite...', 'Tokyo has almost nine million inhabitants in the city alone, and 13 million in the Greater Tokyo region'),
(19, 'Rank these cities by population', 'Ok, I\'m done', 'Put the items in the correct order', 351, 'That\'s correct!', 'Not quite...', 'Tokyo has almost nine million inhabitants in the city alone, and 13 million in the Greater Tokyo region'),
(20, 'Rank these cities by population', 'Ok, I\'m done', 'Put the items in the correct order', 349, 'That\'s correct!', 'Not quite...', 'Tokyo has almost nine million inhabitants in the city alone, and 13 million in the Greater Tokyo region'),
(21, 'Rank these cities by population', 'Ok, I\'m done', 'Put the items in the correct order', 338, 'That\'s correct!', 'Not quite...', 'Tokyo has almost nine million inhabitants in the city alone, and 13 million in the Greater Tokyo region'),
(22, 'Rank these cities by population', 'Ok, I\'m done', 'Put the items in the correct order', 338, 'That\'s correct!', 'Not quite...', 'Tokyo has almost nine million inhabitants in the city alone, and 13 million in the Greater Tokyo region'),
(23, 'Rank these cities by population', 'Ok, I\'m done', 'Put the items in the correct order', 341, 'That\'s correct!', 'Not quite...', 'Tokyo has almost nine million inhabitants in the city alone, and 13 million in the Greater Tokyo region'),
(24, 'Rank these cities by population', 'Ok, I\'m done', 'Put the items in the correct order', 313, 'That\'s correct!', 'Not quite...', 'Tokyo has almost nine million inhabitants in the city alone, and 13 million in the Greater Tokyo region'),
(25, 'Rank these cities by population', 'Ok, I\'m done', 'Put the items in the correct order', 313, 'That\'s correct!', 'Not quite...', 'Tokyo has almost nine million inhabitants in the city alone, and 13 million in the Greater Tokyo region'),
(26, 'Rank these cities by population', 'Ok, I\'m done', 'Put the items in the correct order', 338, 'That\'s correct!', 'Not quite...', 'Tokyo has almost nine million inhabitants in the city alone, and 13 million in the Greater Tokyo region'),
(27, 'Rank these cities by population', 'Ok, I\'m done', 'Put the items in the correct order', 338, 'That\'s correct!', 'Not quite...', 'Tokyo has almost nine million inhabitants in the city alone, and 13 million in the Greater Tokyo region'),
(28, 'Rank these cities by population', 'Ok, I\'m done', 'Put the items in the correct order', 364, 'That\'s correct!', 'Not quite...', 'Tokyo has almost nine million inhabitants in the city alone, and 13 million in the Greater Tokyo region'),
(30, 'Rank these cities by population', 'Ok, I\'m done', 'Put the items in the correct order', 376, 'That\'s correct!', 'Not quite...', 'Tokyo has almost nine million inhabitants in the city alone, and 13 million in the Greater Tokyo region'),
(31, 'Rank these cities by population', 'Ok, I\'m done', 'Put the items in the correct order', 383, 'That\'s correct!', 'Not quite...', 'Tokyo has almost nine million inhabitants in the city alone, and 13 million in the Greater Tokyo region'),
(32, 'Rank these cities by population', 'Ok, I\'m done', 'Put the items in the correct order', 387, 'That\'s correct!', 'Not quite...', 'Mumbai has almost nine million inhabitants in the city alone, and 15 million in the Greater Brahan Mumbai region'),
(33, 'Rank these cities by population', 'Ok, I\'m done', 'Put the items in the correct order', 388, 'That\'s correct!', 'Not quite...', 'Tokyo has almost nine million inhabitants in the city alone, and 13 million in the Greater Tokyo region'),
(34, 'Rank these cities by population', 'Ok, I\'m done', 'Put the items in the correct order', 396, 'That\'s correct!', 'Not quite...', 'Tokyo has almost nine million inhabitants in the city alone, and 13 million in the Greater Tokyo region'),
(35, 'Rank these cities by population', 'Ok, I\'m done', 'Put the items in the correct order', 365, 'That\'s correct!', 'Not quite...', 'Tokyo has almost nine million inhabitants in the city alone, and 13 million in the Greater Tokyo region'),
(36, 'Rank these cities by population', 'Ok, I\'m done', 'Put the items in the correct order', 399, 'That\'s correct!', 'Not quite...', 'Tokyo has almost nine million inhabitants in the city alone, and 13 million in the Greater Tokyo region'),
(37, 'Rank these cities by population', 'Ok, I\'m done', 'Put the items in the correct order', 401, 'That\'s correct!', 'Not quite...', 'Tokyo has almost nine million inhabitants in the city alone, and 13 million in the Greater Tokyo region'),
(38, 'Rank these cities by population', 'Ok, I\'m done', 'Put the items in the correct order', 402, 'That\'s correct!', 'Not quite...', 'Tokyo has almost nine million inhabitants in the city alone, and 13 million in the Greater Tokyo region'),
(39, 'Rank these cities by population', 'Ok, I\'m done', 'Put the items in the correct order', 403, 'That\'s correct!', 'Not quite...', 'Tokyo has almost nine million inhabitants in the city alone, and 13 million in the Greater Tokyo region'),
(40, 'Rank these cities by population', 'Ok, I\'m done', 'Put the items in the correct order', 394, 'That\'s correct!', 'Not quite...', 'Tokyo has almost nine million inhabitants in the city alone, and 13 million in the Greater Tokyo region'),
(41, 'Rank these cities by population', 'Ok, I\'m done', 'Put the items in the correct order', 410, 'That\'s correct!', 'Not quite...', 'Tokyo has almost nine million inhabitants in the city alone, and 13 million in the Greater Tokyo region'),
(42, 'Rank these cities by population', 'Ok, I\'m done', 'Put the items in the correct order', 404, 'That\'s correct!', 'Not quite...', 'Tokyo has almost nine million inhabitants in the city alone, and 13 million in the Greater Tokyo region'),
(43, 'Rank these cities by population', 'Ok, I\'m done', 'Put the items in the correct order', 404, 'That\'s correct!', 'Not quite...', 'Tokyo has almost nine million inhabitants in the city alone, and 13 million in the Greater Tokyo region'),
(44, 'Rank these cities by population', 'Ok, I\'m done', 'Put the items in the correct order', 409, 'That\'s correct!', 'Not quite...', 'Tokyo has almost nine million inhabitants in the city alone, and 13 million in the Greater Tokyo region'),
(45, 'Rank these cities by population', 'Ok, I\'m done', 'Put the items in the correct order', 409, 'That\'s correct!', 'Not quite...', 'Tokyo has almost nine million inhabitants in the city alone, and 13 million in the Greater Tokyo region');

-- --------------------------------------------------------

--
-- Table structure for table `reordering_options`
--

CREATE TABLE `reordering_options` (
  `id` int(10) NOT NULL,
  `options` text,
  `reorder_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reordering_options`
--

INSERT INTO `reordering_options` (`id`, `options`, `reorder_id`) VALUES
(1, 'Tokyo', 1),
(2, 'New York', 1),
(3, 'Sydney', 1),
(4, 'Paris', 1),
(6, 'Tokyo', 3),
(7, 'New York', 3),
(8, 'Sydney', 3),
(9, 'Paris', 3),
(12, 'New York', 2),
(13, 'New York', 6),
(14, 'ssdfds', 4),
(19, 'Tokyo', 6),
(20, 'New York', 6),
(21, 'Sydney', 6),
(22, 'Paris', 6),
(27, 'Sydney', 7),
(28, 'New York', 7),
(29, 'Paris', 7),
(30, 'Tokyo', 7),
(31, 'Tokyo', 8),
(32, 'New York', 8),
(33, 'Sydney', 8),
(34, 'Paris', 8),
(35, 'Tokyo', 9),
(36, 'New York', 9),
(37, 'Sydney', 9),
(38, 'Paris', 9),
(39, 'Tokyo', 10),
(40, 'New York', 10),
(41, 'Sydney', 10),
(42, 'Paris', 10),
(64, 'Birmingham', 12),
(65, 'Moscow', 12),
(66, 'Berlin', 12),
(67, 'London', 12),
(68, 'Manchester', 12),
(77, 'Sydney', 11),
(78, 'Paris', 11),
(79, 'Tokyo', 11),
(80, 'New York', 11),
(97, 'Tokyo', 17),
(98, 'Sydney', 17),
(99, 'New York', 17),
(100, 'Paris', 17),
(149, 'Manchester', 18),
(150, 'Sydney', 18),
(151, 'New York', 18),
(152, 'Paris', 18),
(153, 'Tokyo', 18),
(154, 'Tokyo', 19),
(155, 'New York', 19),
(156, 'Sydney', 19),
(157, 'Paris', 19),
(158, 'Tokyo', 20),
(159, 'New York', 20),
(160, 'Sydney', 20),
(161, 'Paris', 20),
(166, 'Tokyo', 22),
(167, 'New York', 22),
(168, 'Sydney', 22),
(169, 'Paris', 22),
(178, 'Paris', 21),
(179, 'New York', 21),
(180, 'Sydney', 21),
(181, 'Tokyo', 21),
(182, 'Tokyo', 23),
(183, 'New York', 23),
(184, 'Sydney', 23),
(185, 'Paris', 23),
(186, 'Tokyo', 24),
(187, 'New York', 24),
(188, 'Sydney', 24),
(189, 'Paris', 24),
(190, 'Tokyo', 25),
(191, 'New York', 25),
(192, 'Sydney', 25),
(193, 'Paris', 25),
(194, 'Tokyo', 26),
(195, 'New York', 26),
(196, 'Sydney', 26),
(197, 'Paris', 26),
(198, 'Tokyo', 27),
(199, 'New York', 27),
(200, 'Sydney', 27),
(201, 'Paris', 27),
(202, 'Tokyo', 28),
(203, 'New York', 28),
(204, 'Sydney', 28),
(205, 'Paris', 28),
(218, 'Tokyo', 30),
(219, 'New York', 30),
(220, 'Sydney', 30),
(221, 'Paris', 30),
(222, 'Tokyo', 31),
(223, 'New York', 31),
(224, 'Sydney', 31),
(225, 'Paris', 31),
(298, 'Tokyo', 33),
(299, 'Sydney', 33),
(300, 'Paris', 33),
(309, 'Tokyo', 34),
(310, 'New York', 34),
(311, 'Sydney', 34),
(312, 'Paris', 34),
(317, 'Tokyo', 35),
(318, 'New York', 35),
(319, 'Sydney', 35),
(320, 'Paris', 35),
(325, 'pune', 32),
(326, 'Checking 30 characters max lmtasadsadddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd', 32),
(327, 'Mumbai', 32),
(328, 'Patna', 32),
(329, 'Tokyo', 36),
(330, 'New York', 36),
(331, 'Sydney', 36),
(332, 'Paris', 36),
(333, 'Tokyo', 37),
(334, 'New York', 37),
(335, 'Sydney', 37),
(336, 'Paris', 37),
(337, 'Tokyo', 38),
(338, 'New York', 38),
(339, 'Sydney', 38),
(340, 'Paris', 38),
(341, 'Tokyo', 39),
(342, 'New York', 39),
(343, 'Sydney', 39),
(344, 'Paris', 39),
(353, 'Tokyo', 41),
(354, 'New York', 41),
(355, 'Sydney', 41),
(356, 'Paris', 41),
(357, 'Tokyo', 42),
(358, 'New York', 42),
(359, 'Sydney', 42),
(360, 'Paris', 42),
(365, 'pune', 43),
(366, 'kholapur', 43),
(367, 'chinchwad', 43),
(368, 'Paris', 43),
(369, 'Tokyo', 44),
(370, 'New York', 44),
(371, 'Sydney', 44),
(372, 'Paris', 44),
(380, 'pune', 45),
(381, 'nashik', 45),
(382, 'nigdi', 45),
(383, 'Tokyo ', 40),
(384, 'New York', 40),
(385, 'Sydneya', 40),
(386, 'Paris', 40);

-- --------------------------------------------------------

--
-- Table structure for table `resistance_exercise`
--

CREATE TABLE `resistance_exercise` (
  `re_id` int(11) NOT NULL,
  `exercise_type_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `exercise_id` int(11) NOT NULL,
  `number_of_sets` varchar(125) NOT NULL,
  `number_of_repetition` varchar(125) NOT NULL,
  `recovery_betn_sets` varchar(125) NOT NULL,
  `resistance_machine` varchar(125) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `resistance_exercise`
--

INSERT INTO `resistance_exercise` (`re_id`, `exercise_type_id`, `user_id`, `exercise_id`, `number_of_sets`, `number_of_repetition`, `recovery_betn_sets`, `resistance_machine`) VALUES
(1, 4, 28, 8, '2', '3', 'recovery sets', '1%rm'),
(2, 4, 28, 9, '4', '3', 'recoveryBetsets', 'rm'),
(3, 6, 28, 9, 'Jjjjjjuu', 'Hhuuuu', 'Hhhhhhh', 'Hjjjjjiiiu'),
(4, 5, 28, 8, 'Ydifncxg', 'N n xgxbjcn n ', 'Vxncnc mmvjf', 'Xgjvkvmb');

-- --------------------------------------------------------

--
-- Table structure for table `scrollable`
--

CREATE TABLE `scrollable` (
  `id` int(30) NOT NULL,
  `title` varchar(50) NOT NULL,
  `content` text NOT NULL,
  `prompt` varchar(100) NOT NULL,
  `done_text` text NOT NULL,
  `narration` varchar(50) NOT NULL,
  `lesson_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scrollable`
--

INSERT INTO `scrollable` (`id`, `title`, `content`, `prompt`, `done_text`, `narration`, `lesson_id`) VALUES
(1, 'Introduction', 'The Focus Awards Level 4 Certificate in Strength and Conditioning (RQF) is\naimed at learners who are qualified at level 3 and are looking to progress\ninto the specialised field of strength and conditioning.\nThe Focus Awards Level 4 Certificate in Strength and Conditioning (RQF)\nprovides learners with the knowledge and understanding of how to improve\nathletic performance, through a systematic approach developing athletic\nqualities such as speed strength and power, as well as reducing sporting\ninjuries.\nThe Focus Awards Level 4 Certificate in Strength and Conditioning (RQF) is\naimed at learners who are qualified at level 3 and are looking to progress\ninto the specialised field of strength and conditioning.', 'Scroll to see much more', 'Ok, I\'m done', '', 1),
(2, 'Class Descriptions', 'Our group exercise classes, with the exception of aquatics and indoor cycling, take place in our spacious classroom.  Hardwood floors, exposed brick, mirror-lined walls, and large street scape windows host a warm, inviting setting for our comprehensive group exercise class schedule that includes yoga, Pilates, Zumba, cardio-toning, TRX, and more!\n\nOur separate indoor cycling studio holds 16 spinning bikes.  Equipped with overhead theater lighting, a powerful stereo, and plenty of fans for air circulation, these classes will provide a cardio workout ideal for any fitness routine.\n\nClasses range from beginner to experienced, however, most classes are considered appropriate for all levels. Please inquire with our expert staff if you would like assistance choosing the best classes for you. \n\nThe beautifully tranquil, heated pool is open year-round, and usable by fitness center members, as well as aquatic therapy patients during their scheduled appointments. Our whirlpool is adjacent to the pool, and is always available.', 'Scroll to see more', 'Ok, I\'m done', '', 2),
(4, 'Scrolling Content', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus\r\nquis lectus metus, at posuere neque. Sed pharetra nibh eget orci\r\nconvallis at posuere leo convallis. Sed blandit augue vitae augue\r\nscelerisque bibendum. Vivamus sit amet libero turpis, non venenatis\r\nurna. In blandit, odio convallis suscipit venenatis, ante ipsum cursus\r\naugue.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus\r\nquis lectus metus, at posuere neque. Sed pharetra nibh eget orci\r\nconvallis at posuere leo convallis. Sed blandit augue vitae augue\r\nscelerisque bibendum. Vivamus sit amet libero turpis, non venenatis\r\nurna. In blandit, odio convallis suscipit venenatis, ante ipsum cursus\r\naugue.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus\r\nquis lectus metus, at posuere neque. Sed pharetra nibh eget orci\r\nconvallis at posuere leo convallis. Sed blandit augue vitae augue\r\nscelerisque bibendum. Vivamus sit amet libero turpis, non venenatis\r\nurna. In blandit, odio convallis suscipit venenatis, ante ipsum cursus\r\naugue.', 'Scroll to see more', 'Ok, I\'m done', '', 3),
(6, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut \n                labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex \n                ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\n                Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum \n                dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad \n                minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in \n                reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, \n                sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do \n                eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 1),
(7, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut \n                labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex \n                ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\n                Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum \n                dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad \n                minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in \n                reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, \n                sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do \n                eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 1),
(8, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut \n                labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex \n                ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\n                Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum \n                dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad \n                minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in \n                reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, \n                sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do \n                eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 1),
(9, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut \n                labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex \n                ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\n                Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum \n                dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad \n                minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in \n                reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, \n                sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do \n                eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 1),
(10, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut \n                labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex \n                ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\n                Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum \n                dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad \n                minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in \n                reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, \n                sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do \n                eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 1),
(11, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut \n                labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex \n                ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\n                Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum \n                dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad \n                minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in \n                reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, \n                sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do \n                eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 1),
(13, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut \n                labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex \n                ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\n                Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum \n                dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad \n                minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in \n                reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, \n                sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do \n                eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 1),
(14, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut \n                labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex \n                ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\n                Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum \n                dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad \n                minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in \n                reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, \n                sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do \n                eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 1),
(15, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut \n                labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex \n                ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\n                Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum \n                dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad \n                minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in \n                reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, \n                sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do \n                eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 1),
(16, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut \n                labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex \n                ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\n                Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum \n                dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad \n                minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in \n                reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, \n                sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do \n                eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 1),
(17, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut \n                labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex \n                ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\n                Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum \n                dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad \n                minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in \n                reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, \n                sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do \n                eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 1),
(18, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut \n                labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex \n                ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\n                Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum \n                dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad \n                minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in \n                reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, \n                sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do \n                eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 1),
(19, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut \n                labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex \n                ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\n                Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum \n                dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad \n                minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in \n                reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, \n                sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do \n                eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 1),
(20, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut \n                labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex \n                ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\n                Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum \n                dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad \n                minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in \n                reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, \n                sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do \n                eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 1),
(21, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut \n                labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex \n                ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\n                Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum \n                dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad \n                minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in \n                reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, \n                sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do \n                eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 1),
(22, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut \n                labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex \n                ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\n                Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum \n                dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad \n                minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in \n                reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, \n                sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do \n                eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 1),
(23, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut \n                labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex \n                ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\n                Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum \n                dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad \n                minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in \n                reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, \n                sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do \n                eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 1),
(25, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut \n                labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex \n                ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.\n                Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum \n                dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad \n                minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in \n                reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, \n                sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do \n                eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 5),
(30, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 217),
(66, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 298),
(67, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 3),
(68, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 4),
(69, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 6),
(70, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 12),
(71, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 13),
(73, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 46),
(74, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 122),
(77, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 1),
(78, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 1),
(87, 'Bone changes across a lifespan', 'Bone remodelling continually occurs throughout our lifespan. It is a dynamic process involving the balance between bone resorption (where bone tissue is removed from the skeleton) by cells called osteoclasts and bone deposition (bone tissue formation) by cells called osteoblasts. \n\nAt around 10-15 years of age, girls and boys go through puberty where rapidly rising levels of sex hormones influence growth of bone. In girls it is the hormone oestrogen and in boys, testosterone, which greatly accelerates bone mass development resulting in a change in stature. By age twenty, 90% of bone mass is achieved in both girls and boys.\n\nBone mass increases a further 10% during the third decade of life. From the age of 30, bone mass starts to gradually fall in both men and women as a result of declining\nlevels of sex hormones. \n\nRelatively speaking, men are able to achieve a higher bone mass than women due to a\nnumber of factors such as a bigger skeleton, greater overall muscle mass and 10 times\nthe level of testosterone.\n\nHormone levels continue to fall as men and women age, resulting in further reductions\nin bone mass. However, for women, bone loss accelerates around the age of 50 due to\nthe female menopause and the cessation of female hormones. ', 'Scroll to see more', 'Ok, I\'m done', '', 16),
(90, 'Introduction to the muscular system aand the nervo', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.\n', 'Scroll to see more', 'Ok, I\'m done', '', 332),
(91, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 16),
(92, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 16),
(93, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 16),
(94, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 16),
(95, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 16),
(100, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 348),
(102, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 338),
(103, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 338),
(104, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 338);
INSERT INTO `scrollable` (`id`, `title`, `content`, `prompt`, `done_text`, `narration`, `lesson_id`) VALUES
(106, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 356),
(107, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 347),
(110, 'Scrollable', 'scrorablee    \nsda\nad\nsd\ndnnbm\nad\nad\nasd', 'Scroll to see more ', 'Ok, I\'m done', '', 364),
(112, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 354),
(113, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 380),
(114, 'Scrollable', 'UNIT TESTING is a level of software testing where individual units/components of the software are tested. The purpose is to validate that each unit of the software performs as designed. A unit is the smallest testable part of any software. It usually has one or a few inputs and usually a single output.\nIn procedural programming, a unit may be an individual program, function, procedure, etc. In object-oriented programming, the smallest unit is a method, which may belong to a base/ super class, abstract class or derived/ child class. (Some treat a module of an application as a unit. This is to be discouraged as there will probably be many individual units within that module.) Unit testing frameworks, drivers, stubs, and mock/ fake objects are used to assist in unit testing.\nDefinition by ISTQB\nunit testing: See component testing.\ncomponent testing: The testing of individual software components.\nUnit Testing Method\nIt is performed by using the White Box Testing method.\nWhen is it performed?\nUnit Testing is the first level of software testing and is performed prior to Integration Testing.\nWho performs it?\nIt is normally performed by software developers themselves or their peers. In rare cases, it may also be performed by independent software testers.\nUnit Testing Tasks\nUnit Test Plan\nPrepare\nReview\nRework\nBaseline\nUnit Test Cases/Scripts\nPrepare\nReview\nRework\nBaseline\nUnit Test\nPerform\nUnit Testing Benefits\nUnit testing increases confidence in changing/maintaining code. If good unit tests are written and if they are run every time any code is changed, we will be able to promptly catch any defects introduced due to the change. Also, if codes are already made less interdependent to make unit testing possible, the unintended impact of changes to any code is less.\nCodes are more reusable. In order to make unit testing possible, codes need to be modular. This means that codes are easier to reuse.\nDevelopment is faster. How? If you do not have unit testing in place, you write your code and perform that fuzzy ‘developer test’ (You set some breakpoints, fire up the GUI, provide a few inputs that hopefully hit your code, and hope that you are all set.) But, if you have unit testing in place, you write the test, write the code and run the test. Writing tests takes time but the time is compensated by the less amount of time it takes to run the tests; You need not fire up the GUI and provide all those inputs. And, of course, unit tests are more reliable than ‘developer tests’. Development is faster in the long run too. How? The effort required to find and fix defects found during unit testing is very less in comparison to the effort required to fix defects found during system testing or acceptance testing.\nThe cost of fixing a defect detected during unit testing is lesser in comparison to that of defects detected at higher levels. Compare the cost (time, effort, destruction, humiliation) of a defect detected during acceptance testing or when the software is live', 'Scroll to see more', 'Ok, I\'m done', '', 387),
(115, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 387),
(116, 'Scrolable,s\"s\'s', 'scrollable content\n', 'Scroll to see more', 'Ok, I\'m done', '', 388),
(117, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 396),
(118, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 365),
(120, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 401),
(121, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 400),
(122, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 402),
(123, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 403),
(126, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 409),
(127, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do e', 'Scroll to see more', 'Ok, I\'m done', '', 394),
(128, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 418),
(129, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 419),
(130, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 422),
(131, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 423),
(132, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 426),
(133, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 426),
(134, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 427),
(135, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 428),
(136, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 431),
(137, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 432),
(138, 'Scrollable', 'Lorem ipsum doloramet, consectetur adipiscing sdfdsffdf dsfsdf elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim adminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'Scroll to see more', 'Ok, I\'m done', '', 437);

-- --------------------------------------------------------

--
-- Table structure for table `section`
--

CREATE TABLE `section` (
  `id` int(30) NOT NULL,
  `section_name` varchar(150) NOT NULL,
  `section_description` varchar(256) NOT NULL,
  `unit_id` int(30) NOT NULL,
  `image` varchar(256) DEFAULT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'Unpublished',
  `sequence_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `section`
--

INSERT INTO `section` (`id`, `section_name`, `section_description`, `unit_id`, `image`, `status`, `sequence_id`) VALUES
(2, 'Section 1', 'Skeletal System', 1, 'section_images/15911863902031114908.png', 'Published', 1),
(3, 'Muscular System', '', 1, 'section_images/15911927272000134783.png', 'Published', 4),
(7, 'Section 2.1', 'Forming effective relationships with clients', 2, '', 'Published', 0),
(8, 'Behaviour change, \r\nmotivation and exercise \r\nadherence', '', 2, NULL, 'Published', 0),
(9, 'Providing ongoing \r\nservice to clients', '', 2, NULL, 'Published', 0),
(10, 'Understand health and safety \r\nrequirements and emergency \r\nprocedures in a fitness \r\nenvironment', '', 3, NULL, 'Published', 0),
(11, 'Controlling risks in a \r\nfitness environment', '', 3, NULL, 'Published', 0),
(12, 'Understand how to \r\nsafeguard children and \r\nvulnerable adult', '', 3, NULL, 'Published', 0),
(13, 'The effects of exercise \r\non the body', '', 4, NULL, 'Published', 0),
(14, 'Principles of \r\nprogramme design', '', 4, NULL, 'Published', 0),
(15, 'Eating for health', '', 4, NULL, 'Published', 0),
(16, 'Understand how \r\nto collect client \r\ninformation to plan \r\ngym-based exercise', '', 5, NULL, 'Published', 0),
(17, 'Planning gym-based \r\nexercise with clients', '', 5, NULL, 'Published', 0),
(18, 'Section 1', 'The basic function of the cardiovascular system', 7, '', 'Published', 0),
(19, 'Section 2', 'Stabilisation mechanism of the spine', 7, '', 'Unpublished', 0),
(20, 'The musculoskeletal \r\nsystem and its \r\nrelationship to exercise', '', 7, NULL, 'Published', 0),
(21, 'The nervous system \r\nand its relationship to \r\nexercise', '', 7, NULL, 'Published', 0),
(22, 'The endocrine system \r\nand its relationship to \r\nexercise', '', 7, NULL, 'Published', 0),
(23, 'Energy systems and \r\ntheir relationship to \r\nexercise', '', 7, NULL, 'Published', 0),
(24, 'Understanding the \r\nprinciples of nutrition', '', 8, NULL, 'Draft', 0),
(25, 'Key guidelines in \r\nrelation to nutrition', '', 8, NULL, 'Draft', 0),
(26, 'Understand how \r\nto collect and use \r\ninformation relating to \r\nnutrition', '', 8, NULL, 'Draft', 0),
(27, 'Planning personal \r\ntraining with clients', '', 9, NULL, 'Published', 0),
(28, 'Programming personal \r\ntraining with clients', '', 9, NULL, 'Published', 0),
(68, 'Exercise exercise', ' bkb ck ca ma nas s ma', 6, 'section_images/15833168851573935217.png', 'Draft', 0),
(69, 'section', '', 80, '', 'Unpublished', 0),
(111, 'Section 10', '', 218, '', 'Published', 0),
(112, 'Lesson 1.2', 'The aim', 218, '', 'Unpublished', 0),
(113, 'Section 3', 'The aim', 218, '', 'Unpublished', 0),
(117, 'section 68dd', 'aaxkaklllsddd', 224, '', 'Published', 0),
(126, 'Section 2', 'Muscular System', 1, 'section_images/1592291912512390679.png', 'Published', 2),
(128, 'Section 1.0', '', 219, '', 'Unpublished', 0),
(129, 'pta section 1', 'Section 1', 248, '', 'Published', 0),
(130, 'INTRO TO UNIT', '', 1, 'section_images/1591104071621107095.png', 'Unpublished', 3),
(135, 'New section', 'Section 2', 249, 'section_images/1591100680242467039.png', 'Published', 1),
(136, 'Section 1', 'Demo Session', 250, 'section_images/15910864221498454674.png', 'Unpublished', 1),
(138, 'Muscle 1', 'Muscle', 1, '', 'Unpublished', 4),
(139, 'S1', 'Obesity', 284, '', 'Unpublished', 1),
(140, 'newwss', 'sunazaa', 225, '', 'Published', 1),
(142, 'sec number test1 up', 'Section title test 1up', 324, '', 'Published', 1),
(144, 'section 5', 'section 5 description', 328, '', 'Unpublished', 1),
(145, 'section 2 in 5', 'section 2 in 5', 328, '', 'Unpublished', 2),
(146, 'Sectioon for unit 3 t', 'dsffsfewfwf', 326, '', 'Published', 1),
(147, 'another section', 'section description..', 326, '', 'Published', 2),
(148, 'sect', 'aaa', 225, '', 'Published', 2),
(149, 'tty', 'uuuiiiu', 226, '', 'Published', 1),
(152, 'section one', 'ddd', 458, '', 'Published', 1),
(153, 'new kik', 'jkjkkjk', 458, '', 'Published', 2),
(154, 'tgg', 'ftggh', 225, '', 'Published', 3),
(156, 'section one', 'zcc', 306, '', 'Unpublished', 1),
(157, 'AA', 'SSDD', 464, '', 'Published', 1),
(158, 'aaaaa', 'sssss', 470, '', 'Published', 1),
(160, 'New section', 'New section for testing', 478, '', 'Unpublished', 1),
(161, 'theory', 'theory', 477, 'section_images/15953226972008929222.png', 'Published', 1),
(162, 'AS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS AS', 'DE ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS AS', 487, '', 'Unpublished', 1),
(166, 'ddd', 'sss', 492, '', 'Published', 1),
(168, 'section 1.1 ', 'section 1.1 ', 495, '', 'Published', 1),
(169, 'section 1.1', 'section', 478, 'section_images/15956551811438231608.png', 'Unpublished', 2),
(170, 'section 1', 'section 1', 325, '', 'Unpublished', 1),
(173, 'section 2', 'section 2', 511, '', 'Published', 1),
(174, 'section 1', 'section 1', 512, '', 'Published', 1),
(175, '456', '111', 514, '', 'Published', 1),
(176, 'section 1 ......', 'my section', 516, '', 'Published', 0),
(178, 'aaa', 'ssss', 518, '', 'Published', 1),
(179, 'das', 'asda', 228, '', 'Published', 1),
(180, 'asd', 'sad', 519, '', 'Published', 1),
(181, 'asd', 'asd', 520, '', 'Published', 1),
(182, 'ddd', 'aaa', 521, '', 'Published', 1),
(184, 'demo section', 'aa', 524, '', 'Published', 1),
(185, 'demo 2', 'aaa', 524, '', 'Published', 2),
(186, 'deb', 'jjj', 526, '', 'Published', 1),
(188, 'new section', 'ss', 527, '', 'Published', 1),
(189, 'new section', 'new section', 528, '', 'Published', 1),
(190, 'section', 'section', 529, '', 'Published', 1),
(191, 'section 1.1', 'section 1.1', 530, '', 'Published', 1),
(192, 'new sec', 'sec', 531, '', 'Published', 1),
(193, 'section 3.1', 'section3.1', 532, '', 'Published', 1),
(194, 'section 1', 'section 1', 533, '', 'Published', 1),
(195, 'section 1', 'section 1', 534, '', 'Published', 1),
(196, 'section 1', 'section 1', 535, '', 'Published', 1),
(197, 'new section', 'gggggg', 536, '', 'Published', 1),
(198, 'section', 'cvvvvvv', 537, '', 'Published', 1),
(199, 'aaasa', 'aaaa', 538, '', 'Published', 1),
(200, 'ss', 'ssss', 539, '', 'Published', 1),
(201, 'section 1', 'section 1', 540, '', 'Published', 1),
(202, 'another section', '\nkjlk', 541, '', 'Published', 1),
(203, 'gghhh', 'uytuyt', 542, '', 'Published', 1),
(204, 'section1', 'section1', 543, '', 'Published', 1),
(205, 'ee', 'rrr', 544, '', 'Published', 1),
(206, 'ggddd', 'tffff', 545, '', 'Published', 1),
(207, 'hbkk', 'kkk', 547, '', 'Published', 1),
(208, 'dd', 'aa', 548, '', 'Published', 1);

-- --------------------------------------------------------

--
-- Table structure for table `selecting_stretch`
--

CREATE TABLE `selecting_stretch` (
  `s_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `activity` varchar(125) NOT NULL,
  `duration` varchar(125) NOT NULL,
  `stretch_name1` varchar(125) NOT NULL,
  `stretch_name2` varchar(125) NOT NULL,
  `stretch_name3` varchar(125) NOT NULL,
  `stretch_name4` varchar(125) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `selecting_stretch`
--

INSERT INTO `selecting_stretch` (`s_id`, `user_id`, `activity`, `duration`, `stretch_name1`, `stretch_name2`, `stretch_name3`, `stretch_name4`) VALUES
(1, 28, 'Gxhffhhf', 'Hdfhfjjfif', 'Hxckkckcck', 'Jxckkckc', 'Zgxhjcjvjv', 'stretch_name4'),
(2, 1, 'hgj ghjgjg hb', '40s', 'stretchname11', 'stretchname21', 'stretchname13', 'stretch_name4');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(30) NOT NULL,
  `title` varchar(256) DEFAULT NULL,
  `min_value` bigint(50) NOT NULL,
  `max_value` bigint(50) NOT NULL,
  `increment` varchar(30) NOT NULL,
  `label` varchar(30) NOT NULL,
  `answer_text` text NOT NULL,
  `correct_value` bigint(50) NOT NULL,
  `correct_reinforcement` varchar(50) NOT NULL,
  `incorrect_reinforcement` varchar(50) NOT NULL,
  `prompt` varchar(50) NOT NULL,
  `done_text` text NOT NULL,
  `lesson_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `title`, `min_value`, `max_value`, `increment`, `label`, `answer_text`, `correct_value`, `correct_reinforcement`, `incorrect_reinforcement`, `prompt`, `done_text`, `lesson_id`) VALUES
(1, 'In which year was the product introduced?', 1980, 1990, '2', '4', 'OK I\'m done', 1987, 'That\'s correct!', 'Not quite...', 'slider', 'OK I\'m done', 1),
(3, 'Slider', 1985, 1995, '1', '4', 'The product is a true original from 1990!', 1990, 'That\'s correct!', 'Not quite...', 'Slide to the correct value', 'Ok, I\'m done', 1),
(6, 'Slider', 1985, 1995, '1', '4', 'The product is a true original from 1990!', 1990, 'That\'s correct!', 'Not quite...', 'Slide to the correct value', 'Ok, I\'m done', 217),
(24, 'How many bones in the body?', 166, 226, '20', '4', '150', 206, 'That\'s correct!', 'Not quite...', 'How many bones in the body?', 'Ok, I\'m done', 298),
(25, 'Slider', 1985, 1995, '1', '4', 'The product is a true original from 1990!', 1990, 'That\'s correct!', 'Not quite...', 'Slide to the correct value', 'Ok, I\'m done', 7),
(26, 'Slider', 1985, 1995, '1', '4', 'The product is a true original from 1990!', 1990, 'That\'s correct!', 'Not quite...', 'Slide to the correct value', 'Ok, I\'m done', 9),
(27, 'Slider', 1985, 1995, '1', '4', 'The product is a true original from 1990!', 1990, 'That\'s correct!', 'Not quite...', 'Slide to the correct value', 'Ok, I\'m done', 10),
(28, 'Slider', 1985, 1995, '1', '4', 'The product is a true original from 1990!', 1990, 'That\'s correct!', 'Not quite...', 'Slide to the correct value', 'Ok, I\'m done', 45),
(29, 'Slider', 1985, 1995, '1', '4', 'The product is a true original from 1990!', 1990, 'That\'s correct!', 'Not quite...', 'Slide to the correct value', 'Ok, I\'m done', 1),
(30, 'jjj', 1900, 2020, '5', '6', 'The product is a true original from 1990!', 1990, 'That\'s correct!', 'Not quite...', 'Slide to the correct value', 'Ok, I\'m done', 1),
(36, 'Slider', 1985, 1995, '1', '4', 'The product is a true original from 1990!', 1990, 'That\'s correct!', 'Not quite...', 'Slide to the correct value', 'Ok, I\'m done', 16),
(37, 'Slider', 1985, 1995, '1', '4', 'The product is a true original from 1990!', 1990, 'That\'s correct!', 'Not quite...', 'Slide to the correct value', 'Ok, I\'m done', 338),
(41, 'Slider', 1985, 1995, '1', '4', 'The product is a true original from 1990!', 1990, 'That\'s correct!', 'Not quite...', 'Slide to the correct value', 'Ok, I\'m done', 338),
(42, 'Slider', 1985, 1995, '1', '4', 'The product is a true original from 1990!', 1990, 'That\'s correct!', 'Not quite...', 'Slide to the correct value', 'Ok, I\'m done', 338),
(43, 'Slider', 1985, 1995, '1', '4', 'The product is a true original from 1990!', 1990, 'That\'s correct!', 'Not quite...', 'Slide to the correct value', 'Ok, I\'m done', 338),
(44, 'Slider', 1985, 1995, '1', '4', 'The product is a true original from 1990!', 1990, 'That\'s correct!', 'Not quite...', 'Slide to the correct value', 'Ok, I\'m done', 364),
(45, 'Slider', 1985, 1995, '1', '4', 'The product is a true original from 1990!', 1990, 'That\'s correct!', 'Not quite...', 'Slide to the correct value', 'Ok, I\'m done', 354),
(46, 'Slider', 1985, 1995, '1', '4', 'The product is a true original from 1990!', 1990, 'That\'s correct!', 'Not quite...', 'Slide to the correct value', 'Ok, I\'m done', 380),
(47, 'Bod of Alia', 1985, 1995, '1', '4', 'bod is 1991', 1991, 'That\'s correct!', 'Not quite...', 'Slide to the correct value', 'Ok, I\'m done', 387),
(48, 'Slider', 1985, 1995, '1', '5', 'The product is a true original from 1990!', 1990, 'That\'s correct!', 'Not quite...', 'Slide to the correct value', 'Ok, I\'m done', 388),
(49, 'Slider', 1985, 1995, '1', '5', 'The product is a true original from 1990!', 1990, 'That\'s correct!', 'Not quite...', 'Slide to the correct value', 'Ok, I\'m done', 348),
(50, 'Slider', 1985, 1995, '1', '4', 'The product is a true original from 1990!', 1990, 'That\'s correct!', 'Not quite...', 'Slide to the correct value', 'Ok, I\'m done', 379),
(52, 'Slider', 1985, 1995, '1', '4', 'The product is a true original from 1990!', 1990, 'That\'s correct!', 'Not quite...', 'Slide to the correct value', 'Ok, I\'m done', 396),
(53, 'Slider', 1985, 1995, '1', '4', 'The product is a true original from 1990!', 1990, 'That\'s correct!', 'Not quite...', 'Slide to the correct value', 'Ok, I\'m done', 365),
(54, 'Slider', 1985, 1995, '1', '4', 'The product is a true original from 1990!', 1990, 'That\'s correct!', 'Not quite...', 'Slide to the correct value', 'Ok, I\'m done', 347),
(56, 'Slider', 1985, 1995, '1', '4', 'The product is a true original from 1990!', 1990, 'That\'s correct!', 'Not quite...', 'Slide to the correct value', 'Ok, I\'m done', 401),
(57, 'Slider', 1985, 1995, '1', '4', 'The product is a true original from 1990!', 1990, 'That\'s correct!', 'Not quite...', 'Slide to the correct value', 'Ok, I\'m done', 402),
(58, 'Slider', 1985, 1995, '1', '4', 'The product is a true original from 1990!', 1990, 'That\'s correct!', 'Not quite...', 'Slide to the correct value', 'Ok, I\'m done', 402),
(59, 'Slider', 1985, 1995, '1', '4', 'The product is a true original from 1990!', 1990, 'That\'s correct!', 'Not quite...', 'Slide to the correct value', 'Ok, I\'m done', 403),
(60, 'Slider', 1985, 1995, '1', '4', 'The product is a true original from 1990!', 1990, 'That\'s correct!', 'Not quite...', 'Slide to the correct value', 'Ok, I\'m done', 410),
(63, 'Slider', 1985, 1995, '1', '4', 'The product is a true original from 1990!', 1990, 'That\'s correct!', 'Not quite...', 'Slide to the correct value', 'Ok, I\'m done', 415),
(64, 'Slider', 1985, 1995, '1', '4', 'The product is a true original from 1990!', 1990, 'That\'s correct!', 'Not quite...', 'Slide to the correct value', 'Ok, I\'m done', 394);

-- --------------------------------------------------------

--
-- Table structure for table `true_false_statement`
--

CREATE TABLE `true_false_statement` (
  `id` int(10) NOT NULL,
  `text` varchar(256) NOT NULL,
  `correct_answer` varchar(256) NOT NULL,
  `true_false_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `true_false_statement`
--

INSERT INTO `true_false_statement` (`id`, `text`, `correct_answer`, `true_false_id`) VALUES
(74, 'Africa is the continent with the most countries', 'true', 34),
(75, 'Africa is the continent with the most countries', 'true', 35),
(77, 'Africa is the continent with the most countries', 'true', 37),
(78, 'Africa is the continent with the most countries', 'true', 38),
(79, 'Africa is the continent with the most countries', 'true', 39),
(80, 'Africa is the continent with the most countries', 'true', 40),
(81, 'Africa is the continent with the most countries', 'true', 41),
(87, 'Africa is the continent with the most', 'false', 1),
(102, 'Africa is the continent with the most countries', 'true', 53),
(103, 'Africa is the continent with the most countries', 'true', 54),
(113, 'Africa is the continent with the most countries', 'true', 57),
(118, 'Africa is the continent with the most countries safsfsfsfdffreaffffffff4t43t43ffdsfgsdgfdgfdgfdgfdgs', 'true', 55),
(122, 'Africa is the continent with the most countries', 'true', 58),
(124, 'Africa is the continent with the most countries', 'true', 59),
(126, 'Africa is the continent with the most countries', 'true', 60),
(130, 'Africa is the continent with the most countries', 'false', 61),
(131, 'Africa is the continent with the most countries', 'true', 62),
(132, 'Africa is the continent with the most countries', 'true', 63),
(135, 'Africa is the continent with the most countries', 'true', 64),
(136, 'Africa is the continent with the most countries', 'true', 65),
(143, ' DSDM originally sought to provide some discipline to the \"RAD\" method.', 'true', 66),
(158, 'Africa is the continent with the most countries updatinggg', 'true', 67),
(159, 'Africa is the continent with the most countries', 'true', 68),
(161, 'Africa is the continent with the most countries', 'true', 69),
(162, 'Africa is the continent with the most countries', 'true', 70),
(164, 'Africa is the continent with the most countries', 'true', 72),
(165, 'Africa is the continent with the most countries', 'true', 73),
(166, 'Africa is the continent with the most countries', 'true', 74),
(171, 'Africa is the continent with the most countries', 'true', 78),
(172, 'Union cabinet decision will enable the boosting of Space economy by promoting private entities', 'true', 77),
(173, 'Africa is the continent with the most countries', 'true', 79),
(174, 'Africa is the continent with the most countries', 'true', 80),
(176, 'Africa is the continent with the most countries', 'true', 81);

-- --------------------------------------------------------

--
-- Table structure for table `true_or_false`
--

CREATE TABLE `true_or_false` (
  `id` int(30) NOT NULL,
  `title` varchar(256) NOT NULL,
  `description` text NOT NULL,
  `done_text` varchar(200) NOT NULL,
  `lesson_id` int(10) NOT NULL,
  `answer_text` varchar(256) NOT NULL,
  `correct_reinforcement` varchar(256) NOT NULL,
  `incorrect_reinforcement` varchar(256) NOT NULL,
  `prompt` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `true_or_false`
--

INSERT INTO `true_or_false` (`id`, `title`, `description`, `done_text`, `lesson_id`, `answer_text`, `correct_reinforcement`, `incorrect_reinforcement`, `prompt`) VALUES
(1, 'test', 'test', 'Ok, I\'m done', 1, 'test', 'That is correct!', 'Not quite...', 'test'),
(34, 'True or false?', 'Which of these statements is true? Swipe the incorrect statements to the left and the true statements to the right.', 'Ok, I\'m done', 298, 'Definitely. True and false works exactly as you would expect.', 'That\'s correct!', 'Not quite...', 'Swipe to answer'),
(35, 'True or false?', 'Which of these statements is true? Swipe the incorrect statements to the left and the true statements to the right.', 'Ok, I\'m done', 298, 'Definitely. True and false works exactly as you would expect.', 'That\'s correct!', 'Not quite...', 'Swipe to answer'),
(37, 'True or false?', 'Which of these statements is true? Swipe the incorrect statements to the left and the true statements to the right.', 'Ok, I\'m done', 8, 'Definitely. True and false works exactly as you would expect.', 'That\'s correct!', 'Not quite...', 'Swipe to answer'),
(38, 'True or false?', 'Which of these statements is true? Swipe the incorrect statements to the left and the true statements to the right.', 'Ok, I\'m done', 9, 'Definitely. True and false works exactly as you would expect.', 'That\'s correct!', 'Not quite...', 'Swipe to answer'),
(39, 'True or false?', 'Which of these statements is true? Swipe the incorrect statements to the left and the true statements to the right.', 'Ok, I\'m done', 14, 'Definitely. True and false works exactly as you would expect.', 'That\'s correct!', 'Not quite...', 'Swipe to answer'),
(40, 'True or false?', 'Which of these statements is true? Swipe the incorrect statements to the left and the true statements to the right.', 'Ok, I\'m done', 1, 'Definitely. True and false works exactly as you would expect.', 'That\'s correct!', 'Not quite...', 'Swipe to answer'),
(41, 'True or false?', 'Which of these statements is true? Swipe the incorrect statements to the left and the true statements to the right.', 'Ok, I\'m done', 1, 'Definitely. True and false works exactly as you would expect.', 'That\'s correct!', 'Not quite...', 'Swipe to answer'),
(53, 'True or false?', 'Which of these statements is true? Swipe the incorrect statements to the left and the true statements to the right.', 'Ok, I\'m done', 332, 'Definitely. True and false works exactly as you would expect.', 'That\'s correct!', 'Not quite...', 'Swipe to answer'),
(54, 'True or false?', 'Which of these statements is true? Swipe the incorrect statements to the left and the true statements to the right.', 'Ok, I\'m done', 332, 'Definitely. True and false works exactly as you would expect.', 'That\'s correct!', 'Not quite...', 'Swipe to answer'),
(55, 'false', 'Which of these statements is true? Swipe the incorrect statements to the left and the true statements to the right.', 'Ok, I\'m done', 338, 'Definitely. True and false works exactly as you would expect.', 'That\'s correct!', 'Not quite...', 'Swipe to answer'),
(57, 'True or false?', 'Which of these statements is true? Swipe the incorrect statements to the left and the true statements to the right.', 'Ok, I\'m done', 338, 'Definitely. True and false works exactly as you would expect.', 'That\'s correct!', 'Not quite...', 'Swipe to answer'),
(58, 'True or false?', 'Which of these statements is true? Swipe the incorrect statements to the left and the true statements to the right.', 'Ok, I\'m done', 122, 'Definitely. True and false works exactly as you would expect.', 'That\'s correct!', 'Not quite...', 'Swipe to answer'),
(59, 'True or false?', 'Which of these statements is true? Swipe the incorrect statements to the left and the true statements to the right.', 'Ok, I\'m done', 123, 'Definitely. True and false works exactly as you would expect.', 'That\'s correct!', 'Not quite...', 'Swipe to answer'),
(60, 'True or false?', 'Which of these statements is true? Swipe the incorrect statements to the left and the true statements to the right.', 'Ok, I\'m done', 361, 'I am correct', 'That\'s correct!', 'Not quite...', 'Swipe to answer'),
(61, 'True or false?', 'Which of these statements is true? Swipe the incorrect statements to the left and the true statements to the right.', 'Ok, I\'m done', 364, 'Definitely. True and false works exactly as you would expect. good updated yuppy', 'That\'s correct!!!', 'Not quite...', 'Swipe to answer'),
(62, 'True or false?', 'Which of these statements is true? Swipe the incorrect statements to the left and the true statements to the right.', 'Ok, I\'m done', 354, 'Definitely. True and false works exactly as you would expect.', 'That\'s correct!', 'Not quite...', 'Swipe to answer'),
(63, 'True or false?', 'Which of these statements is true? Swipe the incorrect statements to the left and the true statements to the right.', 'Ok, I\'m done', 347, 'Definitely. True and false works exactly as you would expect.', 'That\'s correct!', 'Not quite...', 'Swipe to answer'),
(64, 'True or false?', 'Which of these statements is true? Swipe the incorrect statements to the left and the true statements to the right.', 'Ok, I\'m done', 377, 'Definitely. True and false works exactly as you would expect.', 'That\'s correct!', 'Not quite...', 'Swipe to answer'),
(65, 'True or false?', 'Which of these statements is true? Swipe the incorrect statements to the left and the true statements to the right.', 'Ok, I\'m done', 383, 'Definitely. True and false works exactly as you would expect.', 'That\'s correct!', 'Not quite...', 'Swipe to answer'),
(66, 'True or false?', 'Which of these statements is true? Swipe the incorrect statements to the left and the true statements to the right.', 'Ok, I\'m done', 387, 'DSDM was originally made to provide some structure to Rapid Application Development.', 'That\'s correct!', 'Not quite...', 'Swipe to answer'),
(67, 'A title slide of testing ..', 'description...', 'Ok, I\'m done.........', 388, 'Africa is the continent with the most countries', 'aa', 'no', 'Swipe to answer..up'),
(68, 'True or false?', 'Which of these statements is true? Swipe the incorrect statements to the left and the true statements to the right.', 'Ok, I\'m done', 388, 'Definitely. True and false works exactly as you would expect.', 'That\'s correct!', 'Not quite...', 'Swipe to answer'),
(69, 'True or false?', 'Which of these statements is true? Swipe the incorrect statements to the left and the true statements to the right.', 'Ok, I\'m done', 396, 'Definitely. True and false works exactly as you would expect.', 'That\'s correct!', 'Not quite...', 'Swipe to answer'),
(70, 'True or false?', 'Which of these statements is true? Swipe the incorrect statements to the left and the true statements to the right.', 'Ok, I\'m done', 365, 'Definitely. True and false works exactly as you would expect.', 'That\'s correct!', 'Not quite...', 'Swipe to answer'),
(72, 'True or false?', 'Which of these statements is true? Swipe the incorrect statements to the left and the true statements to the right.', 'Ok, I\'m done', 401, 'Definitely. True and false works exactly as you would expect.', 'That\'s correct!', 'Not quite...', 'Swipe to answer'),
(73, 'True or false?', 'Which of these statements is true? Swipe the incorrect statements to the left and the true statements to the right.', 'Ok, I\'m done', 402, 'Definitely. True and false works exactly as you would expect.', 'That\'s correct!', 'Not quite...', 'Swipe to answer'),
(74, 'True or false?', 'Which of these statements is true? Swipe the incorrect statements to the left and the true statements to the right.', 'Ok, I\'m done', 403, 'Definitely. True and false works exactly as you would expect.', 'That\'s correct!', 'Not quite...', 'Swipe to answer'),
(77, 'True or false?', 'Which of these statements is true? Swipe the incorrect statements to the left and the true statements to the right.', 'Ok, I\'m done', 409, 'Definitely. True and false works exactly as you would expect.', 'That\'s correct!', 'Not quite...', 'Swipe to answer'),
(78, 'True or false?', 'Which of these statements is true? Swipe the incorrect statements to the left and the true statements to the right.', 'Ok, I\'m done', 409, 'Definitely. True and false works exactly as you would expect.', 'That\'s correct!', 'Not quite...', 'Swipe to answer'),
(79, 'True or false?', 'Which of these statements is true? Swipe the incorrect statements to the left and the true statements to the right.', 'Ok, I\'m done', 415, 'Definitely. True and false works exactly as you would expect.', 'That\'s correct!', 'Not quite...', 'Swipe to answer'),
(80, 'True or false?', 'Which of these statements is true? Swipe the incorrect statements to the left and the true statements to the right.', 'Ok, I\'m done', 419, 'Definitely. True and false works exactly as you would expect.', 'That\'s correct!', 'Not quite...', 'Swipe to answer'),
(81, 'True or false?', 'Which of these statements is true? Swipe the incorrect statements to the left and the true statements to the right.', 'Ok, I\'m done', 435, 'Definitely. True and false works exactly as you would expect.', 'That\'s correct!', 'Not quite...', 'Swipe to answer');

-- --------------------------------------------------------

--
-- Table structure for table `unit`
--

CREATE TABLE `unit` (
  `id` int(30) NOT NULL,
  `unit_name` varchar(100) NOT NULL,
  `unit_description` varchar(256) NOT NULL,
  `course_id` int(30) NOT NULL,
  `image` varchar(256) DEFAULT NULL,
  `status` varchar(20) DEFAULT 'Unpublished',
  `is_assessment` varchar(10) NOT NULL,
  `sequence_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `unit`
--

INSERT INTO `unit` (`id`, `unit_name`, `unit_description`, `course_id`, `image`, `status`, `is_assessment`, `sequence_id`) VALUES
(1, 'Unit 1', 'Anatomy and Physiology \nfor Exercise and Health', 1, 'unit_images/1591189356432577346.png', 'Published', 'true', 0),
(2, 'Unit 2', 'Know how to support \nclients who take part in \nphysical activity', 1, '', 'Published', 'true', 1),
(3, 'Unit 3', 'Health, Safety & \r\nWelfare in a Fitness \r\nEnvironment', 1, '', 'Published', 'true', 2),
(4, 'Unit 4', 'Principles of Exercise, \r\nFitness and Health', 1, 'unit_images/1591189462398283523.png', 'Published', 'true', 3),
(5, 'Unit 5', 'Planning gym-based \r\nexercise', 1, 'unit_images/15911908681009733801.png', 'Published', 'true', 4),
(6, 'Unit 6', 'Deliver gym-based \r\nexercise', 1, 'unit_images/15911910501421741011.png', 'Published', 'true', 5),
(7, 'Unit 1', 'Antomy and Physiology \r\nfor Exercise and Health', 2, 'unit_images/15898168981419801813.png', 'Published', 'true', 0),
(8, 'Unit 2', 'Applying the Principles \r\nof Nutrition to a \r\nPhysical Activity\r\nProgramme', 2, '', 'Published', 'true', 1),
(9, 'Unit 3', 'Plan and Programme \r\nPersonal Training\r\nUnit 4\r\nDelivering Personal \r\nTraining', 2, '', 'Published', 'true', 2),
(10, 'Unit 4', 'Delivering Personal \r\nTraining Sessions', 2, '', 'Published', 'true', 3),
(218, 'Unit 1', 'Biomechanics, Physiology and Psychology', 76, '', 'Published', 'true', 0),
(219, 'Unit 2', 'Planning Strength and Conditioning Programmes', 76, '', 'Unpublished', 'true', 0),
(220, 'Unit 3', 'Deliver Strength and Conditioning', 76, '', 'Unpublished', 'true', 0),
(224, 'Unit1myghyuguyggggggggggggggggggggguyguiiuoooooooooooooooooooooouiuigui', 'Unit 1;;', 77, '', 'Published', 'true', 0),
(225, 'Unit 2', 'Unit 2', 77, 'unit_images/159415471548958592.png', 'Published', 'true', 0),
(226, 'Unit 3', 'Unit 3', 77, '', 'Published', 'true', 0),
(228, 'Unit 5', 'Unit 5', 77, '', 'Published', 'true', 0),
(248, 'Unit 1', 'Assessment Pathway', 81, 'unit_images/1593765390917928067.png', 'Published', 'true', 0),
(249, 'Unit 2', 'Unit 2', 81, 'unit_images/1593771301861696924.png', 'Published', 'true', 0),
(250, 'Unit 3', 'Unit 3', 81, NULL, 'Unpublished', 'true', 0),
(251, 'Unit 4', 'Unit 4', 81, NULL, 'Unpublished', 'true', 0),
(252, 'Unit 5', 'Unit 5', 81, NULL, 'Unpublished', 'true', 0),
(254, 'Unit 1', 'Unit 1', 82, '', 'Unpublished', 'true', 0),
(255, 'Unit 2', 'Unit 2', 82, NULL, 'Unpublished', 'true', 0),
(256, 'Unit 3', 'Unit 3', 82, NULL, 'Unpublished', 'true', 0),
(257, 'Unit 4', 'Unit 4', 82, NULL, 'Unpublished', 'true', 0),
(258, 'Unit 6', 'Unit 6', 82, 'unit_images/15910826621449244306.png', 'Unpublished', 'true', 0),
(283, 'Unit 7', '', 81, '', 'Unpublished', 'true', 1),
(284, 'aeroplane', 'raltionships', 86, '', 'Unpublished', 'true', 1),
(285, 'Unit 2', 'Unit 2', 86, NULL, 'Unpublished', 'true', 2),
(286, 'Unit 3', 'Unit 3', 86, NULL, 'Unpublished', 'true', 3),
(287, 'Unit 4', 'Unit 4', 86, NULL, 'Unpublished', 'true', 4),
(296, 'Unit 1', 'Unit 1', 88, NULL, 'Unpublished', 'true', 1),
(297, 'Unit 2', 'Unit 2', 88, NULL, 'Unpublished', 'true', 2),
(298, 'Unit 3', 'Unit 3', 88, NULL, 'Unpublished', 'true', 3),
(299, 'Unit 4', 'Unit 4', 88, NULL, 'Unpublished', 'true', 4),
(300, 'Unit 5', 'Unit 5', 88, NULL, 'Unpublished', 'true', 5),
(301, 'Unit 6', 'Unit 6', 88, NULL, 'Unpublished', 'false', 6),
(306, 'Unit 1', 'Unit 1', 89, '', 'Unpublished', 'true', 1),
(307, 'Unit 2', 'Unit 2', 89, NULL, 'Unpublished', 'true', 2),
(308, 'Unit 3', 'Unit 3', 89, NULL, 'Unpublished', 'true', 3),
(309, 'Unit 4', 'Unit 4', 89, NULL, 'Unpublished', 'true', 4),
(310, 'Unit 5', 'Unit 5', 89, NULL, 'Unpublished', 'true', 5),
(311, 'Unit 6', 'Unit 6', 89, NULL, 'Unpublished', 'false', 6),
(324, 'Ash @Unit 1 up', 'Unit 1 up', 92, '', 'Published', 'true', 1),
(325, 'Unit 2', 'Unit 2', 92, '', 'Unpublished', 'true', 2),
(326, 'Unit 3', 'Unit 3 ', 92, '', 'Unpublished', 'true', 3),
(327, 'Unit 4', 'Unit 4', 92, NULL, 'Unpublished', 'true', 4),
(328, 'Unit 5', 'Unit 5', 92, NULL, 'Unpublished', 'true', 5),
(329, 'Unit 6', 'Unit 6', 92, NULL, 'Unpublished', 'false', 6),
(372, 'Unit 1', 'Unit 1', 100, NULL, 'Unpublished', 'true', 1),
(373, 'Unit 2', 'Unit 2', 100, NULL, 'Unpublished', 'true', 2),
(374, 'Unit 3', 'Unit 3', 100, NULL, 'Unpublished', 'true', 3),
(375, 'Unit 4', 'Unit 4', 100, NULL, 'Unpublished', 'true', 4),
(376, 'Unit 5', 'Unit 5', 100, NULL, 'Unpublished', 'true', 5),
(377, 'Unit 6', 'Unit 6', 100, NULL, 'Unpublished', 'false', 6),
(457, 'swapnil', 'unit 8', 81, '', 'Unpublished', 'true', 2),
(458, 'Unit 1', 'Unit 1', 113, '', 'Published', 'true', 1),
(460, 'Unit 3', 'Unit 3', 113, NULL, 'Unpublished', 'true', 3),
(461, 'Unit 4', 'Unit 4', 113, NULL, 'Unpublished', 'true', 4),
(462, 'Unit 5', 'Unit 5', 113, NULL, 'Unpublished', 'true', 5),
(464, 'Unit 6', 'SHHJSI', 77, '', 'Published', 'true', 2),
(470, 'Unit 9', 'unit 9', 77, '', 'Published', 'false', 5),
(474, 'unit 6', 'unit 6', 113, 'unit_images/15952217211571234076.png', 'Unpublished', 'false', 6),
(475, 'unit 10', 'unit10', 92, '', 'Unpublished', 'true', 7),
(476, 'unit 11', 'unit11', 92, '', 'Unpublished', 'false', 8),
(477, 'Unit Testing', 'Unit Testing', 116, 'unit_images/15953157171554723638.png', 'Published', 'true', 1),
(478, 'NEW Val', 'jk', 117, '', 'Unpublished', 'true', 1),
(481, 'sfdf', 'sdfsdfs', 92, '', 'Unpublished', 'true', 9),
(483, 'Unit 3 Latest', 'UN 3 FOR TESTING', 117, '', 'Unpublished', 'true', 3),
(485, 'Unit 5th', '5th unit for testing', 117, '', 'Unpublished', 'true', 5),
(487, 'AS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS AS', 'DAS ASAS ASASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS ASAS AS', 120, '', 'Unpublished', 'true', 1),
(492, 'unit 10', 'dssddsd', 77, '', 'Published', 'true', 6),
(495, 'unit 1 sdaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 'dgggggggggdgsdggggggggggggggggggggasdddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd', 115, '', 'Published', 'true', 1),
(496, 'unit 7', 'unit 7', 113, '', 'Unpublished', 'true', 7),
(511, 'unit 2', 'unit 2', 123, 'unit_images/15962036021422016806.png', 'Published', 'false', 2),
(512, 'unit 1 ffdf gdgdfgfgfgfg', 'unit 1', 124, '', 'Published', 'true', 1),
(513, 'unit 2', 'unit 2', 124, '', 'Unpublished', 'false', 2),
(514, '123sjjjjjjjjjjjjjjjjjhjjjjjjjskskskskskskskskskskskskskskskskskskskskskhkjjhj', '1322', 124, '', 'Published', 'true', 3),
(515, 'unit 1', 'my unit 1', 124, 'unit_images/15967062912080709103.png', 'Unpublished', 'true', 4),
(516, 'test unit demo', 'my unit', 115, '', 'Published', 'true', 2),
(518, 'agin test', 'aaass', 115, 'unit_images/1596358983171275390.png', 'Published', 'true', 4),
(519, 'asda', 'asd', 77, '', 'Published', 'true', 7),
(520, 'asd', 'asd', 115, '', 'Published', 'true', 5),
(521, 'NEW ONE', 'SSSSSSSS', 77, '', 'Published', 'true', 8),
(522, 'unit 1', 'unit', 121, '', 'Unpublished', 'false', 1),
(524, 'demo unit', 'aa', 125, '', 'Published', 'true', 1),
(526, 'demo3', 'jkkk', 125, '', 'Published', 'true', 2),
(527, 'neww unit', 'ddd', 115, '', 'Published', 'true', 6),
(528, 'new unit', 'new unit', 126, '', 'Published', 'true', 1),
(529, 'unit 1', 'unit 1', 127, 'unit_images/159712510939956327.png', 'Published', 'true', 1),
(530, 'unit 2', 'unit 2', 126, '', 'Published', 'true', 2),
(531, 'again new', 'again new', 115, '', 'Published', 'true', 7),
(532, 'unit 3', 'unit 3', 123, '', 'Published', 'true', 3),
(533, 'unit 4', 'unit 4', 123, '', 'Published', 'true', 4),
(534, 'unit 5', 'unit 5', 123, '', 'Published', 'true', 5),
(535, 'unit 6', 'unit 6', 123, '', 'Published', 'true', 6),
(536, 'Unit 1 ', 'Strength', 128, 'unit_images/15972230852041940693.png', 'Published', 'true', 1),
(537, 'Pass fail unit', 'hnc  hchhchh', 128, 'unit_images/1597223253587175405.png', 'Published', 'false', 2),
(538, 'new one', 'ggg', 128, 'unit_images/15972233321485916173.png', 'Published', 'true', 3),
(539, 'new oneeee', 'dcc', 128, 'unit_images/15972233742126344758.png', 'Published', 'true', 4),
(540, 'unit 7', 'unit 7', 123, '', 'Published', 'true', 7),
(541, 'another one', 'gghgg', 128, 'unit_images/15972234021532403880.png', 'Published', 'true', 5),
(542, 'hhj', 'hhh', 128, 'unit_images/1597223431168066398.png', 'Published', 'true', 6),
(543, 'unit 8', 'unit 8', 123, '', 'Published', 'true', 8),
(544, 'eee', 'eee', 129, '', 'Published', 'true', 1),
(545, 'rrrr', 'sssss', 129, '', 'Published', 'true', 2),
(547, 'pass', 'hhhj', 129, '', 'Published', 'false', 3),
(548, 'ddd', 'ssss', 128, '', 'Published', 'true', 7);

-- --------------------------------------------------------

--
-- Table structure for table `user_chat`
--

CREATE TABLE `user_chat` (
  `chat_id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `text_message` text NOT NULL,
  `date_time_sent` varchar(35) NOT NULL,
  `receiver_role` varchar(11) NOT NULL,
  `sender_role` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_chat`
--

INSERT INTO `user_chat` (`chat_id`, `sender_id`, `receiver_id`, `text_message`, `date_time_sent`, `receiver_role`, `sender_role`) VALUES
(1, 15, 7, 'HI', '2020-05-13 14:46:10', '', ''),
(2, 15, 7, 'How are you?', '2020-05-13 14:46:41', '', ''),
(3, 15, 4, 'How are you?', '2020-05-14 04:45:48', '', ''),
(4, 15, 27, 'Hiiii', '2020-05-15 04:12:51', '', ''),
(5, 15, 28, 'Hi David, your email report is available', '2020-05-20 14:27:46', '', ''),
(6, 11, 22, 'hdhdhdt', '2020-05-20 14:28:27', 'admin', 'admin'),
(7, 11, 22, 'vciwyvciywvicyvwuecwe', '2020-05-22 07:43:47', 'iqa', 'admin'),
(8, 22, 7, 'cchghchchgchhcghchghch', '2020-05-22 07:44:16', 'assessor', 'iqa'),
(9, 22, 11, 'Hii', '2020-05-25 06:41:42', 'admin', 'iqa'),
(10, 22, 11, 'Hello', '2020-05-25 08:25:53', 'admin', 'iqa'),
(11, 22, 11, 'hiii', '2020-05-25 14:36:58', 'admin', 'iqa'),
(12, 15, 4, 'hiiii', '2020-05-29 12:31:07', 'iqa', 'admin'),
(13, 15, 1, 'How are you today?', '2020-06-09 15:01:17', 'learner', 'admin'),
(14, 15, 27, 'How are you today?', '2020-06-09 15:01:17', 'learner', 'admin'),
(15, 15, 28, 'How are you today?', '2020-06-09 15:01:17', 'learner', 'admin'),
(16, 15, 29, 'How are you today?', '2020-06-09 15:01:17', 'learner', 'admin'),
(17, 15, 34, 'How are you today?', '2020-06-09 15:01:17', 'learner', 'admin'),
(18, 15, 35, 'How are you today?', '2020-06-09 15:01:17', 'learner', 'admin'),
(19, 15, 37, 'How are you today?', '2020-06-09 15:01:17', 'learner', 'admin'),
(20, 15, 53, 'How are you today?', '2020-06-09 15:01:17', 'learner', 'admin'),
(21, 15, 56, 'How are you today?', '2020-06-09 15:01:17', 'learner', 'admin'),
(22, 15, 57, 'How are you today?', '2020-06-09 15:01:17', 'learner', 'admin'),
(23, 15, 4, 'hii', '2020-06-09 15:04:01', 'iqa', 'admin'),
(24, 15, 11, 'hii', '2020-06-09 15:04:01', 'iqa', 'admin'),
(25, 15, 14, 'hii', '2020-06-09 15:04:01', 'iqa', 'admin'),
(26, 15, 15, 'hii', '2020-06-09 15:04:01', 'iqa', 'admin'),
(27, 15, 19, 'hii', '2020-06-09 15:04:01', 'iqa', 'admin'),
(28, 15, 22, 'hii', '2020-06-09 15:04:01', 'iqa', 'admin'),
(29, 15, 30, 'hii', '2020-06-09 15:04:01', 'iqa', 'admin'),
(30, 15, 31, 'hii', '2020-06-09 15:04:01', 'iqa', 'admin'),
(31, 15, 32, 'hii', '2020-06-09 15:04:01', 'iqa', 'admin'),
(32, 15, 38, 'hii', '2020-06-09 15:04:01', 'iqa', 'admin'),
(33, 15, 45, 'hii', '2020-06-09 15:04:01', 'iqa', 'admin'),
(34, 15, 46, 'hii', '2020-06-09 15:04:01', 'iqa', 'admin'),
(35, 15, 55, 'hii', '2020-06-09 15:04:01', 'iqa', 'admin'),
(36, 15, 7, 'hiii', '2020-06-09 15:04:24', 'assessor', 'admin'),
(37, 15, 16, 'hiii', '2020-06-09 15:04:24', 'assessor', 'admin'),
(38, 15, 20, 'hiii', '2020-06-09 15:04:24', 'assessor', 'admin'),
(39, 15, 33, 'hiii', '2020-06-09 15:04:24', 'assessor', 'admin'),
(40, 15, 36, 'hiii', '2020-06-09 15:04:24', 'assessor', 'admin'),
(41, 15, 39, 'hiii', '2020-06-09 15:04:24', 'assessor', 'admin'),
(42, 15, 40, 'hiii', '2020-06-09 15:04:24', 'assessor', 'admin'),
(43, 15, 41, 'hiii', '2020-06-09 15:04:24', 'assessor', 'admin'),
(44, 15, 42, 'hiii', '2020-06-09 15:04:24', 'assessor', 'admin'),
(45, 15, 43, 'hiii', '2020-06-09 15:04:24', 'assessor', 'admin'),
(46, 15, 44, 'hiii', '2020-06-09 15:04:24', 'assessor', 'admin'),
(47, 15, 47, 'hiii', '2020-06-09 15:04:24', 'assessor', 'admin'),
(48, 15, 54, 'hiii', '2020-06-09 15:04:24', 'assessor', 'admin'),
(49, 22, 1, 'hiiiii', '2020-06-10 06:00:35', 'admin', 'iqa'),
(50, 22, 2, 'hiiiii', '2020-06-10 06:00:35', 'admin', 'iqa'),
(51, 22, 3, 'hiiiii', '2020-06-10 06:00:35', 'admin', 'iqa'),
(52, 22, 4, 'hiiiii', '2020-06-10 06:00:35', 'admin', 'iqa'),
(53, 22, 5, 'hiiiii', '2020-06-10 06:00:35', 'admin', 'iqa'),
(54, 22, 6, 'hiiiii', '2020-06-10 06:00:35', 'admin', 'iqa'),
(55, 22, 7, 'hiiiii', '2020-06-10 06:00:35', 'admin', 'iqa'),
(56, 22, 8, 'hiiiii', '2020-06-10 06:00:35', 'admin', 'iqa'),
(57, 22, 9, 'hiiiii', '2020-06-10 06:00:35', 'admin', 'iqa'),
(58, 22, 10, 'hiiiii', '2020-06-10 06:00:35', 'admin', 'iqa'),
(59, 22, 11, 'hiiiii', '2020-06-10 06:00:35', 'admin', 'iqa'),
(60, 22, 12, 'hiiiii', '2020-06-10 06:00:35', 'admin', 'iqa'),
(61, 22, 13, 'hiiiii', '2020-06-10 06:00:35', 'admin', 'iqa'),
(62, 22, 14, 'hiiiii', '2020-06-10 06:00:35', 'admin', 'iqa'),
(63, 22, 15, 'hiiiii', '2020-06-10 06:00:35', 'admin', 'iqa'),
(64, 22, 16, 'hiiiii', '2020-06-10 06:00:35', 'admin', 'iqa'),
(65, 22, 17, 'hiiiii', '2020-06-10 06:00:35', 'admin', 'iqa'),
(66, 22, 18, 'hiiiii', '2020-06-10 06:00:35', 'admin', 'iqa'),
(67, 22, 19, 'hiiiii', '2020-06-10 06:00:35', 'admin', 'iqa'),
(68, 22, 20, 'hiiiii', '2020-06-10 06:00:35', 'admin', 'iqa'),
(69, 22, 21, 'hiiiii', '2020-06-10 06:00:35', 'admin', 'iqa'),
(70, 22, 22, 'hiiiii', '2020-06-10 06:00:35', 'admin', 'iqa'),
(71, 22, 23, 'hiiiii', '2020-06-10 06:00:35', 'admin', 'iqa'),
(72, 22, 7, 'hiii', '2020-06-10 06:00:55', 'assessor', 'iqa'),
(73, 22, 16, 'hiii', '2020-06-10 06:00:55', 'assessor', 'iqa'),
(74, 22, 20, 'hiii', '2020-06-10 06:00:55', 'assessor', 'iqa'),
(75, 22, 33, 'hiii', '2020-06-10 06:00:55', 'assessor', 'iqa'),
(76, 22, 36, 'hiii', '2020-06-10 06:00:55', 'assessor', 'iqa'),
(77, 22, 39, 'hiii', '2020-06-10 06:00:55', 'assessor', 'iqa'),
(78, 22, 40, 'hiii', '2020-06-10 06:00:55', 'assessor', 'iqa'),
(79, 22, 41, 'hiii', '2020-06-10 06:00:55', 'assessor', 'iqa'),
(80, 22, 42, 'hiii', '2020-06-10 06:00:55', 'assessor', 'iqa'),
(81, 22, 43, 'hiii', '2020-06-10 06:00:55', 'assessor', 'iqa'),
(82, 22, 44, 'hiii', '2020-06-10 06:00:55', 'assessor', 'iqa'),
(83, 22, 47, 'hiii', '2020-06-10 06:00:55', 'assessor', 'iqa'),
(84, 22, 54, 'hiii', '2020-06-10 06:00:55', 'assessor', 'iqa'),
(85, 22, 1, 'hii', '2020-06-10 06:01:13', 'learner', 'iqa'),
(86, 22, 27, 'hii', '2020-06-10 06:01:13', 'learner', 'iqa'),
(87, 22, 28, 'hii', '2020-06-10 06:01:13', 'learner', 'iqa'),
(88, 22, 29, 'hii', '2020-06-10 06:01:13', 'learner', 'iqa'),
(89, 22, 34, 'hii', '2020-06-10 06:01:13', 'learner', 'iqa'),
(90, 22, 35, 'hii', '2020-06-10 06:01:13', 'learner', 'iqa'),
(91, 22, 37, 'hii', '2020-06-10 06:01:13', 'learner', 'iqa'),
(92, 22, 53, 'hii', '2020-06-10 06:01:13', 'learner', 'iqa'),
(93, 22, 56, 'hii', '2020-06-10 06:01:13', 'learner', 'iqa'),
(94, 22, 57, 'hii', '2020-06-10 06:01:13', 'learner', 'iqa'),
(95, 15, 15, 'nbmnb', '2020-06-14 03:10:53', 'iqa', 'admin'),
(96, 15, 28, 'jbkbkjbk', '2020-06-15 09:33:18', 'learner', 'admin'),
(97, 15, 14, ' nb j', '2020-06-15 09:33:45', 'iqa', 'admin'),
(98, 15, 22, 'Good morning', '2020-06-16 05:46:47', 'iqa', 'admin'),
(99, 15, 7, 'Hello', '2020-07-01 13:40:36', 'assessor', 'admin'),
(100, 15, 1, 'How are you today?', '2020-07-16 13:04:44', 'learner', 'admin'),
(101, 15, 28, 'How are you today?', '2020-07-16 13:04:44', 'learner', 'admin'),
(102, 15, 29, 'How are you today?', '2020-07-16 13:04:44', 'learner', 'admin'),
(103, 15, 34, 'How are you today?', '2020-07-16 13:04:44', 'learner', 'admin'),
(104, 15, 35, 'How are you today?', '2020-07-16 13:04:44', 'learner', 'admin'),
(105, 15, 37, 'How are you today?', '2020-07-16 13:04:44', 'learner', 'admin'),
(106, 15, 53, 'How are you today?', '2020-07-16 13:04:44', 'learner', 'admin'),
(107, 15, 56, 'How are you today?', '2020-07-16 13:04:44', 'learner', 'admin'),
(108, 15, 57, 'How are you today?', '2020-07-16 13:04:44', 'learner', 'admin'),
(109, 15, 58, 'How are you today?', '2020-07-16 13:04:44', 'learner', 'admin'),
(110, 15, 64, 'How are you today?', '2020-07-16 13:04:44', 'learner', 'admin'),
(111, 15, 65, 'How are you today?', '2020-07-16 13:04:44', 'learner', 'admin'),
(112, 15, 66, 'How are you today?', '2020-07-16 13:04:44', 'learner', 'admin'),
(113, 15, 67, 'How are you today?', '2020-07-16 13:04:44', 'learner', 'admin'),
(114, 15, 68, 'How are you today?', '2020-07-16 13:04:44', 'learner', 'admin'),
(115, 15, 71, 'How are you today?', '2020-07-16 13:04:44', 'learner', 'admin'),
(116, 15, 1, 'How are you today?', '2020-07-16 13:17:16', 'learner', 'admin'),
(117, 15, 28, 'How are you today?', '2020-07-16 13:17:16', 'learner', 'admin'),
(118, 15, 29, 'How are you today?', '2020-07-16 13:17:16', 'learner', 'admin'),
(119, 15, 34, 'How are you today?', '2020-07-16 13:17:16', 'learner', 'admin'),
(120, 15, 35, 'How are you today?', '2020-07-16 13:17:16', 'learner', 'admin'),
(121, 15, 37, 'How are you today?', '2020-07-16 13:17:16', 'learner', 'admin'),
(122, 15, 53, 'How are you today?', '2020-07-16 13:17:16', 'learner', 'admin'),
(123, 15, 56, 'How are you today?', '2020-07-16 13:17:16', 'learner', 'admin'),
(124, 15, 57, 'How are you today?', '2020-07-16 13:17:16', 'learner', 'admin'),
(125, 15, 58, 'How are you today?', '2020-07-16 13:17:16', 'learner', 'admin'),
(126, 15, 64, 'How are you today?', '2020-07-16 13:17:16', 'learner', 'admin'),
(127, 15, 65, 'How are you today?', '2020-07-16 13:17:16', 'learner', 'admin'),
(128, 15, 66, 'How are you today?', '2020-07-16 13:17:16', 'learner', 'admin'),
(129, 15, 67, 'How are you today?', '2020-07-16 13:17:16', 'learner', 'admin'),
(130, 15, 68, 'How are you today?', '2020-07-16 13:17:16', 'learner', 'admin'),
(131, 15, 71, 'How are you today?', '2020-07-16 13:17:16', 'learner', 'admin'),
(132, 15, 1, 'How are you today?', '2020-07-17 05:16:18', 'learner', 'admin'),
(133, 15, 28, 'How are you today?', '2020-07-17 05:16:18', 'learner', 'admin'),
(134, 15, 28, 'How are you today?', '2020-07-17 05:17:30', 'learner', 'admin'),
(135, 15, 28, 'Today you did a great job. well done', '2020-07-17 05:25:44', 'learner', 'admin'),
(136, 15, 28, 'Hello', '2020-07-17 05:42:04', 'learner', 'admin'),
(137, 15, 1, 'hello', '2020-07-17 07:34:48', 'learner', 'admin'),
(138, 15, 28, 'Today you did a great job. well done', '2020-07-17 09:15:00', 'learner', 'admin'),
(139, 15, 28, 'Today you did a great job. keep it up.', '2020-07-17 09:15:49', 'learner', 'admin'),
(140, 15, 28, 'Today you did a great job. keep it up.', '2020-07-17 09:16:15', 'learner', 'admin'),
(141, 15, 28, 'Today you did a great job. keep it up.', '2020-07-17 09:23:09', 'learner', 'admin'),
(142, 15, 1, 'Text 1.', '2020-07-17 09:28:56', 'learner', 'admin'),
(143, 15, 1, 'Text 1.', '2020-07-17 09:29:01', 'learner', 'admin'),
(144, 15, 1, 'Text 1.', '2020-07-17 09:29:03', 'learner', 'admin'),
(145, 15, 1, 'Text 1.', '2020-07-17 09:31:13', 'learner', 'admin'),
(146, 15, 1, 'Text 1.', '2020-07-17 09:32:42', 'learner', 'admin'),
(147, 15, 1, 'Text 2, done.', '2020-07-17 09:36:32', 'learner', 'admin'),
(148, 15, 28, 'Text 2, done.', '2020-07-17 09:42:04', 'learner', 'admin'),
(149, 15, 28, 'Text 2, done.', '2020-07-17 09:42:10', 'learner', 'admin'),
(150, 15, 28, 'n.', '2020-07-17 09:50:23', 'learner', 'admin'),
(151, 15, 28, 'please check the assessment', '2020-07-17 09:58:35', 'learner', 'admin'),
(152, 15, 1, 'hello, please check the assessment', '2020-07-17 10:04:51', 'learner', 'admin'),
(153, 15, 1, 'hello, please check the assessment', '2020-07-17 10:14:40', 'learner', 'admin'),
(154, 15, 28, 'hello, please check the assessment', '2020-07-17 10:14:40', 'learner', 'admin'),
(155, 15, 29, 'hello, please check the assessment', '2020-07-17 10:14:40', 'learner', 'admin'),
(156, 15, 34, 'hello, please check the assessment', '2020-07-17 10:14:40', 'learner', 'admin'),
(157, 15, 35, 'hello, please check the assessment', '2020-07-17 10:14:40', 'learner', 'admin'),
(158, 15, 37, 'hello, please check the assessment', '2020-07-17 10:14:40', 'learner', 'admin'),
(159, 15, 53, 'hello, please check the assessment', '2020-07-17 10:14:40', 'learner', 'admin'),
(160, 15, 56, 'hello, please check the assessment', '2020-07-17 10:14:40', 'learner', 'admin'),
(161, 15, 57, 'hello, please check the assessment', '2020-07-17 10:14:40', 'learner', 'admin'),
(162, 15, 58, 'hello, please check the assessment', '2020-07-17 10:14:40', 'learner', 'admin'),
(163, 15, 64, 'hello, please check the assessment', '2020-07-17 10:14:40', 'learner', 'admin'),
(164, 15, 65, 'hello, please check the assessment', '2020-07-17 10:14:40', 'learner', 'admin'),
(165, 15, 66, 'hello, please check the assessment', '2020-07-17 10:14:40', 'learner', 'admin'),
(166, 15, 67, 'hello, please check the assessment', '2020-07-17 10:14:41', 'learner', 'admin'),
(167, 15, 68, 'hello, please check the assessment', '2020-07-17 10:14:41', 'learner', 'admin'),
(168, 15, 71, 'hello, please check the assessment', '2020-07-17 10:14:41', 'learner', 'admin'),
(169, 15, 1, 'hello, please check the assessment1', '2020-07-17 10:19:20', 'learner', 'admin'),
(170, 15, 28, 'hello, please check the assessment1', '2020-07-17 10:19:20', 'learner', 'admin'),
(171, 15, 29, 'hello, please check the assessment1', '2020-07-17 10:19:20', 'learner', 'admin'),
(172, 15, 34, 'hello, please check the assessment1', '2020-07-17 10:19:20', 'learner', 'admin'),
(173, 15, 35, 'hello, please check the assessment1', '2020-07-17 10:19:20', 'learner', 'admin'),
(174, 15, 37, 'hello, please check the assessment1', '2020-07-17 10:19:20', 'learner', 'admin'),
(175, 15, 53, 'hello, please check the assessment1', '2020-07-17 10:19:20', 'learner', 'admin'),
(176, 15, 56, 'hello, please check the assessment1', '2020-07-17 10:19:20', 'learner', 'admin'),
(177, 15, 57, 'hello, please check the assessment1', '2020-07-17 10:19:20', 'learner', 'admin'),
(178, 15, 58, 'hello, please check the assessment1', '2020-07-17 10:19:20', 'learner', 'admin'),
(179, 15, 64, 'hello, please check the assessment1', '2020-07-17 10:19:20', 'learner', 'admin'),
(180, 15, 65, 'hello, please check the assessment1', '2020-07-17 10:19:20', 'learner', 'admin'),
(181, 15, 66, 'hello, please check the assessment1', '2020-07-17 10:19:20', 'learner', 'admin'),
(182, 15, 67, 'hello, please check the assessment1', '2020-07-17 10:19:20', 'learner', 'admin'),
(183, 15, 68, 'hello, please check the assessment1', '2020-07-17 10:19:20', 'learner', 'admin'),
(184, 15, 71, 'hello, please check the assessment1', '2020-07-17 10:19:20', 'learner', 'admin'),
(185, 15, 1, 'Hello', '2020-07-17 10:25:55', 'learner', 'admin'),
(186, 15, 1, 'hello shubham', '2020-07-17 10:26:18', 'learner', 'admin'),
(187, 28, 36, 'hiiii', '2020-07-17 10:34:44', 'assessor', 'learner'),
(188, 15, 1, 'hello, please check the assessment1', '2020-07-17 14:39:00', 'learner', 'admin'),
(189, 15, 28, 'hello, please check the assessment1', '2020-07-17 14:39:00', 'learner', 'admin'),
(190, 15, 29, 'hello, please check the assessment1', '2020-07-17 14:39:00', 'learner', 'admin'),
(191, 15, 34, 'hello, please check the assessment1', '2020-07-17 14:39:00', 'learner', 'admin'),
(192, 15, 35, 'hello, please check the assessment1', '2020-07-17 14:39:00', 'learner', 'admin'),
(193, 15, 37, 'hello, please check the assessment1', '2020-07-17 14:39:00', 'learner', 'admin'),
(194, 15, 53, 'hello, please check the assessment1', '2020-07-17 14:39:00', 'learner', 'admin'),
(195, 15, 56, 'hello, please check the assessment1', '2020-07-17 14:39:01', 'learner', 'admin'),
(196, 15, 57, 'hello, please check the assessment1', '2020-07-17 14:39:01', 'learner', 'admin'),
(197, 15, 58, 'hello, please check the assessment1', '2020-07-17 14:39:01', 'learner', 'admin'),
(198, 15, 64, 'hello, please check the assessment1', '2020-07-17 14:39:01', 'learner', 'admin'),
(199, 15, 65, 'hello, please check the assessment1', '2020-07-17 14:39:01', 'learner', 'admin'),
(200, 15, 66, 'hello, please check the assessment1', '2020-07-17 14:39:01', 'learner', 'admin'),
(201, 15, 67, 'hello, please check the assessment1', '2020-07-17 14:39:01', 'learner', 'admin'),
(202, 15, 68, 'hello, please check the assessment1', '2020-07-17 14:39:01', 'learner', 'admin'),
(203, 15, 71, 'hello, please check the assessment1', '2020-07-17 14:39:01', 'learner', 'admin'),
(204, 15, 1, 'hello, please check the assessment1', '2020-07-17 14:41:42', 'learner', 'admin'),
(205, 15, 1, 'hello, please check the assessment1', '2020-07-17 14:56:53', 'learner', 'admin'),
(206, 15, 1, 'hi , plz test notification', '2020-07-17 14:57:36', 'learner', 'admin'),
(207, 15, 1, 'hello all', '2020-07-17 14:58:21', 'learner', 'admin'),
(208, 15, 1, 'hello all learners', '2020-07-17 14:59:39', 'learner', 'admin'),
(209, 15, 28, 'hello all learners', '2020-07-17 14:59:40', 'learner', 'admin'),
(210, 15, 29, 'hello all learners', '2020-07-17 14:59:40', 'learner', 'admin'),
(211, 15, 34, 'hello all learners', '2020-07-17 14:59:40', 'learner', 'admin'),
(212, 15, 35, 'hello all learners', '2020-07-17 14:59:40', 'learner', 'admin'),
(213, 15, 37, 'hello all learners', '2020-07-17 14:59:40', 'learner', 'admin'),
(214, 15, 53, 'hello all learners', '2020-07-17 14:59:40', 'learner', 'admin'),
(215, 15, 56, 'hello all learners', '2020-07-17 14:59:40', 'learner', 'admin'),
(216, 15, 57, 'hello all learners', '2020-07-17 14:59:40', 'learner', 'admin'),
(217, 15, 58, 'hello all learners', '2020-07-17 14:59:40', 'learner', 'admin'),
(218, 15, 64, 'hello all learners', '2020-07-17 14:59:40', 'learner', 'admin'),
(219, 15, 65, 'hello all learners', '2020-07-17 14:59:40', 'learner', 'admin'),
(220, 15, 66, 'hello all learners', '2020-07-17 14:59:40', 'learner', 'admin'),
(221, 15, 67, 'hello all learners', '2020-07-17 14:59:40', 'learner', 'admin'),
(222, 15, 68, 'hello all learners', '2020-07-17 14:59:40', 'learner', 'admin'),
(223, 15, 71, 'hello all learners', '2020-07-17 14:59:40', 'learner', 'admin'),
(224, 36, 53, 'Hello all, lets meet in conference room', '2020-07-20 12:16:29', 'learner', 'assessor'),
(225, 28, 36, 'Hello , i would like to thank you for your time', '2020-07-20 12:19:41', 'assessor', 'learner'),
(226, 28, 36, 'Hello assessor', '2020-07-20 12:56:34', 'assessor', 'learner'),
(227, 36, 28, 'aga', '2020-07-20 13:33:58', 'learner', 'assessor'),
(228, 15, 89, '                   ', '2020-07-20 14:29:36', 'iqa', 'admin'),
(229, 15, 89, '                   ', '2020-07-20 14:29:38', 'iqa', 'admin'),
(230, 15, 89, '                   ', '2020-07-20 14:29:43', 'iqa', 'admin'),
(231, 15, 89, '                   ', '2020-07-20 14:29:44', 'iqa', 'admin'),
(232, 15, 89, '                   ', '2020-07-20 14:29:44', 'iqa', 'admin'),
(233, 15, 89, '                   ', '2020-07-20 14:29:44', 'iqa', 'admin'),
(234, 15, 89, '                   ', '2020-07-20 14:29:44', 'iqa', 'admin'),
(235, 15, 89, '                   ', '2020-07-20 14:29:45', 'iqa', 'admin'),
(236, 15, 89, '                   ', '2020-07-20 14:29:45', 'iqa', 'admin'),
(237, 15, 89, 'a', '2020-07-20 14:30:47', 'iqa', 'admin'),
(238, 15, 89, 'a', '2020-07-20 14:30:47', 'iqa', 'admin'),
(239, 15, 89, 'a', '2020-07-20 14:30:48', 'iqa', 'admin'),
(240, 15, 89, 'a', '2020-07-20 14:30:48', 'iqa', 'admin'),
(241, 15, 89, 'a', '2020-07-20 14:30:48', 'iqa', 'admin'),
(242, 15, 89, 'a', '2020-07-20 14:30:48', 'iqa', 'admin'),
(243, 15, 89, 'a', '2020-07-20 14:30:48', 'iqa', 'admin'),
(244, 15, 89, 'a', '2020-07-20 14:30:49', 'iqa', 'admin'),
(245, 15, 89, 'a', '2020-07-20 14:30:49', 'iqa', 'admin'),
(246, 15, 89, 'asdsad', '2020-07-20 14:30:54', 'iqa', 'admin'),
(247, 15, 89, 'asdsad', '2020-07-20 14:30:54', 'iqa', 'admin'),
(248, 15, 89, 'asdsad', '2020-07-20 14:30:55', 'iqa', 'admin'),
(249, 15, 89, 'asdsad', '2020-07-20 14:30:55', 'iqa', 'admin'),
(250, 15, 89, 'asdsad', '2020-07-20 14:30:55', 'iqa', 'admin'),
(251, 15, 89, 'asdsad', '2020-07-20 14:30:55', 'iqa', 'admin'),
(252, 15, 89, 'asdsad', '2020-07-20 14:30:55', 'iqa', 'admin'),
(253, 15, 89, 'asdsad', '2020-07-20 14:30:56', 'iqa', 'admin'),
(254, 15, 89, 'asdsad', '2020-07-20 14:30:56', 'iqa', 'admin'),
(255, 15, 89, 'asdsad', '2020-07-20 14:30:56', 'iqa', 'admin'),
(256, 15, 89, 'asdsad', '2020-07-20 14:30:57', 'iqa', 'admin'),
(257, 15, 89, 'hello Aish', '2020-07-20 14:31:37', 'iqa', 'admin'),
(258, 15, 89, 'hello Aish', '2020-07-20 14:31:38', 'iqa', 'admin'),
(259, 15, 89, 'hello Aish', '2020-07-20 14:31:38', 'iqa', 'admin'),
(260, 15, 89, 'hello Aish', '2020-07-20 14:31:38', 'iqa', 'admin'),
(261, 15, 89, 'hello Aish', '2020-07-20 14:31:38', 'iqa', 'admin'),
(262, 15, 89, 'hello Aish', '2020-07-20 14:31:39', 'iqa', 'admin'),
(263, 15, 89, 'hello Aish', '2020-07-20 14:31:43', 'iqa', 'admin'),
(264, 15, 89, 'hello Aish', '2020-07-20 14:31:43', 'iqa', 'admin'),
(265, 15, 89, 'hello Aish', '2020-07-20 14:31:43', 'iqa', 'admin'),
(266, 15, 89, 'hello Aish', '2020-07-20 14:31:44', 'iqa', 'admin'),
(267, 15, 89, 'hello Aish', '2020-07-20 14:31:44', 'iqa', 'admin'),
(268, 15, 89, 'hello Aish', '2020-07-20 14:31:44', 'iqa', 'admin'),
(269, 15, 1, 'hello', '2020-07-20 14:32:09', 'learner', 'admin'),
(270, 15, 1, '', '2020-07-20 14:32:10', 'learner', 'admin'),
(271, 15, 1, '', '2020-07-20 14:32:10', 'learner', 'admin'),
(272, 15, 1, '', '2020-07-20 14:32:10', 'learner', 'admin'),
(273, 15, 1, '', '2020-07-20 14:32:11', 'learner', 'admin'),
(274, 15, 1, '', '2020-07-20 14:32:11', 'learner', 'admin'),
(275, 15, 1, '', '2020-07-20 14:32:11', 'learner', 'admin'),
(276, 15, 1, '', '2020-07-20 14:32:12', 'learner', 'admin'),
(277, 15, 1, '', '2020-07-20 14:32:12', 'learner', 'admin'),
(278, 15, 1, '                        ', '2020-07-20 14:32:24', 'learner', 'admin'),
(279, 15, 1, '                        ', '2020-07-20 14:32:24', 'learner', 'admin'),
(280, 15, 1, '', '2020-07-20 14:32:25', 'learner', 'admin'),
(281, 15, 1, '', '2020-07-20 14:32:25', 'learner', 'admin'),
(282, 15, 1, '', '2020-07-20 14:32:25', 'learner', 'admin'),
(283, 15, 1, '', '2020-07-20 14:32:25', 'learner', 'admin'),
(284, 15, 1, '', '2020-07-20 14:32:26', 'learner', 'admin'),
(285, 15, 1, '', '2020-07-20 14:32:26', 'learner', 'admin'),
(286, 15, 1, 'hi', '2020-07-20 14:32:37', 'learner', 'admin'),
(287, 15, 1, '', '2020-07-20 14:32:37', 'learner', 'admin'),
(288, 15, 1, 'hello', '2020-07-20 14:32:50', 'learner', 'admin'),
(289, 15, 1, '', '2020-07-20 14:32:58', 'learner', 'admin'),
(290, 15, 1, '', '2020-07-20 14:32:59', 'learner', 'admin'),
(291, 15, 1, '', '2020-07-20 14:32:59', 'learner', 'admin'),
(292, 15, 1, '', '2020-07-20 14:32:59', 'learner', 'admin'),
(293, 15, 1, '', '2020-07-20 14:32:59', 'learner', 'admin'),
(294, 15, 1, '', '2020-07-20 14:32:59', 'learner', 'admin'),
(295, 15, 1, '', '2020-07-20 14:33:54', 'learner', 'admin'),
(296, 15, 1, '', '2020-07-20 14:33:54', 'learner', 'admin'),
(297, 15, 1, '', '2020-07-20 14:33:54', 'learner', 'admin'),
(298, 15, 1, '', '2020-07-20 14:33:54', 'learner', 'admin'),
(299, 15, 1, '', '2020-07-20 14:33:54', 'learner', 'admin'),
(300, 15, 1, '', '2020-07-20 14:33:55', 'learner', 'admin'),
(301, 15, 1, '', '2020-07-20 14:33:55', 'learner', 'admin'),
(302, 15, 1, '', '2020-07-20 14:33:55', 'learner', 'admin'),
(303, 15, 1, '', '2020-07-20 14:33:55', 'learner', 'admin'),
(304, 15, 1, '', '2020-07-20 14:33:55', 'learner', 'admin'),
(305, 15, 1, '', '2020-07-20 14:33:56', 'learner', 'admin'),
(306, 15, 1, '', '2020-07-20 14:33:56', 'learner', 'admin'),
(307, 15, 1, '', '2020-07-20 14:33:56', 'learner', 'admin'),
(308, 15, 89, 'dhagdch@@#%#% FVHFHg', '2020-07-20 14:37:49', 'iqa', 'admin'),
(309, 15, 89, 'dhagdch@@#%#% FVHFHg', '2020-07-20 14:37:54', 'iqa', 'admin'),
(310, 15, 89, 'dhagdch@@#%#% FVHFHg', '2020-07-20 14:38:00', 'iqa', 'admin'),
(311, 15, 89, 'dhagdch@@#%#% FVHFHg', '2020-07-20 14:38:00', 'iqa', 'admin'),
(312, 15, 89, 'dhagdch@@#%#% FVHFHg', '2020-07-20 14:38:00', 'iqa', 'admin'),
(313, 15, 89, 'dhagdch@@#%#% FVHFHg', '2020-07-20 14:38:01', 'iqa', 'admin'),
(314, 15, 89, 'dhagdch@@#%#% FVHFHg', '2020-07-20 14:38:01', 'iqa', 'admin'),
(315, 15, 89, 'dhagdch@@#%#% FVHFHg', '2020-07-20 14:38:06', 'iqa', 'admin'),
(316, 15, 89, 'adjkqfnwkefn!#I93p5oi430594', '2020-07-20 14:38:23', 'iqa', 'admin'),
(317, 15, 89, 'adjkqfnwkefn!#I93p5oi430594', '2020-07-20 14:38:24', 'iqa', 'admin'),
(318, 15, 89, 'adjkqfnwkefn!#I93p5oi430594', '2020-07-20 14:38:24', 'iqa', 'admin'),
(319, 15, 89, 'adjkqfnwkefn!#I93p5oi430594', '2020-07-20 14:38:24', 'iqa', 'admin'),
(320, 15, 89, 'adjkqfnwkefn!#I93p5oi430594', '2020-07-20 14:38:24', 'iqa', 'admin'),
(321, 15, 89, 'adjkqfnwkefn!#I93p5oi430594', '2020-07-20 14:38:25', 'iqa', 'admin'),
(322, 15, 89, 'adjkqfnwkefn!#I93p5oi430594', '2020-07-20 14:38:26', 'iqa', 'admin'),
(323, 15, 89, 'adjkqfnwkefn!#I93p5oi430594', '2020-07-20 14:38:26', 'iqa', 'admin'),
(324, 15, 89, 'adjkqfnwkefn!#I93p5oi430594', '2020-07-20 14:38:26', 'iqa', 'admin'),
(325, 15, 89, 'adjkqfnwkefn!#I93p5oi430594', '2020-07-20 14:38:27', 'iqa', 'admin'),
(326, 15, 89, 'adjkqfnwkefn!#I93p5oi430594', '2020-07-20 14:38:27', 'iqa', 'admin'),
(327, 15, 89, 'adjkqfnwkefn!#I93p5oi430594', '2020-07-20 14:38:27', 'iqa', 'admin'),
(328, 15, 89, 'adjkqfnwkefn!#I93p5oi430594', '2020-07-20 14:38:28', 'iqa', 'admin'),
(329, 15, 89, 'adjkqfnwkefn!#I93p5oi430594', '2020-07-20 14:38:28', 'iqa', 'admin'),
(330, 15, 89, 'hello', '2020-07-20 14:42:37', 'iqa', 'admin'),
(331, 15, 89, 'hello', '2020-07-20 14:42:58', 'iqa', 'admin'),
(332, 28, 36, 'Fgfggj', '2020-07-20 15:08:26', 'assessor', 'learner'),
(334, 28, 36, 'How are you', '2020-07-20 16:57:36', 'assessor', 'learner'),
(335, 19, 1, 'hello', '2020-07-21 06:16:44', 'admin', 'iqa'),
(336, 15, 89, 'nbbbn', '2020-07-21 06:42:08', 'iqa', 'admin'),
(337, 15, 89, 'Hello Aishh', '2020-07-21 06:47:40', 'iqa', 'admin'),
(338, 15, 89, 'Hello Aishh', '2020-07-21 06:47:42', 'iqa', 'admin'),
(339, 15, 85, 'sdaf', '2020-07-21 06:58:44', 'assessor', 'admin'),
(340, 15, 85, '', '2020-07-21 06:59:00', 'assessor', 'admin'),
(341, 15, 85, '', '2020-07-21 06:59:00', 'assessor', 'admin'),
(342, 15, 85, '', '2020-07-21 06:59:01', 'assessor', 'admin'),
(343, 15, 85, '', '2020-07-21 06:59:01', 'assessor', 'admin'),
(344, 15, 85, '', '2020-07-21 06:59:01', 'assessor', 'admin'),
(345, 15, 85, '', '2020-07-21 06:59:02', 'assessor', 'admin'),
(346, 36, 15, 'Hi8', '2020-07-21 07:48:03', 'iqa', 'assessor'),
(347, 36, 28, 'Helloo', '2020-07-21 07:51:40', 'learner', 'assessor'),
(348, 36, 28, 'Hiii', '2020-07-21 07:52:03', 'learner', 'assessor'),
(349, 36, 28, 'Rryyhui', '2020-07-21 08:09:04', 'learner', 'assessor'),
(350, 36, 3, 'Hello', '2020-07-21 08:09:49', 'admin', 'assessor'),
(351, 36, 11, 'Hiiiiiii', '2020-07-21 08:12:35', 'iqa', 'assessor'),
(352, 36, 28, 'Dfg', '2020-07-21 08:14:14', 'learner', 'assessor'),
(353, 36, 2, 'Sse', '2020-07-21 08:15:07', 'admin', 'assessor'),
(354, 36, 1, 'Eyu', '2020-07-21 08:15:33', 'admin', 'assessor'),
(355, 36, 11, 'Hii', '2020-07-21 08:15:58', 'iqa', 'assessor'),
(356, 36, 14, 'Cghj', '2020-07-21 08:19:21', 'IQA', 'assessor'),
(357, 36, 11, 'Hiiiiiii iqa', '2020-07-21 08:20:11', 'iqa', 'assessor'),
(358, 36, 22, 'Drg', '2020-07-21 08:21:00', 'iqa', 'assessor'),
(359, 36, 28, 'Hello', '2020-07-21 08:21:34', 'learner', 'assessor'),
(360, 36, 11, 'Hiiiiiii iqa1', '2020-07-21 08:22:30', 'iqa', 'assessor'),
(361, 36, 4, 'Hello', '2020-07-21 08:24:54', 'iqa', 'assessor'),
(362, 36, 3, 'Hiii', '2020-07-21 08:25:14', 'admin', 'assessor'),
(363, 36, 28, 'Hello', '2020-07-21 08:25:28', 'learner', 'assessor'),
(364, 36, 53, 'Hiiiiiii iqa1', '2020-07-21 08:28:00', 'learner', 'assessor'),
(365, 36, 1, 'Ddfg', '2020-07-21 08:47:05', 'admin', 'assessor'),
(366, 36, 3, 'Hiii', '2020-07-21 09:51:48', 'admin', 'assessor'),
(367, 36, 3, 'Hello', '2020-07-21 09:52:03', 'admin', 'assessor'),
(368, 36, 3, 'How are you', '2020-07-21 09:52:14', 'admin', 'assessor'),
(369, 36, 3, 'Ggghhhhjjjjj', '2020-07-21 10:15:54', 'admin', 'assessor'),
(370, 36, 1, 'Dffgg', '2020-07-21 10:17:29', 'admin', 'assessor'),
(371, 36, 2, 'Xvcbvbgj', '2020-07-21 10:42:28', 'admin', 'assessor'),
(372, 36, 2, 'Hiii', '2020-07-21 10:52:22', 'admin', 'assessor'),
(373, 36, 2, 'Hello', '2020-07-21 10:52:30', 'admin', 'assessor'),
(374, 36, 2, 'How are you', '2020-07-21 10:52:38', 'admin', 'assessor'),
(375, 36, 2, 'Where are you', '2020-07-21 10:52:49', 'admin', 'assessor'),
(376, 36, 1, 'Hiiii', '2020-07-21 10:53:05', 'admin', 'assessor'),
(377, 36, 1, 'Hiiii', '2020-07-21 10:53:08', 'admin', 'assessor'),
(378, 36, 1, 'Ihiii', '2020-07-21 10:53:11', 'admin', 'assessor'),
(379, 36, 15, 'Hii', '2020-07-21 10:55:17', 'iqa', 'assessor'),
(380, 15, 66, 'Hello Salman', '2020-07-22 05:38:29', 'learner', 'admin'),
(381, 15, 1, 'Hello all', '2020-07-22 05:46:50', 'learner', 'admin'),
(382, 15, 28, 'Hello all', '2020-07-22 05:46:50', 'learner', 'admin'),
(383, 15, 29, 'Hello all', '2020-07-22 05:46:50', 'learner', 'admin'),
(384, 15, 34, 'Hello all', '2020-07-22 05:46:50', 'learner', 'admin'),
(385, 15, 35, 'Hello all', '2020-07-22 05:46:50', 'learner', 'admin'),
(386, 15, 37, 'Hello all', '2020-07-22 05:46:50', 'learner', 'admin'),
(387, 15, 53, 'Hello all', '2020-07-22 05:46:50', 'learner', 'admin'),
(388, 15, 56, 'Hello all', '2020-07-22 05:46:51', 'learner', 'admin'),
(389, 15, 57, 'Hello all', '2020-07-22 05:46:51', 'learner', 'admin'),
(390, 15, 58, 'Hello all', '2020-07-22 05:46:51', 'learner', 'admin'),
(391, 15, 64, 'Hello all', '2020-07-22 05:46:51', 'learner', 'admin'),
(392, 15, 65, 'Hello all', '2020-07-22 05:46:51', 'learner', 'admin'),
(393, 15, 66, 'Hello all', '2020-07-22 05:46:51', 'learner', 'admin'),
(394, 15, 68, 'Hello all', '2020-07-22 05:46:51', 'learner', 'admin'),
(395, 15, 81, 'Hello all', '2020-07-22 05:46:51', 'learner', 'admin'),
(396, 15, 88, 'Hello all', '2020-07-22 05:46:51', 'learner', 'admin'),
(397, 15, 96, 'Hello all', '2020-07-22 05:46:51', 'learner', 'admin'),
(398, 15, 1, '', '2020-07-22 05:46:52', 'learner', 'admin'),
(399, 15, 28, '', '2020-07-22 05:46:52', 'learner', 'admin'),
(400, 15, 29, '', '2020-07-22 05:46:52', 'learner', 'admin'),
(401, 15, 34, '', '2020-07-22 05:46:52', 'learner', 'admin'),
(402, 15, 35, '', '2020-07-22 05:46:52', 'learner', 'admin'),
(403, 15, 37, '', '2020-07-22 05:46:52', 'learner', 'admin'),
(404, 15, 53, '', '2020-07-22 05:46:52', 'learner', 'admin'),
(405, 15, 56, '', '2020-07-22 05:46:53', 'learner', 'admin'),
(406, 15, 57, '', '2020-07-22 05:46:53', 'learner', 'admin'),
(407, 15, 58, '', '2020-07-22 05:46:53', 'learner', 'admin'),
(408, 15, 64, '', '2020-07-22 05:46:53', 'learner', 'admin'),
(409, 15, 65, '', '2020-07-22 05:46:53', 'learner', 'admin'),
(410, 15, 66, '', '2020-07-22 05:46:53', 'learner', 'admin'),
(411, 15, 68, '', '2020-07-22 05:46:53', 'learner', 'admin'),
(412, 15, 81, '', '2020-07-22 05:46:53', 'learner', 'admin'),
(413, 15, 88, '', '2020-07-22 05:46:53', 'learner', 'admin'),
(414, 15, 96, '', '2020-07-22 05:46:53', 'learner', 'admin'),
(415, 15, 1, 'Hello all, this is admin', '2020-07-22 05:47:09', 'learner', 'admin'),
(416, 15, 28, 'Hello all, this is admin', '2020-07-22 05:47:09', 'learner', 'admin'),
(417, 15, 29, 'Hello all, this is admin', '2020-07-22 05:47:10', 'learner', 'admin'),
(418, 15, 34, 'Hello all, this is admin', '2020-07-22 05:47:10', 'learner', 'admin'),
(419, 15, 35, 'Hello all, this is admin', '2020-07-22 05:47:10', 'learner', 'admin'),
(420, 15, 37, 'Hello all, this is admin', '2020-07-22 05:47:10', 'learner', 'admin'),
(421, 15, 53, 'Hello all, this is admin', '2020-07-22 05:47:10', 'learner', 'admin'),
(422, 15, 56, 'Hello all, this is admin', '2020-07-22 05:47:10', 'learner', 'admin'),
(423, 15, 57, 'Hello all, this is admin', '2020-07-22 05:47:10', 'learner', 'admin'),
(424, 15, 58, 'Hello all, this is admin', '2020-07-22 05:47:10', 'learner', 'admin'),
(425, 15, 64, 'Hello all, this is admin', '2020-07-22 05:47:10', 'learner', 'admin'),
(426, 15, 65, 'Hello all, this is admin', '2020-07-22 05:47:10', 'learner', 'admin'),
(427, 15, 66, 'Hello all, this is admin', '2020-07-22 05:47:10', 'learner', 'admin'),
(428, 15, 68, 'Hello all, this is admin', '2020-07-22 05:47:10', 'learner', 'admin'),
(429, 15, 81, 'Hello all, this is admin', '2020-07-22 05:47:10', 'learner', 'admin'),
(430, 15, 88, 'Hello all, this is admin', '2020-07-22 05:47:10', 'learner', 'admin'),
(431, 15, 96, 'Hello all, this is admin', '2020-07-22 05:47:10', 'learner', 'admin'),
(432, 15, 1, 'Hello all, this is admin', '2020-07-22 05:48:19', 'learner', 'admin'),
(433, 15, 28, 'Hello all, this is admin', '2020-07-22 05:48:19', 'learner', 'admin'),
(434, 15, 29, 'Hello all, this is admin', '2020-07-22 05:48:19', 'learner', 'admin'),
(435, 15, 34, 'Hello all, this is admin', '2020-07-22 05:48:19', 'learner', 'admin'),
(436, 15, 35, 'Hello all, this is admin', '2020-07-22 05:48:19', 'learner', 'admin'),
(437, 15, 37, 'Hello all, this is admin', '2020-07-22 05:48:19', 'learner', 'admin'),
(438, 15, 53, 'Hello all, this is admin', '2020-07-22 05:48:19', 'learner', 'admin'),
(439, 15, 56, 'Hello all, this is admin', '2020-07-22 05:48:19', 'learner', 'admin'),
(440, 15, 57, 'Hello all, this is admin', '2020-07-22 05:48:19', 'learner', 'admin'),
(441, 15, 58, 'Hello all, this is admin', '2020-07-22 05:48:19', 'learner', 'admin'),
(442, 15, 64, 'Hello all, this is admin', '2020-07-22 05:48:19', 'learner', 'admin'),
(443, 15, 65, 'Hello all, this is admin', '2020-07-22 05:48:19', 'learner', 'admin'),
(444, 15, 66, 'Hello all, this is admin', '2020-07-22 05:48:19', 'learner', 'admin'),
(445, 15, 68, 'Hello all, this is admin', '2020-07-22 05:48:19', 'learner', 'admin'),
(446, 15, 81, 'Hello all, this is admin', '2020-07-22 05:48:19', 'learner', 'admin'),
(447, 15, 88, 'Hello all, this is admin', '2020-07-22 05:48:19', 'learner', 'admin'),
(448, 15, 96, 'Hello all, this is admin', '2020-07-22 05:48:19', 'learner', 'admin'),
(449, 15, 66, 'Hello', '2020-07-22 05:55:40', 'learner', 'admin'),
(450, 15, 1, 'hello', '2020-07-22 05:56:03', 'learner', 'admin'),
(451, 15, 28, 'hello', '2020-07-22 05:56:03', 'learner', 'admin'),
(452, 15, 29, 'hello', '2020-07-22 05:56:03', 'learner', 'admin'),
(453, 15, 34, 'hello', '2020-07-22 05:56:03', 'learner', 'admin'),
(454, 15, 35, 'hello', '2020-07-22 05:56:03', 'learner', 'admin'),
(455, 15, 37, 'hello', '2020-07-22 05:56:03', 'learner', 'admin'),
(456, 15, 53, 'hello', '2020-07-22 05:56:03', 'learner', 'admin'),
(457, 15, 56, 'hello', '2020-07-22 05:56:04', 'learner', 'admin'),
(458, 15, 57, 'hello', '2020-07-22 05:56:04', 'learner', 'admin'),
(459, 15, 58, 'hello', '2020-07-22 05:56:04', 'learner', 'admin'),
(460, 15, 64, 'hello', '2020-07-22 05:56:04', 'learner', 'admin'),
(461, 15, 65, 'hello', '2020-07-22 05:56:04', 'learner', 'admin'),
(462, 15, 66, 'hello', '2020-07-22 05:56:04', 'learner', 'admin'),
(463, 15, 68, 'hello', '2020-07-22 05:56:04', 'learner', 'admin'),
(464, 15, 81, 'hello', '2020-07-22 05:56:04', 'learner', 'admin'),
(465, 15, 88, 'hello', '2020-07-22 05:56:04', 'learner', 'admin'),
(466, 15, 96, 'hello', '2020-07-22 05:56:04', 'learner', 'admin'),
(467, 15, 66, 'Hello salman', '2020-07-22 05:57:01', 'learner', 'admin'),
(468, 15, 16, 'Hello', '2020-07-22 06:03:07', 'assessor', 'admin'),
(469, 15, 20, 'Hello', '2020-07-22 06:03:07', 'assessor', 'admin'),
(470, 15, 33, 'Hello', '2020-07-22 06:03:07', 'assessor', 'admin'),
(471, 15, 36, 'Hello', '2020-07-22 06:03:07', 'assessor', 'admin'),
(472, 15, 39, 'Hello', '2020-07-22 06:03:08', 'assessor', 'admin'),
(473, 15, 40, 'Hello', '2020-07-22 06:03:08', 'assessor', 'admin'),
(474, 15, 41, 'Hello', '2020-07-22 06:03:08', 'assessor', 'admin'),
(475, 15, 83, 'Hello', '2020-07-22 06:03:08', 'assessor', 'admin'),
(476, 15, 84, 'Hello', '2020-07-22 06:03:08', 'assessor', 'admin'),
(477, 15, 85, 'Hello', '2020-07-22 06:03:08', 'assessor', 'admin'),
(478, 15, 86, 'Hello', '2020-07-22 06:03:08', 'assessor', 'admin'),
(479, 15, 87, 'Hello', '2020-07-22 06:03:08', 'assessor', 'admin'),
(480, 15, 90, 'Hello', '2020-07-22 06:03:08', 'assessor', 'admin'),
(481, 15, 89, '', '2020-07-22 06:20:54', 'iqa', 'admin'),
(482, 15, 89, '', '2020-07-22 06:20:57', 'iqa', 'admin'),
(483, 15, 88, '', '2020-07-22 06:27:26', 'learner', 'admin'),
(484, 15, 88, '', '2020-07-22 06:27:27', 'learner', 'admin'),
(485, 15, 88, '', '2020-07-22 06:27:27', 'learner', 'admin'),
(486, 15, 88, '', '2020-07-22 06:27:27', 'learner', 'admin'),
(487, 15, 88, '', '2020-07-22 06:27:27', 'learner', 'admin'),
(488, 15, 88, '', '2020-07-22 06:27:27', 'learner', 'admin'),
(489, 15, 88, '', '2020-07-22 06:27:28', 'learner', 'admin'),
(490, 15, 88, '', '2020-07-22 06:27:28', 'learner', 'admin'),
(491, 15, 88, '', '2020-07-22 06:27:28', 'learner', 'admin'),
(492, 15, 88, '', '2020-07-22 06:27:28', 'learner', 'admin'),
(493, 15, 33, '', '2020-07-22 06:29:21', 'assessor', 'admin'),
(494, 15, 33, '', '2020-07-22 06:29:23', 'assessor', 'admin'),
(495, 15, 33, '', '2020-07-22 06:29:25', 'assessor', 'admin'),
(496, 15, 85, '', '2020-07-22 06:30:02', 'assessor', 'admin'),
(497, 15, 85, '', '2020-07-22 06:30:03', 'assessor', 'admin'),
(498, 15, 85, '', '2020-07-22 06:30:03', 'assessor', 'admin'),
(499, 15, 66, '12345!@#$#&%#&%%^^%%56', '2020-07-22 07:37:51', 'learner', 'admin'),
(500, 15, 66, 'Former Deputy Chief Minister Sachin Pilot and 18 other MLAs file a caveat in the apex court. A Supreme Court Bench led by Justice Arun Mishra is scheduled to hear on Thursday an appeal filed by the Rajasthan Speaker’s office challenging the State High Court order to defer anti-defection proceedings against former Deputy Chief Minister Sachin Pilot and 18 breakaway Congress MLAs till July 24.  The other two judges on the Bench are Justices B.R. Gavai and Krishna Murari. The virtual court hearing will begin after 11 a.m.  Mr. Pilot and the other legislators have filed a caveat petition in the court, saying “nothing should be done” without notifying them first.  In a steady progression of events on Wednesday, Speaker C.P. Joshi’s office filed the special leave petition, through advocate Sunil Fernandes, three minutes to noon on Wednesday and within a day of the High Court order. Shortly after the petition was filed, senior advocate Kapil Sibal, while appearing in another case, happened to complain to Chief Justice of India (CJI) Sharad A. Bobde about the lack of a proper mechanism to mention cases for urgent hearing in the current virtual court system. He said his team had been trying hard, but without success, to mention the Rajasthan Speaker’s case for an early hearing.  Also read: Analysis | Does Ashok Gehlot’s outburst against Sachin Pilot deliver a message to Congress’ central leadership?  The CJI told Mr. Sibal that he would apprise the court Registry and see if the case could be put up before a Bench. By evening, the case was listed before Justice Mishra’s Bench.  “Lakshman Rekha” The petition said the High Court crossed the “Lakshman Rekha” by asking the Speaker to put off his decision on the disqualification notices issued to the 19 dissident legislators on July 14. The High Court order was an affront to the powers of the Speaker. It was “illegal and perverse” and “destroys the delicate balance envisaged by the Constitution between the legislature and the judiciary”, it stated.  The Speaker said the High Court’s interim order granting extended time to Mr. Pilot and the other MLAs to file their replies to the July 14 anti-defection notices amounted to violation of Article 212 (courts not to inquire into the proceedings of the legislature).  “The notice dated July 14 is a proceeding in the House and immune from judicial interference at that stage. The notice was only limited to inviting comments from the Respondents [Mr. Pilot and other MLAs] and there was nothing adverse against them at that stage. A notice is much prior to any final determination or decision on disqualification. The proceedings, including the notice, are in the realm of the legislative proceedings under Paragraph 6(2) of the Tenth Schedule,” the Speaker’s office argued.  It said judicial review of an ongoing anti-defection proceedings was limited.', '2020-07-23 04:34:58', 'learner', 'admin'),
(501, 15, 66, 'Former Deputy Chief Minister Sachin Pilot and 18 other MLAs file a caveat in the apex court. A Supreme Court Bench led by Justice Arun Mishra is scheduled to hear on Thursday an appeal filed by the Rajasthan Speaker’s office challenging the State High Court order to defer anti-defection proceedings against former Deputy Chief Minister Sachin Pilot and 18 breakaway Congress MLAs till July 24.  The other two judges on the Bench are Justices B.R. Gavai and Krishna Murari. The virtual court hearing will begin after 11 a.m.  Mr. Pilot and the other legislators have filed a caveat petition in the court, saying “nothing should be done” without notifying them first.  In a steady progression of events on Wednesday, Speaker C.P. Joshi’s office filed the special leave petition, through advocate Sunil Fernandes, three minutes to noon on Wednesday and within a day of the High Court order. Shortly after the petition was filed, senior advocate Kapil Sibal, while appearing in another case, happened to complain to Chief Justice of India (CJI) Sharad A. Bobde about the lack of a proper mechanism to mention cases for urgent hearing in the current virtual court system. He said his team had been trying hard, but without success, to mention the Rajasthan Speaker’s case for an early hearing.  Also read: Analysis | Does Ashok Gehlot’s outburst against Sachin Pilot deliver a message to Congress’ central leadership?  The CJI told Mr. Sibal that he would apprise the court Registry and see if the case could be put up before a Bench. By evening, the case was listed before Justice Mishra’s Bench.  “Lakshman Rekha” The petition said the High Court crossed the “Lakshman Rekha” by asking the Speaker to put off his decision on the disqualification notices issued to the 19 dissident legislators on July 14. The High Court order was an affront to the powers of the Speaker. It was “illegal and perverse” and “destroys the delicate balance envisaged by the Constitution between the legislature and the judiciary”, it stated.  The Speaker said the High Court’s interim order granting extended time to Mr. Pilot and the other MLAs to file their replies to the July 14 anti-defection notices amounted to violation of Article 212 (courts not to inquire into the proceedings of the legislature).  “The notice dated July 14 is a proceeding in the House and immune from judicial interference at that stage. The notice was only limited to inviting comments from the Respondents [Mr. Pilot and other MLAs] and there was nothing adverse against them at that stage. A notice is much prior to any final determination or decision on disqualification. The proceedings, including the notice, are in the realm of the legislative proceedings under Paragraph 6(2) of the Tenth Schedule,” the Speaker’s office argued.  It said judicial review of an ongoing anti-defection proceedings was limited.', '2020-07-23 04:34:59', 'learner', 'admin'),
(502, 15, 66, 'Former Deputy Chief Minister Sachin Pilot and 18 other MLAs file a caveat in the apex court. A Supreme Court Bench led by Justice Arun Mishra is scheduled to hear on Thursday an appeal filed by the Rajasthan Speaker’s office challenging the State High Court order to defer anti-defection proceedings against former Deputy Chief Minister Sachin Pilot and 18 breakaway Congress MLAs till July 24.  The other two judges on the Bench are Justices B.R. Gavai and Krishna Murari. The virtual court hearing will begin after 11 a.m.  Mr. Pilot and the other legislators have filed a caveat petition in the court, saying “nothing should be done” without notifying them first.  In a steady progression of events on Wednesday, Speaker C.P. Joshi’s office filed the special leave petition, through advocate Sunil Fernandes, three minutes to noon on Wednesday and within a day of the High Court order. Shortly after the petition was filed, senior advocate Kapil Sibal, while appearing in another case, happened to complain to Chief Justice of India (CJI) Sharad A. Bobde about the lack of a proper mechanism to mention cases for urgent hearing in the current virtual court system. He said his team had been trying hard, but without success, to mention the Rajasthan Speaker’s case for an early hearing.  Also read: Analysis | Does Ashok Gehlot’s outburst against Sachin Pilot deliver a message to Congress’ central leadership?  The CJI told Mr. Sibal that he would apprise the court Registry and see if the case could be put up before a Bench. By evening, the case was listed before Justice Mishra’s Bench.  “Lakshman Rekha” The petition said the High Court crossed the “Lakshman Rekha” by asking the Speaker to put off his decision on the disqualification notices issued to the 19 dissident legislators on July 14. The High Court order was an affront to the powers of the Speaker. It was “illegal and perverse” and “destroys the delicate balance envisaged by the Constitution between the legislature and the judiciary”, it stated.  The Speaker said the High Court’s interim order granting extended time to Mr. Pilot and the other MLAs to file their replies to the July 14 anti-defection notices amounted to violation of Article 212 (courts not to inquire into the proceedings of the legislature).  “The notice dated July 14 is a proceeding in the House and immune from judicial interference at that stage. The notice was only limited to inviting comments from the Respondents [Mr. Pilot and other MLAs] and there was nothing adverse against them at that stage. A notice is much prior to any final determination or decision on disqualification. The proceedings, including the notice, are in the realm of the legislative proceedings under Paragraph 6(2) of the Tenth Schedule,” the Speaker’s office argued.  It said judicial review of an ongoing anti-defection proceedings was limited.', '2020-07-23 04:34:59', 'learner', 'admin'),
(503, 15, 66, 'Former Deputy Chief Minister Sachin Pilot and 18 other MLAs file a caveat in the apex court. A Supreme Court Bench led by Justice Arun Mishra is scheduled to hear on Thursday an appeal filed by the Rajasthan Speaker’s office challenging the State High Court order to defer anti-defection proceedings against former Deputy Chief Minister Sachin Pilot and 18 breakaway Congress MLAs till July 24.  The other two judges on the Bench are Justices B.R. Gavai and Krishna Murari. The virtual court hearing will begin after 11 a.m.  Mr. Pilot and the other legislators have filed a caveat petition in the court, saying “nothing should be done” without notifying them first.  In a steady progression of events on Wednesday, Speaker C.P. Joshi’s office filed the special leave petition, through advocate Sunil Fernandes, three minutes to noon on Wednesday and within a day of the High Court order. Shortly after the petition was filed, senior advocate Kapil Sibal, while appearing in another case, happened to complain to Chief Justice of India (CJI) Sharad A. Bobde about the lack of a proper mechanism to mention cases for urgent hearing in the current virtual court system. He said his team had been trying hard, but without success, to mention the Rajasthan Speaker’s case for an early hearing.  Also read: Analysis | Does Ashok Gehlot’s outburst against Sachin Pilot deliver a message to Congress’ central leadership?  The CJI told Mr. Sibal that he would apprise the court Registry and see if the case could be put up before a Bench. By evening, the case was listed before Justice Mishra’s Bench.  “Lakshman Rekha” The petition said the High Court crossed the “Lakshman Rekha” by asking the Speaker to put off his decision on the disqualification notices issued to the 19 dissident legislators on July 14. The High Court order was an affront to the powers of the Speaker. It was “illegal and perverse” and “destroys the delicate balance envisaged by the Constitution between the legislature and the judiciary”, it stated.  The Speaker said the High Court’s interim order granting extended time to Mr. Pilot and the other MLAs to file their replies to the July 14 anti-defection notices amounted to violation of Article 212 (courts not to inquire into the proceedings of the legislature).  “The notice dated July 14 is a proceeding in the House and immune from judicial interference at that stage. The notice was only limited to inviting comments from the Respondents [Mr. Pilot and other MLAs] and there was nothing adverse against them at that stage. A notice is much prior to any final determination or decision on disqualification. The proceedings, including the notice, are in the realm of the legislative proceedings under Paragraph 6(2) of the Tenth Schedule,” the Speaker’s office argued.  It said judicial review of an ongoing anti-defection proceedings was limited.', '2020-07-23 04:35:26', 'learner', 'admin');
INSERT INTO `user_chat` (`chat_id`, `sender_id`, `receiver_id`, `text_message`, `date_time_sent`, `receiver_role`, `sender_role`) VALUES
(504, 15, 66, 'Former Deputy Chief Minister Sachin Pilot and 18 other MLAs file a caveat in the apex court. A Supreme Court Bench led by Justice Arun Mishra is scheduled to hear on Thursday an appeal filed by the Rajasthan Speaker’s office challenging the State High Court order to defer anti-defection proceedings against former Deputy Chief Minister Sachin Pilot and 18 breakaway Congress MLAs till July 24.  The other two judges on the Bench are Justices B.R. Gavai and Krishna Murari. The virtual court hearing will begin after 11 a.m.  Mr. Pilot and the other legislators have filed a caveat petition in the court, saying “nothing should be done” without notifying them first.  In a steady progression of events on Wednesday, Speaker C.P. Joshi’s office filed the special leave petition, through advocate Sunil Fernandes, three minutes to noon on Wednesday and within a day of the High Court order. Shortly after the petition was filed, senior advocate Kapil Sibal, while appearing in another case, happened to complain to Chief Justice of India (CJI) Sharad A. Bobde about the lack of a proper mechanism to mention cases for urgent hearing in the current virtual court system. He said his team had been trying hard, but without success, to mention the Rajasthan Speaker’s case for an early hearing.  Also read: Analysis | Does Ashok Gehlot’s outburst against Sachin Pilot deliver a message to Congress’ central leadership?  The CJI told Mr. Sibal that he would apprise the court Registry and see if the case could be put up before a Bench. By evening, the case was listed before Justice Mishra’s Bench.  “Lakshman Rekha” The petition said the High Court crossed the “Lakshman Rekha” by asking the Speaker to put off his decision on the disqualification notices issued to the 19 dissident legislators on July 14. The High Court order was an affront to the powers of the Speaker. It was “illegal and perverse” and “destroys the delicate balance envisaged by the Constitution between the legislature and the judiciary”, it stated.  The Speaker said the High Court’s interim order granting extended time to Mr. Pilot and the other MLAs to file their replies to the July 14 anti-defection notices amounted to violation of Article 212 (courts not to inquire into the proceedings of the legislature).  “The notice dated July 14 is a proceeding in the House and immune from judicial interference at that stage. The notice was only limited to inviting comments from the Respondents [Mr. Pilot and other MLAs] and there was nothing adverse against them at that stage. A notice is much prior to any final determination or decision on disqualification. The proceedings, including the notice, are in the realm of the legislative proceedings under Paragraph 6(2) of the Tenth Schedule,” the Speaker’s office argued.  It said judicial review of an ongoing anti-defection proceedings was limited.', '2020-07-23 04:35:57', 'learner', 'admin'),
(505, 15, 66, '21561561', '2020-07-23 04:36:18', 'learner', 'admin'),
(506, 15, 4, '', '2020-07-25 12:07:07', 'iqa', 'admin'),
(507, 15, 89, 'hello', '2020-07-25 12:10:24', 'iqa', 'admin'),
(508, 15, 89, 'hello', '2020-07-25 12:10:42', 'iqa', 'admin'),
(509, 15, 89, 'hello', '2020-07-25 12:12:04', 'iqa', 'admin'),
(510, 15, 58, 'Good morning', '2020-07-25 12:14:22', 'learner', 'admin'),
(511, 15, 58, 'hello', '2020-07-25 12:15:11', 'learner', 'admin'),
(512, 36, 2, 'Hello', '2020-07-26 12:40:07', 'admin', 'assessor'),
(513, 36, 28, 'Hiii', '2020-07-26 15:03:18', 'learner', 'assessor'),
(514, 36, 53, 'Hiiiiiii iqa1', '2020-07-26 19:54:47', 'learner', 'assessor'),
(515, 15, 66, 'test message', '2020-07-27 09:54:33', 'learner', 'admin'),
(516, 15, 66, '', '2020-07-27 09:54:38', 'learner', 'admin'),
(517, 15, 28, 'heelo ', '2020-07-27 11:40:59', 'learner', 'admin'),
(518, 15, 28, 'hello', '2020-07-27 11:43:27', 'learner', 'admin'),
(519, 15, 1, 'hello all learners, check notification', '2020-07-27 12:04:09', 'learner', 'admin'),
(520, 15, 28, 'hello all learners, check notification', '2020-07-27 12:04:09', 'learner', 'admin'),
(521, 15, 29, 'hello all learners, check notification', '2020-07-27 12:04:09', 'learner', 'admin'),
(522, 15, 34, 'hello all learners, check notification', '2020-07-27 12:04:09', 'learner', 'admin'),
(523, 15, 35, 'hello all learners, check notification', '2020-07-27 12:04:09', 'learner', 'admin'),
(524, 15, 37, 'hello all learners, check notification', '2020-07-27 12:04:09', 'learner', 'admin'),
(525, 15, 53, 'hello all learners, check notification', '2020-07-27 12:04:10', 'learner', 'admin'),
(526, 15, 56, 'hello all learners, check notification', '2020-07-27 12:04:10', 'learner', 'admin'),
(527, 15, 57, 'hello all learners, check notification', '2020-07-27 12:04:10', 'learner', 'admin'),
(528, 15, 58, 'hello all learners, check notification', '2020-07-27 12:04:10', 'learner', 'admin'),
(529, 15, 64, 'hello all learners, check notification', '2020-07-27 12:04:10', 'learner', 'admin'),
(530, 15, 65, 'hello all learners, check notification', '2020-07-27 12:04:10', 'learner', 'admin'),
(531, 15, 66, 'hello all learners, check notification', '2020-07-27 12:04:10', 'learner', 'admin'),
(532, 15, 68, 'hello all learners, check notification', '2020-07-27 12:04:10', 'learner', 'admin'),
(533, 15, 81, 'hello all learners, check notification', '2020-07-27 12:04:10', 'learner', 'admin'),
(534, 15, 88, 'hello all learners, check notification', '2020-07-27 12:04:10', 'learner', 'admin'),
(535, 15, 96, 'hello all learners, check notification', '2020-07-27 12:04:10', 'learner', 'admin'),
(536, 15, 100, 'hello all learners, check notification', '2020-07-27 12:04:10', 'learner', 'admin'),
(537, 15, 28, 'how are you?', '2020-07-27 12:24:13', 'learner', 'admin'),
(538, 15, 28, 'sent message from admin successfully.', '2020-07-27 12:26:06', 'learner', 'admin'),
(539, 36, 22, 'Hello', '2020-07-27 16:02:05', 'iqa', 'assessor'),
(540, 36, 22, 'Good afternoon', '2020-07-27 16:04:21', 'iqa', 'assessor'),
(541, 36, 4, 'Hello', '2020-07-27 16:05:43', 'iqa', 'assessor'),
(542, 36, 15, 'Hiii', '2020-07-27 16:07:36', 'iqa', 'assessor'),
(543, 36, 1, 'Hiii', '2020-07-28 10:07:33', 'learner', 'assessor'),
(544, 36, 3, ' Hiii', '2020-07-29 12:00:28', 'admin', 'assessor'),
(545, 36, 2, 'Hii', '2020-07-29 12:13:09', 'admin', 'assessor'),
(546, 28, 36, 'Hii', '2020-07-29 20:16:05', 'assessor', 'learner'),
(550, 36, 21, 'Hii', '2020-07-30 00:49:55', 'admin', 'assessor'),
(551, 36, 21, 'Hello', '2020-07-30 00:50:05', 'admin', 'assessor'),
(552, 36, 21, '', '2020-07-30 00:50:07', 'admin', 'assessor'),
(553, 36, 15, 'Hii', '2020-07-30 00:50:30', 'iqa', 'assessor'),
(554, 36, 15, '', '2020-07-30 00:50:31', 'iqa', 'assessor'),
(555, 36, 11, 'Hiio', '2020-07-30 01:49:01', 'admin', 'assessor'),
(556, 36, 7, 'Hiiii', '2020-07-30 02:00:14', 'admin', 'assessor'),
(557, 36, 88, 'Hiiii', '2020-07-30 09:16:47', 'learner', 'assessor'),
(558, 15, 4, 'Good morning', '2020-07-30 11:23:01', 'iqa', 'admin'),
(559, 15, 11, 'Good morning', '2020-07-30 11:23:01', 'iqa', 'admin'),
(560, 15, 14, 'Good morning', '2020-07-30 11:23:01', 'iqa', 'admin'),
(561, 15, 15, 'Good morning', '2020-07-30 11:23:01', 'iqa', 'admin'),
(562, 15, 19, 'Good morning', '2020-07-30 11:23:01', 'iqa', 'admin'),
(563, 15, 22, 'Good morning', '2020-07-30 11:23:01', 'iqa', 'admin'),
(564, 15, 30, 'Good morning', '2020-07-30 11:23:01', 'iqa', 'admin'),
(565, 15, 31, 'Good morning', '2020-07-30 11:23:01', 'iqa', 'admin'),
(566, 15, 32, 'Good morning', '2020-07-30 11:23:01', 'iqa', 'admin'),
(567, 15, 38, 'Good morning', '2020-07-30 11:23:01', 'iqa', 'admin'),
(568, 15, 45, 'Good morning', '2020-07-30 11:23:01', 'iqa', 'admin'),
(569, 15, 46, 'Good morning', '2020-07-30 11:23:01', 'iqa', 'admin'),
(570, 15, 62, 'Good morning', '2020-07-30 11:23:01', 'iqa', 'admin'),
(571, 15, 89, 'Good morning', '2020-07-30 11:23:01', 'iqa', 'admin'),
(572, 15, 91, 'Good morning', '2020-07-30 11:23:01', 'iqa', 'admin'),
(573, 15, 98, 'Good morning', '2020-07-30 11:23:01', 'iqa', 'admin'),
(574, 15, 22, 'hello', '2020-07-30 11:25:33', 'iqa', 'admin'),
(575, 15, 22, 'hello', '2020-07-30 11:25:36', 'iqa', 'admin'),
(576, 15, 22, 'hello', '2020-07-30 11:25:40', 'iqa', 'admin'),
(577, 15, 31, 'hello', '2020-07-30 11:26:12', 'iqa', 'admin'),
(578, 15, 36, 'hello, how are you today?', '2020-07-30 11:27:02', 'assessor', 'admin'),
(579, 15, 1, 'hello', '2020-07-30 11:27:21', 'learner', 'admin'),
(580, 15, 4, 'hello', '2020-07-30 12:08:14', 'iqa', 'admin'),
(581, 36, 22, 'Hii', '2020-07-30 14:18:27', 'iqa', 'assessor'),
(582, 36, 53, 'Hii', '2020-07-30 14:19:15', 'learner', 'assessor'),
(583, 36, 28, 'Hey pop', '2020-07-30 14:20:13', 'learner', 'assessor'),
(586, 36, 15, 'Hiii', '2020-07-30 15:00:31', 'admin', 'assessor'),
(593, 36, 1, 'Hii', '2020-07-30 15:43:44', 'admin', 'assessor'),
(596, 88, 36, 'kdsjnf df ds', '2020-07-30 12:25:35', 'assessor', 'learner'),
(597, 36, 15, 'Hiii', '2020-07-30 17:46:30', 'admin', 'assessor'),
(598, 36, 53, 'Hiii', '2020-07-30 17:47:38', 'learner', 'assessor'),
(599, 28, 36, 'Hello', '2020-07-30 22:53:09', 'assessor', 'learner'),
(600, 28, 36, 'Hii', '2020-07-30 22:53:22', 'assessor', 'learner'),
(601, 28, 36, 'Heyyy', '2020-07-30 22:54:32', 'assessor', 'learner'),
(602, 28, 36, 'Jiii', '2020-07-30 22:56:26', 'assessor', 'learner'),
(603, 28, 36, 'Hii', '2020-07-30 23:14:14', 'assessor', 'learner'),
(604, 28, 36, 'Hii', '2020-07-31 10:14:57', 'assessor', 'learner'),
(605, 28, 36, 'Hiii', '2020-07-31 10:37:23', 'assessor', 'learner'),
(606, 28, 36, 'Hiii', '2020-07-31 10:40:23', 'assessor', 'learner'),
(607, 28, 36, 'Good morning', '2020-07-31 11:38:01', 'assessor', 'learner'),
(608, 28, 36, 'Hii', '2020-07-31 12:09:06', 'assessor', 'learner'),
(609, 36, 22, 'Hii', '2020-07-31 13:14:32', 'iqa', 'assessor'),
(610, 36, 22, 'Hii', '2020-07-31 13:15:13', 'iqa', 'assessor'),
(611, 36, 15, 'Hii', '2020-07-31 13:17:37', 'admin', 'assessor'),
(612, 36, 15, 'Hii', '2020-07-31 13:18:47', 'admin', 'assessor'),
(613, 36, 4, 'Hii', '2020-07-31 13:21:09', 'admin', 'assessor'),
(614, 36, 22, '', '2020-07-31 13:30:16', 'iqa', 'assessor'),
(615, 36, 17, 'Hii', '2020-07-31 13:30:27', 'admin', 'assessor'),
(616, 36, 28, 'Hey pqrker', '2020-07-31 13:30:41', 'learner', 'assessor'),
(617, 28, 36, 'Hkkk', '2020-07-31 14:06:00', 'assessor', 'learner'),
(618, 36, 1, 'Hii', '2020-07-31 15:02:00', 'admin', 'assessor'),
(619, 36, 14, 'Bii', '2020-07-31 15:11:13', 'IQA', 'assessor'),
(620, 36, 17, 'Hii', '2020-07-31 15:11:19', 'admin', 'assessor'),
(621, 28, 36, 'Hello', '2020-07-31 15:19:58', 'assessor', 'learner'),
(622, 28, 36, 'Hii', '2020-07-31 15:20:08', 'assessor', 'learner'),
(623, 28, 36, 'Hey parker', '2020-07-31 15:20:20', 'assessor', 'learner'),
(624, 68, 16, 'Hello', '2020-07-31 19:39:22', 'assessor', 'learner'),
(625, 15, 4, 'hello', '2020-07-31 19:51:41', 'iqa', 'admin'),
(626, 68, 16, 'Hiii', '2020-07-31 20:00:56', 'assessor', 'learner'),
(627, 16, 16, 'Hiii', '2020-07-31 20:08:14', 'assessor', 'assessor'),
(628, 16, 58, 'Hiii', '2020-07-31 20:08:40', 'learner', 'assessor'),
(629, 36, 4, 'Hiii', '2020-07-31 20:13:18', 'admin', 'assessor'),
(630, 36, 6, 'Hii', '2020-07-31 20:13:25', 'admin', 'assessor'),
(631, 68, 16, 'Hii', '2020-08-01 13:48:47', 'assessor', 'learner'),
(632, 68, 16, 'Hii', '2020-08-01 20:24:23', 'assessor', 'learner'),
(633, 68, 16, 'Hii', '2020-08-01 20:24:36', 'assessor', 'learner'),
(634, 68, 16, '', '2020-08-01 20:24:39', 'assessor', 'learner'),
(635, 68, 16, '', '2020-08-01 20:24:43', 'assessor', 'learner'),
(636, 16, 68, 'Hii', '2020-08-02 07:12:35', 'learner', 'assessor'),
(637, 16, 103, 'Hii', '2020-08-02 07:14:20', 'learner', 'assessor'),
(638, 68, 16, 'Good morning', '2020-08-02 08:35:42', 'assessor', 'learner'),
(639, 68, 16, 'Hello', '2020-08-02 10:52:15', 'assessor', 'learner'),
(640, 16, 22, 'Hello', '2020-08-02 12:03:55', 'iqa', 'assessor'),
(641, 16, 16, 'Jhello', '2020-08-02 12:04:17', 'assessor', 'assessor'),
(642, 16, 68, 'Heyy ram', '2020-08-02 12:04:33', 'learner', 'assessor'),
(643, 16, 68, '', '2020-08-02 12:04:35', 'learner', 'assessor'),
(644, 16, 68, 'Hello', '2020-08-02 12:04:44', 'learner', 'assessor'),
(645, 28, 36, 'Hello sir', '2020-08-02 12:51:32', 'assessor', 'learner'),
(646, 28, 36, 'Hii', '2020-08-02 12:51:46', 'assessor', 'learner'),
(647, 36, 15, 'Hiii', '2020-08-02 12:54:55', 'admin', 'assessor'),
(648, 36, 31, 'Hello', '2020-08-02 12:55:11', 'iqa', 'assessor'),
(649, 36, 38, 'Hiii', '2020-08-02 12:55:18', 'iqa', 'assessor'),
(650, 28, 36, 'How are you?', '2020-08-02 16:16:44', 'assessor', 'learner'),
(651, 28, 36, 'Hello', '2020-08-02 16:43:51', 'assessor', 'learner'),
(652, 28, 36, 'Hello', '2020-08-02 16:44:08', 'assessor', 'learner'),
(653, 36, 28, 'Hello', '2020-08-02 16:44:56', 'learner', 'assessor'),
(654, 36, 28, 'Hiii', '2020-08-02 16:45:25', 'learner', 'assessor'),
(655, 15, 28, 'hello', '2020-08-02 16:52:32', 'learner', 'admin'),
(656, 36, 28, 'Hiiiioi', '2020-08-02 17:08:55', 'learner', 'assessor'),
(657, 28, 36, 'Hiii', '2020-08-03 20:43:35', 'assessor', 'learner'),
(658, 36, 21, 'Hii', '2020-08-03 20:44:25', 'admin', 'assessor'),
(659, 36, 1, 'Hii', '2020-08-03 20:44:36', 'learner', 'assessor'),
(660, 36, 28, 'Hiii', '2020-08-03 20:44:42', 'learner', 'assessor'),
(661, 36, 88, 'Hiii', '2020-08-03 20:44:52', 'learner', 'assessor'),
(662, 36, 17, 'Her name', '2020-08-03 20:47:35', 'admin', 'assessor'),
(663, 28, 36, 'Hiii', '2020-08-03 20:47:57', 'assessor', 'learner'),
(664, 36, 28, 'Hey', '2020-08-03 20:52:53', 'learner', 'assessor'),
(665, 28, 36, '', '2020-08-05 15:04:41', 'assessor', 'learner'),
(666, 28, 36, 'Hello', '2020-08-05 15:15:39', 'assessor', 'learner'),
(667, 28, 36, 'Hiiii', '2020-08-05 18:25:06', 'assessor', 'learner'),
(668, 28, 36, 'Hello', '2020-08-05 18:26:41', 'assessor', 'learner'),
(669, 28, 36, 'Hello dear', '2020-08-05 18:35:10', 'assessor', 'learner'),
(670, 28, 36, 'Hi', '2020-08-05 20:03:25', 'assessor', 'learner'),
(671, 36, 15, 'Hello', '2020-08-06 10:22:10', 'admin', 'assessor'),
(672, 28, 36, 'Hiiii', '2020-08-06 10:59:45', 'assessor', 'learner'),
(673, 36, 15, 'Hiip', '2020-08-06 11:00:29', 'admin', 'assessor'),
(674, 36, 28, 'Hello', '2020-08-06 11:00:50', 'learner', 'assessor'),
(675, 28, 36, 'Hello', '2020-08-07 17:58:07', 'assessor', 'learner'),
(676, 15, 22, 'hello', '2020-08-07 19:07:49', 'iqa', 'admin'),
(677, 22, 1, 'Hello', '2020-08-08 11:13:46', 'learner', 'iqa'),
(678, 22, 1, '', '2020-08-08 11:13:48', 'learner', 'iqa'),
(679, 22, 1, '', '2020-08-08 11:13:49', 'learner', 'iqa'),
(680, 22, 1, '', '2020-08-08 11:13:49', 'learner', 'iqa'),
(681, 22, 1, '', '2020-08-08 11:13:49', 'learner', 'iqa'),
(682, 15, 28, 'Hii', '2020-08-08 13:46:41', 'learner', 'admin'),
(683, 15, 16, 'hiii', '2020-08-08 13:47:17', 'assessor', 'admin'),
(684, 15, 20, 'hiii', '2020-08-08 13:47:17', 'assessor', 'admin'),
(685, 15, 33, 'hiii', '2020-08-08 13:47:18', 'assessor', 'admin'),
(686, 15, 36, 'hiii', '2020-08-08 13:47:18', 'assessor', 'admin'),
(687, 15, 39, 'hiii', '2020-08-08 13:47:18', 'assessor', 'admin'),
(688, 15, 40, 'hiii', '2020-08-08 13:47:18', 'assessor', 'admin'),
(689, 15, 41, 'hiii', '2020-08-08 13:47:18', 'assessor', 'admin'),
(690, 15, 83, 'hiii', '2020-08-08 13:47:18', 'assessor', 'admin'),
(691, 15, 84, 'hiii', '2020-08-08 13:47:18', 'assessor', 'admin'),
(692, 15, 85, 'hiii', '2020-08-08 13:47:18', 'assessor', 'admin'),
(693, 15, 87, 'hiii', '2020-08-08 13:47:18', 'assessor', 'admin'),
(694, 15, 90, 'hiii', '2020-08-08 13:47:18', 'assessor', 'admin'),
(695, 15, 99, 'hiii', '2020-08-08 13:47:18', 'assessor', 'admin'),
(696, 15, 102, 'hiii', '2020-08-08 13:47:18', 'assessor', 'admin'),
(697, 15, 16, 'sent message successfully', '2020-08-08 13:47:51', 'assessor', 'admin'),
(698, 15, 20, 'sent message successfully', '2020-08-08 13:47:51', 'assessor', 'admin'),
(699, 15, 33, 'sent message successfully', '2020-08-08 13:47:51', 'assessor', 'admin'),
(700, 15, 36, 'sent message successfully', '2020-08-08 13:47:51', 'assessor', 'admin'),
(701, 15, 39, 'sent message successfully', '2020-08-08 13:47:51', 'assessor', 'admin'),
(702, 15, 40, 'sent message successfully', '2020-08-08 13:47:51', 'assessor', 'admin'),
(703, 15, 41, 'sent message successfully', '2020-08-08 13:47:51', 'assessor', 'admin'),
(704, 15, 83, 'sent message successfully', '2020-08-08 13:47:51', 'assessor', 'admin'),
(705, 15, 84, 'sent message successfully', '2020-08-08 13:47:51', 'assessor', 'admin'),
(706, 15, 85, 'sent message successfully', '2020-08-08 13:47:51', 'assessor', 'admin'),
(707, 15, 87, 'sent message successfully', '2020-08-08 13:47:51', 'assessor', 'admin'),
(708, 15, 90, 'sent message successfully', '2020-08-08 13:47:51', 'assessor', 'admin'),
(709, 15, 99, 'sent message successfully', '2020-08-08 13:47:51', 'assessor', 'admin'),
(710, 15, 102, 'sent message successfully', '2020-08-08 13:47:52', 'assessor', 'admin'),
(711, 15, 36, 'hello dear', '2020-08-08 13:48:17', 'assessor', 'admin'),
(712, 15, 28, 'hiii jhon', '2020-08-09 11:10:33', 'learner', 'admin'),
(713, 15, 16, 'hello everyone', '2020-08-09 11:11:29', 'assessor', 'admin'),
(714, 15, 20, 'hello everyone', '2020-08-09 11:11:29', 'assessor', 'admin'),
(715, 15, 33, 'hello everyone', '2020-08-09 11:11:29', 'assessor', 'admin'),
(716, 15, 36, 'hello everyone', '2020-08-09 11:11:29', 'assessor', 'admin'),
(717, 15, 39, 'hello everyone', '2020-08-09 11:11:29', 'assessor', 'admin'),
(718, 15, 40, 'hello everyone', '2020-08-09 11:11:29', 'assessor', 'admin'),
(719, 15, 41, 'hello everyone', '2020-08-09 11:11:29', 'assessor', 'admin'),
(720, 15, 83, 'hello everyone', '2020-08-09 11:11:29', 'assessor', 'admin'),
(721, 15, 84, 'hello everyone', '2020-08-09 11:11:29', 'assessor', 'admin'),
(722, 15, 85, 'hello everyone', '2020-08-09 11:11:29', 'assessor', 'admin'),
(723, 15, 87, 'hello everyone', '2020-08-09 11:11:29', 'assessor', 'admin'),
(724, 15, 90, 'hello everyone', '2020-08-09 11:11:29', 'assessor', 'admin'),
(725, 15, 99, 'hello everyone', '2020-08-09 11:11:29', 'assessor', 'admin'),
(726, 15, 102, 'hello everyone', '2020-08-09 11:11:30', 'assessor', 'admin'),
(727, 15, 1, 'hello everyone', '2020-08-09 11:12:52', 'learner', 'admin'),
(728, 15, 28, 'hello everyone', '2020-08-09 11:12:52', 'learner', 'admin'),
(729, 15, 29, 'hello everyone', '2020-08-09 11:12:52', 'learner', 'admin'),
(730, 15, 34, 'hello everyone', '2020-08-09 11:12:52', 'learner', 'admin'),
(731, 15, 35, 'hello everyone', '2020-08-09 11:12:52', 'learner', 'admin'),
(732, 15, 37, 'hello everyone', '2020-08-09 11:12:52', 'learner', 'admin'),
(733, 15, 53, 'hello everyone', '2020-08-09 11:12:52', 'learner', 'admin'),
(734, 15, 58, 'hello everyone', '2020-08-09 11:12:53', 'learner', 'admin'),
(735, 15, 65, 'hello everyone', '2020-08-09 11:12:53', 'learner', 'admin'),
(736, 15, 66, 'hello everyone', '2020-08-09 11:12:53', 'learner', 'admin'),
(737, 15, 68, 'hello everyone', '2020-08-09 11:12:53', 'learner', 'admin'),
(738, 15, 81, 'hello everyone', '2020-08-09 11:12:53', 'learner', 'admin'),
(739, 15, 88, 'hello everyone', '2020-08-09 11:12:53', 'learner', 'admin'),
(740, 15, 96, 'hello everyone', '2020-08-09 11:12:53', 'learner', 'admin'),
(741, 15, 100, 'hello everyone', '2020-08-09 11:12:53', 'learner', 'admin'),
(742, 15, 101, 'hello everyone', '2020-08-09 11:12:53', 'learner', 'admin'),
(743, 15, 103, 'hello everyone', '2020-08-09 11:12:53', 'learner', 'admin'),
(744, 15, 104, 'hello everyone', '2020-08-09 11:12:54', 'learner', 'admin'),
(745, 15, 110, 'hello everyone', '2020-08-09 11:12:54', 'learner', 'admin'),
(746, 15, 28, 'hii  jhon', '2020-08-09 11:14:06', 'learner', 'admin'),
(747, 15, 28, 'hii joy', '2020-08-09 11:14:17', 'learner', 'admin'),
(748, 15, 36, 'hii poonam', '2020-08-09 11:14:54', 'assessor', 'admin'),
(749, 15, 36, 'Hii dear', '2020-08-09 11:15:28', 'assessor', 'admin'),
(750, 28, 36, ' Hii dear', '2020-08-09 11:17:20', 'assessor', 'learner'),
(751, 28, 36, 'How are you', '2020-08-09 11:17:38', 'assessor', 'learner'),
(752, 36, 28, 'Hello joy', '2020-08-09 11:19:31', 'learner', 'assessor'),
(753, 15, 28, 'hello joy parker', '2020-08-09 11:40:24', 'learner', 'admin'),
(754, 28, 36, 'Hey poo', '2020-08-09 11:40:56', 'assessor', 'learner'),
(755, 28, 36, 'Jiii hey.    Ghjk', '2020-08-09 11:42:49', 'assessor', 'learner'),
(756, 36, 28, 'Heyyy joy', '2020-08-09 11:43:52', 'learner', 'assessor'),
(757, 15, 28, 'hello', '2020-08-09 11:44:43', 'learner', 'admin'),
(758, 15, 1, 'hello all learners, check notification', '2020-08-09 12:09:08', 'learner', 'admin'),
(759, 15, 28, 'hello all learners, check notification', '2020-08-09 12:09:08', 'learner', 'admin'),
(760, 15, 29, 'hello all learners, check notification', '2020-08-09 12:09:09', 'learner', 'admin'),
(761, 15, 34, 'hello all learners, check notification', '2020-08-09 12:09:09', 'learner', 'admin'),
(762, 15, 35, 'hello all learners, check notification', '2020-08-09 12:09:09', 'learner', 'admin'),
(763, 15, 37, 'hello all learners, check notification', '2020-08-09 12:09:09', 'learner', 'admin'),
(764, 15, 53, 'hello all learners, check notification', '2020-08-09 12:09:09', 'learner', 'admin'),
(765, 15, 58, 'hello all learners, check notification', '2020-08-09 12:09:09', 'learner', 'admin'),
(766, 15, 65, 'hello all learners, check notification', '2020-08-09 12:09:09', 'learner', 'admin'),
(767, 15, 66, 'hello all learners, check notification', '2020-08-09 12:09:09', 'learner', 'admin'),
(768, 15, 68, 'hello all learners, check notification', '2020-08-09 12:09:09', 'learner', 'admin'),
(769, 15, 81, 'hello all learners, check notification', '2020-08-09 12:09:09', 'learner', 'admin'),
(770, 15, 88, 'hello all learners, check notification', '2020-08-09 12:09:09', 'learner', 'admin'),
(771, 15, 96, 'hello all learners, check notification', '2020-08-09 12:09:09', 'learner', 'admin'),
(772, 15, 100, 'hello all learners, check notification', '2020-08-09 12:09:09', 'learner', 'admin'),
(773, 15, 101, 'hello all learners, check notification', '2020-08-09 12:09:09', 'learner', 'admin'),
(774, 15, 103, 'hello all learners, check notification', '2020-08-09 12:09:10', 'learner', 'admin'),
(775, 15, 104, 'hello all learners, check notification', '2020-08-09 12:09:10', 'learner', 'admin'),
(776, 15, 110, 'hello all learners, check notification', '2020-08-09 12:09:10', 'learner', 'admin'),
(777, 15, 1, 'hello all learners, check notification', '2020-08-09 12:26:21', 'learner', 'admin'),
(778, 15, 28, 'hello all learners, check notification', '2020-08-09 12:26:21', 'learner', 'admin'),
(779, 15, 29, 'hello all learners, check notification', '2020-08-09 12:26:21', 'learner', 'admin'),
(780, 15, 34, 'hello all learners, check notification', '2020-08-09 12:26:21', 'learner', 'admin'),
(781, 15, 35, 'hello all learners, check notification', '2020-08-09 12:26:21', 'learner', 'admin'),
(782, 15, 37, 'hello all learners, check notification', '2020-08-09 12:26:21', 'learner', 'admin'),
(783, 15, 53, 'hello all learners, check notification', '2020-08-09 12:26:21', 'learner', 'admin'),
(784, 15, 58, 'hello all learners, check notification', '2020-08-09 12:26:21', 'learner', 'admin'),
(785, 15, 65, 'hello all learners, check notification', '2020-08-09 12:26:21', 'learner', 'admin'),
(786, 15, 66, 'hello all learners, check notification', '2020-08-09 12:26:21', 'learner', 'admin'),
(787, 15, 68, 'hello all learners, check notification', '2020-08-09 12:26:22', 'learner', 'admin'),
(788, 15, 81, 'hello all learners, check notification', '2020-08-09 12:26:22', 'learner', 'admin'),
(789, 15, 88, 'hello all learners, check notification', '2020-08-09 12:26:22', 'learner', 'admin'),
(790, 15, 96, 'hello all learners, check notification', '2020-08-09 12:26:22', 'learner', 'admin'),
(791, 15, 100, 'hello all learners, check notification', '2020-08-09 12:26:22', 'learner', 'admin'),
(792, 15, 101, 'hello all learners, check notification', '2020-08-09 12:26:22', 'learner', 'admin'),
(793, 15, 103, 'hello all learners, check notification', '2020-08-09 12:26:22', 'learner', 'admin'),
(794, 15, 104, 'hello all learners, check notification', '2020-08-09 12:26:22', 'learner', 'admin'),
(795, 15, 110, 'hello all learners, check notification', '2020-08-09 12:26:22', 'learner', 'admin'),
(796, 15, 1, 'hello all learners, check notification', '2020-08-09 12:26:28', 'learner', 'admin'),
(797, 15, 28, 'hello all learners, check notification', '2020-08-09 12:26:28', 'learner', 'admin'),
(798, 15, 29, 'hello all learners, check notification', '2020-08-09 12:26:28', 'learner', 'admin'),
(799, 15, 34, 'hello all learners, check notification', '2020-08-09 12:26:28', 'learner', 'admin'),
(800, 15, 35, 'hello all learners, check notification', '2020-08-09 12:26:28', 'learner', 'admin'),
(801, 15, 37, 'hello all learners, check notification', '2020-08-09 12:26:28', 'learner', 'admin'),
(802, 15, 53, 'hello all learners, check notification', '2020-08-09 12:26:28', 'learner', 'admin'),
(803, 15, 58, 'hello all learners, check notification', '2020-08-09 12:26:29', 'learner', 'admin'),
(804, 15, 65, 'hello all learners, check notification', '2020-08-09 12:26:29', 'learner', 'admin'),
(805, 15, 66, 'hello all learners, check notification', '2020-08-09 12:26:29', 'learner', 'admin'),
(806, 15, 68, 'hello all learners, check notification', '2020-08-09 12:26:29', 'learner', 'admin'),
(807, 15, 81, 'hello all learners, check notification', '2020-08-09 12:26:29', 'learner', 'admin'),
(808, 15, 88, 'hello all learners, check notification', '2020-08-09 12:26:29', 'learner', 'admin'),
(809, 15, 96, 'hello all learners, check notification', '2020-08-09 12:26:29', 'learner', 'admin'),
(810, 15, 100, 'hello all learners, check notification', '2020-08-09 12:26:29', 'learner', 'admin'),
(811, 15, 101, 'hello all learners, check notification', '2020-08-09 12:26:29', 'learner', 'admin'),
(812, 15, 103, 'hello all learners, check notification', '2020-08-09 12:26:29', 'learner', 'admin'),
(813, 15, 104, 'hello all learners, check notification', '2020-08-09 12:26:29', 'learner', 'admin'),
(814, 15, 110, 'hello all learners, check notification', '2020-08-09 12:26:30', 'learner', 'admin'),
(815, 15, 28, 'heloo', '2020-08-09 12:44:47', 'learner', 'admin'),
(816, 15, 1, 'hello all learners, please check notification', '2020-08-09 12:59:04', 'learner', 'admin'),
(817, 15, 28, 'hello all learners, please check notification', '2020-08-09 12:59:04', 'learner', 'admin'),
(818, 15, 29, 'hello all learners, please check notification', '2020-08-09 12:59:04', 'learner', 'admin'),
(819, 15, 34, 'hello all learners, please check notification', '2020-08-09 12:59:04', 'learner', 'admin'),
(820, 15, 35, 'hello all learners, please check notification', '2020-08-09 12:59:04', 'learner', 'admin'),
(821, 15, 37, 'hello all learners, please check notification', '2020-08-09 12:59:04', 'learner', 'admin'),
(822, 15, 53, 'hello all learners, please check notification', '2020-08-09 12:59:04', 'learner', 'admin'),
(823, 15, 58, 'hello all learners, please check notification', '2020-08-09 12:59:05', 'learner', 'admin'),
(824, 15, 65, 'hello all learners, please check notification', '2020-08-09 12:59:05', 'learner', 'admin'),
(825, 15, 66, 'hello all learners, please check notification', '2020-08-09 12:59:05', 'learner', 'admin'),
(826, 15, 68, 'hello all learners, please check notification', '2020-08-09 12:59:05', 'learner', 'admin'),
(827, 15, 81, 'hello all learners, please check notification', '2020-08-09 12:59:05', 'learner', 'admin'),
(828, 15, 88, 'hello all learners, please check notification', '2020-08-09 12:59:05', 'learner', 'admin'),
(829, 15, 96, 'hello all learners, please check notification', '2020-08-09 12:59:05', 'learner', 'admin'),
(830, 15, 100, 'hello all learners, please check notification', '2020-08-09 12:59:05', 'learner', 'admin'),
(831, 15, 101, 'hello all learners, please check notification', '2020-08-09 12:59:05', 'learner', 'admin'),
(832, 15, 103, 'hello all learners, please check notification', '2020-08-09 12:59:05', 'learner', 'admin'),
(833, 15, 104, 'hello all learners, please check notification', '2020-08-09 12:59:05', 'learner', 'admin'),
(834, 15, 110, 'hello all learners, please check notification', '2020-08-09 12:59:06', 'learner', 'admin'),
(835, 15, 1, 'hello learner........', '2020-08-09 13:09:31', 'learner', 'admin'),
(836, 15, 28, 'hello learner........', '2020-08-09 13:09:31', 'learner', 'admin'),
(837, 15, 29, 'hello learner........', '2020-08-09 13:09:31', 'learner', 'admin'),
(838, 15, 34, 'hello learner........', '2020-08-09 13:09:31', 'learner', 'admin'),
(839, 15, 35, 'hello learner........', '2020-08-09 13:09:31', 'learner', 'admin'),
(840, 15, 37, 'hello learner........', '2020-08-09 13:09:31', 'learner', 'admin'),
(841, 15, 53, 'hello learner........', '2020-08-09 13:09:31', 'learner', 'admin'),
(842, 15, 58, 'hello learner........', '2020-08-09 13:09:31', 'learner', 'admin'),
(843, 15, 65, 'hello learner........', '2020-08-09 13:09:31', 'learner', 'admin'),
(844, 15, 66, 'hello learner........', '2020-08-09 13:09:31', 'learner', 'admin'),
(845, 15, 68, 'hello learner........', '2020-08-09 13:09:32', 'learner', 'admin'),
(846, 15, 81, 'hello learner........', '2020-08-09 13:09:32', 'learner', 'admin'),
(847, 15, 88, 'hello learner........', '2020-08-09 13:09:32', 'learner', 'admin'),
(848, 15, 96, 'hello learner........', '2020-08-09 13:09:32', 'learner', 'admin'),
(849, 15, 100, 'hello learner........', '2020-08-09 13:09:32', 'learner', 'admin'),
(850, 15, 101, 'hello learner........', '2020-08-09 13:09:32', 'learner', 'admin'),
(851, 15, 103, 'hello learner........', '2020-08-09 13:09:32', 'learner', 'admin'),
(852, 15, 104, 'hello learner........', '2020-08-09 13:09:32', 'learner', 'admin'),
(853, 15, 110, 'hello learner........', '2020-08-09 13:09:32', 'learner', 'admin'),
(854, 15, 28, 'hello larner how are you', '2020-08-09 13:10:39', 'learner', 'admin'),
(855, 28, 36, 'Hiii', '2020-08-09 13:44:15', 'assessor', 'learner'),
(856, 28, 36, 'Hello assessor', '2020-08-10 12:21:34', 'assessor', 'learner'),
(857, 28, 36, 'Hii assessor', '2020-08-10 12:23:32', 'assessor', 'learner'),
(858, 36, 28, 'Hii learner', '2020-08-10 12:24:05', 'learner', 'assessor'),
(859, 36, 28, 'Hiii', '2020-08-10 13:27:07', 'learner', 'assessor'),
(860, 114, 115, 'Hiii', '2020-08-10 14:32:50', 'learner', 'assessor'),
(861, 115, 114, 'Hii', '2020-08-10 14:33:16', 'assessor', 'learner'),
(862, 114, 115, 'Hii', '2020-08-10 14:41:10', 'learner', 'assessor'),
(863, 15, 16, 'hello all assessor', '2020-08-10 16:26:12', 'assessor', 'admin'),
(864, 15, 20, 'hello all assessor', '2020-08-10 16:26:12', 'assessor', 'admin'),
(865, 15, 33, 'hello all assessor', '2020-08-10 16:26:12', 'assessor', 'admin'),
(866, 15, 36, 'hello all assessor', '2020-08-10 16:26:12', 'assessor', 'admin'),
(867, 15, 39, 'hello all assessor', '2020-08-10 16:26:12', 'assessor', 'admin'),
(868, 15, 40, 'hello all assessor', '2020-08-10 16:26:12', 'assessor', 'admin'),
(869, 15, 41, 'hello all assessor', '2020-08-10 16:26:12', 'assessor', 'admin'),
(870, 15, 83, 'hello all assessor', '2020-08-10 16:26:12', 'assessor', 'admin'),
(871, 15, 84, 'hello all assessor', '2020-08-10 16:26:12', 'assessor', 'admin'),
(872, 15, 85, 'hello all assessor', '2020-08-10 16:26:13', 'assessor', 'admin'),
(873, 15, 87, 'hello all assessor', '2020-08-10 16:26:13', 'assessor', 'admin'),
(874, 15, 90, 'hello all assessor', '2020-08-10 16:26:13', 'assessor', 'admin'),
(875, 15, 99, 'hello all assessor', '2020-08-10 16:26:13', 'assessor', 'admin'),
(876, 15, 102, 'hello all assessor', '2020-08-10 16:26:13', 'assessor', 'admin'),
(877, 15, 114, 'hello all assessor', '2020-08-10 16:26:13', 'assessor', 'admin'),
(878, 28, 36, 'Hi hiiiiiii', '2020-08-10 16:28:01', 'assessor', 'learner'),
(879, 28, 36, 'Hiiii', '2020-08-11 09:23:41', 'assessor', 'learner'),
(880, 28, 36, 'Hello', '2020-08-11 11:38:25', 'assessor', 'learner'),
(881, 28, 36, 'Good morning', '2020-08-11 11:38:37', 'assessor', 'learner'),
(882, 22, 28, 'hello', '2020-08-11 11:42:53', 'learner', 'iqa'),
(883, 36, 38, 'Hello', '2020-08-11 15:07:03', 'iqa', 'assessor'),
(884, 115, 114, 'Hello ', '2020-08-11 16:40:54', 'assessor', 'learner'),
(885, 120, 122, 'Hii Can ', '2020-08-11 19:26:44', 'learner', 'assessor'),
(886, 120, 122, 'Good morning', '2020-08-12 12:52:42', 'learner', 'assessor'),
(887, 15, 16, 'hello', '2020-08-12 18:58:00', 'assessor', 'admin'),
(888, 15, 16, 'hiii', '2020-08-12 19:02:47', 'assessor', 'admin'),
(889, 15, 122, 'hello good evening', '2020-08-12 19:26:17', 'learner', 'admin'),
(890, 15, 120, 'hello good evening', '2020-08-12 19:27:06', 'assessor', 'admin'),
(891, 15, 120, 'hello good evening hhh', '2020-08-12 19:27:20', 'assessor', 'admin'),
(892, 120, 122, 'Hiii', '2020-08-12 19:28:15', 'learner', 'assessor'),
(893, 122, 120, 'Hiii', '2020-08-12 19:32:11', 'assessor', 'learner'),
(894, 15, 119, 'hii', '2020-08-12 19:32:11', 'learner', 'admin'),
(895, 122, 120, 'Hii', '2020-08-12 19:32:43', 'assessor', 'learner'),
(896, 15, 16, 'hiiiii', '2020-08-12 19:32:46', 'assessor', 'admin'),
(897, 15, 29, 'heyyyy', '2020-08-12 19:42:54', 'learner', 'admin'),
(898, 15, 34, 'heyyyy', '2020-08-12 19:42:54', 'learner', 'admin'),
(899, 15, 35, 'heyyyy', '2020-08-12 19:42:54', 'learner', 'admin'),
(900, 15, 37, 'heyyyy', '2020-08-12 19:42:54', 'learner', 'admin'),
(901, 15, 115, 'heyyyy', '2020-08-12 19:42:54', 'learner', 'admin'),
(902, 15, 119, 'heyyyy', '2020-08-12 19:42:54', 'learner', 'admin'),
(903, 15, 122, 'heyyyy', '2020-08-12 19:42:54', 'learner', 'admin'),
(904, 122, 120, 'Heyyy', '2020-08-12 19:44:34', 'assessor', 'learner'),
(905, 122, 120, 'Hiii', '2020-08-12 22:17:27', 'assessor', 'learner'),
(906, 16, 119, 'Hello', '2020-08-12 22:23:59', 'learner', 'assessor'),
(907, 119, 16, 'Hiii', '2020-08-12 22:24:26', 'assessor', 'learner'),
(908, 15, 120, 'Hello Good afternoon ', '2020-08-14 12:25:48', 'assessor', 'admin'),
(909, 15, 112, 'klk', '2020-08-14 12:26:42', 'iqa', 'admin'),
(910, 15, 112, 'hello', '2020-08-14 12:28:23', 'iqa', 'admin'),
(911, 15, 16, 'hello how are you???????', '2020-08-14 12:29:04', 'assessor', 'admin'),
(912, 15, 20, 'hello how are you???????', '2020-08-14 12:29:05', 'assessor', 'admin'),
(913, 15, 33, 'hello how are you???????', '2020-08-14 12:29:05', 'assessor', 'admin'),
(914, 15, 36, 'hello how are you???????', '2020-08-14 12:29:05', 'assessor', 'admin'),
(915, 15, 39, 'hello how are you???????', '2020-08-14 12:29:05', 'assessor', 'admin'),
(916, 15, 40, 'hello how are you???????', '2020-08-14 12:29:05', 'assessor', 'admin'),
(917, 15, 41, 'hello how are you???????', '2020-08-14 12:29:05', 'assessor', 'admin'),
(918, 15, 83, 'hello how are you???????', '2020-08-14 12:29:05', 'assessor', 'admin'),
(919, 15, 84, 'hello how are you???????', '2020-08-14 12:29:05', 'assessor', 'admin'),
(920, 15, 85, 'hello how are you???????', '2020-08-14 12:29:05', 'assessor', 'admin'),
(921, 15, 87, 'hello how are you???????', '2020-08-14 12:29:05', 'assessor', 'admin'),
(922, 15, 90, 'hello how are you???????', '2020-08-14 12:29:05', 'assessor', 'admin'),
(923, 15, 99, 'hello how are you???????', '2020-08-14 12:29:05', 'assessor', 'admin'),
(924, 15, 102, 'hello how are you???????', '2020-08-14 12:29:05', 'assessor', 'admin'),
(925, 15, 114, 'hello how are you???????', '2020-08-14 12:29:05', 'assessor', 'admin'),
(926, 15, 116, 'hello how are you???????', '2020-08-14 12:29:05', 'assessor', 'admin'),
(927, 15, 117, 'hello how are you???????', '2020-08-14 12:29:05', 'assessor', 'admin'),
(928, 15, 120, 'hello how are you???????', '2020-08-14 12:29:05', 'assessor', 'admin'),
(929, 120, 122, 'Hiiiii', '2020-08-14 13:18:54', 'learner', 'assessor'),
(930, 120, 122, 'Heeey', '2020-08-14 13:19:13', 'learner', 'assessor'),
(931, 120, 9, 'Hii', '2020-08-14 13:19:30', 'admin', 'assessor'),
(932, 120, 19, 'Nsks', '2020-08-14 13:19:36', 'admin', 'assessor'),
(933, 15, 29, 'heyyy', '2020-08-14 15:02:37', 'learner', 'admin'),
(934, 15, 34, 'heyyy', '2020-08-14 15:02:37', 'learner', 'admin'),
(935, 15, 35, 'heyyy', '2020-08-14 15:02:37', 'learner', 'admin'),
(936, 15, 37, 'heyyy', '2020-08-14 15:02:37', 'learner', 'admin'),
(937, 15, 115, 'heyyy', '2020-08-14 15:02:37', 'learner', 'admin'),
(938, 15, 119, 'heyyy', '2020-08-14 15:02:37', 'learner', 'admin'),
(939, 15, 122, 'heyyy', '2020-08-14 15:02:37', 'learner', 'admin'),
(940, 122, 120, 'Hiiiii', '2020-08-14 15:13:11', 'assessor', 'learner'),
(941, 120, 122, 'Hello', '2020-08-14 15:13:53', 'learner', 'assessor'),
(942, 120, 122, 'Hello', '2020-08-14 15:13:53', 'learner', 'assessor'),
(943, 15, 16, 'hellloooo', '2020-08-14 15:42:52', 'assessor', 'admin'),
(944, 15, 20, 'hellloooo', '2020-08-14 15:42:52', 'assessor', 'admin'),
(945, 15, 33, 'hellloooo', '2020-08-14 15:42:52', 'assessor', 'admin'),
(946, 15, 36, 'hellloooo', '2020-08-14 15:42:52', 'assessor', 'admin'),
(947, 15, 39, 'hellloooo', '2020-08-14 15:42:52', 'assessor', 'admin'),
(948, 15, 40, 'hellloooo', '2020-08-14 15:42:52', 'assessor', 'admin'),
(949, 15, 41, 'hellloooo', '2020-08-14 15:42:52', 'assessor', 'admin'),
(950, 15, 83, 'hellloooo', '2020-08-14 15:42:52', 'assessor', 'admin'),
(951, 15, 84, 'hellloooo', '2020-08-14 15:42:52', 'assessor', 'admin'),
(952, 15, 85, 'hellloooo', '2020-08-14 15:42:52', 'assessor', 'admin'),
(953, 15, 87, 'hellloooo', '2020-08-14 15:42:52', 'assessor', 'admin'),
(954, 15, 90, 'hellloooo', '2020-08-14 15:42:52', 'assessor', 'admin'),
(955, 15, 99, 'hellloooo', '2020-08-14 15:42:52', 'assessor', 'admin'),
(956, 15, 102, 'hellloooo', '2020-08-14 15:42:52', 'assessor', 'admin'),
(957, 15, 114, 'hellloooo', '2020-08-14 15:42:53', 'assessor', 'admin'),
(958, 15, 116, 'hellloooo', '2020-08-14 15:42:53', 'assessor', 'admin'),
(959, 15, 117, 'hellloooo', '2020-08-14 15:42:53', 'assessor', 'admin'),
(960, 15, 120, 'hellloooo', '2020-08-14 15:42:53', 'assessor', 'admin'),
(961, 120, 122, 'Hiiii', '2020-08-14 16:25:56', 'learner', 'assessor'),
(962, 120, 122, 'Good afternoon', '2020-08-14 16:32:34', 'learner', 'assessor'),
(963, 120, 122, 'Hioiiio', '2020-08-14 16:42:43', 'learner', 'assessor'),
(964, 120, 122, 'Hoii', '2020-08-14 16:44:57', 'learner', 'assessor'),
(965, 15, 120, 'HIII', '2020-08-14 16:47:13', 'assessor', 'admin'),
(966, 15, 16, 'hiiii', '2020-08-14 16:48:00', 'assessor', 'admin'),
(967, 15, 16, 'hiiiii', '2020-08-14 17:57:03', 'assessor', 'admin'),
(968, 15, 16, 'good evening', '2020-08-14 17:59:02', 'assessor', 'admin'),
(969, 15, 16, 'hello', '2020-08-14 18:01:25', 'assessor', 'admin'),
(970, 15, 16, 'Hoii', '2020-08-14 18:50:55', 'assessor', 'admin'),
(971, 15, 16, 'hello', '2020-08-14 18:57:55', 'assessor', 'admin'),
(972, 15, 16, 'Good evening ', '2020-08-14 19:00:36', 'assessor', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `user_id` int(10) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email_id` varchar(256) NOT NULL,
  `password` varchar(256) NOT NULL,
  `mobile_number` varchar(20) NOT NULL,
  `registration_datetime` varchar(20) DEFAULT '0000-00-00',
  `temp_code` varchar(10) NOT NULL,
  `verify_status` varchar(50) NOT NULL DEFAULT 'pending'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`user_id`, `first_name`, `last_name`, `email_id`, `password`, `mobile_number`, `registration_datetime`, `temp_code`, `verify_status`) VALUES
(1, 'demo', 'account', 'demo@gmail.com', '12345678', '', '2019-11-29 10:03:56', '', 'verified'),
(2, 'Jdczbb', 'sdfd', 'dud@gmail.com', '12345678', '', '2019-11-29 10:03:56', '', 'verified'),
(3, 'Sushil', 'Deokar', 'sushil.deokar@rslsolution.com', '12345678', '9850145236', '2019-11-29 10:03:56', '', 'verified'),
(4, 'Djskj', 'sdfsdfsd', 'sheh@gmail.com', '12345678', '24562726', '2019-11-29 10:03:56', '', 'verified'),
(5, 'Djdidk', 'sdfsd', 'sgsg@gmail.com', '12344555t', '354456663', '2019-11-29 10:03:56', '', 'verified'),
(6, 'Dhduueu', 'sdsf', 'dgeg@gmail.com', '12345678', '123565545', '2019-11-29 10:03:56', '', 'verified'),
(7, 'Oibch', 'xcvx', 'qee@gmail.com', '1234qwer', '9999885622587888888', '2019-11-29 10:03:56', '', 'verified'),
(8, 'Offdi', 'weww', 'dgdheh@gmail.com', 'xwddd', '856565656', '2019-11-29 10:03:56', '', 'verified'),
(9, 'Fred', 'xcv', 'Flintstone', '2345', '64876795', '2019-11-29 10:03:56', '', 'verified'),
(10, 'poonam', 'ghevande', 'dkhfkh@gmail.com', '68768', '7547854', '2019-11-29 10:03:56', '', 'verified'),
(11, 'shubham', 'sawant', 'shubhamsawant8055@gmail.com', 'shubham@12', '9874859632', '2019-11-29 10:03:56', 'MJNg0', 'verified'),
(12, 'zxc', 'sdfs', 'zxc@gmail.com', '1234567890', '321654987', '2019-11-29 10:03:56', '', 'verified'),
(13, 'G', 'ssdf', 'rhy', 'rgu', '523', '2019-11-29 10:03:56', '', 'verified'),
(14, 'Akash', 'Lohar', 'skyward5129@gmail.com', '5129sky00', '8605568001', '2019-11-29 10:03:56', '', 'verified'),
(15, 'aarti', 'kale', 'admin@gmail.com', 'admin', '7219251513', '2019-11-29 10:03:56', 'IVT6s', 'verified'),
(16, 'swapnil', 'machale', 'swapnilmachale38@gmail.com', '', '12345678', '2019-11-29 10:03:56', '', 'verified'),
(17, 'samadhan', 'vidhate', 'samadhan.vidhate1@gmail.com', 'Sam12345678', '9860731823', '2019-11-29 10:03:56', '', 'verified'),
(18, 'Sushil ', 'Deokar', 'sushildeokar12@gmail.com', 'Sushil12345', '9021900488', '2019-11-29 10:03:56', '', 'verified'),
(19, 'Shripad', 'Kulkarni', 'shripad@gmail.com', 'Sripad123456', '9021900488', '2019-11-29 10:03:56', '', 'verified'),
(20, 'Jitendra', 'Chaudhari', 'jitu@gmail.com', '12345678', '8958496266', '2019-11-29 10:03:56', '', 'verified'),
(21, 'shubham', 'sawant', 'shubham.sawant@rslsolution.com', '87654321', '9922252501', '2019-11-29 10:03:56', 'BiNrz', 'verified'),
(22, 'asdsa', 'asdas', 'sada@gmail.com', 'sadds', '3213213', '2019-11-29 10:03:56', '', 'verified'),
(25, 'Neha', 'Sharma', 'neha@gmail.com', '23456789', '80808080808', '0000-00-00', '', 'verified'),
(27, 'akash', 'lohar', 'akash@gmail.com', '12345678', '9911231232', '0000-00-00', '', 'verified'),
(28, 'abc', 'abc', 'john.stevens@myenergyquote.co.uk', '123456789', '123456789', '0000-00-00', '', 'verified'),
(29, 'xyza', 'xyza', 'xyz@gmail.com', '1234567890', '12345678901', '0000-00-00', '', 'verified'),
(30, 'akira', 'kale', 'kale25@gmail.com', '12345', '6755463789', '19/12/2019', '', 'verified'),
(31, 'siya', 'sharma', 'sharma@gmail.com', 'siya12345', '2635478909', '2019-12-12', '', 'verified'),
(32, 'ram', 'raut', 'ram@gmail.com', '1234567890', '1234567890', '0000-00-00', '', 'verified'),
(33, 'aarti', 'kale', 'aarti@gmail.com', '12345678', '123456789', '0000-00-00', '', 'verified');

-- --------------------------------------------------------

--
-- Table structure for table `user_details_new`
--

CREATE TABLE `user_details_new` (
  `id` int(30) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `date_of_birth` varchar(30) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` varchar(150) NOT NULL,
  `learner_difficulty` varchar(50) DEFAULT NULL,
  `course_type` varchar(50) DEFAULT NULL,
  `regions` varchar(50) NOT NULL,
  `type_of_learner` varchar(50) DEFAULT NULL,
  `payment_plan` varchar(50) DEFAULT NULL,
  `contact` varchar(256) DEFAULT NULL,
  `candidate_no` varchar(30) DEFAULT NULL,
  `added_by` varchar(30) NOT NULL,
  `accessor_id` varchar(30) DEFAULT NULL,
  `post_code` varchar(10) DEFAULT NULL,
  `role` varchar(10) NOT NULL,
  `password` varchar(50) NOT NULL,
  `image` varchar(256) DEFAULT NULL,
  `temp_code` varchar(20) DEFAULT NULL,
  `verify_status` varchar(10) DEFAULT NULL,
  `payment_status` varchar(16) NOT NULL DEFAULT 'open',
  `notes` text,
  `registered_date` varchar(125) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_details_new`
--

INSERT INTO `user_details_new` (`id`, `first_name`, `middle_name`, `last_name`, `date_of_birth`, `gender`, `email`, `address`, `learner_difficulty`, `course_type`, `regions`, `type_of_learner`, `payment_plan`, `contact`, `candidate_no`, `added_by`, `accessor_id`, `post_code`, `role`, `password`, `image`, `temp_code`, `verify_status`, `payment_status`, `notes`, `registered_date`) VALUES
(4, 'raju', 'datta', 'mane', '1/26/2020', 'male', 'ksb98600@gmail.com', 'pune', NULL, NULL, '1', NULL, NULL, '44 9874563215', NULL, 'abc', NULL, '411033', 'iqa', 'jy2nhy1j', NULL, NULL, NULL, '', '', NULL),
(11, 'Sam', 'Deeley', 'Robin', '9/30/2019', 'male', 'sam@ptacademy.com', '92 Broad St', NULL, NULL, '6', NULL, NULL, '44 570000000', NULL, 'David Parker', NULL, 'BA 1', 'iqa', 'of68qo5v', NULL, NULL, NULL, '', '', NULL),
(14, 'raju', 'sham', 'kate', '1/19/2000', 'male', 'raju@gmail.com', 'pune', NULL, NULL, '1', NULL, NULL, '44 8564775552', NULL, 'abc', NULL, '411055', 'iqa', '6a01vi1c', NULL, NULL, NULL, '', '', NULL),
(15, 'mohan', 'raju', 'kate', '1/10/2000', 'male', 'mohovig185@upcmaill.com', 'pune', NULL, NULL, '16', NULL, NULL, '44 5234585652', NULL, 'abc', NULL, '411033', 'iqa', 'uj3rzzxa', NULL, NULL, NULL, '', '', NULL),
(16, 'rajendra', 'shri', 'mane', '12/23/1999', 'male', 'raju@gmail.com', 'pune', NULL, NULL, '16', NULL, NULL, '44 9568458565', NULL, 'abc', NULL, '410012', 'assessor', '12345678', 'users_image/1597155151418067253.jpeg', NULL, NULL, '', '', NULL),
(19, 'been', 'tee', 'khan', '1/22/2000', 'male', 'been@gmail.com', 'pune', NULL, NULL, '16', NULL, NULL, '44 9568452526', NULL, 'abc', NULL, '411056', 'iqa', '6da43fao', NULL, NULL, NULL, '', '', NULL),
(20, 'shrikant', 'anil', 'kate', '1/15/2001', 'male', 'shri@gmail.com', 'pune', NULL, NULL, '16', NULL, NULL, '44 8956487525', NULL, 'xyz', NULL, '411085', 'assessor', '87654321', 'users_image/1589356267421397598.jpeg', NULL, NULL, '', '', NULL),
(22, 'uddhav', 'ramdhas', 'bade', '1/3/1994', 'male', 'uddhav394@gmail.com', 'pune', NULL, NULL, '16', NULL, NULL, '91 9822459068', NULL, 'Shubham', NULL, '411033', 'iqa', 'uddhav@123', 'iqa_images/1597138154816565360.png', NULL, NULL, '', '', NULL),
(29, 'rupesh', '', 'munot', '3/20/1980', 'male', 'munotrupesh@gmail.com', 'pune', 'abc', '8', '16', '1', '1', '91 9586475254', '000004', 'abc', '20', NULL, 'learner', 'rupesh@123', NULL, NULL, NULL, '', 'asdsa', '2020/07/07'),
(30, 'Sam', 'John', 'Deeley', '3/5/2020', 'male', 'sam@ptacademy.com', '92 Broad street', NULL, NULL, '16', NULL, NULL, '44 73927323923923', NULL, 'David Parker', NULL, 'BA1 2AD', 'iqa', 'pblcsh4s', NULL, NULL, NULL, '', '', NULL),
(31, 'David', '', 'Parker', '4/7/2020', 'male', 'david@ptacademy.com', 'Swindon', NULL, NULL, '4', NULL, NULL, '44 888888888888', NULL, 'David parker', NULL, 'BA1', 'iqa', 'z1myugvx', NULL, NULL, NULL, '', '', NULL),
(32, 'David', '', 'Parker', '4/7/2020', 'male', 'david@ptacademy.com', 'Swindon', NULL, NULL, '4', NULL, NULL, '44 888888888888', NULL, 'David parker', NULL, 'BA1', 'iqa', 'lnj86a0i', NULL, NULL, NULL, '', '', NULL),
(33, 'Sam', '', 'Deeley', '3/1/2020', 'male', 'sam@ptacademy.com', 'London', NULL, NULL, '5', NULL, NULL, '44 58558587597559', NULL, 'David Parker', NULL, 'NW1', 'assessor', 'ewt45p1h', NULL, NULL, NULL, '', '', NULL),
(34, 'John', 'Dave', 'Doe', '4/12/2020', 'male', 'john@ptacademy.com', '12 West London', 'Partially blind', '8', '5', '1', '2', '44 07236123123', '000005', 'David Parker', '33', NULL, 'learner', 'jgyxcu5o', NULL, NULL, NULL, '', '', '2020/07/07'),
(35, 'Dave', '', 'Deeley', '4/13/2020', 'male', 'dave@ptacademy.com', 'London Town', 'Blind', '1', '1', '2', '1', '44 89696986969696', '000006', 'David Parker', '7', NULL, 'learner', '6uhp8861', NULL, NULL, NULL, '', '', '2020/07/07'),
(36, 'poonam', '', 'ghewande', '5/5/1995', 'female', 'ghewandep88@gmail.com', 'pune', NULL, NULL, '16', NULL, NULL, '44 8589565865', NULL, 'xyz', NULL, '411033', 'assessor', '12345678', 'users_image/15971174991435618685.jpeg', 'EB8QB', NULL, '', '', NULL),
(37, 'David', '', 'Parker', '5/2/1970', 'male', 'david@imovetraining.com', 'Swindon', 'Fit and healthy', '8', '5', '2', '1', '44 77482614230', '000007', 'TEE', '33', NULL, 'learner', 'fhsm5hol', NULL, NULL, NULL, '', '', '2020/07/07'),
(38, 'Mike', '', 'Kplomedo', '4/24/1980', 'male', 'mikey@ptacademy.com', 'Birmingam', NULL, NULL, '3', NULL, NULL, '44 7748261423', NULL, 'DP', NULL, 'BA1', 'iqa', '1onebhjw', NULL, NULL, NULL, '', '', NULL),
(39, 'John', '', 'Doe', '4/20/1999', 'female', 'fptraining@gmail.com', 'Birmingam', NULL, NULL, '8', NULL, NULL, '44 9874563215', NULL, 'DP', NULL, '411033', 'assessor', 'wtsk81dg', NULL, NULL, NULL, '', '', NULL),
(40, 'David', '', 'Parker', '4/24/1980', 'female', 'david@ptacademy.com', 'Swindon', NULL, NULL, '11', NULL, NULL, '44 9874563215', NULL, 'DP', NULL, 'SN2', 'assessor', 'p81eiu6y', NULL, NULL, NULL, '', '', NULL),
(41, 'Adam', '', 'Kiani', '6/4/1977', 'female', 'adam@ptacademy.com', 'Birmingham', NULL, NULL, '3', NULL, NULL, '44 9874563215', NULL, 'David Parker', NULL, 'BA1', 'assessor', 'dojt83oa', NULL, NULL, NULL, '', '', NULL),
(45, 'David', '', 'Kplomedo', '4/20/1999', 'female', 'david@ptacademy.com', '14 Whittington Grove, Birmingham', NULL, NULL, '2', NULL, NULL, '44 7748261423', NULL, 'David Parker', NULL, 'B33 8HE', 'iqa', 'hi59q7aa', NULL, NULL, NULL, '', '', NULL),
(46, 'Simi', 'P', 'Same', '1/1/2000', 'female', 'fptraining@gmail.com', 'Swindon', NULL, NULL, '11', NULL, NULL, '44 7748261423', NULL, 'David Parker', NULL, 'SN1', 'iqa', 'ruhjez3n', NULL, NULL, NULL, '', '', NULL),
(62, 'aishwarya', '', 'mallad', '7/18/1995', 'female', 'aishwaryamallad@rslsolution.com', 'pune', NULL, NULL, '16', NULL, NULL, '44 958648575', NULL, 'shubham', NULL, '410033', 'iqa', '2cx566ni', NULL, NULL, NULL, 'open', NULL, NULL),
(83, 'Rushikesh', 'Dnyanoba', 'Borchate', '7/20/2020', 'male', 'rushiathome@gmail.com', 'Rsl solution, working from home', NULL, NULL, '3', NULL, NULL, '44 08888164906', NULL, 'me', NULL, '411033', 'assessor', '6u1vnl5o', NULL, NULL, NULL, 'open', NULL, NULL),
(84, 'Dhananjay', 'Dnyanoba', 'Borchate', '7/8/2020', 'female', 'rushiathome7@gmail.com', 'Amar Medical at Dake Accident Hospital Georai road', NULL, NULL, '12', NULL, NULL, '44 09096884644', NULL, 'me', NULL, '431131', 'assessor', 'zr0ws8ve', 'users_image/159706188785513325.jpeg', NULL, NULL, 'open', NULL, NULL),
(85, 'Aishwarya', 'p', 'Mallad', '7/18/1995', 'female', 'asd@gm.vhn', 'sangli', NULL, NULL, '9', NULL, NULL, '44 9090909090', NULL, 'aish', NULL, '123', 'assessor', '6oculid0', NULL, NULL, NULL, 'open', NULL, NULL),
(87, 'Dhananjay', '', 'Borchate', '7/3/2020', 'male', 'rushikesh@gmail.com', 'qwerty uiop asdfg hjkl zxcvbnm n qwert hgtrews qwe', NULL, NULL, '13', NULL, NULL, '44 09096884643', NULL, 'me', NULL, '431131', 'assessor', 'pukpdywb', NULL, NULL, NULL, 'open', NULL, NULL),
(89, 'Aish', '', 'IQA', '6/30/2020', 'female', 'aishiqa@gmail.com', 'sangli', NULL, NULL, '14', NULL, NULL, '44 90901212', NULL, 'aish', NULL, '416', 'iqa', 'l9wjemv8', NULL, NULL, NULL, 'open', NULL, NULL),
(90, 'Dhananjay', '', 'Borchate', '7/9/2020', 'male', 'beyolip307@kartk5.com', 'lane no 9, fdgfdgdfgdfg fdgdfg dsfg dfsgsdfgdfg', NULL, NULL, '14', NULL, NULL, '44 08866164907', NULL, 'me', NULL, '431131', 'assessor', 'n0wkx4z1', NULL, NULL, NULL, 'open', NULL, NULL),
(91, 'Dhananjay', 'Dnyanoba', 'Borchate', '7/2/2020', 'male', 'borchate@rslsolution.com', 'Amf agadgda fgg dagdfa', NULL, NULL, '15', NULL, NULL, '44 09096884666', NULL, 'me', NULL, '431131', 'iqa', 'iw7vl8aw', NULL, NULL, NULL, 'open', NULL, NULL),
(98, 'Aishwarya', 'IQA', 'Mallad', '7/18/1995', 'female', 'pivovog301@dnawr.com', 'sangli', NULL, NULL, '13', NULL, NULL, '44 1212121212', NULL, 'Adminn', NULL, '415415', 'iqa', '08od2wc5', NULL, NULL, NULL, 'open', NULL, NULL),
(99, 'Aishwarya', 'Assessor', 'Mallad', '7/18/1995', 'female', 'aishwaryamallad@gmail.com', 'sangli', NULL, NULL, '10', NULL, NULL, '44 12345677', NULL, 'Admin', NULL, '345345', 'assessor', 'gq0iu3s0', NULL, NULL, NULL, 'open', NULL, NULL),
(102, 'Neha', 'PRIT', 'Das', '7/1/2020', 'female', 'poonamghewande403@gmail.com', 'kolkata', NULL, NULL, '16', NULL, NULL, '44 65876897689070', NULL, 'poonam', NULL, 'yghguhgu', 'assessor', 'j6xth7ww', NULL, 'nsaXD', NULL, 'open', NULL, NULL),
(112, 'fsdf', '', 'asfsa', '7/28/2020', 'male', 'sadas@dfg.gd', 'asgd ayiouwq8e983e3e;[p=/[[sa]dDPE+P+ E{R+W[=32=r432\'\'sdf;sdfffffffffffffffffffffffffff][\\]/[][093439kfdfkdsp[sd;fsd;fsd;f[sd;sd;fsdfsd[fsdp[f=--e0w-r', NULL, NULL, '2', NULL, NULL, '44 34324334534', NULL, 'as', NULL, 'assad', 'iqa', 'sc36dgx9', NULL, NULL, NULL, 'open', NULL, NULL),
(114, 'priya', '', 'varma', '7/26/1990', 'female', 'shubham.sawant@rslsolution.com', 'pune', NULL, NULL, '16', NULL, NULL, '44 8564875255', NULL, 'me', NULL, '411033', 'assessor', '12345678', 'users_image/1597050139229552556.jpeg', '', NULL, 'open', NULL, NULL),
(115, 'pratik', '', 'patil', '6/1/1991', 'male', 'shubham.sawant@rwntrading.com', 'pune', 'xyz', '30', '2', '1', '1', '44 985647582', '000026', 'me', '114', NULL, 'learner', '12345678', 'users_image/15972173341536744549.jpeg', 'StM0Z', NULL, 'open', NULL, '2020/08/10'),
(116, 'Sam', 'nick', 'Prince', '1/28/1995', 'male', 'poonamghewande403@gmial.com', 'Banglore', NULL, NULL, '3', NULL, NULL, '44 6577775575', NULL, 'me', NULL, '44889', 'assessor', 'z5lokbtd', NULL, NULL, NULL, 'open', NULL, NULL),
(117, 'shreekant', '', 'bhosle', '7/27/1990', 'male', 'shreekant@gmail.com', 'sy87213\'/\'/\']/sa[==-2321[2[\\\\][,../da[]p=p-spos S-0 SDFSDF8e6r354]]\'];..;\';]]\\\\=34=][][34054343[;;3443;5439rbffsfdfdvr ff sdfgsdf sdf f s f sdfsdfp fg', NULL, NULL, '2', NULL, NULL, '44 9832137123', NULL, 'me', NULL, '41kdq', 'assessor', 'oe8fiiso', NULL, NULL, NULL, 'open', NULL, NULL),
(118, 'jay', '', 'patil', '7/2/1992', 'male', 'jay@gmail.com', 'sy87213\'/\'/\']/sa[==-2321[2[\\\\][,../da[]p=p-spos S-0 SDFSDF8e6r354]]\'];..;\';]]\\\\=34=][][34054343[;;3443;5439rbffsfdfdvr ff sdfgsdf sdf f s f sdfsdfp fg', NULL, NULL, '3', NULL, NULL, '44 2132321213', NULL, 'me', NULL, '32sd3', 'iqa', 'k3svbjmg', NULL, NULL, NULL, 'open', NULL, NULL),
(119, 'pooja', '', 'landge', '7/26/2019', 'female', 'pooja@gmail.com', 'sy87213\'/\'/\']/sa[==-2321[2[\\\\][,../da[]p=p-spos S-0 SDFSDF8e6r354]]\'];..;\';]]\\\\=34=][][34054343[;;3443;5439rbffsfdfdvr ff sdfgsdf sdf f s f sdfsdfp fg', 'hard', '30', '12', '2', '1', '44 35435433', '000027', 'me', '16', NULL, 'learner', '87654321', 'users_image/15973075121307761266.jpeg', NULL, NULL, 'open', NULL, '2020/08/11'),
(120, 'Ribel', 'harry', 'Wilson', '7/27/1997', 'female', 'poonam.ghewande@rwntrading.com', 'Amstadram', NULL, NULL, '16', NULL, NULL, '44 636737783', NULL, 'me', NULL, '224443', 'assessor', '12345678', 'users_image/159739145446072892.jpeg', '', NULL, 'open', NULL, NULL),
(122, 'CAN', 'Harry', 'yaman', '8/3/1994', 'male', 'yamancan946@gmail.com', 'Istanbul', 'no', '17', '16', '2', '1', '44 6664646764', '000029', 'me', '120', NULL, 'learner', '12345678', 'users_image/15973044931626575173.jpeg', 'eFyr1', NULL, 'open', NULL, '2020/08/11');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `role` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`id`, `user_id`, `role`) VALUES
(4, 4, 'learner'),
(5, 5, 'learner'),
(6, 6, 'learner'),
(7, 7, 'learner'),
(8, 8, 'learner'),
(9, 9, 'learner'),
(10, 10, 'learner'),
(11, 11, 'learner'),
(12, 12, 'learner'),
(13, 13, 'learner'),
(14, 14, 'learner'),
(15, 15, 'learner'),
(22, 20, 'learner'),
(23, 20, 'iqa'),
(24, 21, 'iqa'),
(25, 21, 'mentor'),
(26, 21, 'learner'),
(27, 22, 'admin'),
(28, 22, 'iqa'),
(29, 22, 'mentor'),
(76, 1, 'learner'),
(77, 2, 'learner'),
(78, 3, 'learner'),
(92, 25, 'learner'),
(94, 27, 'learner'),
(95, 27, 'learner'),
(120, 29, ''),
(121, 29, ''),
(122, 29, ''),
(123, 29, ''),
(124, 29, ''),
(130, 28, ''),
(131, 28, ''),
(132, 28, ''),
(133, 28, ''),
(134, 28, ''),
(135, 28, 'learner'),
(136, 28, 'mentor'),
(137, 28, 'iqa'),
(138, 19, ''),
(140, 33, 'iqa'),
(141, 33, 'mentor'),
(142, 33, 'learner'),
(143, 32, '');

-- --------------------------------------------------------

--
-- Table structure for table `video_slide`
--

CREATE TABLE `video_slide` (
  `id` int(30) NOT NULL,
  `title` varchar(50) NOT NULL,
  `video` varchar(50) NOT NULL,
  `prompt` varchar(50) NOT NULL,
  `lesson_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `video_slide`
--

INSERT INTO `video_slide` (`id`, `title`, `video`, `prompt`, `lesson_id`) VALUES
(9, 'Video', 'video_slide/1465061179.mp4', 'Skip Video', 242),
(10, 'Video', 'video_slide/1465061179.mp4', 'Skip Video', 217),
(12, 'Video', 'video_slide/1465061179.mp4', 'Skip Video', 238),
(13, 'Video', 'video_slide/1465061179.mp4', 'Skip Video', 237),
(26, 'Bone formation', 'video_slide/kezia lifting web.mp4', 'Skip Video', 298),
(28, 'Video', 'video_slide/1465061179.mp4', 'Skip Video', 4),
(29, 'Video', 'video_slide/1465061179.mp4', 'Skip Video', 6),
(30, 'Video', 'video_slide/1465061179.mp4', 'Skip Video', 8),
(31, 'Video', 'video_slide/1465061179.mp4', 'Skip Video', 11),
(32, 'Video', 'video_slide/1465061179.mp4', 'Skip Video', 13),
(33, 'Video', 'video_slide/1465061179.mp4', 'Skip Video', 15),
(34, 'Video', 'video_slide/1465061179.mp4', 'Skip Video', 47),
(35, 'Video', 'video_slide/1465061179.mp4', 'Skip Video', 122),
(36, 'Video', 'video_slide/1465061179.mp4', 'Skip Video', 125),
(37, 'Video', 'video_slide/1465061179.mp4', 'Skip Video', 2),
(39, 'Video', 'video_slide/1465061179.mp4', 'Skip Video', 1),
(40, 'burt', 'video_slide/Breaststrok.wmv', 'Skip Video', 1),
(50, 'Intro to muscular system', 'video_slide/zoom mike and sam_0-2.mp4', 'Skip Video', 16),
(51, 'Movement control', 'video_slide/ZOOM TEST WARM UP L3.mp4', 'Skip Video', 332),
(52, 'Video', 'video_slide/zoom-timeline.mp4', 'Skip Video', 16),
(53, 'Watch teh video ', 'video_slide/zoom-timeline.mp4', 'Skip Video', 338),
(54, 'Video', 'video_slide/1465061179.mp4', 'Skip Video', 338),
(55, 'Video', 'video_slide/1465061179.mp4', 'Skip Video', 361),
(57, 'Video', 'video_slide/1465061179.mp4', 'Skip Video', 303),
(58, 'Video', 'video_slide/1465061179.mp4', 'Skip Video', 348),
(60, 'Video', 'video_slide/20200701_130509.mp4', 'Skip Video', 364),
(61, 'Video', 'video_slide/1465061179.mp4', 'Skip Video', 354),
(62, 'Video', 'video_slide/1465061179.mp4', 'Skip Video', 389),
(63, 'Video', 'video_slide/20200722_121308.mp4', 'Skip Video', 387),
(65, '', 'video_slide/20200701_130427.mp4', 'Skip Video', 388),
(66, 'Video', 'video_slide/1465061179.mp4', 'Skip Video', 396),
(67, 'Video', 'video_slide/1465061179.mp4', 'Skip Video', 365),
(69, 'Video', 'video_slide/1465061179.mp4', 'Skip Video', 401),
(70, 'Video', 'video_slide/1465061179.mp4', 'Skip Video', 403),
(71, 'Video', 'video_slide/18731597-preview.mp4', 'Skip Video', 347),
(72, 'Video', 'video_slide/18731597-preview.mp4', 'Skip Video', 394),
(73, 'Video', 'video_slide/1465061179.mp4', 'Skip Video', 419),
(74, 'Video', 'video_slide/1465061179.mp4', 'Skip Video', 422),
(75, 'Video', 'video_slide/1465061179.mp4', 'Skip Video', 425);

-- --------------------------------------------------------

--
-- Table structure for table `vimeo_video`
--

CREATE TABLE `vimeo_video` (
  `v_id` int(11) NOT NULL,
  `video_id` varchar(25) NOT NULL,
  `title` varchar(256) NOT NULL,
  `lesson_id` int(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vimeo_video`
--

INSERT INTO `vimeo_video` (`v_id`, `video_id`, `title`, `lesson_id`) VALUES
(1, '87701971', 'first video', 122),
(3, '87701971', 'A List Slide', 355),
(4, '87701971', 'A List Slide', 362),
(5, '87701971', 'A List Slide', 347),
(6, '367990283', 'A List Slide', 338),
(7, '87701971', 'A List Slide', 348),
(8, '87701971', 'A List Slide', 364),
(9, '87701971', 'A List Slide', 354),
(11, '87701971', 'A List Slide', 388),
(12, '87701971', 'A List Slide', 396),
(13, '87701971', 'A List Slide', 365),
(15, '87701971', 'A List Slide', 401),
(16, '87701971', 'A List Slide', 402),
(17, '87701971', 'A List Slide', 403);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accessor`
--
ALTER TABLE `accessor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`ac_id`);

--
-- Indexes for table `admin_details`
--
ALTER TABLE `admin_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `AO`
--
ALTER TABLE `AO`
  ADD PRIMARY KEY (`ao_id`);

--
-- Indexes for table `assessment_attempt`
--
ALTER TABLE `assessment_attempt`
  ADD PRIMARY KEY (`a_id`);

--
-- Indexes for table `assessment_expandable`
--
ALTER TABLE `assessment_expandable`
  ADD PRIMARY KEY (`ae_id`);

--
-- Indexes for table `assessment_expandable_answers`
--
ALTER TABLE `assessment_expandable_answers`
  ADD PRIMARY KEY (`aexans_id`);

--
-- Indexes for table `assessment_expandable_list`
--
ALTER TABLE `assessment_expandable_list`
  ADD PRIMARY KEY (`ael_id`);

--
-- Indexes for table `assessment_multiple_choice`
--
ALTER TABLE `assessment_multiple_choice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assessment_multiple_choice_options`
--
ALTER TABLE `assessment_multiple_choice_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assessment_pool_questions`
--
ALTER TABLE `assessment_pool_questions`
  ADD PRIMARY KEY (`pq_id`);

--
-- Indexes for table `assessment_question_result`
--
ALTER TABLE `assessment_question_result`
  ADD PRIMARY KEY (`ar_id`);

--
-- Indexes for table `assessment_result_by_assessor`
--
ALTER TABLE `assessment_result_by_assessor`
  ADD PRIMARY KEY (`ass_res_id`);

--
-- Indexes for table `assessor_expandable_ans`
--
ALTER TABLE `assessor_expandable_ans`
  ADD PRIMARY KEY (`ass_ex_id`);

--
-- Indexes for table `assessor_expandable_option_ans`
--
ALTER TABLE `assessor_expandable_option_ans`
  ADD PRIMARY KEY (`ass_exOpt_id`);

--
-- Indexes for table `assign_ao_regdate`
--
ALTER TABLE `assign_ao_regdate`
  ADD PRIMARY KEY (`aoreg_id`);

--
-- Indexes for table `ass_expandable_option_answers`
--
ALTER TABLE `ass_expandable_option_answers`
  ADD PRIMARY KEY (`aexopt_id`);

--
-- Indexes for table `ass_pool_question_options`
--
ALTER TABLE `ass_pool_question_options`
  ADD PRIMARY KEY (`opt_id`);

--
-- Indexes for table `ass_submitdate_bylearner`
--
ALTER TABLE `ass_submitdate_bylearner`
  ADD PRIMARY KEY (`sd_id`);

--
-- Indexes for table `circle_the_answer`
--
ALTER TABLE `circle_the_answer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `circle_the_answer_options`
--
ALTER TABLE `circle_the_answer_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `component`
--
ALTER TABLE `component`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `corss_word_details`
--
ALTER TABLE `corss_word_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cross_word_id` (`cross_word_id`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course_type`
--
ALTER TABLE `course_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cpd`
--
ALTER TABLE `cpd`
  ADD PRIMARY KEY (`cpd_id`);

--
-- Indexes for table `cpd_course`
--
ALTER TABLE `cpd_course`
  ADD PRIMARY KEY (`q_id`);

--
-- Indexes for table `crossword_data`
--
ALTER TABLE `crossword_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cross_word`
--
ALTER TABLE `cross_word`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cv_exercise`
--
ALTER TABLE `cv_exercise`
  ADD PRIMARY KEY (`cv_id`);

--
-- Indexes for table `drag_the_Match`
--
ALTER TABLE `drag_the_Match`
  ADD PRIMARY KEY (`dtm_id`);

--
-- Indexes for table `drag_to_match_options`
--
ALTER TABLE `drag_to_match_options`
  ADD PRIMARY KEY (`dtmopt_id`);

--
-- Indexes for table `equipment`
--
ALTER TABLE `equipment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`e_id`);

--
-- Indexes for table `exercise`
--
ALTER TABLE `exercise`
  ADD PRIMARY KEY (`rex_id`);

--
-- Indexes for table `exercise_type`
--
ALTER TABLE `exercise_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expandable_details`
--
ALTER TABLE `expandable_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expandable_list`
--
ALTER TABLE `expandable_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`f_id`);

--
-- Indexes for table `first_last_slide`
--
ALTER TABLE `first_last_slide`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `freetext_feedback_by_assessor`
--
ALTER TABLE `freetext_feedback_by_assessor`
  ADD PRIMARY KEY (`ftfeed_id`);

--
-- Indexes for table `free_text`
--
ALTER TABLE `free_text`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`group_id`);

--
-- Indexes for table `group_courses`
--
ALTER TABLE `group_courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `group_users`
--
ALTER TABLE `group_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `image_coordinates`
--
ALTER TABLE `image_coordinates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `image_waypoint`
--
ALTER TABLE `image_waypoint`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `insert_video`
--
ALTER TABLE `insert_video`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `iqareport_iqa_list`
--
ALTER TABLE `iqareport_iqa_list`
  ADD PRIMARY KEY (`list_id`);

--
-- Indexes for table `iqa_feedback`
--
ALTER TABLE `iqa_feedback`
  ADD PRIMARY KEY (`if_id`);

--
-- Indexes for table `iqa_submitted_report`
--
ALTER TABLE `iqa_submitted_report`
  ADD PRIMARY KEY (`sr_id`);

--
-- Indexes for table `learner_report_by_iqa`
--
ALTER TABLE `learner_report_by_iqa`
  ADD PRIMARY KEY (`lr_id`);

--
-- Indexes for table `learner_type_master`
--
ALTER TABLE `learner_type_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lesson`
--
ALTER TABLE `lesson`
  ADD PRIMARY KEY (`id`),
  ADD KEY `section_id` (`section_id`);

--
-- Indexes for table `lesson_attempt`
--
ALTER TABLE `lesson_attempt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lesson_question_relation`
--
ALTER TABLE `lesson_question_relation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `list_slide`
--
ALTER TABLE `list_slide`
  ADD PRIMARY KEY (`l_id`);

--
-- Indexes for table `list_slide_options`
--
ALTER TABLE `list_slide_options`
  ADD PRIMARY KEY (`lopt_id`);

--
-- Indexes for table `login_statistic`
--
ALTER TABLE `login_statistic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `memory`
--
ALTER TABLE `memory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `memory_details`
--
ALTER TABLE `memory_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `memory_id` (`memory_id`);

--
-- Indexes for table `multiple_choice`
--
ALTER TABLE `multiple_choice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `multiple_choice_image`
--
ALTER TABLE `multiple_choice_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `multiple_choice_image_options`
--
ALTER TABLE `multiple_choice_image_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `multiple_choice_options`
--
ALTER TABLE `multiple_choice_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_plan_master`
--
ALTER TABLE `payment_plan_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `peer_authoring`
--
ALTER TABLE `peer_authoring`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `peer_authoring_answers`
--
ALTER TABLE `peer_authoring_answers`
  ADD PRIMARY KEY (`pans_id`);

--
-- Indexes for table `peer_authoring_ans_videos`
--
ALTER TABLE `peer_authoring_ans_videos`
  ADD PRIMARY KEY (`paa_id`);

--
-- Indexes for table `plans_master`
--
ALTER TABLE `plans_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `programme_card`
--
ALTER TABLE `programme_card`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question_pool`
--
ALTER TABLE `question_pool`
  ADD PRIMARY KEY (`pool_id`);

--
-- Indexes for table `question_result`
--
ALTER TABLE `question_result`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `regions_master`
--
ALTER TABLE `regions_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `registration_user`
--
ALTER TABLE `registration_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reordering`
--
ALTER TABLE `reordering`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reordering_options`
--
ALTER TABLE `reordering_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `resistance_exercise`
--
ALTER TABLE `resistance_exercise`
  ADD PRIMARY KEY (`re_id`);

--
-- Indexes for table `scrollable`
--
ALTER TABLE `scrollable`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `section`
--
ALTER TABLE `section`
  ADD PRIMARY KEY (`id`),
  ADD KEY `unit_id` (`unit_id`);

--
-- Indexes for table `selecting_stretch`
--
ALTER TABLE `selecting_stretch`
  ADD PRIMARY KEY (`s_id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `true_false_statement`
--
ALTER TABLE `true_false_statement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `true_or_false`
--
ALTER TABLE `true_or_false`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unit`
--
ALTER TABLE `unit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `course_id` (`course_id`);

--
-- Indexes for table `user_chat`
--
ALTER TABLE `user_chat`
  ADD PRIMARY KEY (`chat_id`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_details_new`
--
ALTER TABLE `user_details_new`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `video_slide`
--
ALTER TABLE `video_slide`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vimeo_video`
--
ALTER TABLE `vimeo_video`
  ADD PRIMARY KEY (`v_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accessor`
--
ALTER TABLE `accessor`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `ac_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `admin_details`
--
ALTER TABLE `admin_details`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `AO`
--
ALTER TABLE `AO`
  MODIFY `ao_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `assessment_attempt`
--
ALTER TABLE `assessment_attempt`
  MODIFY `a_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `assessment_expandable`
--
ALTER TABLE `assessment_expandable`
  MODIFY `ae_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `assessment_expandable_answers`
--
ALTER TABLE `assessment_expandable_answers`
  MODIFY `aexans_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `assessment_expandable_list`
--
ALTER TABLE `assessment_expandable_list`
  MODIFY `ael_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=248;

--
-- AUTO_INCREMENT for table `assessment_multiple_choice`
--
ALTER TABLE `assessment_multiple_choice`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;

--
-- AUTO_INCREMENT for table `assessment_multiple_choice_options`
--
ALTER TABLE `assessment_multiple_choice_options`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=600;

--
-- AUTO_INCREMENT for table `assessment_pool_questions`
--
ALTER TABLE `assessment_pool_questions`
  MODIFY `pq_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;

--
-- AUTO_INCREMENT for table `assessment_question_result`
--
ALTER TABLE `assessment_question_result`
  MODIFY `ar_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `assessment_result_by_assessor`
--
ALTER TABLE `assessment_result_by_assessor`
  MODIFY `ass_res_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `assessor_expandable_ans`
--
ALTER TABLE `assessor_expandable_ans`
  MODIFY `ass_ex_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `assessor_expandable_option_ans`
--
ALTER TABLE `assessor_expandable_option_ans`
  MODIFY `ass_exOpt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `assign_ao_regdate`
--
ALTER TABLE `assign_ao_regdate`
  MODIFY `aoreg_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `ass_expandable_option_answers`
--
ALTER TABLE `ass_expandable_option_answers`
  MODIFY `aexopt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `ass_pool_question_options`
--
ALTER TABLE `ass_pool_question_options`
  MODIFY `opt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=639;

--
-- AUTO_INCREMENT for table `ass_submitdate_bylearner`
--
ALTER TABLE `ass_submitdate_bylearner`
  MODIFY `sd_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `circle_the_answer`
--
ALTER TABLE `circle_the_answer`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `circle_the_answer_options`
--
ALTER TABLE `circle_the_answer_options`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=191;

--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `component`
--
ALTER TABLE `component`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `corss_word_details`
--
ALTER TABLE `corss_word_details`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=130;

--
-- AUTO_INCREMENT for table `course_type`
--
ALTER TABLE `course_type`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `cpd`
--
ALTER TABLE `cpd`
  MODIFY `cpd_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `cpd_course`
--
ALTER TABLE `cpd_course`
  MODIFY `q_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `crossword_data`
--
ALTER TABLE `crossword_data`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=345;

--
-- AUTO_INCREMENT for table `cross_word`
--
ALTER TABLE `cross_word`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `cv_exercise`
--
ALTER TABLE `cv_exercise`
  MODIFY `cv_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `drag_the_Match`
--
ALTER TABLE `drag_the_Match`
  MODIFY `dtm_id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `drag_to_match_options`
--
ALTER TABLE `drag_to_match_options`
  MODIFY `dtmopt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT for table `equipment`
--
ALTER TABLE `equipment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `e_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `exercise`
--
ALTER TABLE `exercise`
  MODIFY `rex_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `exercise_type`
--
ALTER TABLE `exercise_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `expandable_details`
--
ALTER TABLE `expandable_details`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=533;

--
-- AUTO_INCREMENT for table `expandable_list`
--
ALTER TABLE `expandable_list`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `f_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT for table `first_last_slide`
--
ALTER TABLE `first_last_slide`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1211;

--
-- AUTO_INCREMENT for table `freetext_feedback_by_assessor`
--
ALTER TABLE `freetext_feedback_by_assessor`
  MODIFY `ftfeed_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `free_text`
--
ALTER TABLE `free_text`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `group_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `group_courses`
--
ALTER TABLE `group_courses`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=244;

--
-- AUTO_INCREMENT for table `group_users`
--
ALTER TABLE `group_users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;

--
-- AUTO_INCREMENT for table `image_coordinates`
--
ALTER TABLE `image_coordinates`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `image_waypoint`
--
ALTER TABLE `image_waypoint`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `insert_video`
--
ALTER TABLE `insert_video`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `iqareport_iqa_list`
--
ALTER TABLE `iqareport_iqa_list`
  MODIFY `list_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `iqa_feedback`
--
ALTER TABLE `iqa_feedback`
  MODIFY `if_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `iqa_submitted_report`
--
ALTER TABLE `iqa_submitted_report`
  MODIFY `sr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `learner_report_by_iqa`
--
ALTER TABLE `learner_report_by_iqa`
  MODIFY `lr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `learner_type_master`
--
ALTER TABLE `learner_type_master`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `lesson`
--
ALTER TABLE `lesson`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=438;

--
-- AUTO_INCREMENT for table `lesson_attempt`
--
ALTER TABLE `lesson_attempt`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `lesson_question_relation`
--
ALTER TABLE `lesson_question_relation`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1840;

--
-- AUTO_INCREMENT for table `list_slide`
--
ALTER TABLE `list_slide`
  MODIFY `l_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `list_slide_options`
--
ALTER TABLE `list_slide_options`
  MODIFY `lopt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- AUTO_INCREMENT for table `login_statistic`
--
ALTER TABLE `login_statistic`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=147;

--
-- AUTO_INCREMENT for table `memory`
--
ALTER TABLE `memory`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `memory_details`
--
ALTER TABLE `memory_details`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=478;

--
-- AUTO_INCREMENT for table `multiple_choice`
--
ALTER TABLE `multiple_choice`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;

--
-- AUTO_INCREMENT for table `multiple_choice_image`
--
ALTER TABLE `multiple_choice_image`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `multiple_choice_image_options`
--
ALTER TABLE `multiple_choice_image_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=146;

--
-- AUTO_INCREMENT for table `multiple_choice_options`
--
ALTER TABLE `multiple_choice_options`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=659;

--
-- AUTO_INCREMENT for table `payment_plan_master`
--
ALTER TABLE `payment_plan_master`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `peer_authoring`
--
ALTER TABLE `peer_authoring`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `peer_authoring_answers`
--
ALTER TABLE `peer_authoring_answers`
  MODIFY `pans_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `peer_authoring_ans_videos`
--
ALTER TABLE `peer_authoring_ans_videos`
  MODIFY `paa_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `plans_master`
--
ALTER TABLE `plans_master`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `programme_card`
--
ALTER TABLE `programme_card`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `question_pool`
--
ALTER TABLE `question_pool`
  MODIFY `pool_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `question_result`
--
ALTER TABLE `question_result`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `regions_master`
--
ALTER TABLE `regions_master`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `registration_user`
--
ALTER TABLE `registration_user`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `reordering`
--
ALTER TABLE `reordering`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `reordering_options`
--
ALTER TABLE `reordering_options`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=387;

--
-- AUTO_INCREMENT for table `resistance_exercise`
--
ALTER TABLE `resistance_exercise`
  MODIFY `re_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `scrollable`
--
ALTER TABLE `scrollable`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;

--
-- AUTO_INCREMENT for table `section`
--
ALTER TABLE `section`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=209;

--
-- AUTO_INCREMENT for table `selecting_stretch`
--
ALTER TABLE `selecting_stretch`
  MODIFY `s_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `true_false_statement`
--
ALTER TABLE `true_false_statement`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=177;

--
-- AUTO_INCREMENT for table `true_or_false`
--
ALTER TABLE `true_or_false`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `unit`
--
ALTER TABLE `unit`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=549;

--
-- AUTO_INCREMENT for table `user_chat`
--
ALTER TABLE `user_chat`
  MODIFY `chat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=973;

--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `user_details_new`
--
ALTER TABLE `user_details_new`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;

--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;

--
-- AUTO_INCREMENT for table `video_slide`
--
ALTER TABLE `video_slide`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT for table `vimeo_video`
--
ALTER TABLE `vimeo_video`
  MODIFY `v_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
